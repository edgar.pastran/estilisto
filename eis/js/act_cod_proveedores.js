var urlServerPage = ruta.concat("/servidor/php/act_cod_proveedores.php");

$(function() {
    $.fn.editable.defaults.mode = 'inline';

    usuarioSucursal(inicializarDatosSucursal);

    // Configurar modal de proveedores
    $('#bootgrid-proveedores-cambio-codigo').bootgrid({
        css: {
            icon: 'icon',
            iconColumns: 'ion-ios-list-outline',
            iconDown: 'ion-chevron-down',
            iconRefresh: 'ion-refresh',
            iconSearch: 'ion-search',
            iconUp: 'ion-chevron-up',
            dropDownMenuItems: 'dropdown-menu dropdown-menu-right'
        },
        rowCount: [/*-1, */5, 10, 25, 50, 100],
        selection: true,
        multiSelect: false,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    }).
    on("selected.rs.jquery.bootgrid", function(e, rows) {
        var tipo_codigo = $('#tipo_codigo').val();
        limpiarDatosCabecera();
        // Cargar los datos de la cabecera
        var proveedor = JSON.parse(rows[0].pro_data);
        $('#proveedor_codigo'+tipo_codigo).val(proveedor.codpro);
        $('#proveedor_razon'+tipo_codigo).val(proveedor.codpro+" "+proveedor.razon);

        // Cerrar el modal
        $("#modal_proveedores .close").click();

    }).
    on("deselected.rs.jquery.bootgrid", function(e, rows) {
    });

});

$(document).on('change', '#select2-1', function(e) {
    e.preventDefault();
    inicializarDatosSucursal();
});

function inicializarDatosSucursal() {
    resetearVista();
}

$(document).on('click', '#btnBuscarProveedorAnterior', function(e) {
    e.preventDefault();
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        $('#tipo_codigo').val('_anterior');
        cargarModalProveedores();
    }
});

$(document).on('click', '#btnBuscarProveedorActual', function(e) {
    e.preventDefault();
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        $('#tipo_codigo').val('_actual');
        cargarModalProveedores();
    }
});

function cargarModalProveedores() {
    var codsuc = $('#select2-1').val();
    var params = {"sucursal": codsuc, "operacion": "proveedores"};
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(data) {
            if (data.exito == true) {
                // console.log(JSON.stringify(data));
                if (data.exito == true) {
                    $("#bootgrid-proveedores-cambio-codigo").bootgrid("clear");
                    for (var i = 0; i < data.proveedores.length; i++) {
                        var proveedor = data.proveedores[i];
                        $("#bootgrid-proveedores-cambio-codigo").bootgrid().bootgrid("append", [{
                            "pro_codpro": proveedor['codpro'],
                            "pro_razon": proveedor['razon'],
                            "pro_obsoleto": proveedor['obsoleto_format'],
                            "pro_data": JSON.stringify(proveedor)
                        }]);
                    };
                    //Show Modal
                    $('#modal_proveedores').modal('show');
                };
            }
            else {
                console.log(data);
            }
        },
        error: function(data) {
            console.log(JSON.stringify(data));
        }
    });
}

function limpiarDatosCabecera() {
    var tipo_codigo = $("#tipo_codigo").val();
    if (tipo_codigo != "") {
        $("#proveedor_codigo"+tipo_codigo).val("");
        $("#proveedor_razon"+tipo_codigo).val("");
    }
    else {
        $("#proveedor_codigo_anterior").val("");
        $("#proveedor_razon_anterior").val("");
        $("#proveedor_codigo_actual").val("");
        $("#proveedor_razon_actual").val("");
    }
}

function resetearVista(){
    $('#tipo_codigo').val("");
    limpiarDatosCabecera();
    $('#eliminar_codigo_anterior').prop('checked', false);
}

$(document).on('click', '#btnCancelar', function(e) {
    e.preventDefault();
    swal({
            title: 'Estas seguro?',
            text: 'Si cancelas esta operacion no se guardaran los datos que has ingresado!',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'No, me arrepenti',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Si, cancelarlo',
            closeOnConfirm: false
        },
        function () {
            resetearVista();
            swal('Cancelado!', 'Tus datos has sido cancelados.', 'success');
        });
});

$(document).on('click', '#btnGuardar', function(e) {
    e.preventDefault();
    var codsuc = $('#select2-1').val();
    var codproant = $('#proveedor_codigo_anterior').val();
    var codproact = $('#proveedor_codigo_actual').val();
    if (codsuc != "" && codproant != "" && codproact != "") {
        swal({
            title: 'Estas seguro?',
            text: 'Si guardas el cambio de codigo no podras reversar la operacion!',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'No, me arrepenti',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Si, guardarlo',
            closeOnConfirm: false
        },
        function () {
            procesarCambioCodigo();
        });
    }
});

function procesarCambioCodigo() {
    var codsuc = $('#select2-1').val();
    var datos_cambio_codigo = {};
    datos_cambio_codigo.codigo_anterior = $('#proveedor_codigo_anterior').val();
    datos_cambio_codigo.codigo_actual = $('#proveedor_codigo_actual').val();
    datos_cambio_codigo.eliminar_anterior = $('#eliminar_codigo_anterior').is(':checked');
    var params = {"sucursal": codsuc, "operacion": "procesar_cambio_codigo", "datos_cambio_codigo": JSON.stringify(datos_cambio_codigo)};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(data) {
            // console.log(JSON.stringify(data));
            if (data.exito == true) {
                resetearVista();
                swal('Codigos Actualizados!', '', 'success');
            }
            else {
                console.log(JSON.stringify(data));
            }
        },
        error: function(data) {
            console.log(JSON.stringify(data));
        }
    });
}

$('#select2-1').select2();