var sucursales = new Array();

$(function() {
    inicializar();
});

function inicializar() {
    serverPage = ruta.concat("/servidor/php/consul_suc_usuario.php");
    $.post(serverPage, {"session": session}, function (data) {

        if (data.exito == true) {

            $('#select2-2').text(" ");

            for (var i = 0; i < data.nume_regis; i++) {

                if (sucursal.indexOf(data.sucursales_usuario[i]['codsuc']) >= 0) {

                    $('#select2-2').append('<option value="' + data.sucursales_usuario[i]['codsuc'] + '"selected>' + data.sucursales_usuario[i]['codsuc'] + ' ' + data.sucursales_usuario[i]['razemp'] + '</option>');

                } else {

                    $('#select2-2').append('<option value="' + data.sucursales_usuario[i]['codsuc'] + '">' + data.sucursales_usuario[i]['codsuc'] + ' ' + data.sucursales_usuario[i]['razemp'] + '</option>');

                }
                ;


                sucursales[i] = new Array();

                sucursales[i]['codsuc'] = data.sucursales_usuario[i]['codsuc'];

                sucursales[i]['razemp'] = data.sucursales_usuario[i]['razemp'];

                sucursales[i]['moneda'] = data.sucursales_usuario[i]['moneda'];

            }
            ;

        } else {

            //alert('error');

        }
        ;


    }); //Fin .post

    var ano = fecha.getFullYear();
    var ano_a = 0;
    for (var i = ano - 0; i >= ano - 4; i--) {
        ano_a = i-1;
        $('#eje_anterior').append('<option value="' + ano_a + '">' + ano_a + '</option>');
        $('#eje_actual').append('<option value="' + i + '">' + i + '</option>');
    }
    ;

    $('#reporte').hide('fast');
    $('#btnNuevoReporte').hide('fast');
}

function imprimir_fila(i,monto,icono) {
    var fila_imprimir = "";
    var icono = "<em class='ion-arrow-up-c text-success mr-1 icon-lg text-success'></em>";

    if (-1 < monto && monto < 1) {
        fila_imprimir = "<td></td>";
    }else if (i == 2 || i == 4) {
        if (monto < 0) { icono = "<em class='ion-arrow-down-c text-danger mr-1 icon-lg text-success'></em>"}
        fila_imprimir = "<td class='text-bold bg-grey-50' align='center'>"+icono+monto+"</td>";
    }else{
        fila_imprimir = "<td align='right'>"+monto+"</td>";
    }
    return formatNumber.new(fila_imprimir);
}

$("#btnReporte").click(function (event){

    event.preventDefault();

    getSucursal();

    $datos = $("#formulario-reporte").serialize();



    serverPage = ruta.concat("/servidor/php/resumen_mensual.php");

    $.post(serverPage, $("#formulario-reporte").serialize(), function (data) {

        if (data.exito == true ){

            $('#formulario').hide('fast');
            $('#reporte').show('fast');
            $('#btnReporte').hide('fast');
            $('#btnNuevoReporte').show('fast');

            $('#TituloPagina').hide('fast');

            var nSuc = 0;

            var iSuc = 0;

            for (var i = 0; i < sucursales.length; i++) {

                if (document.getElementById("sucursal").value.indexOf(sucursales[i]['codsuc']) >= 0) { iSuc = i; nSuc=nSuc+1;};

            }
            guardarSucursal(document.getElementById("sucursal").value);

            var desSuc = sucursales[iSuc]['razemp'];

            if (nSuc > 1) { desSuc = ""};

            $('#TituloTable1').text(document.getElementById("sucursal").value +" "+desSuc+ " -- Comparativo : " + $('#eje_anterior').val() + " -- " + $('#eje_actual').val());
            $('#TituloTable2').text(document.getElementById("sucursal").value +" "+desSuc+ " -- Comparativo : " + $('#eje_anterior').val() + " -- " + $('#eje_actual').val());
            $('#TituloTable3').text(document.getElementById("sucursal").value +" "+desSuc+ " -- Comparativo : " + $('#eje_anterior').val() + " -- " + $('#eje_actual').val());
            $('#TituloTable4').text(document.getElementById("sucursal").value +" "+desSuc+ " -- Comparativo : " + $('#eje_anterior').val() + " -- " + $('#eje_actual').val());
            var mes ="";
            var acumulado=0;
            var total_acumulado=0;
            var beneficio="";
            var ventas=0;
            var compras=0;

            $("#tbody1").text("");
            $("#tbody2").text("");
            $("#tbody3").text("");
            $("#tbody4").text("");
            // beneficio = formatNumber.new(data.totalben_ant.toFixed(2));
            var filas = [$('#eje_anterior').val(), $('#eje_actual').val(), "Var %", "AvrTrim", "AvrT%"];
            for (var i = 0; i < filas.length; i++) {

                
                var fila ="<tr>"+
                "<td class='text-bold'>"+filas[i]+"</td>"+
                imprimir_fila(i,data.tot_tik_mes[i][0].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[i][1].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[i][2].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[i][3].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[i][4].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[i][5].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[i][6].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[i][7].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[i][8].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[i][9].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[i][10].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[i][11].toFixed(0),"X")+
                "</tr>";
                $("#tbody1").append(fila);

                
                fila ="<tr>"+
                "<td class='text-bold'>"+filas[i]+"</td>"+
                imprimir_fila(i,data.tot_ven_mes[i][0].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[i][1].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[i][2].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[i][3].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[i][4].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[i][5].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[i][6].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[i][7].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[i][8].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[i][9].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[i][10].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[i][11].toFixed(0),"X")+
                "</tr>";
                $("#tbody3").append(fila);

                if( i == 2 || i == 4 ) {
                    fila ="<tr>"+ // $("#bootgrid-ticket_dia")
                    "<td class='text-bold'>"+filas[i]+"</td>"+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][0].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][1].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][2].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][3].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][4].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][5].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][6].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][7].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][8].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][9].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][10].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][11].toFixed(0),"X")+
                    "</tr>";
                    $("#tbody2").append(fila);

                    fila ="<tr>"+ // $("#bootgrid-venta_ticket")
                    "<td class='text-bold'>"+filas[i]+"</td>"+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][0].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][1].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][2].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][3].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][4].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][5].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][6].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][7].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][8].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][9].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][10].toFixed(0),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][11].toFixed(0),"X")+
                    "</tr>";
                    $("#tbody4").append(fila);


                }else{ 
                    fila ="<tr>"+ // $("#bootgrid-ticket_dia")
                    "<td class='text-bold'>"+filas[i]+"</td>"+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][0].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][1].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][2].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][3].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][4].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][5].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][6].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][7].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][8].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][9].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][10].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_tik_dia_avg[i][11].toFixed(2),"X")+
                    "</tr>";
                    $("#tbody2").append(fila);

                    fila ="<tr>"+ // $("#bootgrid-venta_ticket")
                    "<td class='text-bold'>"+filas[i]+"</td>"+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][0].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][1].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][2].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][3].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][4].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][5].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][6].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][7].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][8].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][9].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][10].toFixed(2),"X")+
                    imprimir_fila(i,data.tot_ven_tik_avg[i][11].toFixed(2),"X")+
                    "</tr>";
                    $("#tbody4").append(fila);
                }
                
            }

            guardarSucursal(document.getElementById("sucursal").value);

            guardarDatos(JSON.stringify(data), sucursales[iSuc]['moneda']);

            $('#grafico1').text("");
            $('#grafico1').append('<iframe id="external" frameborder="0" style="width:100%;height:305px" src="graficos/ver_resumen_mensual_1.html"></iframe>');
            $('#grafico2').text("");
            $('#grafico2').append('<iframe id="external" frameborder="0" style="width:100%;height:305px" src="graficos/ver_resumen_mensual_2.html"></iframe>');
            $('#grafico3').text("");
            $('#grafico3').append('<iframe id="external" frameborder="0" style="width:100%;height:305px" src="graficos/ver_resumen_mensual_3.html"></iframe>');
            $('#grafico4').text("");
            $('#grafico4').append('<iframe id="external" frameborder="0" style="width:100%;height:305px" src="graficos/ver_resumen_mensual_4.html"></iframe>');

            //borrarDatos();
        }else{//No existen Registros

            toastr.options.positionClass = 'toast-bottom-right';

            toastr['warning']('No existen Datos');

        };

    }); //Fin .post

});

$("#btnNuevoReporte").click(function (event){

    $('#formulario').show('fast');

    $('#btnReporte').show('fast');

    $('#btnNuevoReporte').hide('fast');

    //$('#tabla1').text(" ");

    $('#reporte').hide('fast');
    
    $('#TituloPagina').show('fast');
});


// // Configurar combos
$('#select2-2').select2();
