var urlServerPage = ruta.concat("/servidor/php/actualizar_empresa.php");

$(function() {
    cargarZonasHorarias();
    cargarCatalogoEmpleados();
    cargarCatalogoClientes();
    inicializarEmpresa();
    usuarioSucursalEmpresa();
});

function cargarZonasHorarias() {
    $('#select2-zonhor').text(" ");
    $('#select2-zonhor').append('<option value="">Seleccione...</option>');
    var zonas = moment.tz.names();
    $.each(zonas, function(index, element) {
        $('#select2-zonhor').append('<option value="' + element + '">' + element + '</option>');
    });
}

function cargarCatalogoEmpleados() {
    $('#bootgrid-empleado').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        selection: true,
        multiSelect: false,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on("selected.rs.jquery.bootgrid", function(e, rows) {
        $("#empleadotpv").val(rows[0].emp_codemp);
        //$("#nomcli").val(rows[0].emp_nomemp);
        $("#closeMemp").click();
    })
    .on("deselected.rs.jquery.bootgrid", function(e, rows) {

    });
}

function cargarCatalogoClientes() {
    $('#bootgrid-cliente').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        selection: true,
        multiSelect: false,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on("selected.rs.jquery.bootgrid", function(e, rows) {
        $("#clientetpv").val(rows[0].cli_codcli);
        //$("#nomcli").val(rows[0].cli_nomcli);

        $("#closeMcli").click();

    })
    .on("deselected.rs.jquery.bootgrid", function(e, rows) {

    });
}

function inicializarEmpresa(){
    $('#btn-empresa').hide('fast');

    $('#btn-general').hide('fast');

    $('#btn-ventas').hide('fast');

    $('#btn-compras').hide('fast');

    $("#domemp").val("");
    $("#email").val("");
    $("#fax").val("");
    $("#nifemp").val("");
    $("#pais").val("");
    $("#select2-zonhor").val("");
    $("#pobemp").val("");
    $("#proemp").val("");
    $("#razemp").val("");
    $("#telefono").val("");
    $("#webemp").val("");

    $("#impuesto1").val("");
    $("#re1").val("");
    $("#impuesto2").val("");
    $("#re2").val("");
    $("#moneda").val("");
    $("#actarqueo").val(""); setCheckbox("actarqueo","X");
    $("#actseguridad").val(""); setCheckbox("actseguridad","X");
    $("#actlogininac").val(""); setCheckbox("actlogininac","X");
    $("#segundos").val("");

    $("#clientetpv").val("");
    $("#empleadotpv").val("");
    $("#codfp").val("");
    $("#idconcepto").val("");

    $("#impincventas").val(""); setCheckbox("impincventas","X");
    $("#calcomincluido").val(""); setCheckbox("calcomincluido","X");
    $("#pedcliticket").val(""); setCheckbox("pedcliticket","X");
    $("#codaltcliente").val(""); setCheckbox("codaltcliente","X");
    $("#permodfecha").val(""); setCheckbox("permodfecha","X");
    $("#pedemplinea").val(""); setCheckbox("pedemplinea","X");
    $("#noempcrear").val(""); setCheckbox("noempcrear","X");
    $("#ocutotdiatic").val(""); setCheckbox("ocutotdiatic","X");
    $("#segmostotdiatic").val("");
    //$("#nomodcobrados").val(data.nomodcobrados); setCheckbox("nomodcobrados","X");

    $("#proveedorcompras").val("");
    $("#idconceptocompras").val("");
    $("#impinccompras").val("");
    setCheckbox("impinccompras");
}

function datosSucursal(){
    $("#suc_empresa").val($("#select2-1").val());
    $("#opc_empresa").val("consulta");
    $.post(urlServerPage, $("#formulario-empresa").serialize(), function (data) {
        // console.log(JSON.stringify(data));
        if (data.exito == true){
            $("#domemp").val(data.domemp);
            $("#email").val(data.email);
            $("#fax").val(data.fax);
            if (data.imaemp != "") {
                $("#logo").attr("src", data.imaemp);
            }
            $("#nifemp").val( data.nifemp);
            $("#select2-pais").val(data.pais); $('#select2-pais').trigger('change');
            $("#select2-zonhor").val(data.zonhor); $('#select2-zonhor').trigger('change');
            $("#pobemp").val(data.pobemp);
            $("#proemp").val(data.proemp);
            $("#razemp").val(data.razemp);
            $("#telefono").val(data.telefono);
            $("#webemp").val(data.webemp);

            $("#impuesto1").val(data.impuesto1);
            $("#re1").val(data.re1);
            $("#impuesto2").val(data.impuesto2);
            $("#re2").val(data.re2);
            $("#moneda").val(data.moneda);
            $("#serie").val(data.serie);
            $("#actarqueo").val(data.actarqueo); setCheckbox("actarqueo","X");
            $("#actseguridad").val(data.actseguridad); setCheckbox("actseguridad","X");
            $("#actlogininac").val(data.actlogininac); setCheckbox("actlogininac","X");
            $("#segundos").val(data.segundos);

            $("#clientetpv").val(data.clientetpv);
            $("#empleadotpv").val(data.empleadotpv);
            formapagos(data.codfp); //$("#codfp").val(data.codfp); 
            $("#codfp").val(data.codfp);
            
            conceptos(data.idconcepto, data.idconceptocompras);//$("#idconcepto").val(data.idconcepto);
            cajas(data.codcajven, data.codcajcom);
            grupoEmpresas(data.codemp);//Codigo del grupo de empresa.

            $("#impincventas").val(data.impincventas); setCheckbox("impincventas","X");
            $("#calcomincluido").val(data.calcomincluido); setCheckbox("calcomincluido","X");
            $("#pedcliticket").val(data.pedcliticket); setCheckbox("pedcliticket","X");
            $("#codaltcliente").val(data.codaltcliente); setCheckbox("codaltcliente","X");
            $("#permodfecha").val(data.permodfecha); setCheckbox("permodfecha","X");
            $("#pedemplinea").val(data.pedemplinea); setCheckbox("pedemplinea","X");
            $("#noempcrear").val(data.noempcrear); setCheckbox("noempcrear","X");
            $("#ocutotdiatic").val(data.ocutotdiatic); setCheckbox("ocutotdiatic","X");
            $("#segmostotdiatic").val(data.segmostotdiatic);
            //$("#nomodcobrados").val(data.nomodcobrados); setCheckbox("nomodcobrados");

            proveedores();
            $('#proveedorcompras').val(data.proveedorcompras);
            $("#impinccompras").val(data.impinccompras); setCheckbox("impinccompras","X");

            $('#btn-empresa').show('fast');

            $('#btn-general').show('fast');

            $('#btn-ventas').show('fast');

            $('#btn-compras').show('fast');
        }
    }); //Fin .post

}

$("#btn-empresa").click(function (event){
    event.preventDefault();
    $("#suc_empresa").val($("#select2-1").val());
    $("#opc_empresa").val("empresa");

    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: new FormData($("#formulario-empresa")[0]),
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.exito == true) {
                swal('Empresa Actualizada!', '', 'success');
            }
            else {//alert('error');
                console.log("SIN EXITO: " + JSON.stringify(data));
            }
        },
        error: function (respuesta) {
            console.log("ERROR: " + JSON.stringify(respuesta));
        }
    });
}); //Fin de btn

$(".remove-image").click(function (event) {
    event.preventDefault();
    $("#suc_empresa").val($("#select2-1").val());
    $("#opc_empresa").val("remover_imagen");

    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: $("#formulario-empresa").serialize(),
        cache: false,
        success: function (data) {
            if (data.exito == true) {
                $("#logo").attr("src", ruta.concat("/servidor/images/nothing.png"));
                swal('Logo Eliminado!', '', 'success');
            }
            else {//alert('error');
                console.log("SIN EXITO: " + JSON.stringify(data));
            }
        },
        error: function (respuesta) {
            console.log("ERROR: " + JSON.stringify(respuesta));
        }
    });
});

$("#btn-general").click(function (event){
    event.preventDefault();
    setCheckBoxes('formulario-general', 'X');
    $("#suc_general").val($("#select2-1").val());
    $("#opc_general").val("general");
    var params = $("#formulario-general").serialize();
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(data) {
            if (data.exito == true) {
                swal('Conf. General Actualizada!', '', 'success');
            }
            else {
                console.log(JSON.stringify(data));
            }
        },
        error: function(data) {
            console.log(JSON.stringify(data));
        }
    });
}); //Fin de btn

$("#btn-ventas").click(function (event){
    event.preventDefault();
    setCheckBoxes('formulario-ventas', 'X');
    $("#suc_ventas").val($("#select2-1").val());
    $("#opc_ventas").val("ventas");
    var params = $("#formulario-ventas").serialize();
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(data) {
            if (data.exito == true) {
                swal('Conf. Ventas Actualizadas!', '', 'success');
            }
            else {
                console.log(JSON.stringify(data));
            }
        },
        error: function(data) {
            console.log(JSON.stringify(data));
        }
    });
}) //Fin de btn

$("#btn-compras").click(function (event){
    event.preventDefault();
    setCheckBoxes('formulario-compras', 'X');
    $("#suc_compras").val($("#select2-1").val());
    $("#opc_compras").val("compras");

    $.post(urlServerPage, $("#formulario-compras").serialize(), function (data) {
        if (data.exito == true){
            swal('Conf. Compras Actualizadas!', '', 'success');
        }
        else{//alert('error');

        }
    }); //Fin .post

}) //Fin de btn

function usuarioSucursalEmpresa(){
    var urlserverPageAux = ruta.concat("/servidor/php/consul_suc_usuario.php");
    idSuc = "";
    $.post(urlserverPageAux, {"session": session}, function (data) {
        if (data.exito == true) {
            for (var i = 0; i < data.nume_regis; i++) {
                $('#select2-1').append('<option value="' + data.sucursales_usuario[i]['codsuc'] + '">' + data.sucursales_usuario[i]['codsuc'] + ' ' + data.sucursales_usuario[i]['razemp'] + '</option>');
                if (i == 0) {
                    $("#select2-1").val(data.sucursales_usuario[i]['codsuc']);
                }
                if (sucursal.indexOf(data.sucursales_usuario[i]['codsuc']) >= 0) {
                    $("#select2-1").val(data.sucursales_usuario[i]['codsuc']);
                }

                sucursales[i] = new Array();
                sucursales[i]['codsuc'] = data.sucursales_usuario[i]['codsuc'];
                sucursales[i]['razemp'] = data.sucursales_usuario[i]['razemp'];
                sucursales[i]['moneda'] = data.sucursales_usuario[i]['moneda'];
            }
            datosSucursal();
        }
        else {
            //alert('error');
        }
    }); //Fin .post
}

function formapagos(valor) {
    $("#suc_ventas").val($("#select2-1").val());
    $("#opc_ventas").val("consulta");
    var urlserverPageAux = ruta.concat("/servidor/php/formapago.php");

    $.post(urlserverPageAux, $("#formulario-ventas").serialize(), function (data) {
        if (data.exito == true && data.nume_regis >= 1) {
            $('#codfp').text(" ");
            for (var i = 0; i < data.nume_regis; i++) {
                if (valor === data.formapagos[i]['codfp']) {
                    $('#codfp').append('<option value="' + data.formapagos[i]['codfp'] + '"selected>' + data.formapagos[i]['des'] + '</option>');
                }
                else {
                    $('#codfp').append('<option value="' + data.formapagos[i]['codfp'] + '">' + data.formapagos[i]['des'] + '</option>');
                }
            }
        }
    }); //Fin .post
}

function grupoEmpresas(valor) {
    //$("#codemp").val(valor);
    $("#opc_general").val("consulta");
    var urlserverPageAux = ruta.concat("/servidor/php/grupo_empresa.php");

    $.post(urlserverPageAux, $("#formulario-general").serialize(), function (data) {
        if (data.exito == true && data.nume_regis >= 1) {
            $('#select2-codemp').text(" ");
            $('#select2-codemp').append('<option value=""selected>Independiente</option>');
            for (var i = 0; i < data.nume_regis; i++) {
                if (valor === data.empresas[i]['codemp']) {
                    $('#select2-codemp').append('<option value="' + data.empresas[i]['codemp'] + '"selected>' + data.empresas[i]['nomgrupo'] + '</option>');
                }
                else {
                    $('#select2-codemp').append('<option value="' + data.empresas[i]['codemp'] + '">' + data.empresas[i]['nomgrupo'] + '</option>');
                }
            }
        }
    }); //Fin .post
}

function conceptos(valorVentas, valorCompras) {
    $("#suc_ventas").val($("#select2-1").val());
    $("#opc_ventas").val("consulta");
    var urlserverPageAux = ruta.concat("/servidor/php/conceptos.php");

    $.post(urlserverPageAux, $("#formulario-ventas").serialize(), function (data) {
        if (data.exito == true && data.nume_regis >= 1) {
            $('#idconcepto').text(" ");
            $('#idconceptocompras').text(" ");
            for (var i = 0; i < data.nume_regis; i++) {
                if (valorVentas === data.conceptos[i]['idconcepto']) {
                    $('#idconcepto').append('<option value="' + data.conceptos[i]['idconcepto'] + '" selected>' + data.conceptos[i]['desconcep'] + '</option>');
                }
                else {
                    $('#idconcepto').append('<option value="' + data.conceptos[i]['idconcepto'] + '">' + data.conceptos[i]['desconcep'] + '</option>');
                }
                if (valorCompras === data.conceptos[i]['idconcepto']) {
                    $('#idconceptocompras').append('<option value="' + data.conceptos[i]['idconcepto'] + '" selected>' + data.conceptos[i]['desconcep'] + '</option>');
                }
                else {
                    $('#idconceptocompras').append('<option value="' + data.conceptos[i]['idconcepto'] + '">' + data.conceptos[i]['desconcep'] + '</option>');
                }
            }
        }
    }); //Fin .post
};

function cajas(cajaVentas, cajaCompras) {
    $("#suc_ventas").val($("#select2-1").val());
    $("#opc_ventas").val("consulta");
    var urlserverPageAux = ruta.concat("/servidor/php/cajas.php");

    $.post(urlserverPageAux, $("#formulario-ventas").serialize(), function (data) {
        if (data.exito == true && data.nume_regis >= 1) {
            $('#codcajven').text(" ");
            $('#codcajven').append('<option value="">Seleccione...</option>');
            $('#codcajcom').text(" ");
            $('#codcajcom').append('<option value="">Seleccione...</option>');
            for (var i = 0; i < data.nume_regis; i++) {
                if (data.cajas[i]['tipo'] == 'C') {
                    if (cajaVentas === data.cajas[i]['codcaj']) {
                        $('#codcajven').append('<option value="' + data.cajas[i]['codcaj'] + '" selected>' + data.cajas[i]['nomcaj'] + '</option>');
                    }
                    else {
                        $('#codcajven').append('<option value="' + data.cajas[i]['codcaj'] + '">' + data.cajas[i]['nomcaj'] + '</option>');
                    }
                }
                if (cajaCompras === data.cajas[i]['codcaj']) {
                    $('#codcajcom').append('<option value="' + data.cajas[i]['codcaj'] + '" selected>' + data.cajas[i]['nomcaj'] + '</option>');
                }
                else {
                    $('#codcajcom').append('<option value="' + data.cajas[i]['codcaj'] + '">' + data.cajas[i]['nomcaj'] + '</option>');
                }
            }
        }
    }); //Fin .post
};

function proveedores(valor){
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        var urlserverPageAux = ruta.concat("/servidor/php/compras.php");
        var params = {"sucursal": codsuc, "operacion": "proveedores"};
        $.ajax({
            type: "POST",
            url: urlserverPageAux,
            data: params,
            success: function(data) {
                if (data.exito == true) {
                    //console.log(JSON.stringify(data));
                    $('#proveedorcompras').text(" ");
                    //$('#proveedorcompras').append('<option value="">Seleccione...</option>');
                    for (i=0; i<data.proveedores.length; i++) {
                        var proveedor = {
                            value: data.proveedores[i].codpro,
                            text: data.proveedores[i].razon
                        }
                        $('#proveedorcompras').append('<option value="' + proveedor.value + '">' + proveedor.text + '</option>');
                    }
                }
                else {
                    console.log(data);
                }
            },
            error: function(data) {
                console.log(JSON.stringify(data));
            }
        });
    }
};

$("#btn-clientes").click(function (event){
    $("#suc_ventas").val($("#select2-1").val());
    $("#opc_ventas").val("consulta");
    var urlserverPageAux = ruta.concat("/servidor/php/clientes.php");

    $.post(urlserverPageAux, $("#formulario-ventas").serialize(), function (data){
        if ( data.exito == true && data.nume_regis >= 1 ){
            if (data.nume_regis >= 1) {
                $("#bootgrid-cliente").bootgrid("clear");
                for (var i = 0; i < data.nume_regis; i++) {
                    $("#bootgrid-cliente").bootgrid().bootgrid("append", [{
                        "cli_codcli": ""+data.clientes[i]['codcli']+"",
                        "cli_nomcli": ""+data.clientes[i]['nomcli']+"",
                        "cli_ape1cli": ""+data.clientes[i]['ape1cli']+"",
                        "cli_fecnac": ""+data.clientes[i]['fecnac']+"",
                        "cli_codcli2": ""+data.clientes[i]['codcli2']+"",
                        "cli_tel1cli": ""+data.clientes[i]['tel1cli']+"",
                        "cli_email": ""+data.clientes[i]['email']+"",
                        "cli_dnicli": ""+data.clientes[i]['dnicli']+"",
                        "cli_sexo": ""+data.clientes[i]['sexo']+"",
                        "cli_foto": ""+data.clientes[i]['foto']+""
                    }]);
                }
                $('#modal_clientes').modal('show');
            }//No hay Datos
        }
    }); //Fin .post
}); //Fin .btn

$("#btn-empleados").click(function (event){
    $("#suc_ventas").val($("#select2-1").val());
    $("#opc_ventas").val("consulta");
    var urlserverPageAux = ruta.concat("/servidor/php/empleados.php");

    $.post(urlserverPageAux, $("#formulario-ventas").serialize(), function (data){
        if ( data.exito == true && data.nume_regis >= 1 ){
            if (data.nume_regis >= 1) {
                $("#bootgrid-empleado").bootgrid("clear");
                for (var i = 0; i < data.nume_regis; i++) {
                    $("#bootgrid-empleado").bootgrid().bootgrid("append", [{
                        "emp_codemp": ""+data.empleados[i]['codemp']+"",
                        "emp_nomemp": ""+data.empleados[i]['nomemp']+"",
                        "emp_ape1emp": ""+data.empleados[i]['ape1emp']+"",
                        "emp_fecnac": ""+data.empleados[i]['fecnac']+"",
                        "emp_codemp2": ""+data.empleados[i]['codemp2']+"",
                        "emp_tel1emp": ""+data.empleados[i]['tel1emp']+"",
                        "emp_email": ""+data.empleados[i]['email']+"",
                        "emp_dniemp": ""+data.empleados[i]['dniemp']+"",
                        "emp_sexo": ""+data.empleados[i]['sexo']+"",
                        "emp_foto": ""+data.empleados[i]['foto']+""
                    }]);
                }
                $('#modal_empleados').modal('show');
            }//No hay Datos
        }
    }); //Fin .post
}); //Fin .btn

$('#select2-1').select2();

