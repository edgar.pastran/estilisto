var formatNumber = {
 separador: ",", // separador para los miles
 sepDecimal: '.', // separador para los decimales
 formatear:function (num){
   num +='';
   var splitStr = num.split('.');
   var splitLeft = splitStr[0];
   var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
   var regx = /(\d+)(\d{3})/;
   while (regx.test(splitLeft)) {
     splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
   }
   return this.simbol + splitLeft +splitRight;
 },
 new:function(num, simbol){
   this.simbol = simbol ||'';
   return this.formatear(num);
 }
}

var ioniconCss = {
    icon: 'icon',
    iconColumns: 'ion-ios-list-outline',
    iconDown: 'ion-chevron-down',
    iconRefresh: 'ion-refresh',
    iconSearch: 'ion-search',
    iconUp: 'ion-chevron-up',
    dropDownMenuItems: 'dropdown-menu dropdown-menu-right'
};

if (!window.location.protocol.startsWith("file")) {
    var ruta = window.location.protocol + "//" + window.location.host;
    var is_local = (window.location.host == "localhost") || (window.location.host.startsWith("10.")) || (window.location.host.startsWith("192."));
    ruta += (is_local) ? "/" + window.location.pathname.split('/')[1] : "";
}
else {
    var ruta = "http://estilisto.com";
}


function getSucursal()
{
  var suc= "";
  var x=document.getElementById("select2-2");
  for (var i = 0; i < x.options.length; i++) {
   if(x.options[i].selected ==true){
    if (suc == "") {suc = suc.concat("'",x.options[i].value,"'");}
    else{suc = suc.concat(",'",x.options[i].value,"'");}
  }
}
document.getElementById("sucursal").value = suc;
}

var sucursales = new Array();

function usuarioSucursales(){
  serverPage = ruta.concat("/servidor/php/consul_suc_usuario.php");
    $.post(serverPage, {"session":session}, function (data) {
      if (data.exito == true){
        $('#select2-2').text(" ");
        for (var i = 0; i < data.nume_regis; i++) {
          if (sucursal.indexOf(data.sucursales_usuario[i]['codsuc']) >= 0) {
            $('#select2-2').append('<option value="'+data.sucursales_usuario[i]['codsuc']+'"selected>'+data.sucursales_usuario[i]['codsuc']+' '+data.sucursales_usuario[i]['razemp']+'</option>');
          }else{
            $('#select2-2').append('<option value="'+data.sucursales_usuario[i]['codsuc']+'">'+data.sucursales_usuario[i]['codsuc']+' '+data.sucursales_usuario[i]['razemp']+'</option>');
          };
          sucursales[i] = new Array();
          sucursales[i]['codsuc'] = data.sucursales_usuario[i]['codsuc'];
          sucursales[i]['razemp'] = data.sucursales_usuario[i]['razemp'];
          sucursales[i]['moneda'] = data.sucursales_usuario[i]['moneda'];
        };
      }else{
        //alert('error');
      };
    }); //Fin .post
}

function usuarioSucursal(callbackFunction) {
    serverPage = ruta.concat("/servidor/php/consul_suc_usuario.php");
    idSuc = "";
    $.post(serverPage, {"session": session}, function (data) {
        if (data.exito == true) {
            $('#select2-1').text(" ");
            $('#select2-1').append('<option value=""selected></option>');
            for (var i = 0; i < data.nume_regis; i++) {
                $('#select2-1').append('<option value="' + data.sucursales_usuario[i]['codsuc'] + '">' + data.sucursales_usuario[i]['codsuc'] + ' ' + data.sucursales_usuario[i]['razemp'] + '</option>');
                if (i == 0) {
                    document.getElementById("select2-1").value = data.sucursales_usuario[i]['codsuc'];
                }
                if (sucursal.indexOf(data.sucursales_usuario[i]['codsuc']) >= 0) {
                    document.getElementById("select2-1").value = data.sucursales_usuario[i]['codsuc'];
                }

                sucursales[i] = new Array();
                sucursales[i]['codsuc'] = data.sucursales_usuario[i]['codsuc'];
                sucursales[i]['razemp'] = data.sucursales_usuario[i]['razemp'];
                sucursales[i]['moneda'] = data.sucursales_usuario[i]['moneda'];
            }
            if (callbackFunction !== undefined) {
                callbackFunction();
            }
        }
        else {
            console.log('error');
        }
        ;
    }); //Fin .post
}

function formapagos(valor) {
    if (valor == undefined) {
        valor = 'EFECTIVO'
    };
    var suc = document.getElementById("select2-1").value;

    serverPage = ruta.concat("/servidor/php/formapago.php");

    $.post(serverPage, {"sucursal":suc,"opcion":'consulta'}, function (data){
        if ( data.exito == true && data.nume_regis >= 1 ){
            $('#codfp').text(" ");
          for (var i = 0; i < data.nume_regis; i++) {
              if (valor === data.formapagos[i]['codfp']) {
                $('#codfp').append('<option value="'+data.formapagos[i]['codfp']+'"selected>'+data.formapagos[i]['des']+'</option>');
              }else{
                $('#codfp').append('<option value="'+data.formapagos[i]['codfp']+'">'+data.formapagos[i]['des']+'</option>');
              };
          };

        };
    }); //Fin .post
};

function cargarFamilas(){
    var suc = document.getElementById("select2-1").value;
    serverPage = ruta.concat("/servidor/php/familias.php");
    $.post(serverPage, {"sucursal":suc,"opcion":'consulta'}, function (data){
        if ( data.exito == true && data.nume_regis >= 1 ){
            $('#familia1').text(" ");
          for (var i = 0; i < data.nume_regis; i++) {
            $('#familia1').append('<option value="'+data.familias[i]['codfam1']+'">'+data.familias[i]['codfam1']+' '+data.familias[i]['desfam1']+'</option>');
          };
        };
        $('#familia1').val("");
    }); //Fin .post
};

function cargarImpuestos(){
    var suc = document.getElementById("select2-1").value;
    serverPage = ruta.concat("/servidor/php/articulos.php");
    $.post(serverPage, {"sucursal":suc,"opcion":'impuestos'}, function (data){
        if ( data.exito == true && data.nume_regis >= 1 ){
            $('#ivaart').text(" ");
            $('#ivaart').append('<option value="1">'+data.impuesto1+'</option>');
            $('#ivaart').append('<option value="2">'+data.impuesto2+'</option>');
        };
        $('#ivaart').val("");
    }); //Fin .post
};

function usuarioGrupos(){
  serverPage = ruta.concat("/servidor/php/grupo_usuario.php");
    $.post(serverPage, {"opcion":'consulta'}, function (data) {
      if (data.exito == true){
        $('#select2-codgru').text(" ");
        $('#select2-codgru').append('<option value=""> </option>');
        for (var i = 0; i < data.nume_regis; i++) {
            $('#select2-codgru').append('<option value="'+data.grupousuario[i]['codgru']+'">'+data.grupousuario[i]['desgru']+'</option>');
        };
      }else{
        //alert('error');
      };
    }); //Fin .post
}

function setCheckBoxes(form_id, valor) {
    $.each($('#'+form_id+' input:checkbox'), function(i, element) {
        myCheckbox($(element).attr('id'), valor);
    });
}

function myCheckbox(myCheck, valor) {
    $("#"+myCheck).val("");
    if ($("#"+myCheck).is(":checked")) {
        $("#"+myCheck).val(valor);
    }
    // var checkBox = document.getElementById(myCheck);
    // if (checkBox.checked == true){
    //     checkBox.value = valor;
    // }
    // else {
    //     checkBox.value = "";
    // }
}

function setCheckbox(myCheck, valor) {
    $('#'+myCheck).prop('checked', $('#'+myCheck).val() == valor);
    // var checkBox = document.getElementById(myCheck);
    // if (checkBox.value == valor){
    //     checkBox.checked = true;
    // }
    // else {
    //     checkBox.checked = false;
    // }
}

// function fix_xeditable_conflict() {
    // $('.datepicker > div:first-of-type').css('display', 'initial');
// }

/*---------- Consultar Pantallas de Acceso ----------*/
function usuarioPantallas(pantallas){
  pantallas = "";
  var pant = new Array();
  serverPage = ruta.concat("/servidor/php/pantallas.php");
    $.post(serverPage, {"session":session, "opcion":"usuario_pantallas"}, function (data) {
      if (data.exito == true){
        for (var i = 0; i < data.nume_regis; i++) {
          pantallas = pantallas.concat(data.pantallas[i]['pantalla']+",");
        };
      }else{//alert('error');
      };
    }); //Fin .post
};

// Funcion usada en la validacion de formularios
// con JQuery Validation
// Necessary to place dyncamic error messages
// without breaking the expected markup for custom input
function errorPlacementInput(error, element) {
    if (element.is(':radio') || element.is(':checkbox')) {
        error.insertAfter(element.parent());
    }
    else if (element.is('.select2-hidden-accessible')) {
        error.insertAfter(element.siblings('span.select2'));
    }
    else {
        error.insertAfter(element);
    }
}