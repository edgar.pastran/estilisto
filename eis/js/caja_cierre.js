var urlServerPage = ruta.concat("/servidor/php/caja_cierre.php");

$(function() {
    $.fn.editable.defaults.mode = 'inline';
    $(formValidation);
    usuarioSucursal(inicializar);
    $('#select2-1').select2();
});

function formValidation() {
    if (!$.fn.validate) return;
    $('#formulario-caja-traspaso').validate({
        errorPlacement: errorPlacementInput,
        errorClass: "my-error-class",
        // Form rules
        rules: {
            codmov: {
                required: true
            },
            codcajdes: {
                required: true
            }
        }
    });
}

var sucursal_data = [];
function inicializar() {
    for (var a=0; a<sucursales.length; a++) {
        sucursal_data[sucursales[a]['codsuc']] = {
            moneda: sucursales[a]['moneda']
        };
    }
    cargarData();
}

var formas_pago = [];
var cajas = [];
function cargarData() {
    var dataToSend = {
        "opcion": "inicializar",
        "sucursal": $("#select2-1").val()
    };
    // console.log("DATA TO SEND: "+JSON.stringify(dataToSend));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: dataToSend,
        success: function (data) {
            if (data.exito == true) {
                // console.log("CON EXITO: " + JSON.stringify(data.cajas));
                cajas = data.cajas;
                formas_pago = data.formas_pago;
                mostrarCajas();
            }
            else {
                console.log("SIN EXITO: " + JSON.stringify(data));
            }
        },
        error: function (respuesta) {
            console.log("ERROR: " + JSON.stringify(respuesta));
        }
    });
}

function mostrarCajas() {
    // Cajas
    $('#pestanas').text(" ");
    $('#pestanas_contenido').text(" ");
    for (var i = 0; i < cajas.length; i++) {
        var caja = cajas[i];
        mostrarCaja(caja, i);
    }
}

function mostrarCaja(caja, index) {
    // Crear la pestana
    var pestana =
        '<li class="nav-item">' +
        '<a class="nav-link '+(index==0?'active':'')+'" id="'+caja['codcaj']+'-tab" data-toggle="tab" data-index="'+index+'" href="#'+caja['codcaj']+'" role="tab" aria-controls="home" aria-selected="true">'+caja['nomcaj']+'</a>' +
        '</li>';
    $('#pestanas').append(pestana);

    // Agregar contenido a la pestana
    var contenido =
        '<div class="tab-pane pt-2 fade '+(index==0?'show active':'')+'" id="'+caja['codcaj']+'" role="tabpanel" aria-labelledby="'+caja['codcaj']+'-tab">' +
            '<div class="row">' +
                '<div class="col-lg-4">' +
                    '<div class="cardbox text-white bg-gradient-success b0">' +
                        '<div class="cardbox-body p-0">' +
                            '<div class="row">' +
                                '<div class="col-1 ml-0 dashbord-icono">' +
                                    '<i class="ion-minus-round"></i>' +
                                '</div>' +
                                '<div class="col-10 text-right">' +
                                    '<div id="'+caja['codcaj']+'-caja-teorico-format" class="text-bold dashbord-data">' +
                                        sucursal_data[$('#select2-1').val()]['moneda']+' '+$.number(caja['total_teorico'], 2, ',', '.' ) +
                                    '</div>' +
                                    '<p class="text-bold">Teorico</p>' +
                                    '<input type="hidden" id="'+caja['codcaj']+'-caja-teorico" value="'+caja['total_teorico']+'">' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="col-lg-4">' +
                    '<div class="cardbox text-white bg-gradient-danger b0">' +
                        '<div class="cardbox-body p-0">' +
                            '<div class="row">' +
                                '<div class="col-1 ml-0 dashbord-icono">' +
                                    '<i class="ion-plus-round"></i>' +
                                '</div>' +
                                '<div class="col-10 text-right">' +
                                    '<div id="'+caja['codcaj']+'-caja-real-format" class="text-bold dashbord-data">' +
                                        sucursal_data[$('#select2-1').val()]['moneda']+' '+$.number(0, 2, ',', '.' ) +
                                    '</div>' +
                                    '<p class="text-bold">Real</p>' +
                                    '<input type="hidden" id="'+caja['codcaj']+'-caja-real" value="0">' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="col-lg-4">' +
                    '<div class="cardbox text-white bg-gradient-info b0">' +
                        '<div class="cardbox-body p-0">' +
                            '<div class="row">' +
                                '<div class="col-1 ml-0 dashbord-icono">' +
                                    '<i class="ion-navicon-round"></i>' +
                                '</div>' +
                                '<div class="col-10 text-right">' +
                                    '<div id="'+caja['codcaj']+'-caja-diferencia-format" class="text-bold dashbord-data">' +
                                        sucursal_data[$('#select2-1').val()]['moneda']+' '+$.number(caja['total_teorico'] * -1, 2, ',', '.' ) +
                                    '</div>' +
                                    '<p class="text-bold">Diferencia</p>' +
                                    '<input type="hidden" id="'+caja['codcaj']+'-caja-diferencia" value="'+(caja['total_teorico'] * -1)+'">' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="cardbox-heading text-bold">Totales en caja desde el dia ' +
                caja['fecultcie'] + ' a las ' + caja['horultcie'] +
                '<input type="hidden" id="'+caja['codcaj']+'-caja-ultimo-movimiento" value="'+caja['ultmov']+'">' +
            '</div>' +
            '<div class="table-responsive">' +
                '<table id="'+caja['codcaj']+'-caja-teoricos" class="table table-bordered table-sm">' +
                    '<thead class="thead-inverse">' +
                        '<tr>' +
                            '<th class="text-center">Forma de Pago</th>' +
                            '<th class="text-center d-none d-md-table-cell">Ingresos</th>' +
                            '<th class="text-center d-none d-md-table-cell">Egresos</th>' +
                            '<th class="text-center">Teorico</th>' +
                            '<th class="text-center">Real</th>' +
                            '<th class="text-center">Diferencia</th>' +
                        '</tr>' +
                    '</thead>' +
                    '<tfoot>' +
                        '<tr class="text-bold">' +
                            '<td class="text-center">' +
                                'Totales' +
                                '<input type="hidden" class="total_ingresos" value="0">' +
                                '<input type="hidden" class="total_egresos" value="0">' +
                                '<input type="hidden" class="total_teorico" value="0">' +
                                '<input type="hidden" class="total_real" value="0">' +
                                '<input type="hidden" class="total_diferencia" value="0">' +
                            '</td>' +
                            '<td class="ingresos text-right d-none d-md-table-cell"></td>' +
                            '<td class="egresos text-right d-none d-md-table-cell"></td>' +
                            '<td class="teorico text-right"></td>' +
                            '<td class="real text-right"></td>' +
                            '<td class="diferencia text-right"></td>' +
                        '</tr>' +
                    '</tfoot>' +
                    '<tbody>' +
                    '</tbody>' +
                '</table>' +
            '</div>' +
            '<div>' +
                '<textarea id="'+caja['codcaj']+'-obscie" class="col-lg-12" style="height: 100%" placeholder="Observaciones"></textarea>' +
            '</div>' +
        '</div>';
    $('#pestanas_contenido').append(contenido);

    // Cargar teoricos por Forma de Pago
    for (var m=0; m<caja['teoricos'].length; m++) {
        var forma_pago = caja['teoricos'][m];
        agregarTeoricoFormaPago(caja['codcaj'], forma_pago);
    }
}

function agregarTeoricoFormaPago(codcaj, forma_pago) {
    var row =
        '<tr id="'+codcaj+'-'+forma_pago.codfp+'">' +
            '<td class="text-center">' +
                forma_pago.des +
                '<input type="hidden" class="codfp" value="'+forma_pago.codfp+'">' +
                '<input type="hidden" class="desfp" value="'+forma_pago.des+'">' +
            '</td>' +
            '<td class="text-right d-none d-md-table-cell">' +
                /*sucursal_data[$('#select2-1').val()]['moneda']+' '+*/$.number(forma_pago.I, 2, ',', '.' ) +
                '<input type="hidden" class="ingresos" value="'+forma_pago.I+'">' +
            '</td>' +
            '<td class="text-right d-none d-md-table-cell">' +
                /*sucursal_data[$('#select2-1').val()]['moneda']+' '+*/$.number(forma_pago.E, 2, ',', '.' ) +
                '<input type="hidden" class="egresos" value="'+forma_pago.E+'">' +
            '</td>' +
            '<td class="text-right">' +
                /*sucursal_data[$('#select2-1').val()]['moneda']+' '+*/$.number((forma_pago.I-forma_pago.E), 2, ',', '.' ) +
            '</td>' +
            '<td class="text-right montos">' +
                '<a href="#" class="impreal">' + /*sucursal_data[$('#select2-1').val()]['moneda']+' '+*/$.number(0, 2, ',', '.' ) + '</a>' +
                '<input type="hidden" class="teorico" value="'+(forma_pago.I-forma_pago.E)+'">' +
                '<input type="hidden" class="real" value="0">' +
                '<input type="hidden" class="diferencia" value="'+(forma_pago.E-forma_pago.I)+'">' +
                '<input type="hidden" class="fondo" value="'+forma_pago.fondo+'">' +
            '</td>' +
            '<td class="text-right impdiferencia">' +
                /*sucursal_data[$('#select2-1').val()]['moneda']+' '+*/$.number((forma_pago.E-forma_pago.I), 2, ',', '.' ) +
            '</td>' +
        '</tr>';
    if ($('#'+codcaj+'-caja-teoricos > tbody > tr').length > 0) {
        $('#'+codcaj+'-caja-teoricos > tbody:last-child').append(row);
    }
    else {
        $('#'+codcaj+'-caja-teoricos > tbody').html(row);
    }

    $('.impreal').editable({
        type: 'text',
        title: 'Real',
        value: 0,
        display: function (value, response) {
            $(this).text(/*sucursal_data[$('#select2-1').val()]['moneda']+" "+*/$.number(value, 2, ',', '.'));
        },
        success: function (response, value) {
            var real = !isNaN(parseFloat(value))?parseFloat(value):0;
            var teorico = parseFloat($(this).siblings('.teorico').val());
            var diferencia = real - teorico;
            $(this).siblings('.real').val(real);
            $(this).siblings('.diferencia').val(diferencia);
            $(this).parent().siblings('.impdiferencia').html(/*sucursal_data[$('#select2-1').val()]['moneda']+" "+*/$.number(diferencia, 2, ',', '.'));
            calcularTotal(codcaj);
        }
    }).
    on('shown', function(ev, editable) {
        setTimeout(function() {
            editable.input.$input.select();
        },0);
    });

    var total_ingresos = parseFloat($('#'+codcaj+'-caja-teoricos > tfoot > tr > td > .total_ingresos').val());
    total_ingresos += parseFloat(forma_pago.I);
    $('#'+codcaj+'-caja-teoricos > tfoot > tr > td > .total_ingresos').val(total_ingresos);
    $('#'+codcaj+'-caja-teoricos > tfoot > tr > td.ingresos').html(/*sucursal_data[$('#select2-1').val()]['moneda']+' '+*/$.number(total_ingresos, 2, ',', '.' ));

    var total_egresos = parseFloat($('#'+codcaj+'-caja-teoricos > tfoot > tr > td > .total_egresos').val());
    total_egresos += parseFloat(forma_pago.E);
    $('#'+codcaj+'-caja-teoricos > tfoot > tr > td > .total_egresos').val(total_egresos);
    $('#'+codcaj+'-caja-teoricos > tfoot > tr > td.egresos').html(/*sucursal_data[$('#select2-1').val()]['moneda']+' '+*/$.number(total_egresos, 2, ',', '.' ));

    var total_teorico = parseFloat($('#'+codcaj+'-caja-teoricos > tfoot > tr > td > .total_teorico').val());
    total_teorico += parseFloat(forma_pago.I) - parseFloat(forma_pago.E);
    $('#'+codcaj+'-caja-teoricos > tfoot > tr > td > .total_teorico').val(total_teorico);
    $('#'+codcaj+'-caja-teoricos > tfoot > tr > td.teorico').html(/*sucursal_data[$('#select2-1').val()]['moneda']+' '+*/$.number(total_teorico, 2, ',', '.' ));

    var total_real = parseFloat($('#'+codcaj+'-caja-teoricos > tfoot > tr > td > .total_real').val());
    total_real += 0;
    $('#'+codcaj+'-caja-teoricos > tfoot > tr > td > .total_real').val(total_real);
    $('#'+codcaj+'-caja-teoricos > tfoot > tr > td.real').html(/*sucursal_data[$('#select2-1').val()]['moneda']+' '+*/$.number(total_real, 2, ',', '.' ));

    var total_diferencia = parseFloat($('#'+codcaj+'-caja-teoricos > tfoot > tr > td > .total_diferencia').val());
    total_diferencia -= parseFloat(forma_pago.I) - parseFloat(forma_pago.E);
    $('#'+codcaj+'-caja-teoricos > tfoot > tr > td > .total_diferencia').val(total_diferencia);
    $('#'+codcaj+'-caja-teoricos > tfoot > tr > td.diferencia').html(/*sucursal_data[$('#select2-1').val()]['moneda']+' '+*/$.number(total_diferencia, 2, ',', '.' ));
}

function calcularTotal(codcaj) {
    var total_teorico = 0;
    var total_real = 0;
    var total_diferencia = 0;
    $.each($('#'+codcaj+'-caja-teoricos > tbody > tr'), function(i, row) {
        var teorico = parseFloat($(this).find('.montos > .teorico').val());
        var real = parseFloat($(this).find('.montos > .real').val());
        var diferencia = parseFloat($(this).find('.montos > .diferencia').val());
        total_teorico += teorico;
        total_real += real;
    });
    total_diferencia = total_real - total_teorico;

    $('#'+codcaj+'-caja-teoricos > tfoot > tr > td > .total_teorico').val(total_teorico);
    $('#'+codcaj+'-caja-teoricos > tfoot > tr > td.teorico').html(/*sucursal_data[$('#select2-1').val()]['moneda']+' '+*/$.number(total_teorico, 2, ',', '.' ));
    $('#'+codcaj+'-caja-teorico').val(total_teorico);
    $('#'+codcaj+'-caja-teorico-format').html(sucursal_data[$('#select2-1').val()]['moneda']+' '+$.number(total_teorico, 2, ',', '.' ));

    $('#'+codcaj+'-caja-teoricos > tfoot > tr > td > .total_real').val(total_real);
    $('#'+codcaj+'-caja-teoricos > tfoot > tr > td.real').html(/*sucursal_data[$('#select2-1').val()]['moneda']+' '+*/$.number(total_real, 2, ',', '.' ));
    $('#'+codcaj+'-caja-real').val(total_real);
    $('#'+codcaj+'-caja-real-format').html(sucursal_data[$('#select2-1').val()]['moneda']+' '+$.number(total_real, 2, ',', '.' ));

    $('#'+codcaj+'-caja-teoricos > tfoot > tr > td > .total_diferencia').val(total_diferencia);
    $('#'+codcaj+'-caja-teoricos > tfoot > tr > td.diferencia').html(/*sucursal_data[$('#select2-1').val()]['moneda']+' '+*/$.number(total_diferencia, 2, ',', '.' ));
    $('#'+codcaj+'-caja-diferencia').val(total_diferencia);
    $('#'+codcaj+'-caja-diferencia-format').html(sucursal_data[$('#select2-1').val()]['moneda']+' '+$.number(total_diferencia, 2, ',', '.' ));
}

function guardarCierre() {
    var pestana_activa = $('#pestanas > li > a.active');;
    if (pestana_activa.length > 0) {
        var index = pestana_activa.attr('data-index');
        var codcaj = cajas[index].codcaj;
        var codsuc = $('#select2-1').val();
        var cierre = {};
        cierre.codcaj = codcaj;
        cierre.ultmov = $('#'+codcaj+'-caja-ultimo-movimiento').val();
        cierre.impcie = $('#'+codcaj+'-caja-real').val();
        cierre.obscie = $('#'+codcaj+'-obscie').val();
        cierre.usucre = (session != undefined)?session:"";
        cierre.lineas = [];
        $.each($('#'+codcaj+'-caja-teoricos > tbody > tr'), function(i, row) {
            var linea = {};
            linea.codfp = $(this).find('.codfp').val();
            linea.des = $(this).find('.desfp').val();
            linea.impteo = $(this).find('.montos .teorico').val();
            linea.imprea = $(this).find('.montos .real').val();
            linea.impdif = $(this).find('.montos .diferencia').val();
            cierre.lineas.push(linea);
        });
        var params = {"sucursal": codsuc, "opcion": "guardar", "cierre": JSON.stringify(cierre)};
        // console.log("DATA TO SEND: "+JSON.stringify(params));
        $.ajax({
            type: "POST",
            url: urlServerPage,
            data: params,
            success: function(data) {
                if (data.exito == true) {
                    $("#modal_ajuste_cierre .close").click();
                    swal('Guardado!', 'El cierre de caja se ha guardado.', 'success');
                    if (cajas[index].traspasos.length > 0) {
                        preguntarPorTraspasoDeSaldo();
                    }
                    else {
                        resetearVista();
                    }
                }
                else {
                    console.log(JSON.stringify(data));
                }
            },
            error: function(data) {
                console.log(JSON.stringify(data));
            }
        });
    }
}

function resetearVista() {
    inicializar();
}

function cargarFormasDePago() {
    var codcaj = $('#codcaj').val();
    $('#modal_fila_codforpag').text(" ");
    for (var j = 0; j < formas_pago.length; j++) {
        var imprea = $('#'+codcaj+'-'+formas_pago[j].codfp+' > .montos > .real').val();
        if (imprea == "") {
            imprea = "0";
        }
        var fondo = $('#'+codcaj+'-'+formas_pago[j].codfp+' > .montos > .fondo').val();
        if (fondo == "") {
            fondo = "0";
        }
        var row =
            '<div class="fila-con-borde modal_fila_forma_pago" id="modal_fila_codforpag_'+formas_pago[j].codfp+'">' +
                '<div class="row">' +
                    '<div class="col-4">' +
                        '<h5>'+formas_pago[j].des+'</h5>' +
                    '</div>' +
                    '<div class="col-8">' +
                        '<input name="impmov" class="col form-control importe '+formas_pago[j].codfp+'" type="number">' +
                        '<input type="hidden" class="minimpmov" value="'+fondo+'">' +
                        '<input type="hidden" class="maximpmov" value="'+imprea+'">' +
                        '<input type="hidden" class="codforpag" value="'+formas_pago[j].codfp+'">' +
                    '</div>' +
                '</div>' +
            '</div>';
        $('#modal_fila_codforpag').append(row);
    }
}

function realizarTraspaso() {
    var pestana_activa = $('#pestanas > li > a.active');;
    if (pestana_activa.length > 0) {
        var index = pestana_activa.attr('data-index');
        $('#codcaj').val(cajas[index].codcaj);
        $('#ingegr').val("E");
        // Movimientos
        $('#codmov').text(" ");
        $('#codmov').append('<option value=""selected></option>');
        for (var i = 0; i < cajas[index].traspasos.length; i++) {
            $('#codmov').append('<option value="' + cajas[index].traspasos[i]['codmov'] + '" data-json=\''+JSON.stringify(cajas[index].traspasos[i])+'\'>' + cajas[index].traspasos[i]['desmov'] + '</option>');
        }
        $('#codmov').select2();
        // Cajas
        $('#codcajdes').text(" ");
        $('#codcajdes').append('<option value=""selected></option>');
        for (var i = 0; i < cajas.length; i++) {
            if (cajas[i]['codcaj'] != $('#codcaj').val()) {
                $('#codcajdes').append('<option value="' + cajas[i]['codcaj'] + '">'+cajas[i]['nomcaj']+'</option>');
            }
        }
        $('#codcajdes').select2();
        // Formas de Pago
        cargarFormasDePago();

        $('span.select2.select2-container.select2-container--default').css("width", "100%");

        $('#modal_caja_traspaso').modal('show');
    }
}

function preguntarPorTraspasoDeSaldo() {
    swal({
        title: 'Desea traspasar saldo a otra caja?',
        text: 'Si traspasas saldo a otra caja se registrara un egreso en esta caja y un ingreso en la otra!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'No en este momento',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Si, quiero hacerlo',
        closeOnConfirm: true
    },
    function () {
        realizarTraspaso();
    });
}

$("#select2-1").change(function (event) {
    inicializar();
});

$('#btnGuardar').click(function(e) {
    e.preventDefault();
    var pestana_activa = $('#pestanas > li > a.active');;
    if (pestana_activa.length > 0) {
        var index = pestana_activa.attr('data-index');
        var codcaj = cajas[index].codcaj;
        var total_teorico = parseFloat($('#'+codcaj+'-caja-teorico').val());
        if (total_teorico != 0) {
            var ajusteNecesario = false;
            $.each($('#'+codcaj+'-caja-teoricos > tbody > tr'), function(i, row) {
                var diferencia = parseFloat($(this).find('.montos > .diferencia').val());
                if (diferencia != 0) {
                    ajusteNecesario = true;
                }
            });
            if (ajusteNecesario) {
                swal({
                    title: 'Estas seguro?',
                    text: 'Si continuas este cierre con una diferencia distinta a cero en alguna forma de pago se creara un movimiento de ajuste automatico!',
                    type: 'warning',
                    showCancelButton: true,
                    cancelButtonText: 'No, me arrepenti',
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Si, continuar',
                    closeOnConfirm: true
                },
                function () {
                    guardarCierre();
                });
            }
            else {
                guardarCierre();
            }
        }
    }
});

$('#btnCancelar').click(function(e) {
    e.preventDefault();
    swal({
        title: 'Estas seguro?',
        text: 'Si cancelas no se guardaran los datos que has ingresado!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'No, me arrepenti',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Si, cancelarlo',
        closeOnConfirm: false
    },
    function () {
        swal('Cancelado!', 'El cierre de caja ha sido cancelado.', 'success');
        resetearVista();
    });
});

$('#btnGuardarTraspaso').click(function(e) {
    e.preventDefault();
    if ($('#formulario-caja-traspaso').valid()) {
        var mensaje_error = "Ingrese un valor en cada forma de pago que desea traspasar";
        var formas_pago = [];
        $.each($('.modal_fila_forma_pago:visible'), function() {
            var importe = $(this).find('.importe').val();
            if (importe != "") {
                if (parseFloat(importe) > 0 &&
                    parseFloat(importe) <= parseFloat($(this).find('.maximpmov').val()) &&
                    parseFloat(importe) >= parseFloat($(this).find('.minimpmov').val())) {
                    if (mensaje_error.indexOf("entre") < 0) {
                        mensaje_error = "";
                    }
                    var forma_pago = {
                        'codforpag': $(this).find('.codforpag').val(),
                        'impmov': parseFloat(importe)
                    }
                    formas_pago.push(forma_pago);
                }
                else {
                    mensaje_error = "Ingrese un valor entre "+$.number($(this).find('.minimpmov').val(), 2, ',', '.' )+" y "+$.number($(this).find('.maximpmov').val(), 2, ',', '.' )+" en "+$(this).find('h5').text();
                }
            }
        });
        if (mensaje_error != "") {
            swal(mensaje_error, '', 'error');
        }
        else {
            var movimiento = {
                'codsuc': $('#select2-1').val(),
                'codcaj': $('#codcaj').val(),
                'ingegr': $('#ingegr').val(),
                'tipo':   $('#tipo').val(),
                'codmov': $('#codmov').val(),
                'codcajdes': $('#codcajdes').val(),
                'obsmov': $('#obsmov').val(),
                'forpag': formas_pago
            }
            var dataToSend = {
                'sucursal': $('#select2-1').val(),
                'opcion': 'traspasar',
                'movimiento': JSON.stringify(movimiento)
            };
            // console.log("DATA TO SEND: "+JSON.stringify(dataToSend));
            $.ajax({
                type: "POST",
                url: urlServerPage,
                data: dataToSend,
                success: function (data) {
                    if (data.exito == true) {
                        // console.log("CON EXITO: " + JSON.stringify(data));
                        $("#modal_caja_traspaso .close").click();
                        swal('Guardado!', 'El traspaso de saldo se ha guardado.', 'success');
                        resetearVista();
                    }
                    else {
                        swal(data.mensaje, '', 'error');
                        console.log("SIN EXITO: " + JSON.stringify(data));
                    }
                },
                error: function (respuesta) {
                    swal(JSON.stringify(respuesta), '', 'error');
                    console.log("ERROR: " + JSON.stringify(respuesta));
                }
            });
        }
    }
});

$("#codmov").change(function (event) {
    $("#codcajdes").val("").trigger('change');
    $("#codcajdes").removeAttr("readonly");
    $('#codcajdes').removeAttr('disabled');
    $('.modal_fila_forma_pago').hide();

    if ($('#codmov').val() != "") {
        var json = $("#codmov option:selected").attr('data-json');

        var movimiento = JSON.parse(json);
        // Tipo
        $('#tipo').val(movimiento.tipo);
        // Caja
        if (movimiento.solcaj != null && movimiento.solcaj == 1) {
            if (movimiento.codcaj != null) {
                $("#codcajdes").val(movimiento.codcaj).trigger('change');
                $("#codcajdes").attr("readonly", true);
                $('#codcajdes').attr('disabled', true);
            }
        }
        // Forma de Pago
        if (movimiento.codforpag != null) {
            $('#modal_fila_codforpag_'+movimiento.codforpag).show();
        }
        else {
            $('.modal_fila_forma_pago').show();
        }
    }
    else {
        $('.modal_fila_forma_pago').show();
    }
});
