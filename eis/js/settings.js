$(function() {
    showUserData();
});

function showUserData() {
    document.getElementById("username").value = session;
    document.getElementById("usernamep").value = session;
    document.getElementById("usernamen").value = session;

    $('#btn-perfil').hide('fast');
    $('#btn-password').hide('fast');
    $('#btn-notificacion').hide('fast');

    serverPage = ruta.concat("/servidor/php/consul_usuario.php");
    $.ajax({
        type: "POST",
        url: serverPage,
        data: {"session": session},
        cache: false,
        success: function (data) {
            if (data.exito == true) {
                $('#btn-perfil').show('fast');
                $('#btn-password').show('fast');
                $('#btn-notificacion').show('fast');
                document.getElementById("nombre").value = data.nombre;
                document.getElementById("apellido").value = data.apellido;
                document.getElementById("direccion").value = data.direccion;
                document.getElementById("compania").value = data.compania;
                if (data.foto != "") {
                    document.getElementById("foto").src = data.foto;
                }
            }
            else {
                console.log("SIN EXITO: " + JSON.stringify(data));
            }
        },
        error: function (respuesta) {
            console.log("ERROR: " + JSON.stringify(respuesta));
        }
    });
}

$(".remove-image").click(function (event) {
    event.preventDefault();
    serverPage = ruta.concat("/servidor/php/actualizar_usuario.php");
    $.ajax({
        type: "POST",
        url: serverPage,
        data: {operacion: 'remover_imagen', username: $('#username').val()},
        cache: false,
        success: function (data) {
            if (data.exito == true) {
                if (data.foto != "") {
                    guardarFoto(data.foto);
                }
                swal({
                    title: 'Imagen Eliminada!',
                    text: '',
                    type: 'success',
                    confirmButtonText: 'Ok',
                    closeOnConfirm: true
                },
                function () {
                    document.location.reload();
                });
            }
            else {//alert('error');
                swal(data.mensaje, '', 'error');
                console.log("SIN EXITO: " + JSON.stringify(data));
            }
        },
        error: function (respuesta) {
            swal(JSON.stringify(respuesta), '', 'error');
            console.log("ERROR: " + JSON.stringify(respuesta));
        }
    });
});

$("#btn-perfil").click(function (event) {
    event.preventDefault();
    serverPage = ruta.concat("/servidor/php/actualizar_usuario.php");
    $.ajax({
        type: "POST",
        url: serverPage,
        data: new FormData($("#formulario-perfil")[0]),
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.exito == true) {
                guardarNombre($('#nombre').val());
                guardarApellido($('#apellido').val());
                if (data.foto != "") {
                    guardarFoto(data.foto);
                }
                swal({
                    title: 'Usuario Actualizado!',
                    text: '',
                    type: 'success',
                    confirmButtonText: 'Ok',
                    closeOnConfirm: true
                },
                function () {
                    document.location.reload();
                });
            }
            else {//alert('error');
                swal(data.mensaje, '', 'error');
                console.log("SIN EXITO: " + JSON.stringify(data));
            }
            ;
        },
        error: function (respuesta) {
            swal(JSON.stringify(respuesta), '', 'error');
            console.log("ERROR: " + JSON.stringify(respuesta));
        }
    });
}); //Fin de btn

$("#btn-password").click(function (event) {
    event.preventDefault();
    if (document.getElementById("password_a").value.length == 0) {
        swal('Complete Datos!', 'Ingrese Password Actual', 'warning');
    }
    else if (document.getElementById("password_n").value.length == 0) {
        swal('Complete Datos!', 'Ingrese Nuevo Password', 'warning');
    }
    else if (document.getElementById("password_n").value != document.getElementById("password_2").value) {
        swal('Verifique Datos!', 'Verifique el Nuevo Password', 'warning');
        document.getElementById("password_n").value = "";
        document.getElementById("password_2").value = "";
    }
    else {
        serverPage = ruta.concat("/servidor/php/actualizar_password.php");
        $.post(serverPage, $("#formulario-password").serialize(), function (data) {
            if (data.exito == true) {
                swal('Password Actualizado!', '', 'success');
            }
            else {
                swal('Password Invalido!', 'Verifique los datos', 'warning');
                document.getElementById("password_a").value = "";
            }
            ;
        }); //Fin .post
    }
    ;
}); //Fin de btn

$("#btn-notificacion").click(function (event) {
    event.preventDefault();
    serverPage = ruta.concat("/servidor/php/actualizar_notificacion.php");
    $.post(serverPage, $("#formulario-notificacion").serialize(), function (data) {
        if (data.exito == true) {
            swal('Notificaciones Actualizadas!', '', 'success');
        }
        else {//alert('error');
        }
        ;
    }); //Fin .post
}); //Fin de btn