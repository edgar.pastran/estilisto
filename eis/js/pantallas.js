var urlServerPage = ruta.concat("/servidor/php/pantallas.php");

$(function() {
    cargarCatalogo();
    inicializar();
});

function cargarCatalogo() {
    $('#bootgrid-pantallas').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        selection: true,
        multiSelect: false,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on("selected.rs.jquery.bootgrid", function(e, rows) {
        $("#codpan").val(rows[0].pa_codpan);
        $("#despan").val(rows[0].pa_despan);
        $("#pantalla").val(rows[0].pa_pantalla);

        $("#codpan").removeAttr("readonly");
        $("#despan").removeAttr("readonly");
        $("#pantalla").removeAttr("readonly");

        $("#opc_pantalla").val("actualizar");
        $("#closeMpa").click();
    })
    .on("deselected.rs.jquery.bootgrid", function(e, rows) {

    });
}

$("#btn-pantallas").click(function (event){
    event.preventDefault();
    $("#opc_pantalla").val("consulta");

    $.post(urlServerPage, $("#formulario-pantalla").serialize(), function (data){
        if ( data.exito == true && data.nume_regis >= 1 ){

            $("#bootgrid-pantallas").bootgrid("clear");
            for (var i = 0; i < data.nume_regis; i++) {
                $("#bootgrid-pantallas").bootgrid().bootgrid("append",[{"pa_codpan": ""+data.pantallas[i]['codpan']+"",
                    "pa_despan": ""+data.pantallas[i]['despan']+"",
                    "pa_pantalla": ""+data.pantallas[i]['pantalla']+""}]);
            };
            //Show Modal
            $('#modal_pantallas').modal('show');
        };//No hay Datos
    }); //Fin .post
}); //Fin .btn

function eliminar() {
    if ($("#codpan").val() != "" && $("#opc_pantalla").val() != "insertar") {
        $("#opc_pantalla").val("eliminar");
        guardar();
    }
}

function guardar(){
    //$("#suc_formapago").val($("#select2-1").val());
    if ($("#opc_pantalla").val() != "" && $("#codpan").val() != "") {
        $.post(urlServerPage, $("#formulario-pantalla").serialize(), function (data) {
            if (data.exito == true){
                if ($("#opc_pantalla").val() == "actualizar") {
                    swal('PANTALLA Actualizado!', '', 'success');
                }
                if ($("#opc_pantalla").val() == "insertar") {
                    swal('PANTALLA Creado!', '', 'success');
                }
                if ($("#opc_pantalla").val() == "eliminar") {
                    swal('PANTALLA Eliminada!', '', 'success');
                }

                cancelar();

            }
            else{
                //alert('error');
            }
        }); //Fin .post
    }
} //Fin de Funcion

function consultar_pantalla(){
    var opc = $("#opc_pantalla").val();
    if (opc == "insertar") {
        $("#opc_pantalla").val("consultar");
        if ($("#opc_pantalla").val() != "" && $("#codpan").val() != "") {
            $.post(urlServerPage, $("#formulario-pantalla").serialize(), function (data) {
                if (data.exito == true){1
                    swal('PANTALLA YA Existe!', '', 'error');
                    $("#codpan").focus();
                }
                else{
                    //alert('error');
                    $("#codpan").attr("readonly", true);
                }
            }); //Fin .post
        }
    } //Fin del IF
    $("#opc_pantalla").val(opc);
} //Fin de Funcion

function cancelar(){
    inicializar();
} //Fin de Funcion

function nuevo(){
    $("#opc_pantalla").val("insertar");
    $("#codpan").val("");
    $("#despan").val("");

    $("#codpan").removeAttr("readonly");
    $("#codpan").focus();
    $("#despan").removeAttr("readonly");
    $("#pantalla").removeAttr("readonly");
    $("#btn-pantallas").attr("disabled", true);

} //Fin de Funcion

function inicializar(){
    $("#opc_pantalla").val("");
    $("#codpan").val("");
    $("#despan").val("");
    $("#pantalla").val("");

    $("#codpan").attr("readonly", true);
    $("#despan").attr("readonly", true);

    $("#pantalla").attr("readonly" , true);
    $("#btn-pantallas").attr("disabled", false);
}
