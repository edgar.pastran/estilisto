var urlServerPage = ruta.concat("/servidor/php/actualizar_empresa.php");

$(function() {
    usuarioSucursal(inicializar);
    cargarCatalogo();
});

function cargarCatalogo() {
    $('#bootgrid-empresas').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        selection: true,
        multiSelect: false,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on("selected.rs.jquery.bootgrid", function(e, rows) {
        cancelar();
        $("#select2-1").val("");
        $("#codsucN").val(rows[0].emp_codsucN);
        $("#razemp").val(rows[0].emp_razemp);
        $("#domemp").val(rows[0].emp_domemp);
        $("#select2-pais").val(rows[0].emp_pais);
        $("#opcion").val("actualizar");

        $("#closeM").click();

        //Quitar ReadOnly
        //$("#select2-1").attr("readonly" , false);
        //$("#codsucN").removeAttr("readonly");
        //$("#razemp").removeAttr("readonly");
        //$("#domemp").removeAttr("readonly");
        //$("#select2-pais").removeAttr("disabled");

    })
    .on("deselected.rs.jquery.bootgrid", function(e, rows) {

    });
}

$("#btn-empresas").click(function (event) {
    event.preventDefault();
    $("#opcion").val("consulta_sucursales_usuario");

    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: {"sucursal":"*****","opcion":"consulta_sucursales_usuario"},
        success: function (data) {
            if (data.exito == true && data.nume_regis >= 1) {
                if (data.nume_regis >= 1) {
                    $("#bootgrid-empresas").bootgrid("clear");
                    for (var i = 0; i < data.nume_regis; i++) {
                        $("#bootgrid-empresas").bootgrid().bootgrid("append", [{
                            "emp_codsucN": "" + data.empresas[i]['codsuc'] + "",
                            "emp_razemp": data.empresas[i]['razemp'],
                            "emp_domemp": data.empresas[i]['domemp'],
                            "emp_pais": "" + data.empresas[i]['pais'] + ""
                        }]);
                    }
                    ;
                    //Show Modal
                    $('#modal_empresas').modal('show');
                }
                ;//No hay Datos
            }
            else {
                console.log("SIN EXITO: " + JSON.stringify(data));
            }
        },
        error: function (respuesta) {
            console.log("ERROR: " + JSON.stringify(respuesta));
        }
    });
}); //Fin .btn


function eliminar() {
    if ($("#codsucN").val() != "" && $("#opcion").val() != "crear") {
        $("#opcion").val("eliminar");
        $("#select2-1").val("*****");
        guardar();
    }
}

function guardar() {
    if ($("#codsucN").val() != "" && $("#opcion").val() != "") {
        if ($("#opcion").val() != "actualizar"){
            $.ajax({
                type: "POST",
                url: urlServerPage,
                data: new FormData($("#form-crear-empresa")[0]),
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.exito == true) {
                        if ($("#opcion").val() == "actualizar") {
                            swal('Empresa Actualizada!', '', 'success');
                        }
                        else if ($("#opcion").val() == "crear") {
                            swal('Empresa Creada!', '', 'success');
                        }
                        else if ($("#opcion").val() == "eliminar") {
                            swal('Empresa Eliminada!', '', 'success');
                        }

                        cancelar();
                    }
                    else {//alert('error');
                        swal(data.mensaje, '', 'error');
                        console.log("SIN EXITO: " + JSON.stringify(data));
                    }
                    ;
                },
                error: function (respuesta) {
                    swal(JSON.stringify(respuesta), '', 'error');
                    console.log("ERROR: " + JSON.stringify(respuesta));
                }
            });
        }
        else if ($("#opcion").val() == "actualizar") {
            swal('No se Permite Modificar!', '', 'error');
        }
    }
}; //Fin de Funcion

function consultar_empresa() {
    if ($("#opcion").val() == "crear") {
        if ($("#codsucN").val() != "" && $("#opcion").val() != "") {
            $.post(urlServerPage, {"sucursal":$("#codsucN").val(), "opcion":"consultar_sucursal"}, function (data) {
                if (data.exito == true) {
                    swal('Empresa YA Existe!', '', 'error');
                    $("#codsucN").val("");
                    $("#codsucN").focus();
                }
                else {//No pasa Nada
                    $("#codsucN").attr("readonly" , true);
                }
            }); //Fin .post
        }
    } //Fin del IF
} //Fin de Funcion

function grupoEmpresas(valor){
    var urlServerPageAux = ruta.concat("/servidor/php/grupo_empresa.php");
    $.post(urlServerPageAux, {"sucursal":"*****","opcion":"consulta"}, function (data){
        if ( data.exito == true && data.nume_regis >= 1 ) {
            $('#select2-codemp').text(" ");
            $('#select2-codemp').append('<option value=""selected>Independiente</option>');
            for (var i = 0; i < data.nume_regis; i++) {
                if (valor === data.empresas[i]['codemp']) {
                    $('#select2-codemp').append('<option value="'+data.empresas[i]['codemp']+'"selected>'+data.empresas[i]['nomgrupo']+'</option>');
                }
                else{
                    $('#select2-codemp').append('<option value="'+data.empresas[i]['codemp']+'">'+data.empresas[i]['nomgrupo']+'</option>');
                }
            }
        }
    }); //Fin .post
}

function cancelar() {
    guardarSucursal($("#select2-1").val());//Guardar la Sucursal con la que se Esta trabajando
    $("#opcion").val("");
    
    $("#codsucN").val("");
    $("#razemp").val("");
    $("#domemp").val("");
    $("#select2-1").val("");
    $('#select2-1').trigger('change');
    $("#select2-pais").val("");
    $('#select2-pais').trigger('change');
    $("#select2-codemp").val("");
    $('#select2-codemp').trigger('change');

    $("#select2-1").attr("disabled", true);
    $("#codsucN").attr("readonly", true);
    $("#razemp").attr("readonly", true);
    $("#domemp").attr("readonly", true);
    $("#select2-pais").attr("disabled", true);
    $("#select2-codemp").attr("disabled", true);

} //Fin de Funcion

function nuevo() {
    $("#opcion").val("crear");
    $("#codsucN").val("");
    $("#razemp").val("");
    $("#domemp").val("");
    $("#select2-1").val("");
    $('#select2-1').trigger('change');
    $("#select2-pais").val("");
    $('#select2-pais').trigger('change');
    $("#select2-codemp").val("");
    $('#select2-codemp').trigger('change');

    $("#select2-1").removeAttr("disabled");
    $("#codsucN").removeAttr("readonly");
    $("#codsucN").focus();
    $("#razemp").removeAttr("readonly");
    $("#domemp").removeAttr("readonly");
    $("#select2-pais").removeAttr("disabled");
    $("#select2-codemp").removeAttr("disabled");
    
} //Fin de Funcion

function inicializar() {
    cancelar();
    grupoEmpresas();//Codigo del grupo de empresa.
}

$('#select2-1').select2();
