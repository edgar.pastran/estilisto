var urlServerPage = ruta.concat("/servidor/php/clientes.php");

$(function() {
    $(formValidation);
    usuarioSucursal(formapagos);
    inicializar();
    cargarCatalogo();
})

function formValidation() {

    if (!$.fn.validate) return;

    $('#formulario-clientes').validate({
        errorPlacement: errorPlacementInput,
        errorClass: "my-error-class",
        // Form rules
        rules: {
            email: {
                required: true,
                email: true
            },
            nomcli: {
                required: true
            },
            ape1cli: {
                required: true
            }
        }
    });
}

function cargarCatalogo() {
    $('#bootgrid-clientesDM').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        selection: true,
        multiSelect: false,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on("selected.rs.jquery.bootgrid", function(e, rows) {
        $("#codcli").val(rows[0].cli_codcli);
        $("#codcli2").val(rows[0].cli_codcli2);
        $("#nomcli").val(rows[0].cli_nomcli);
        $("#ape1cli").val(rows[0].cli_ape1cli);
        $("#tel1cli").val(rows[0].cli_tel1cli);
        $("#dnicli").val(rows[0].cli_dnicli);
        $("#email").val(rows[0].cli_email);
        $("#select2-sexo").val(rows[0].cli_sexo);
        $("#fecnac").val(rows[0].cli_fecnac);
        if (rows[0].cli_foto != "") {
            $("#foto").attr("src", rows[0].cli_foto);
        }

        $("#codfp").val(rows[0].cli_codfp);
        $("#obsoleto").val(rows[0].cli_obsoleto); setCheckbox('obsoleto','1');
        $("#actpuntos").val(rows[0].cli_actpuntos); setCheckbox('actpuntos','1');

        $("#opc_clientes").val("actualizar");

        $("#closeMcli").click();
        deshabilitarCampos(false);
    })
    .on("deselected.rs.jquery.bootgrid", function(e, rows){

    });
}

$("#btn-clientes").click(function (event){
    event.preventDefault();
    $("#suc_clientes").val($("#select2-1").val());
    $("#opc_clientes").val("consulta");

    $.post(urlServerPage, $("#formulario-clientes").serialize(), function (data){
        if ( data.exito == true && data.nume_regis >= 1 ){
            if (data.nume_regis >= 1) {
                $("#bootgrid-clientesDM").bootgrid("clear");
                for (var i = 0; i < data.nume_regis; i++) {
                    $("#bootgrid-clientesDM").bootgrid().bootgrid("append", [{
                        "cli_codcli": ""+data.clientes[i]['codcli']+"",
                        "cli_nomcli": ""+data.clientes[i]['nomcli']+"",
                        "cli_ape1cli": ""+data.clientes[i]['ape1cli']+"",
                        "cli_fecnac": ""+data.clientes[i]['fecnac']+"",
                        "cli_codcli2": ""+data.clientes[i]['codcli2']+"",
                        "cli_tel1cli": ""+data.clientes[i]['tel1cli']+"",
                        "cli_email": ""+data.clientes[i]['email']+"",
                        "cli_dnicli": ""+data.clientes[i]['dnicli']+"",
                        "cli_sexo": ""+data.clientes[i]['sexo']+"",
                        "cli_foto": ""+data.clientes[i]['foto']+"",
                        "cli_codfp": ""+data.clientes[i]['forpagcli']+"",
                        "cli_actpuntos": ""+data.clientes[i]['actpuntos']+"",
                        "cli_obsoleto": ""+data.clientes[i]['obsoleto']+""
                    }]);
                };
                //Show Modal
                $('#modal_clientes').modal('show');
            };//No hay Datos
        };
    }); //Fin .post
}); //Fin .btn

$("#btn_remover_imagen").click(function (event) {
    if ($("#codcli").val() != "" && $("#opc_clientes").val() != "insertar") {
        $("#opc_clientes").val("remover_imagen");
        guardar();
    }
    else {
        $("#foto").attr("src", ruta.concat("/servidor/images/nothing.png"));
    }
});

function eliminar() {
    $("#opc_clientes").val("eliminar");
    guardar();
}

function guardar(){
    if ($('#formulario-clientes').valid() == false) {
        //swal('Complete Campos!', '', 'error');
    }
    else{
        myCheckbox('obsoleto','1'); myCheckbox('actpuntos','1');
        $("#suc_clientes").val($("#select2-1").val());
        if ($("#select2-1").val() != "" && $("#opc_clientes").val() != "") {
            $.ajax({
                type: "POST",
                url: urlServerPage,
                data: new FormData($("#formulario-clientes")[0]),
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.exito == true) {
                        if ($("#opc_clientes").val() == "actualizar") {
                            swal('Cliente Actualizado!', '', 'success');
                        }
                        else if ($("#opc_clientes").val() == "insertar") {
                            swal('Cliente Creado!', '', 'success');
                        }
                        else if ($("#opc_clientes").val() == "eliminar") {
                            swal('Cliente Eliminado!', '', 'success');
                        }
                        else if ($("#opc_clientes").val() == "remover_imagen") {
                            swal('Imagen Eliminada!', '', 'success');
                        }
                        cancelar();
                    }
                    else if ($("#opc_clientes").val() == "eliminar") {
                        cancelar();
                    }
                    else {//alert('error');
                        swal(data.mensaje, '', 'error');
                        console.log("SIN EXITO: " + JSON.stringify(data));
                    }
                },
                error: function (respuesta) {
                    swal(JSON.stringify(respuesta), '', 'error');
                    console.log("ERROR: " + JSON.stringify(respuesta));
                }
            });
        };
    };
} //Fin de Funcion

function consultar_clientes(){
    $("#suc_clientes").val($("#select2-1").val());
    var opc = $("#opc_clientes").val();
    if (opc == "insertar") {
        $("#opc_clientes").val("consultar");
        if ($("#select2-1").val() != "" && $("#opc_clientes").val() != "" && $("#codcli").val() != "") {
            $.post(urlServerPage, $("#formulario-clientes").serialize(), function (data) {
                if (data.exito == true){1
                    swal('Cliente YA Existe!', '', 'error');
                    $("#codcli").focus();
                }else{//alert('error');
                    $("#codcli").prop('readonly', true);
                };
            }); //Fin .post
        };
    } //Fin del IF
    $("#opc_clientes").val(opc);
} //Fin de Funcion

function consultar_codcli2(){

}

$(document).on('click', '#btn-cancelar', function(e) {
    if ( $('#opc_clientes').val() == "insertar" || $('#opc_clientes').val() == "actualizar") {
        e.preventDefault();
        swal({
                title: '¿Estas seguro?',
                text: 'Si cancelas no se guardaran los datos que has ingresado!',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'No, me arrepenti',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Si, cancelarlo',
                closeOnConfirm: false
            },
            function () {
                swal('Cancelado!', 'La Creacion de Cliente ha sido cancelada.', 'success');
                cancelar();
            });
    }
});

$(document).on('click', '#btn-eliminar', function(e) {
    if ($('#codcli').val() != "" && $('#opc_clientes').val() != "insertar") {
        e.preventDefault();
        swal({
                title: '¿Estas seguro?',
                text: 'Si Eliminas se borraran los datos del Cliente!',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'No, me arrepenti',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Si, eliminar',
                closeOnConfirm: false
            },
            function () {
                swal('Eliminado!', 'El Cliente ha sido Eliminado.', 'success');
                eliminar();
            });
    }
});

function cancelar() {
    // Configurar la caja de texto de fecha
    $('#fecnac').
    datepicker({
        endDate : 'now',
        autoclose: true,
        container: '#fecnac-datepicker-container'
    });
    var validator = $( "#formulario-clientes" ).validate();
    validator.resetForm();

    limpiarDatos();

    deshabilitarCampos(true);

    $("#btn-codcli2").prop('disabled', false);
    $("#btn-clientes").prop('disabled', false);
} //Fin de Funcion

function nuevo() {
    if ($("#select2-1").val() != "") {
        $("#opc_clientes").val("insertar");
        limpiarDatos();

        $("#actpuntos").val("1"); setCheckbox('actpuntos','1');
        deshabilitarCampos(false);
        $("#nomcli").focus();

        var fecha=new Date();
        var fecha_diames=fecha.getDate(); if ( fecha_diames < 10 ) { fecha_diames = '0'+fecha_diames};
        var fecha_mes=fecha.getMonth() +1; if ( fecha_mes < 10 ) { fecha_mes = '0'+fecha_mes};
        var fecha_ano=fecha.getFullYear()-18;
        var fecha_actual = fecha_diames+'/'+fecha_mes+'/'+fecha_ano;
        $("#fecnac").val(fecha_actual);
        $('#fecnac').datepicker('setDate',fecha_actual);
    }else{
        $("#select2-1").focus();
        swal('Seleccione Sucursal!');

    }
} //Fin de Funcion

function limpiarDatos() {
    $("#codcli").val("");
    $("#codcli2").val("");
    $("#nomcli").val("");
    $("#ape1cli").val("");
    $("#fecnac").val("");
    $("#select2-sexo").val("M");
    $("#email").val("");
    $("#dnicli").val("");
    $("#tel1cli").val("");
    $("#obsoleto").val(""); setCheckbox('obsoleto','1');
    $("#actpuntos").val(""); setCheckbox('actpuntos','1');

    $("#foto").attr("src", ruta.concat("/servidor/images/nothing.png"));
    $("#btn_remover_imagen").click();
    $("#btn_remover_imagen_nueva").click();
}

function deshabilitarCampos(valor) {
    $("#codcli2").prop('disabled', valor);
    $("#nomcli").prop('disabled', valor);
    $("#ape1cli").prop('disabled', valor);
    $("#fecnac").prop('disabled', valor);
    $("#select2-sexo").prop('disabled', valor);
    $("#email").prop('disabled', valor);
    $("#dnicli").prop('disabled', valor);
    $("#tel1cli").prop('disabled', valor);
    $("#codfp").prop('disabled', valor);
    $("#obsoleto").prop('disabled', valor);
    $("#actpuntos").prop('disabled', valor);

    $("#btn_remover_imagen").prop('disabled', valor);
    $("#imagen_nueva").prop('disabled', valor);

    if (valor) {
        $('.remove-image').removeClass('btn-danger').addClass('btn-secondary');
        $('.edit-image').removeClass('btn-info').addClass('btn-secondary');
    }
    else {
        $('.remove-image').removeClass('btn-secondary').addClass('btn-danger');
        $('.edit-image').removeClass('btn-secondary').addClass('btn-info');
    }
}

function limpiarCamposSucursal() {
    guardarSucursal($("#select2-1").val());//Guardar la Sucursal con la que se Esta trabajando
    cancelar();
    formapagos('EFECTIVO');
}

function inicializar(){
    cancelar();
}

$('#select2-1').select2();
