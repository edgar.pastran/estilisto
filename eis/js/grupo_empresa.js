var urlServerPage = ruta.concat("/servidor/php/grupo_empresa.php");

$(function() {
    cargarCatalogo();
    inicializar();
});

function cargarCatalogo() {
    $('#bootgrid-grupo_empresas').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        selection: true,
        multiSelect: false,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on("selected.rs.jquery.bootgrid", function(e, rows) {
        $("#codemp").val(rows[0].ge_codemp);
        $("#nomgrupo").val(rows[0].ge_nomgrupo);
        $("#nomgrupo").removeAttr("readonly");
        $("#opc_grupo_empresa").val("actualizar");

        $("#closeMge").click();
    })
    .on("deselected.rs.jquery.bootgrid", function(e, rows) {

    });
}

$("#btn-grupo_empresa").click(function (event){
    event.preventDefault();
    $("#opc_grupo_empresa").val("consulta");

    $.post(urlServerPage, $("#formulario-grupo_empresa").serialize(), function (data){
        if ( data.exito == true && data.nume_regis >= 1 ){

            if (data.nume_regis >= 1) {
                $("#bootgrid-grupo_empresas").bootgrid("clear");
                for (var i = 0; i < data.nume_regis; i++) {

                    $("#bootgrid-grupo_empresas").bootgrid().bootgrid("append", [{"ge_codemp": ""+data.empresas[i]['codemp']+"", "ge_nomgrupo": ""+data.empresas[i]['nomgrupo']+""}] );

                };
                //Show Modal
                $('#modal_grupo_empresas').modal('show');
            };//No hay Datos
        };
    }); //Fin .post
}); //Fin .btn

function eliminar() {
    if ($("#codemp").val() != "" && $("#opc_grupo_empresa").val() != "insertar") {
        $("#opc_grupo_empresa").val("eliminar");
        guardar();
    }
}

function guardar(){
    //$("#suc_formapago").val($("#select2-1").val());
    if ($("#opc_grupo_empresa").val() != "" && $("#codemp").val() != "") {;
        $.post(urlServerPage, $("#formulario-grupo_empresa").serialize(), function (data) {
            if (data.exito == true){
                if ($("#opc_grupo_empresa").val() == "actualizar") {
                    swal('GRUPO Actualizado!', '', 'success');
                }
                if ($("#opc_grupo_empresa").val() == "insertar") {
                    swal('GRUPO Creado!', '', 'success');
                }
                if ($("#opc_grupo_empresa").val() == "eliminar") {
                    swal('GRUPO Eliminada!', '', 'success');
                }
                cancelar();
            }
            else{//alert('error');

            }
        }); //Fin .post
    }
} //Fin de Funcion

function consultar_grupoEmpresa(){
    var opc = $("#opc_grupo_empresa").val();
    if (opc == "insertar") {
        $("#opc_grupo_empresa").val("consultar");
        if ($("#opc_grupo_empresa").val() != "" && $("#codemp").val() != "") {
            $.post(urlServerPage, $("#formulario-grupo_empresa").serialize(), function (data) {
                if (data.exito == true){1
                    swal('GRUPO YA Existe!', '', 'error');
                    $("#codemp").focus();
                }
                else{
                    //alert('error');
                    $("#codemp").attr("readonly", true);
                }
            }); //Fin .post
        }
    } //Fin del IF
    $("#opc_grupo_empresa").val(opc);
} //Fin de Funcion

function cancelar(){
    inicializar();


} //Fin de Funcion

function nuevo(){
    $("#opc_grupo_empresa").val("insertar");
    $("#codemp").val("");
    $("#nomgrupo").val("");

    $("#codemp").removeAttr("readonly");
    $("#codemp").focus();
    $("#nomgrupo").removeAttr("readonly");

    $("#btn-grupo_empresa").attr("disabled", true);
} //Fin de Funcion

function inicializar(){
    $("#opc_grupo_empresa").val("");
    $("#codemp").val("");
    $("#nomgrupo").val("");

    $("#codemp").attr("readonly", true);
    $("#nomgrupo").attr("readonly", true);

    $("#btn-grupo_empresa").attr("disabled",  false);
}