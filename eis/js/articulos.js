var urlServerPage = ruta.concat("/servidor/php/articulos.php");

$(function() {
    usuarioSucursal(cargarDatos);
    inicializar();
    cargarCatalogo();
})

function cargarCatalogo() {
    $('#bootgrid-articulosDM').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        selection: true,
        multiSelect: false,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on("selected.rs.jquery.bootgrid", function(e, rows) {
        deshabilitarCampos(false);

        $("#codart").prop('readonly', true);

        $("#codart").val(rows[0].art_codart);
        $("#codartdos").val(rows[0].art_codartdos);
        $("#desart").val(rows[0].art_desart);
        $("#tipart").val(rows[0].art_tipart);
        $("#familia1").val(rows[0].art_familia1);
        $("#pvpa").val(rows[0].art_pvpa);
        $("#ivaart").val(rows[0].art_ivaart);
        $("#orden").val(rows[0].art_orden);
        $("#puntos").val(rows[0].art_puntos);
        if (rows[0].art_foto != "") {
            $("#foto").attr("src", rows[0].art_foto);
        }

        $("#vender").val(rows[0].art_vender); setCheckbox('vender','1');
        $("#comprar").val(rows[0].art_comprar); setCheckbox('comprar','1');
        $("#obsoleto").val(rows[0].art_obsoleto); setCheckbox('obsoleto','1');

        $("#opcion").val("actualizar");
        $("#closeMart").click();

    })
    .on("deselected.rs.jquery.bootgrid", function(e, rows) {});
}

$("#btn-articulos").click(function (event) {
    event.preventDefault();
    $("#codsuc").val($("#select2-1").val());
    $("#opcion").val("consulta");

    $.post(urlServerPage, $("#formulario-articulo").serialize(), function (data) {
        if (data.exito == true && data.nume_regis >= 1) {

            if (data.nume_regis >= 1) {
                $("#bootgrid-articulosDM").bootgrid("clear");
                for (var i = 0; i < data.nume_regis; i++) {

                    $("#bootgrid-articulosDM").bootgrid().bootgrid("append", [{
                        "art_codart": "" + data.articulos[i]['codart'] + "",
                        "art_desart": "" + data.articulos[i]['desart'] + "",
                        "art_codartdos": "" + data.articulos[i]['codartdos'] + "",
                        "art_pvpa": "" + data.articulos[i]['pvpa'] + "",
                        "art_familia1": "" + data.articulos[i]['codfam1'] + "",
                        "art_desfam1": "" + data.articulos[i]['desfam1'] + "",
                        "art_ivaart": "" + data.articulos[i]['ivaart'] + "",
                        "art_orden": "" + data.articulos[i]['orden'] + "",
                        "art_puntos": "" + data.articulos[i]['puntos'] + "",
                        "art_tipart": "" + data.articulos[i]['tipart'] + "",
                        "art_foto": "" + data.articulos[i]['foto'] + "",
                        "art_vender": "" + data.articulos[i]['vender'] + "",
                        "art_comprar": "" + data.articulos[i]['comprar'] + "",
                        "art_obsoleto": "" + data.articulos[i]['obsoleto'] + ""
                    }]);
                }
                ;
                //Show Modal
                $('#modal_articulos').modal('show');
            }
            ;//No hay Datos
        }
        ;
    }); //Fin .post
}); //Fin .btn

$("#btn_remover_imagen").click(function (event) {
    if ($("#codart").val() != "" && $("#opcion").val() != "insertar") {
        $("#opcion").val("remover_imagen");
        guardar();
    }
    else {
        $("#foto").attr("src", ruta.concat("/servidor/images/nothing.png"));
    }
});

function eliminar() {
    $("#opcion").val("eliminar");
    guardar();
}

function guardar() {
    if ($('#formulario-articulo').valid() == false) {
        //swal('Complete Campos!', '', 'error');
    }
    else {
        myCheckbox('vender', '1');
        myCheckbox('comprar', '1');
        myCheckbox('obsoleto', '1');
        $("#codsuc").val($("#select2-1").val());
        if ($("#select2-1").val() != "" && $("#opcion").val() != "") {

            $.ajax({
                type: "POST",
                url: urlServerPage,
                data: new FormData($("#formulario-articulo")[0]),
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.exito == true) {
                        if ($("#opcion").val() == "actualizar") {
                            swal('ARTICULO Actualizado!', '', 'success');
                        }
                        else if ($("#opcion").val() == "insertar") {
                            swal('ARTICULO Creado!', '', 'success');
                        }
                        else if ($("#opcion").val() == "eliminar") {
                            swal('ARTICULO Eliminado!', '', 'success');
                        }
                        else if ($("#opcion").val() == "remover_imagen") {
                            swal('Imagen Eliminada!', '', 'success');
                        }
                        cancelar();
                    }
                    else if ($("#opcion").val() == "eliminar") {
                        cancelar();
                    }
                    else {//alert('error');
                        swal(data.mensaje, '', 'error');
                        console.log("SIN EXITO: " + JSON.stringify(data));
                    }
                },
                error: function (respuesta) {
                    swal(JSON.stringify(respuesta), '', 'error');
                    console.log("ERROR: " + JSON.stringify(respuesta));
                }
            });
        }
        ;
    }
    ;
} //Fin de Funcion

function consultar_articulo() {
    $("#codsuc").val($("#select2-1").val());
    var opc = $("#opcion").val();
    if (opc == "insertar") {
        $("#opcion").val("consultar");
        if ($("#select2-1").val() != "" && $("#opcion").val() != "" && $("#codart").val() != "") {
            $.post(urlServerPage, $("#formulario-articulo").serialize(), function (data) {
                if (data.exito == true) {
                    1
                    swal('ARTICULO YA Existe!', '', 'error');
                    $("#codart").focus();
                } else {//alert('error');
                    $("#codart").prop('readonly', true);
                }
                ;
            }); //Fin .post
        }
        ;
    } //Fin del IF
    $("#opcion").val(opc);
} //Fin de Funcion

$(document).on('click', '#btn-cancelar', function (e) {
    if ($('#opcion').val() == "insertar" || $('#opcion').val() == "actualizar") {
        e.preventDefault();
        swal({
                title: '¿Estas seguro?',
                text: 'Si cancelas no se guardaran los datos que has ingresado!',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'No, me arrepenti',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Si, cancelarlo',
                closeOnConfirm: false
            },
            function () {
                swal('Cancelado!', 'La Creacion del Articulo ha sido cancelada.', 'success');
                cancelar();
            });
    }
});

$(document).on('click', '#btn-eliminar', function (e) {
    if ($('#codart').val() != "" && $('#opcion').val() != "insertar") {
        e.preventDefault();
        swal({
                title: '¿Estas seguro?',
                text: 'Si Eliminas se borraran los datos del Proveedor!',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'No, me arrepenti',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Si, eliminar',
                closeOnConfirm: false
            },
            function () {
                swal('Eliminado!', 'El Articulo ha sido Eliminado.', 'success');
                eliminar();
            });
    }
});

function cancelar() {
    // Configurar la caja de texto de fecha
    var validator = $("#formulario-articulo").validate();
    validator.resetForm();
    limpiarDatos();

    deshabilitarCampos(true);
    $("#opcion").val("");

    $("#btn-articulos").prop('disabled', false);
} //Fin de Funcion

function nuevo() {
    if ($("#select2-1").val() != "") {
        $("#opcion").val("insertar");
        limpiarDatos();
        deshabilitarCampos(false);
        $("#ivaart").val("1");
        $("#vender").val("1");
        setCheckbox('vender', '1');
        $("#comprar").val("1");
        setCheckbox('comprar', '1');
        $("#codart").focus();

    } else {
        $("#select2-1").focus();
        swal('Seleccione Sucursal!');

    }
} //Fin de Funcion

function limpiarDatos() {
    $("#tipart").val("");
    $("#codart").val("");
    $("#puntos").val("");
    $("#desart").val("");
    $("#ivaart").val("");
    $("#orden").val("");
    $("#codartdos").val("");
    $("#pvpa").val("");
    $("#vender").val("");
    setCheckbox('vender', '1');
    $("#comprar").val("");
    setCheckbox('comprar', '1');
    $("#select2-pais").val("");
    $('#select2-pais').trigger('change');
    $("#familia1").val("");
    $("#obsoleto").val("");
    setCheckbox('obsoleto', '1');

    $("#foto").attr("src", ruta.concat("/servidor/images/nothing.png"));
    $("#btn_remover_imagen").click();
    $("#btn_remover_imagen_nueva").click();
}

function deshabilitarCampos(valor) {
    $("#tipart").prop('disabled', valor);
    $("#codart").prop('readonly', valor);
    $("#puntos").prop('disabled', valor);
    $("#desart").prop('disabled', valor);
    $("#ivaart").prop('disabled', valor);
    $("#orden").prop('disabled', valor);
    $("#codartdos").prop('disabled', valor);
    $("#pvpa").prop('disabled', valor);
    $("#vender").prop('disabled', valor);
    $("#comprar").prop('disabled', valor);
    $("#select2-pais").prop('disabled', valor);
    $("#familia1").prop('disabled', valor);
    $("#obsoleto").prop('disabled', valor);

    $("#btn_remover_imagen").prop('disabled', valor);
    $("#imagen_nueva").prop('disabled', valor);

    if (valor) {
        $('.remove-image').removeClass('btn-danger').addClass('btn-secondary');
        $('.edit-image').removeClass('btn-info').addClass('btn-secondary');
    }
    else {
        $('.remove-image').removeClass('btn-secondary').addClass('btn-danger');
        $('.edit-image').removeClass('btn-secondary').addClass('btn-info');
    }

}

function cargarDatos() {
    cargarFamilas();
    cargarImpuestos();
}

function limpiarCamposSucursal() {
    guardarSucursal($("#select2-1").val());//Guardar la Sucursal con la que se Esta trabajando
    cancelar();
    cargarDatos();
}

function inicializar() {
    cancelar();
}

$('#select2-1').select2();
