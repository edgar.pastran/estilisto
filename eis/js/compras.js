var urlServerPage = ruta.concat("/servidor/php/compras.php");

$(function() {
    $('#select2-1').select2();

    $.fn.editable.defaults.mode = 'inline';

    usuarioSucursal(inicializarDatosSucursal);

    // Configurar la caja de texto de fecha
    $('#fecfac').
    // on('show', fix_xeditable_conflict).
    datepicker({
        // endDate : 'now',
        autoclose: true,
        container: '#fecfac-datepicker-container'
    });

    // Configurar combos de proveedor y concepto
    $('#select2-codpro').select2();
    $('#select2-idconcepto').select2();

    // Configurar modal de facturas
    $('#bootgrid-facturas').bootgrid({
        css: {
            icon: 'icon',
            iconColumns: 'ion-ios-list-outline',
            iconDown: 'ion-chevron-down',
            iconRefresh: 'ion-refresh',
            iconSearch: 'ion-search',
            iconUp: 'ion-chevron-up',
            dropDownMenuItems: 'dropdown-menu dropdown-menu-right'
        },
        rowCount: [-1/*, 5, 10, 25, 50, 100*/],
        selection: true,
        multiSelect: false,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    }).
    on("selected.rs.jquery.bootgrid", function(e, rows) {
        limpiarDatosCabecera();
        $("#operacion").val("actualizar");
        // Cargar los datos de la cabecera
        var factura = JSON.parse(rows[0].facproc_data);
        if (factura.idconcepto == null) {
            $("#tipo_factura").val("movimiento_caja");
        }
        $('#numfac').val(factura.numfacp);
        $('#ejefac').val(factura.ejefacp);
        $('#select2-idconcepto').val(factura.idconcepto).trigger('change');
        $("#fecfac").datepicker("update", factura.fecfacp_format);
        $('#select2-codpro').val(factura.codpro).trigger('change');
        $('#numalb').val(factura.numalb);
        $('#obsfac').val(factura.obsfac);
        // Cargar los datos de las lineas
        resetearLineas();
        $.each(factura.lineas_array, function(index, linea) {
            adicionarLineaFactura(linea);
        });
        // Habilitar los campos
        deshabilitarDatosCabecera($("#tipo_factura").val() == "movimiento_caja");
        // Cerrar el modal
        $("#modal_facturas .close").click();

    }).
    on("deselected.rs.jquery.bootgrid", function(e, rows) {
    });

    // Configurar modal de articulos
    $('#bootgrid-articulos').bootgrid({
        css: {
            icon: 'icon',
            iconColumns: 'ion-ios-list-outline',
            iconDown: 'ion-chevron-down',
            iconRefresh: 'ion-refresh',
            iconSearch: 'ion-search',
            iconUp: 'ion-chevron-up',
            dropDownMenuItems: 'dropdown-menu dropdown-menu-right'
        },
        rowCount: [-1/*, 5, 10, 25, 50, 100*/],
        selection: true,
        multiSelect: false,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    }).
    on("selected.rs.jquery.bootgrid", function(e, rows) {
        // var articulo = rows[0];
        var articulo = JSON.parse(rows[0].data);
        adicionarLineaFactura(articulo);
        $("#modal_articulos .close").click();
        var id_row = $('#factura_lineas > tbody > tr:last-child').attr("id")
        $('#'+id_row+' .coste').click();
    }).
    on("deselected.rs.jquery.bootgrid", function(e, rows) {
    });

    resetearVista();
});

function inicializarDatosSucursal() {
    guardarSucursal($("#select2-1").val());//Guardar la Sucursal con la que se esta trabajando
    cargarDatosSucursal();
    cargarEjercicios();
    cargarProveedores();
    cargarConceptos();
    cargarImpuestos();
    cargarArticulos();
}

var sucursal_data = [];
function cargarDatosSucursal() {
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        if (!sucursal_data.hasOwnProperty(codsuc)) {
            var params = {"sucursal": codsuc, "operacion": "sucursal_data"};
            $.ajax({
                type: "POST",
                url: urlServerPage,
                data: params,
                success: function(data) {
                    if (data.exito == true) {
                        sucursal_data[codsuc] = {
                            timezone: data.sucursal.timezone,
                            moneda: data.sucursal.moneda,
                            idconcepto: data.sucursal.idconcepto_compras,
                            proveedor: data.sucursal.proveedor_compras,
                            impuesto_incluido: data.sucursal.impuesto_incluido
                        };
                        resetearLineas();
                    }
                    else {
                        console.log(data);
                    }
                },
                error: function(data) {
                    console.log(JSON.stringify(data));
                }
            });
        }
        else {
            resetearLineas();
        }
    }
}

var ejercicios = {};
function cargarEjercicios() {
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        if (!ejercicios.hasOwnProperty(codsuc)) {
            var params = {"sucursal": codsuc, "operacion": "ejercicios"};
            $.ajax({
                type: "POST",
                url: urlServerPage,
                data: params,
                success: function(data) {
                    if (data.exito == true) {
                        //console.log(JSON.stringify(data));
                        ejercicios[codsuc] = [];
                        $('#ejefac').text(" ");
                        //$('#ejefac').append('<option value="">Seleccione...</option>');
                        for (i=0; i<data.ejercicios.length; i++) {
                            var ejercicio = {
                                value: data.ejercicios[i],
                                text: data.ejercicios[i]
                            }
                            ejercicios[codsuc].push(ejercicio);
                            $('#ejefac').append('<option value="' + ejercicio.value + '">' + ejercicio.text + '</option>');
                        }
                    }
                    else {
                        console.log(data);
                    }
                },
                error: function(data) {
                    console.log(JSON.stringify(data));
                }
            });
        }
        else {
            $('#ejefac').text(" ");
            //$('#ejefac').append('<option value="">Seleccione...</option>');
            $.each(ejercicios[codsuc], function(index, element) {
                $('#ejefac').append('<option value="' + element.value + '">' + element.text + '</option>');
            });
        }
    }
}

var proveedores = {};
function cargarProveedores() {
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        if (!proveedores.hasOwnProperty(codsuc)) {
            var params = {"sucursal": codsuc, "operacion": "proveedores"};
            $.ajax({
                type: "POST",
                url: urlServerPage,
                data: params,
                success: function(data) {
                    if (data.exito == true) {
                        //console.log(JSON.stringify(data));
                        proveedores[codsuc] = [];
                        $('#select2-codpro').text(" ");
                        //$('#select2-codpro').append('<option value="">Seleccione...</option>');
                        for (i=0; i<data.proveedores.length; i++) {
                            var proveedor = {
                                value: data.proveedores[i].codpro,
                                text: data.proveedores[i].razon
                            }
                            proveedores[codsuc].push(proveedor);
                            $('#select2-codpro').append('<option value="' + proveedor.value + '">' + proveedor.text + '</option>');
                        }
                        if (sucursal_data[codsuc] != undefined) {
                            $('#select2-codpro').val(sucursal_data[codsuc].proveedor).trigger('change');
                        }
                    }
                    else {
                        console.log(data);
                    }
                },
                error: function(data) {
                    console.log(JSON.stringify(data));
                }
            });
        }
        else {
            $('#select2-codpro').text(" ");
            //$('#select2-codpro').append('<option value="">Seleccione...</option>');
            $.each(proveedores[codsuc], function(index, element) {
                $('#select2-codpro').append('<option value="' + element.value + '">' + element.text + '</option>');
            });
            if (sucursal_data[codsuc] != undefined) {
                $('#select2-codpro').val(sucursal_data[codsuc].proveedor).trigger('change');
            }
        }
    }
}

var conceptos = {};
function cargarConceptos() {
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        if (!conceptos.hasOwnProperty(codsuc)) {
            var params = {"sucursal": codsuc, "operacion": "conceptos"};
            $.ajax({
                type: "POST",
                url: urlServerPage,
                data: params,
                success: function(data) {
                    if (data.exito == true) {
                        //console.log(JSON.stringify(data));
                        conceptos[codsuc] = [];
                        $('#select2-idconcepto').text(" ");
                        //$('#select2-idconcepto').append('<option value="">Seleccione...</option>');
                        for (i=0; i<data.conceptos.length; i++) {
                            var concepto = {
                                value: data.conceptos[i].idconcepto,
                                text: data.conceptos[i].desconcep
                            }
                            conceptos[codsuc].push(concepto);
                            $('#select2-idconcepto').append('<option value="' + concepto.value + '">' + concepto.text + '</option>');
                        }
                        if (sucursal_data[codsuc] != undefined) {
                            $('#select2-idconcepto').val(sucursal_data[codsuc].idconcepto).trigger('change');
                        }
                    }
                    else {
                        console.log(data);
                    }
                },
                error: function(data) {
                    console.log(JSON.stringify(data));
                }
            });
        }
        else {
            $('#select2-idconcepto').text(" ");
            //$('#select2-idconcepto').append('<option value="">Seleccione...</option>');
            $.each(conceptos[codsuc], function(index, element) {
                $('#select2-idconcepto').append('<option value="' + element.value + '">' + element.text + '</option>');
            });
            if (sucursal_data[codsuc] != undefined) {
                $('#select2-idconcepto').val(sucursal_data[codsuc].idconcepto).trigger('change');
            }
        }
    }
}

var impuestos = {};
function cargarImpuestos() {
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        if (!impuestos.hasOwnProperty(codsuc)) {
            var params = {"sucursal": codsuc, "operacion": "impuestos"};
            $.ajax({
                type: "POST",
                url: urlServerPage,
                data: params,
                success: function(data) {
                    if (data.exito == true) {
                        //console.log(JSON.stringify(data));
                        impuestos[codsuc] = [];
                        for (i=0; i<data.impuestos.length; i++) {
                            var impuesto = {
                                value: data.impuestos[i].value,
                                text: data.impuestos[i].text
                            }
                            impuestos[codsuc].push(impuesto);
                        }
                    }
                    else {
                        console.log(data);
                    }
                },
                error: function(data) {
                    console.log(JSON.stringify(data));
                }
            });
        }
    }
}

var articulos = {};
function cargarArticulos(callbackFunction) {
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        if (!articulos.hasOwnProperty(codsuc)) {
            var params = {"sucursal": codsuc, "operacion": "articulos"};
            $.ajax({
                type: "POST",
                url: urlServerPage,
                data: params,
                success: function(data) {
                    if (data.exito == true) {
                        //console.log(JSON.stringify(data));
                        articulos[codsuc] = [];
                        for (i=0; i<data.articulos.length; i++) {
                            articulos[codsuc].push(data.articulos[i]);
                        }
                        if (callbackFunction !== undefined) {
                            callbackFunction();
                        }
                    }
                    else {
                        console.log(data);
                    }
                },
                error: function(data) {
                    console.log(JSON.stringify(data));
                }
            });
        }
    }
}

$(document).on('change', '#select2-1', function(e) {
    e.preventDefault();
    inicializarDatosSucursal();
    resetearVista();
});

$(document).on('click', '#btn-facturas', function(e) {
    e.preventDefault();
    $('#meses').val('0');
    cargarFacturas();
});

$(document).on('change', '#meses', function(e) {
    e.preventDefault();
    cargarFacturas();
});

function cargarFacturas() {
    var codsuc = $('#select2-1').val();
    var ejefac = $('#ejefac').val();
    var meses = $('#meses').val();
    if (codsuc != "") {
        var params = {"sucursal": codsuc, "ejercicio": ejefac, "meses": meses, "operacion": "consulta"};
        $.ajax({
            type: "POST",
            url: urlServerPage,
            data: params,
            success: function(data) {
                if (data.exito == true) {
                    //console.log(JSON.stringify(data));
                    if (data.exito == true) {
                        $("#bootgrid-facturas").bootgrid("clear");
                        for (var i = 0; i < data.facturas.length; i++) {
                            $("#bootgrid-facturas").bootgrid().bootgrid("append", [{
                                "facproc_key": data.facturas[i]['ejefacp']+"_"+data.facturas[i]['serfacp']+"_"+data.facturas[i]['numfacp'],
                                "facproc_numfac": data.facturas[i]['numfacp'],
                                "facproc_ejefac": data.facturas[i]['ejefacp'],
                                "facproc_concepto": data.facturas[i]['desconcep'],
                                "facproc_fecfac": data.facturas[i]['fecfacp_format'],
                                "facproc_codpro": data.facturas[i]['razon'],
                                "facproc_totfacp": data.facturas[i]['totfacp_format'],
                                "facproc_numalb": data.facturas[i]['numalb'],
                                "facproc_data": JSON.stringify(data.facturas[i])
                            }]);
                        };
                        //Show Modal
                        $('#modal_facturas').modal('show');
                    };
                }
                else {
                    console.log(data);
                }
            },
            error: function(data) {
                console.log(JSON.stringify(data));
            }
        });
    }
}

$(document).on('click', '#btnAgregarLinea', function(e) {
    e.preventDefault();
    var codsuc = $('#select2-1').val();
    if (codsuc == "") {
        swal('Error', 'Escoja una sucursal primero', 'error');
    }
    else if (articulos.hasOwnProperty(codsuc)){
        mostrarModalArticulos();
    }
    else {
        cargarArticulos(mostrarModalArticulos);
    }
});

$(document).on('click', '#btnNuevaFactura', function(e) {
    limpiarDatosCabecera();
    $("#operacion").val("insertar");
    deshabilitarDatosCabecera(false);
    buscarNuevoNumeroFactura();
});

$(document).on('change', '#ejefac', function(e) {
    var ejefac = $('#ejefac').val();
    limpiarDatosCabecera();
    $('#ejefac').val(ejefac);
    $("#operacion").val("insertar");
    deshabilitarDatosCabecera(false);
    buscarNuevoNumeroFactura();
});

$(document).on('change', '#fecfac', function(e) {
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        if (sucursal_data.hasOwnProperty(codsuc)) {
            var timezone = sucursal_data[codsuc].timezone;
            var today = moment.tz(new Date(), timezone).format('DD/MM/YYYY');
            if ($("#fecfac").val() != "" && $("#fecfac").val() != today) {
                var year = moment($('#fecfac').val(), "DD/MM/YYYY").year();
                if (year != $('#ejefac').val()) {
                    $('#ejefac').val(year);
                    $("#operacion").val("insertar");
                    deshabilitarDatosCabecera(false);
                    buscarNuevoNumeroFactura(false);
                }
            }
        }
    }
});

function buscarNuevoNumeroFactura(updateDate) {
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        var params = {"sucursal": codsuc, "ejercicio": $('#ejefac').val(), "operacion": "proxima_factura"};
        //console.log("PARAMS: "+JSON.stringify(params));
        $.ajax({
            type: "POST",
            url: urlServerPage,
            data: params,
            success: function(data) {
                if (data.exito == true) {
                    $('#numfac').val(data.numero_factura);
                    $('#ejefac').val(data.ejercicio);
                    if (sucursal_data.hasOwnProperty(codsuc)) {
                        if (updateDate == undefined || updateDate == true) {
                            var timezone = sucursal_data[codsuc].timezone;
                            var fecha = moment.tz(new Date(), timezone).format('DD/MM/YYYY');
                            $("#fecfac").datepicker("update", fecha);
                        }
                        $('#select2-idconcepto').val(sucursal_data[codsuc].idconcepto).trigger('change');
                        $('#select2-codpro').val(sucursal_data[codsuc].proveedor).trigger('change');
                        resetearLineas();
                    }
                }
                else {
                    console.log('error');
                }
            },
            error: function(data) {
                console.log(JSON.stringify(data));
            }
        });
    }
    else{
        $("#select2-1").focus();
        swal('Seleccione Sucursal!');
    }
}

function mostrarModalArticulos() {
    var codsuc = $('#select2-1').val();
    if (codsuc != "" && articulos.hasOwnProperty(codsuc)) {
        $("#bootgrid-articulos").bootgrid("clear");
        for (var i = 0; i < articulos[codsuc].length; i++) {
            $("#bootgrid-articulos").bootgrid().bootgrid("append", [{
                "codart": articulos[codsuc][i]['codart'],
                "desart": articulos[codsuc][i]['desart'],
                "data": JSON.stringify(articulos[codsuc][i])
            }]);
        };
        //Show Modal
        $('#modal_articulos').modal('show');
    }
}

function resetearLineas() {
    $.each($('#factura_lineas > tbody > tr'), function(i, row) {
        $(row).remove();
    });
    calcularTotal();
}

function calcularTotal() {
    var codsuc = $('#select2-1').val();
    var total_base_imponible = 0;
    var total_impuesto = 0;
    $.each($('#factura_lineas > tbody > tr'), function(i, row) {
        var cantidad = parseInt($(this).find('.quantity > .canart').val());
        var base_imponible = parseFloat($(this).find('.impuesto > .basimpart').val()) * cantidad;
        var impuesto = parseFloat($(this)   .find('.impuesto > .monimpart').val()) * cantidad;
        total_base_imponible += base_imponible;
        total_impuesto += impuesto;
    });
    $('#base_imponible_format').html(sucursal_data[codsuc].moneda+" "+$.number( total_base_imponible, 2, ',', '.' ));
    $('#impuesto_format').html(sucursal_data[codsuc].moneda+" "+$.number( total_impuesto, 2, ',', '.' ));
    var total = total_base_imponible + total_impuesto;
    total = parseFloat($.number( total, 2, '.', '' ));
    $('#total').val(total);
    $('#total_format').html(sucursal_data[codsuc].moneda+" "+$.number( total, 2, ',', '.' ));
}

function limpiarDatosCabecera() {
    $("#operacion").val("");
    $("#tipo_factura").val("");
    $("#numfac").val("");
    $("#ejefac").val("");
    $("#fecfac").val("");
    $("#select2-codpro").val("").trigger('change');
    $("#numalb").val("");
    $("#select2-idconcepto").val("").trigger('change');
    $("#obsfac").val("");
}

function deshabilitarDatosCabecera(valor) {
    $("#numfac").prop('disabled', valor);
    $("#ejefac").prop('disabled', valor);
    $("#fecfac").prop('disabled', valor);
    $("#select2-codpro").prop('disabled', valor);
    $("#numalb").prop('disabled', valor);
    $("#select2-idconcepto").prop('disabled', valor);
    $("#btnAgregarLinea").prop('disabled', valor);
    $("#obsfac").prop('disabled', valor);
}

$(document).on('click', '#btnCancelarFactura', function(e) {
    e.preventDefault();
    swal({
        title: 'Estas seguro?',
        text: 'Si cancelas esta factura no se guardaran los datos que has ingresado!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'No, me arrepenti',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Si, cancelarlo',
        closeOnConfirm: false
    },
    function () {
        swal('Cancelado!', 'Tu factura ha sido cancelada.', 'success');
        resetearVista();
    });
});

function resetearVista(){
    limpiarDatosCabecera();
    deshabilitarDatosCabecera(true);
    inicializarDatosSucursal();
};

$(document).on('click', '#btnEliminarFactura', function(e) {
    var codsuc = $('#select2-1').val();
    var operacion = $('#operacion').val();
    var numfac = $('#numfac').val();
    if (codsuc != "" && operacion == "actualizar" && numfac != "") {
        if ($("#tipo_factura").val() == "movimiento_caja") {
            swal('Error', 'No puede eliminar esta factura porque esta relacionada a un movimiento de caja', 'error');
        }
        else {
            swal({
                title: 'Estas seguro?',
                text: 'Si eliminas esta factura no podras recuperar los datos que has ingresado!',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'No, me arrepenti',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Si, eliminarla',
                closeOnConfirm: false
            },
            function () {
                $('#operacion').val('eliminar');
                procesarFactura();
            });
        }
    }
});

function adicionarLineaFactura(articulo) {
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        //console.log(JSON.stringify(articulo));
        var numero_linea = 1;
        if ($('#factura_lineas > tbody > tr').length > 0) {
            var id_linea = $('#factura_lineas > tbody > tr:last-child').attr("id");
            numero_linea = parseInt(id_linea.split('-')[1])+1;
        }
        var id_row = 'factura_linea-'+numero_linea;
        var row = '';
        row += '<tr id="'+id_row+'">';
        row += '<td class="quantity text-center">' +
            '<a href="#" class="cantidad">'+$.number(articulo.canser, 0, ',', '.')+'</a>' +
            '<input type="hidden" class="canart" value="'+$.number(articulo.canser, 0, '.', '')+'"/>' +
            '</td>';
        row += '<td class="article">' +
            articulo.desart +
            '<input type="hidden" class="codart" value="' + articulo.codart + '"/>' +
            '<input type="hidden" class="desart" value="' + articulo.desart + '"/>' +
            '</td>';
        row += '<td class="price text-right">' +
            '<a href="#" class="coste">' + sucursal_data[codsuc].moneda+" "+$.number(articulo.preven, 2, ',', '.') + '</a>' +
            '<input type="hidden" class="preart" value="' + $.number(articulo.preven, 2, '.', '') + '"/>' +
            '<input type="hidden" class="subtotal" value="' + $.number(articulo.subtot, 2, '.', '') + '"/>' +
            '</td>';
        row += '<td class="descuento text-right d-none d-md-table-cell">'+
            '<a href="#" class="desart">' + $.number(articulo.descuento, 2, ',', '.') + " %" + '</a>' +
            '<input type="hidden" class="pordesart" value="' + $.number(articulo.descuento, 2, '.', '') + '"/>' +
            '<input type="hidden" class="mondesart" value="' + $.number(articulo.impdescuento, 2, '.', '') + '"/>' +
            '</td>';
        row += '<td class="subtotal_line text-right">'
            + sucursal_data[codsuc].moneda+" "+$.number(articulo.subtot, 2, ',', '.') +
            '</td>';
        row += '<td class="impuesto text-right d-none d-md-table-cell">'+
            '<a href="#" class="tax">' + $.number(articulo.iva, 0, ',', '.') + " %" + '</a>' +
            '<input type="hidden" class="porimpart" value="' + $.number(articulo.iva, 0, '.', '') + '"/>' +
            '<input type="hidden" class="monimpart" value="' + $.number(articulo.impiva, 2, '.', '') + '"/>' +
            '<input type="hidden" class="basimpart" value="' + $.number(articulo.impbas, 2, '.', '') + '"/>' +
            '</td>';
        row += '<td class="text-center">' +
            '<button class="btn btn-sm btn-primary btn-edit-line" type="button" title="Editar Linea"><em class="ion-edit"></em></button>&nbsp;' +
            '<button class="btn btn-sm btn-danger btn-delete-line" type="button" title="Eliminar Linea"><em class="ion-trash-b"></em></button>' +
            '</td>';
        row += '</tr>';
        if ($('#factura_lineas > tbody > tr').length > 0) {
            $('#factura_lineas > tbody:last-child').append(row);
        }
        else {
            $('#factura_lineas > tbody').html(row);
        }
        calcularTotal();

        if ($("#tipo_factura").val() != "movimiento_caja") {
            $('.cantidad').editable({
                type: 'text',
                title: 'Cantidad',
                value: $.number(articulo.canser, 0, ',', '.'),
                emptytext: 1,
                display: function (value, response) {
                    value = !isNaN(parseInt(value))?parseInt(value):1;
                    $(this).text($.number(value, 0, ',', '.'));
                },
                success: function (response, value) {
                    var cantidad = !isNaN(parseInt(value))?parseInt(value):1;
                    $(this).siblings('.canart').val(cantidad);
                    var precio = parseFloat($(this).parent().siblings('.price').find('.preart').val());
                    var descuento_porcentaje = parseFloat($(this).parent().siblings('.descuento').find('.pordesart').val());
                    descuento_porcentaje = (descuento_porcentaje>100)?100:descuento_porcentaje;
                    var descuento_monto = precio * (descuento_porcentaje / 100);
                    var precio_con_descuento = precio - descuento_monto;
                    var subtotal = cantidad * precio_con_descuento;
                    $(this).parent().siblings('.price').find('.subtotal').val(subtotal);
                    $(this).parent().siblings('.subtotal_line').html(sucursal_data[codsuc].moneda+" "+$.number(subtotal, 2, ',', '.'));
                    calcularTotal();
                }
            }).
            on('shown', function(ev, editable) {
                setTimeout(function() {
                    editable.input.$input.select();
                },0);
            });

            $('.coste').editable({
                type: 'text',
                title: 'Coste',
                value: $.number(articulo.preven, 2, '.', ''),
                display: function (value, response) {
                    $(this).text(sucursal_data[codsuc].moneda+" "+$.number(value, 2, ',', '.'));
                },
                success: function (response, value) {
                    var precio = !isNaN(parseFloat(value))?parseFloat(value):0;
                    var cantidad = parseInt($(this).parent().siblings('.quantity').find('.canart').val());
                    var descuento_porcentaje = parseFloat($(this).parent().siblings('.descuento').find('.pordesart').val());
                    descuento_porcentaje = (descuento_porcentaje>100)?100:descuento_porcentaje;
                    var descuento_monto = precio * (descuento_porcentaje / 100);
                    var precio_con_descuento = precio - descuento_monto;
                    var subtotal = cantidad * precio_con_descuento;
                    $(this).siblings('.preart').val(precio);
                    $(this).siblings('.subtotal').val(subtotal);
                    $(this).parent().siblings('.descuento').find('.mondesart').val(descuento_monto);
                    $(this).parent().siblings('.subtotal_line').html(sucursal_data[codsuc].moneda+" "+$.number(subtotal, 2, ',', '.'));
                    // Calculo de la base imponible y monto del impuesto del articulo
                    var impuesto_porcentaje = $(this).parent().siblings('.impuesto').find('.porimpart').val();
                    var base_imponible = precio_con_descuento;
                    var impuesto_monto = precio_con_descuento * (impuesto_porcentaje / 100);
                    if (sucursal_data[codsuc].impuesto_incluido == "X") {
                        base_imponible = precio_con_descuento / (1+(impuesto_porcentaje/100));
                        impuesto_monto = precio_con_descuento - base_imponible;
                    }
                    $(this).parent().siblings('.impuesto').find('.monimpart').val(impuesto_monto);
                    $(this).parent().siblings('.impuesto').find('.basimpart').val(base_imponible);
                    calcularTotal();
                }
            }).
            on('shown', function(ev, editable) {
                setTimeout(function() {
                    editable.input.$input.select();
                    // Si el valor del campo es cero
                    // se muestra vacia la caja de texto
                    if (parseFloat(editable.input.$input.val()) == 0) {
                        editable.input.$input.val("");
                        editable.input.$input.focus();
                    }
                },0);
            });

            $('.desart').editable({
                type: 'text',
                title: 'Descuento',
                value: $.number(articulo.descuento, 2, '.', ''),
                display: function (value, response) {
                    $(this).text($.number(value, 2, ',', '.')+" %");
                },
                success: function (response, value) {
                    var descuento_porcentaje = !isNaN(parseFloat(value))?parseFloat(value):0;
                    descuento_porcentaje = (descuento_porcentaje>100)?100:descuento_porcentaje;
                    var cantidad = parseInt($(this).parent().siblings('.quantity').find('.canart').val());
                    var precio = parseFloat($(this).parent().siblings('.price ').find('.preart').val());
                    var descuento_monto = precio * (descuento_porcentaje / 100);
                    var precio_con_descuento = precio - descuento_monto;
                    var subtotal = cantidad * precio_con_descuento;
                    $(this).siblings('.pordesart').val(descuento_porcentaje);
                    $(this).siblings('.mondesart').val(descuento_monto);
                    $(this).parent().siblings('.price').find('.subtotal').val(subtotal);
                    $(this).parent().siblings('.subtotal_line').html(sucursal_data[codsuc].moneda+" "+$.number(subtotal, 2, ',', '.'));
                    // Calculo de la base imponible y monto del impuesto del articulo
                    var impuesto_porcentaje = $(this).parent().siblings('.impuesto').find('.porimpart').val();
                    var base_imponible = precio_con_descuento;
                    var impuesto_monto = precio_con_descuento * (impuesto_porcentaje / 100);
                    if (sucursal_data[codsuc].impuesto_incluido == "X") {
                        base_imponible = precio_con_descuento / (1+(impuesto_porcentaje/100));
                        impuesto_monto = precio_con_descuento - base_imponible;
                    }
                    $(this).parent().siblings('.impuesto').find('.monimpart').val(impuesto_monto);
                    $(this).parent().siblings('.impuesto').find('.basimpart').val(base_imponible);
                    calcularTotal();
                }
            }).
            on('shown', function(ev, editable) {
                setTimeout(function() {
                    editable.input.$input.select();
                },0);
            });

            $('.tax').editable({
                type: 'select',
                title: 'Impuesto',
                value: $.number(articulo.iva, 0, '.', ''),
                source: impuestos[codsuc],
                display: function (value, response) {
                    $(this).text($.number(value, 2, ',', '.')+" %");
                },
                success: function (response, value) {
                    var impuesto_porcentaje = value;
                    var precio = parseFloat($(this).parent().siblings('.price ').find('.preart').val());
                    var descuento_porcentaje = parseFloat($(this).parent().siblings('.descuento').find('.pordesart').val());
                    var descuento_monto = precio * (descuento_porcentaje / 100);
                    var precio_con_descuento = precio - descuento_monto;
                    // Calculo de la base imponible y monto del impuesto del articulo
                    var base_imponible = precio_con_descuento;
                    var impuesto_monto = precio_con_descuento * (impuesto_porcentaje / 100);
                    if (sucursal_data[codsuc].impuesto_incluido == "X") {
                        base_imponible = precio_con_descuento / (1+(impuesto_porcentaje/100));
                        impuesto_monto = precio_con_descuento - base_imponible;
                    }
                    $(this).siblings('.porimpart').val(impuesto_porcentaje);
                    $(this).siblings('.monimpart').val(impuesto_monto);
                    $(this).siblings('.basimpart').val(base_imponible);
                    calcularTotal();
                }
            });

            $('.btn-delete-line').click(function () {
                $(this).closest('tr').remove();
                calcularTotal();
            });

            $('.btn-edit-line').click(function () {
                var id_linea = $(this).closest('tr').attr('id');
                $('#editar_linea_id').val(id_linea);
                $('#editar_linea_moneda').val(sucursal_data[codsuc].moneda);

                var descripcion = $('#'+id_linea).find('.article .desart').val();
                $('#editar_linea_descripcion').html(descripcion);

                var cantidad = $('#'+id_linea).find('.quantity .canart').val();
                $('#canart_editar').val(cantidad);
                $('#editar_linea_cantidad').editable({
                    type: 'text',
                    title: 'Cantidad',
                    value: cantidad,
                    display: function (value, response) {
                        value = !isNaN(parseInt(value))?parseInt(value):1;
                        $(this).text($.number(value, 0, ',', '.'));
                    },
                    success: function (response, value) {
                        var cantidad = !isNaN(parseInt(value))?parseInt(value):1;
                        var precio = parseFloat($('#preart_editar').val());
                        var descuento_porcentaje = parseFloat($('#pordesart_editar').val());
                        descuento_porcentaje = (descuento_porcentaje>100)?100:descuento_porcentaje;
                        var descuento_monto = precio * (descuento_porcentaje / 100);
                        var precio_con_descuento = precio - descuento_monto;
                        var subtotal = cantidad * precio_con_descuento;
                        $('#canart_editar').val(cantidad);
                        $('#subtotal_editar').val(subtotal);
                        $('#editar_linea_subtotal').html($('#editar_linea_moneda').val()+" "+$.number(subtotal, 2, ',', '.'));
                    }
                }).
                on('shown', function(ev, editable) {
                    setTimeout(function() {
                        editable.input.$input.select();
                    },0);
                });
                $('#editar_linea_cantidad').editable('option','value',cantidad);
                $('#editar_linea_cantidad').removeClass('editable-unsaved');

                var precio = $('#'+id_linea).find('.price .preart').val();
                $('#preart_editar').val(precio);
                $('#editar_linea_precio').editable({
                    type: 'text',
                    title: 'Coste',
                    value: precio,
                    display: function (value, response) {
                        $(this).text($('#editar_linea_moneda').val()+" "+$.number(value, 2, ',', '.'));
                    },
                    success: function (response, value) {
                        var precio = !isNaN(parseFloat(value))?parseFloat(value):0;
                        var cantidad = parseInt($('#canart_editar').val());
                        var descuento_porcentaje = parseFloat($('#pordesart_editar').val());
                        descuento_porcentaje = (descuento_porcentaje>100)?100:descuento_porcentaje;
                        var descuento_monto = precio * (descuento_porcentaje / 100);
                        var precio_con_descuento = precio - descuento_monto;
                        var subtotal = cantidad * precio_con_descuento;
                        $('#preart_editar').val(precio);
                        $('#subtotal_editar').val(subtotal);
                        $('#mondesart_editar').val(descuento_monto);
                        $('#editar_linea_subtotal').html($('#editar_linea_moneda').val()+" "+$.number(subtotal, 2, ',', '.'));
                        // Calculo de la base imponible y monto del impuesto del articulo
                        var impuesto_porcentaje = $('#porimpart_editar').val();
                        var base_imponible = precio_con_descuento;
                        var impuesto_monto = precio_con_descuento * (impuesto_porcentaje / 100);
                        if (sucursal_data[codsuc].impuesto_incluido == "X") {
                            base_imponible = precio_con_descuento / (1+(impuesto_porcentaje/100));
                            impuesto_monto = precio_con_descuento - base_imponible;
                        }
                        $('#monimpart_editar').val(impuesto_monto);
                        $('#basimpart_editar').val(base_imponible);
                    }
                }).
                on('shown', function(ev, editable) {
                    setTimeout(function() {
                        editable.input.$input.select();
                    },0);
                });
                $('#editar_linea_precio').editable('option','value',precio);
                $('#editar_linea_precio').removeClass('editable-unsaved');

                var descuento_porcentaje = $('#'+id_linea).find('.descuento .pordesart').val();
                $('#pordesart_editar').val(descuento_porcentaje);
                var descuento_monto = $('#'+id_linea).find('.descuento .mondesart').val();
                $('#mondesart_editar').val(descuento_monto);
                $('#editar_linea_descuento').editable({
                    type: 'text',
                    title: 'Descuento',
                    value: descuento_porcentaje,
                    display: function (value, response) {
                        $(this).text($.number(value, 2, ',', '.')+" %");
                    },
                    success: function (response, value) {
                        var descuento_porcentaje = !isNaN(parseFloat(value))?parseFloat(value):0;
                        descuento_porcentaje = (descuento_porcentaje>100)?100:descuento_porcentaje;
                        var cantidad = parseInt($('#canart_editar').val());
                        var precio = parseFloat($('#preart_editar').val());
                        var descuento_monto = precio * (descuento_porcentaje / 100);
                        var precio_con_descuento = precio - descuento_monto;
                        var subtotal = cantidad * precio_con_descuento;
                        $('#pordesart_editar').val(descuento_porcentaje);
                        $('#mondesart_editar').val(descuento_monto);
                        $('#subtotal_editar').val(subtotal);
                        $('#editar_linea_subtotal').html($('#editar_linea_moneda').val()+" "+$.number(subtotal, 2, ',', '.'));
                        // Calculo de la base imponible y monto del impuesto del articulo
                        var impuesto_porcentaje = $('#porimpart_editar').val();
                        var base_imponible = precio_con_descuento;
                        var impuesto_monto = precio_con_descuento * (impuesto_porcentaje / 100);
                        if (sucursal_data[codsuc].impuesto_incluido == "X") {
                            base_imponible = precio_con_descuento / (1+(impuesto_porcentaje/100));
                            impuesto_monto = precio_con_descuento - base_imponible;
                        }
                        $('#monimpart_editar').val(impuesto_monto);
                        $('#basimpart_editar').val(base_imponible);
                    }
                }).
                on('shown', function(ev, editable) {
                    setTimeout(function() {
                        editable.input.$input.select();
                    },0);
                });
                $('#editar_linea_descuento').editable('option','value',descuento_porcentaje);
                $('#editar_linea_descuento').removeClass('editable-unsaved');

                var impuesto = $('#'+id_linea).find('.impuesto .porimpart').val();
                $('#porimpart_editar').val(impuesto);
                $('#editar_linea_impuesto').editable({
                    type: 'select',
                    title: 'Impuesto',
                    value: impuesto,
                    source: impuestos[codsuc],
                    display: function (value, response) {
                        $(this).text($.number(value, 2, ',', '.')+" %");
                    },
                    success: function (response, value) {
                        var impuesto_porcentaje = value;
                        var precio = parseFloat($('#preart_editar').val());
                        var descuento_porcentaje = parseFloat($('#pordesart_editar').val());
                        var descuento_monto = precio * (descuento_porcentaje / 100);
                        var precio_con_descuento = precio - descuento_monto;
                        // Calculo de la base imponible y monto del impuesto del articulo
                        var base_imponible = precio_con_descuento;
                        var impuesto_monto = precio_con_descuento * (impuesto_porcentaje / 100);
                        if (sucursal_data[codsuc].impuesto_incluido == "X") {
                            base_imponible = precio_con_descuento / (1+(impuesto_porcentaje/100));
                            impuesto_monto = precio_con_descuento - base_imponible;
                        }
                        $('#porimpart_editar').val(impuesto_porcentaje);
                        $('#monimpart_editar').val(impuesto_monto);
                        $('#basimpart_editar').val(base_imponible);
                    }
                });
                $('#editar_linea_impuesto').editable('option','source',impuestos[codsuc]);
                $('#editar_linea_impuesto').editable('option','value',impuesto);
                $('#editar_linea_impuesto').removeClass('editable-unsaved');

                var subtotal = $('#'+id_linea).find('.price .subtotal').val();
                $('#subtotal_editar').val(subtotal);
                $('#editar_linea_subtotal').html($('#editar_linea_moneda').val()+" "+$.number(subtotal, 2, ',', '.'));

                var impuesto_porcentaje = $('#'+id_linea).find('.impuesto .porimpart').val();
                $('#porimpart_editar').val(impuesto_porcentaje);
                var impuesto_monto = $('#'+id_linea).find('.impuesto .monimpart').val();
                $('#monimpart_editar').val(impuesto_monto);
                var base_imponible = $('#'+id_linea).find('.impuesto .basimpart').val();
                $('#basimpart_editar').val(base_imponible);

                $('#modal_factura_linea').modal('show');
            });
        }
    }
}

$(document).on('click', '#btnGuardarFacturaLinea', function(e) {
    e.preventDefault();
    var id_linea = $('#editar_linea_id').val();
    var cantidad = $('#canart_editar').val();
    var precio = $('#preart_editar').val();
    var descuento_porcentaje = $('#pordesart_editar').val();
    var descuento_monto = $('#mondesart_editar').val();
    var impuesto_porcentaje = $('#porimpart_editar').val();
    var impuesto_monto = $('#monimpart_editar').val();
    var base_imponible = $('#basimpart_editar').val();
    var subtotal = $('#subtotal_editar').val();

    $('#'+id_linea).find('.quantity .canart').val(cantidad);
    $('#'+id_linea+' .cantidad').editable('option','value',cantidad);
    $('#'+id_linea).find('.price .preart').val(precio);
    $('#'+id_linea+' .coste').editable('option','value',precio);
    $('#'+id_linea).find('.descuento .pordesart').val(descuento_porcentaje);
    $('#'+id_linea+' .desart').editable('option','value',descuento_porcentaje);
    $('#'+id_linea).find('.descuento .mondesart').val(descuento_monto);
    $('#'+id_linea).find('.price .subtotal').val(subtotal);
    $('#'+id_linea+' .subtotal_line').html($('#editar_linea_moneda').val()+" "+$.number( subtotal, 2, ',', '.' ));
    $('#'+id_linea).find('.impuesto .porimpart').val(impuesto_porcentaje);
    $('#'+id_linea).find('.impuesto .monimpart').val(impuesto_monto);
    $('#'+id_linea).find('.impuesto .basimpart').val(base_imponible);
    $('#'+id_linea+' .tax').editable('option','value',impuesto_porcentaje);
    $('#modal_factura_linea .close').click();
    calcularTotal();
});

$(document).on('click', '#btnGuardarFactura', function(e) {
    e.preventDefault();
    var codsuc = $('#select2-1').val();
    var operacion = $('#operacion').val();
    var numfac = $('#numfac').val();
    if (codsuc != "" && operacion != "" && numfac != "") {
        var lineas = $('#factura_lineas > tbody > tr').length;
        if ($("#tipo_factura").val() == "movimiento_caja") {
            swal('Error', 'No puede modificar esta factura porque esta relacionada a un movimiento de caja', 'error');
        }
        else if (lineas <= 0) {
            swal('Error', 'Debe incluir articulos a la factura', 'error');
        }
        else {
            procesarFactura();
        }
    }
});

function procesarFactura() {
    var operacion = $('#operacion').val();
    var codsuc = $('#select2-1').val();
    var factura = {};
    factura.codsuc = codsuc;
    factura.numfacp = $('#numfac').val();
    factura.ejefacp = $('#ejefac').val();
    factura.idconcepto = $('#select2-idconcepto').val();
    factura.fecfacp = $('#fecfac').val();
    factura.numalb = $('#numalb').val();
    factura.codpro = $('#select2-codpro').val();
    factura.razpro = $('#select2-codpro :selected').text();
    factura.obsfac = $('#obsfac').val();
    factura.totimpbas = 0;
    factura.totimpdto = 0;
    factura.totimpiva = 0;
    factura.totfacp = parseFloat($('#total').val());
    factura.lineas_array = [];
    $.each($('#factura_lineas > tbody > tr'), function(i, row) {
        var linea = {};
        linea.codart = $(this).find('.article .codart').val();
        linea.desart = $(this).find('.article .desart').val();
        linea.preven = parseFloat($(this).find('.price .preart').val());
        linea.canser = parseInt($(this).find('.quantity .canart').val());
        linea.subtot = parseFloat($(this).find('.price .subtotal').val());
        linea.descuento = parseFloat($(this).find('.descuento .pordesart').val());
        linea.impdescuento = parseFloat($(this).find('.descuento .mondesart').val()) * linea.canser;
        linea.impbas = parseFloat($(this).find('.impuesto .basimpart').val()) * linea.canser;
        linea.iva = parseFloat($(this).find('.impuesto .porimpart').val());
        linea.impiva = parseFloat($(this).find('.impuesto .monimpart').val()) * linea.canser;
        factura.totimpbas += linea.impbas;
        factura.totimpdto += linea.impdescuento;
        factura.totimpiva += linea.impiva;
        factura.lineas_array.push(linea);
    });
    factura.usumod = (session != undefined)?session:"";
    if (operacion == "insertar") {
        factura.usucre = (session != undefined)?session:"";
    }
    var params = {"sucursal": codsuc, "operacion": operacion, "factura": JSON.stringify(factura)};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(data) {
            if (data.exito == true) {
                if (operacion == "actualizar") {
                    swal('Factura Actualizada!', '', 'success');
                }
                if (operacion == "insertar") {
                    swal('Factura Creada!', '', 'success');
                }
                if (operacion == "eliminar") {
                    swal('Factura Eliminada!', '', 'success');
                }
                resetearVista();
            }
            else {
                console.log(JSON.stringify(data));
            }
        },
        error: function(data) {
            console.log(JSON.stringify(data));
            $('#btnCobrarConfirmado').removeClass("btn-secondary");
        }
    });
}
