var web = window.location.pathname;
var posicion = web.lastIndexOf('/')+1;
web = web.substring(posicion);

/*---------- Manejo de la Variable de Session ----------*/
var session = window.localStorage.usuario;
var nombre = window.localStorage.nombre;
var apellido = window.localStorage.apellido;
var foto = window.localStorage.foto;
var compania = window.localStorage.compania;
var sucursal = window.localStorage.sucursal;
var administrador = window.localStorage.administrador;
var pantallas = window.localStorage.pantallas;

if (sucursal == null) { sucursal="******";}
if (pantallas == null) { pantallas="";}
if (nombre == null) { nombre="";}
if (apellido == null) { apellido="";}
if (foto == null) { foto="";}
if (compania == null) { compania="";}
if (session == null) { session="";}

if (web == "pageapp-login.html") {
	if (session == "") {//No existe Session Permanecer en la pagina de Login
	}else{
	    document.location.href="index.html";
	};
//}else if (web == "index.html" || web == "contacto.html"){ 
	//No hacer nada permanecer en la Pagina
}else if (session == "") {
    //No existe Session Enviar en la pagina de Login
    document.location.href="pageapp-login.html";
}else if (pantallas.indexOf(web) < 0){
    document.location.href="index.html";
};

var webanterior = window.localStorage.webanterior;
var abierto = sessionStorage.getItem('abierto');

/*---------- Funciones ----------*/
function guardarSession(usuario, nombre, apellido, compania, administrador, foto) { //Guardar Datos de Usuario
    window.localStorage.setItem('nombre',nombre);
    window.localStorage.setItem('usuario',usuario);
    window.localStorage.setItem('apellido',apellido);
    window.localStorage.setItem('compania',compania);
    window.localStorage.setItem('administrador',administrador);
    window.localStorage.setItem('foto', foto);
};

function cerrarSession() { //Borrar la Session o locales con JavaScript
    window.localStorage.clear();
    document.location.href="index.html";
};

function borrarDatos() { //Borrar la Session o datos locales con JavaScript
    sessionStorage.clear('datos');
    sessionStorage.clear('moneda');
    sessionStorage.setItem('abierto', abierto);
};

function guardarDatos(datos, moneda) { //Borrar la Session o datos locales con JavaScript
    sessionStorage.setItem('datos', datos);
    sessionStorage.setItem('moneda', moneda);
};

function obtenerData() { //Borrar la Session o datos locales con JavaScript
    var data = JSON.parse(sessionStorage.getItem('datos'));
    return data;
};

function obtenerMoneda() { //Borrar la Session o datos locales con JavaScript
    var moneda = sessionStorage.getItem('moneda');
    return moneda;
};

function guardarSucursal(sucursal) { //Borrar la Session o locales con JavaScript
    window.localStorage.setItem('sucursal',sucursal);
};

/*---------- Guardar Pantallas de Acceso ----------*/
function guardarPantallas(pantallas) { //Guardar Datos de Usuario
  window.localStorage.setItem('pantallas',pantallas);
};

function obtenerPantallas(pantallas) { //Borrar la Session o datos locales con JavaScript
    pantallas = localStorage.getItem('pantallas');
};