var sucursales = new Array();
var grupos = new Array();

$(function() {
    inicializar();
});

function inicializar() {
    var urlServerPageAux = ruta.concat("/servidor/php/consul_suc_usuario.php");
    $.post(urlServerPageAux, {"session": session}, function (data) {
        if (data.exito == true) {
            $('#select2-1').text(" ");
            for (var i = 0; i < data.nume_regis; i++) {
                if (sucursal == data.sucursales_usuario[i]['codsuc']) {
                    $('#select2-1').append('<option value="' + data.sucursales_usuario[i]['codsuc'] + '"selected>' + data.sucursales_usuario[i]['codsuc'] + ' ' + data.sucursales_usuario[i]['razemp'] + '</option>');
                }
                else {
                    $('#select2-1').append('<option value="' + data.sucursales_usuario[i]['codsuc'] + '">' + data.sucursales_usuario[i]['codsuc'] + ' ' + data.sucursales_usuario[i]['razemp'] + '</option>');
                }
                sucursales[i] = new Array();
                sucursales[i]['codsuc'] = data.sucursales_usuario[i]['codsuc'];
                sucursales[i]['razemp'] = data.sucursales_usuario[i]['razemp'];
                sucursales[i]['moneda'] = data.sucursales_usuario[i]['moneda'];
            }
        }
        else {
            //alert('error');
        }
    }); //Fin .post

    var urlServerPageAux = ruta.concat("/servidor/php/consul_sincronizador.php");
    $.post(urlServerPageAux, {"session": session}, function (data) {
        if (data.exito == true) {
            $('#grupo').text(" ");
            for (var i = 0; i < data.nume_regis; i++) {
                $('#grupo').append('<option value="' + data.grupos[i]['grupo'] + '">' + data.grupos[i]['nombre'] + '</option>');
            }
        }
        else {
            //alert('error');
        }
    }); //Fin .post

    $('#reporte').hide('fast');
    $('#btnNuevoReporte').hide('fast');
}

$("#btnReporte").click(function (event){
    event.preventDefault();
    $datos = $("#formulario-reporte").serialize();

    swal({
            title: 'Esta Seguro?',
            text: 'Se Borraran los datos de Dicha Area!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Si, Borrar!',
            closeOnConfirm: false
        },
        function() {
            var urlServerPageAux = ruta.concat("/servidor/php/sincronizador_borrar_datos.php");
            $("#opc_session").val(session);
            $.post(urlServerPageAux, $("#formulario-reporte").serialize(), function (data) {
                if ((data.exito == true) && (data.nume_regis >= 1)){
                    //$('#formulario').hide('fast');
                    //$('#reporte').show('fast');
                    //$('#btnReporte').hide('fast');
                    //$('#btnNuevoReporte').show('fast');
                    var iSuc = 0;
                    for (var i = 0; i < sucursales.length; i++) {
                        if (data.sucursal == sucursales[i]['codsuc']) {
                            iSuc = i;
                        }
                    }
                    guardarSucursal(document.getElementById("select2-1").value);
                    if (data.nume_regis >= 1) {
                        $('#TituloTable1').text(data.sucursal + " " + sucursales[iSuc]['razemp'] + " -- Moneda: " + sucursales[iSuc]['moneda']);
                    }
                    swal('Borrado!', 'Los Datos han sido Borrados.', 'success');
                }
                else{//No existen Registros
                    toastr.options.positionClass = 'toast-bottom-right';
                    toastr['warning']('No existen Datos');
                }
            }); //Fin .post
        });
});

$("#btnNuevoReporte").click(function (event){
    $('#formulario').show('fast');
    $('#btnReporte').show('fast');
    $('#btnNuevoReporte').hide('fast');

    //$('#tabla1').text(" ");
    $('#reporte').hide('fast');
});