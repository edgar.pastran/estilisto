var urlServerPage = ruta.concat("/servidor/php/formapago.php");

$(function() {
    usuarioSucursal(inicializar);
    cargarCatalogo();
});

function cargarCatalogo() {
    $('#bootgrid-formapago').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        selection: true,
        multiSelect: false,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on("selected.rs.jquery.bootgrid", function(e, rows) {
        $("#codfp").val(rows[0].fp_codfp);
        $("#des").val(rows[0].fp_des);
        $("#serie").val(rows[0].fp_serie);
        if (rows[0].fp_foto != "") {
            $("#foto").attr("src", rows[0].fp_foto);
        }
        $("#vender").val(rows[0].fp_vender);
        setCheckbox("vender","1");

        $("#opc_formapago").val("actualizar");

        $("#closeMfp").click();

        //Quitar ReadOnly
        $("#codfp").attr("readonly" , false);
        $("#des").removeAttr("readonly");
        $("#serie").removeAttr("readonly");
        $("#vender").removeAttr("disabled");
        $('.remove-image').removeClass('btn-secondary').addClass('btn-danger');
        $("#btn_remover_imagen").attr("disabled",false);
        $('.edit-image').removeClass('btn-secondary').addClass('btn-info');
        $("#imagen_nueva").attr("disabled", false);

    })
    .on("deselected.rs.jquery.bootgrid", function(e, rows) {

    });
}

$("#btn-formapago").click(function (event){
    event.preventDefault();
    $("#suc_formapago").val($("#select2-1").val());
    $("#opc_formapago").val("consulta");

    $.post(urlServerPage, $("#formulario-formapago").serialize(), function (data){
        if ( data.exito == true && data.nume_regis >= 1 ){
            if (data.nume_regis >= 1) {
                $("#bootgrid-formapago").bootgrid("clear");
                for (var i = 0; i < data.nume_regis; i++) {
                    $("#bootgrid-formapago").bootgrid().bootgrid("append", [{
                        "fp_codfp": ""+data.formapagos[i]['codfp']+"",
                        "fp_des": ""+data.formapagos[i]['des']+"",
                        "fp_serie": ""+data.formapagos[i]['serie']+"",
                        "fp_vender": ""+data.formapagos[i]['vender']+"",
                        "fp_foto": ""+data.formapagos[i]['foto']+""
                    }]);
                };
                //Show Modal
                $('#modal_formapago').modal('show');
            };//No hay Datos
        };
    }); //Fin .post
}); //Fin .btn

$("#btn_remover_imagen").click(function (event) {
    if ($("#codfp").val() != "" && $("#opc_formapago").val() != "insertar") {
        $("#opc_formapago").val("remover_imagen");
        guardar();
    }
    else {
        $("#foto").attr("src", ruta.concat("/servidor/images/nothing.png"));
    }
});

function eliminar() {
    if ($("#codfp").val() != "" && $("#opc_formapago").val() != "insertar") {
        $("#opc_formapago").val("eliminar");
        guardar();
    }
}

function guardar(){
    $("#suc_formapago").val($("#select2-1").val());
    if ($("#select2-1").val() != "" && $("#opc_formapago").val() != "" && $("#codfp").val() != "") {
        $.ajax({
            type: "POST",
            url: urlServerPage,
            data: new FormData($("#formulario-formapago")[0]),
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.exito == true) {
                    if ($("#opc_formapago").val() == "actualizar") {
                        swal('Forma de Pago Actualizada!', '', 'success');
                    }
                    else if ($("#opc_formapago").val() == "insertar") {
                        swal('Forma de Pago Creada!', '', 'success');
                    }
                    else if ($("#opc_formapago").val() == "eliminar") {
                        swal('Forma de Pago Eliminada!', '', 'success');
                    }
                    else if ($("#opc_formapago").val() == "remover_imagen") {
                        swal('Imagen Eliminada!', '', 'success');
                    }

                    cancelar();
                }
                else {//alert('error');
                    swal(data.mensaje, '', 'error');
                    console.log("SIN EXITO: " + JSON.stringify(data));
                }
                ;
            },
            error: function (respuesta) {
                swal(JSON.stringify(respuesta), '', 'error');
                console.log("ERROR: " + JSON.stringify(respuesta));
            }
        });
    };
} //Fin de Funcion

function consultar_formapago(){
    $("#suc_formapago").val($("#select2-1").val());
    var opc = $("#opc_formapago").val();
    if (opc == "insertar") {
        $("#opc_formapago").val("consultar");
        if ($("#select2-1").val() != "" && $("#opc_formapago").val() != "" && $("#codfp").val() != "") {
            $.post(urlServerPage, $("#formulario-formapago").serialize(), function (data) {
                if (data.exito == true){
                    swal('Forma de Pago YA Existe!', '', 'error');
                    $("#codfp").focus();
                }else{//alert('error');
                    $("#codfp").attr("readonly" , false);
                };
            }); //Fin .post
        };
    } //Fin del IF
    $("#opc_formapago").val(opc);
} //Fin de Funcion

function cancelar(){
    guardarSucursal($("#select2-1").val());//Guardar la Sucursal con la que se Esta trabajando
    $("#opc_formapago").val("");
    $("#codfp").val("");
    $("#des").val("");
    $("#serie").val("");
    $("#vender").val("");
    setCheckbox("vender","1");
    $("#foto").attr("src", ruta.concat("/servidor/images/nothing.png"));
    $("#btn_remover_imagen").click();
    $("#btn_remover_imagen_nueva").click();

    $("#codfp").attr("readonly", true);
    $("#des").attr("readonly",true);
    $("#serie").attr("readonly", true);
    $("#vender").attr("disabled", true);

    $('.remove-image').removeClass('btn-danger').addClass('btn-secondary');
    $("#btn_remover_imagen").attr("disabled", true);
    $('.edit-image').removeClass('btn-info').addClass('btn-secondary');
    $("#imagen_nueva").attr("disabled", true);
} //Fin de Funcion

function nuevo(){
    if ($("#select2-1").val() != "") {
        $("#opc_formapago").val("insertar");
        $("#codfp").val("");
        $("#des").val("");
        $("#serie").val("");
        $("#vender").val("");
        setCheckbox("vender","1");

        $("#codfp").removeAttr("readonly");
        $("#codfp").focus();
        $("#des").removeAttr("readonly");
        $("#serie").removeAttr("readonly");
        $("#vender").removeAttr("disabled");

        $('.remove-image').removeClass('btn-secondary').addClass('btn-danger');
        $("#btn_remover_imagen").attr("disabled", false);
        $('.edit-image').removeClass('btn-secondary').addClass('btn-info');
        $("#imagen_nueva").attr("disabled", false);
    }
    else{
        $("#select2-1").focus();
        swal('Seleccione Sucursal!');
    }
} //Fin de Funcion

function inicializar(){
    $("#suc_formapago").val("");
    $("#opc_formapago").val("");
    $("#codfp").val("");
    $("#des").val("");
    $("#serie").val("");
    $("#vender").val("");
    setCheckbox("vender","1");
    $("#foto").attr("src", ruta.concat("/servidor/images/nothing.png"));

    $('.remove-image').removeClass('btn-danger').addClass('btn-secondary');
    $("#btn_remover_imagen").attr("disabled", true);
    $('.edit-image').removeClass('btn-info').addClass('btn-secondary');
    $("#imagen_nueva").attr("disabled", true);
}

$('#select2-1').select2();
