var urlServerPage = ruta.concat("/servidor/php/cajas.php");

$(function() {
    usuarioSucursal(inicializar);
    cargarCatalogo();
    $('#select2-1').select2();
});

function cargarCatalogo() {
    $('#bootgrid-cajas').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        selection: true,
        multiSelect: false,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on("selected.rs.jquery.bootgrid", function(e, rows) {
        var caja = JSON.parse(rows[0].fp_data);
        $("#codcaj").val(caja.codcaj);
        $("#nomcaj").val(caja.nomcaj);
        //$("#fondo").val(rows[0].fp_fondo);
        $(".caja_forma_pago > .fondo").val("0");
        for (i=0; i<caja.formas_pago.length; i++) {
            var codfp = caja.formas_pago[i]['codfp'];
            var fondo = caja.formas_pago[i]['fondo'];
            $(".caja_forma_pago."+codfp+" > .fondo").val(fondo);
        }
        $("#tipo").val(caja.tipo);
        if (caja.traautfoncie != null && caja.traautfoncie == "1") {
            $('#traautfoncie').prop('checked', true);
            if (caja.codcajdescie != null) {
                $('#codcajdescie').val(caja.codcajdescie).trigger('change');
                $("#codcajdescie").removeAttr("readonly");
                $("#codcajdescie").removeAttr("disabled");
            }
        }
        else {
            $('#traautfoncie').prop('checked', false);
            $("#codcajdescie").val("");
            $("#codcajdescie").attr("readonly", true);
            $("#codcajdescie").attr("disabled", true);
        }

        $("#opcion").val("actualizar");

        $("#closeModal").click();

        //Quitar ReadOnly
        //$("#codcaj").attr("readonly" , false);
        $("#nomcaj").removeAttr("readonly");
        //$("#fondo").removeAttr("readonly");
        $(".caja_forma_pago > .fondo").removeAttr("readonly");
        $("#tipo").removeAttr("disabled");
        $("#tipo").removeAttr("readonly");
        $("#traautfoncie").removeAttr("disabled");
    })
    .on("nomcajelected.rs.jquery.bootgrid", function(e, rows) {

    });
}

function eliminar() {
    if ($("#codcaj").val() != "" && $("#opcion").val() != "insertar") {
        $("#opcion").val("eliminar");
        guardar();
    }
}

function guardar(){
    $("#suc_caja").val($("#select2-1").val());
    if ($("#select2-1").val() != "" && $("#opcion").val() != "") { //&& $("#codcaj").val() != ""
        formas_pago = [];
        $('.caja_forma_pago').each(function() {
            var forma_pago = {
                "codfp": $(this).find('.codfp').val(),
                "fondo": ($(this).find('.fondo').val()!="")?$(this).find('.fondo').val():'0'
            }
            formas_pago.push(forma_pago);
        });
        var dataToSend = {
            "opcion": $("#opcion").val(),
            "sucursal": $("#select2-1").val(),
            "codcaj": $('#codcaj').val(),
            "tipo": $('#tipo').val(),
            "nomcaj": $('#nomcaj').val(),
            "traautfoncie": $("#traautfoncie").is(":checked")?1:0,
            "codcajdescie": ($("#codcajdescie").val()!="")?$("#codcajdescie").val():null,
            "formas_pago": JSON.stringify(formas_pago)
        }
        console.log(JSON.stringify(dataToSend));
        $.ajax({
            type: "POST",
            url: urlServerPage,
            data: dataToSend,
            success: function (data) {
                if (data.exito == true) {
                    if ($("#opcion").val() == "actualizar") {
                        swal('Caja Actualizada!', '', 'success');
                    }
                    else if ($("#opcion").val() == "insertar") {
                        swal('Caja Creada!', '', 'success');
                    }
                    else if ($("#opcion").val() == "eliminar") {
                        swal('Caja Eliminada!', '', 'success');
                    }
                    cancelar();
                }
                else {//alert('error');
                    swal(data.mensaje, '', 'error');
                    console.log("SIN EXITO: " + JSON.stringify(data));
                }
                ;
            },
            error: function (respuesta) {
                swal(JSON.stringify(respuesta), '', 'error');
                console.log("ERROR: " + JSON.stringify(respuesta));
            }
        });
    };
} //Fin de Funcion

function consultar_caja(){
    $("#suc_caja").val($("#select2-1").val());
    var opc = $("#opcion").val();
    if (opc == "insertar") {
        $("#opcion").val("consultar");
        if ($("#select2-1").val() != "" && $("#opcion").val() != "" && $("#codcaj").val() != "") {
            $.post(urlServerPage, $("#formulario-01").serialize(), function (data) {
                if (data.exito == true){
                    swal('Caja YA Existe!', '', 'error');
                    $("#codcaj").focus();
                }else{//alert('error');
                    $("#codcaj").attr("readonly" , false);
                };
            }); //Fin .post
        };
    } //Fin del IF
    $("#opcion").val(opc);
} //Fin de Funcion

function cancelar(){
    guardarSucursal($("#select2-1").val());//Guardar la Sucursal con la que se Esta trabajando
    $("#opcion").val("");
    $("#codcaj").val("");
    $("#nomcaj").val("");
    //$("#fondo").val("");
    $(".caja_forma_pago > .fondo").val("");
    $("#tipo").val("");
    $("#traautfoncie").prop("checked", false);
    $("#codcajdescie").val("");

    $("#codcaj").attr("readonly", true);
    $("#nomcaj").attr("readonly",true);
    //$("#fondo").attr("readonly", true);
    $(".caja_forma_pago > .fondo").attr("readonly", true);
    $("#tipo").attr("readonly", true);
    $("#tipo").attr("disabled", true);
    $("#traautfoncie").attr("disabled", true);
    $("#codcajdescie").attr("readonly", true);
    $("#codcajdescie").attr("disabled", true);
} //Fin de Funcion

function nuevo(){
    if ($("#select2-1").val() != "") {
        $("#opcion").val("insertar");
        $("#codcaj").val("");
        $("#nomcaj").val("");
        //$("#fondo").val("");
        $(".caja_forma_pago > .fondo").val("");
        $("#tipo").val("");
        $('#traautfoncie').prop('checked', false);
        $("#codcajdescie").val("");


        //$("#codcaj").removeAttr("readonly");
        $("#nomcaj").focus();
        $("#nomcaj").removeAttr("readonly");
        //$("#fondo").removeAttr("readonly");
        $(".caja_forma_pago > .fondo").removeAttr("readonly");
        $("#tipo").removeAttr("disabled");
        $("#tipo").removeAttr("readonly");
        $("#traautfoncie").removeAttr("disabled");
        $("#codcajdescie").attr("readonly", true);
        $("#codcajdescie").attr("disabled", true);
    }
    else{
        $("#select2-1").focus();
        swal('Seleccione Sucursal!');
    }
} //Fin de Funcion

function inicializar(){
    $("#suc_caja").val("");
    $("#opcion").val("");
    $("#codcaj").val("");
    $("#nomcaj").val("");
    $("#fondo").val("");
    $("#tipo").val("");
    consultarDatosPorSucursal();
}

function consultarDatosPorSucursal() {
    var dataToSend = {
        "opcion": "consultar_datos",
        "sucursal": $("#select2-1").val()
    };
    // console.log("DATA TO SEND: "+JSON.stringify(dataToSend));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: dataToSend,
        success: function (data) {
            if (data.exito == true) {
                cargarFormasPago(data.formas_pago);
                cargarCajas(data.cajas);
            }
            else {
                console.log("SIN EXITO: " + JSON.stringify(data));
            }
        },
        error: function (respuesta) {
            console.log("ERROR: " + JSON.stringify(respuesta));
        }
    });
}

function cargarFormasPago(formas_pago) {
    $('#fila_codforpag').text(" ");
    for (var j = 0; j < formas_pago.length; j++) {
        var row =
            '<div class="form-group row">' +
                '<label class="col col-md-2 col-lg-2">Fondo de ' + formas_pago[j].des + '</label>' +
                '<div class="col col-md-2 col-lg-2 caja_forma_pago '+formas_pago[j].codfp+'">' +
                    '<input type="hidden" name="codfp" class="codfp" value="' + formas_pago[j].codfp + '">' +
                    '<input readonly="true" name="fondo" class="col form-control fondo" type="number">' +
                '</div>' +
            '</div>'
        $('#fila_codforpag').append(row);
    }
}

function cargarCajas(cajas) {
    $('#codcajdescie').text(" ");
    $('#codcajdescie').append('<option value=""selected></option>');
    for (var i = 0; i < cajas.length; i++) {
        $('#codcajdescie').append('<option value="' + cajas[i]['codcaj'] + '">' + cajas[i]['nomcaj'] + '</option>');
    }
    // $('#codcajdescie').select2();
    $('#codcajdescie').attr('disabled', true);
}

$("#btn-caja").click(function (event){

    event.preventDefault();
    $("#suc_caja").val($("#select2-1").val());

    $("#opcion").val("consulta");
    $.post(urlServerPage, $("#formulario-01").serialize(), function (data){
        if ( data.exito == true && data.nume_regis >= 1 ){
            if (data.nume_regis >= 1) {
                $("#bootgrid-cajas").bootgrid("clear");
                for (var i = 0; i < data.nume_regis; i++) {
                    $("#bootgrid-cajas").bootgrid().bootgrid("append", [{
                        "fp_codcaj": ""+data.cajas[i]['codcaj']+"",
                        "fp_nomcaj": ""+data.cajas[i]['nomcaj']+"",
                        //"fp_fondo": ""+data.cajas[i]['fondo']+"",
                        "fp_tipo": ""+data.cajas[i]['tipo_string']+"",
                        "fp_data": JSON.stringify(data.cajas[i])
                    }]);
                };
                //Show Modal
                $('#modal_cajas').modal('show');
            };//No hay Datos
        };
    }); //Fin .post

    $('#select2-1').select2();
}); //Fin .btn

$("#traautfoncie").click(function(event) {
    $('#codcajdescie option').show();
    if ($("#traautfoncie").is(":checked")) {
        $('#codcajdescie option[value=""]').hide();
        if ($('#codcaj').val() != "") {
            $('#codcajdescie option[value="'+$('#codcaj').val()+'"]').hide();
        }
        $("#codcajdescie").removeAttr("readonly");
        $('#codcajdescie').removeAttr('disabled');
    }
    else {
        // $('#codcajdescie option[value=""]').show();
        // if ($('#codcaj').val() != "") {
        //     $('#codcajdescie option[value="'+$('#codcaj').val()+'"]').show();
        // }
        $("#codcajdescie").val("").trigger('change');
        $("#codcajdescie").attr("readonly", true);
        $('#codcajdescie').attr('disabled', true);
    }
});

$("#select2-1").change(function (event) {
    consultarDatosPorSucursal()
});