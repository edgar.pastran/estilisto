var urlServerPage = ruta.concat("/servidor/php/comision_empleados.php");

$(function() {
    $.fn.editable.defaults.mode = 'inline';

    usuarioSucursal(inicializarDatosSucursal);

    // Configurar modal de empleados
    $('#bootgrid-empleados-comision').bootgrid({
        css: {
            icon: 'icon',
            iconColumns: 'ion-ios-list-outline',
            iconDown: 'ion-chevron-down',
            iconRefresh: 'ion-refresh',
            iconSearch: 'ion-search',
            iconUp: 'ion-chevron-up',
            dropDownMenuItems: 'dropdown-menu dropdown-menu-right'
        },
        rowCount: [/*-1, */5, 10, 25, 50, 100],
        selection: true,
        multiSelect: false,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    }).
    on("selected.rs.jquery.bootgrid", function(e, rows) {
        var codsuc = $('#select2-1').val();
        limpiarDatosCabecera();
        // Cargar los datos de la cabecera
        var empleado = JSON.parse(rows[0].emp_data);
        $('#empleado_codigo').val(empleado.codemp);
        $('#empleado_nombre').val(empleado.nomemp+((empleado.ape1emp != null && empleado.ape1emp.length)>0?" "+empleado.ape1emp:""));
        var comision = (empleado.comision != null)?empleado.comision:0;
        $('#empleado_comision').html($.number(comision, 2, ',', '.') + " %" );
        $('#comision_general').val(comision);
        $('#comision_familias').val(JSON.stringify(empleado.comision_familias));
        $('#comision_articulos').val(JSON.stringify(empleado.comision_articulos));
        // Cargar los datos de las familias
        resetearTablaFamilias();
        $.each(familias[codsuc], function(index, familia) {
            adicionarFamilia(familia);
        });
        // Habilitar los campos
        deshabilitarDatosCabecera();

        // Cerrar el modal
        $("#modal_empleados .close").click();

    }).
    on("deselected.rs.jquery.bootgrid", function(e, rows) {
    });

})

$(document).on('change', '#select2-1', function(e) {
    e.preventDefault();
    inicializarDatosSucursal();
});

function inicializarDatosSucursal() {
    cargarFamilias();
    resetearVista();
}

var familias = {};
function cargarFamilias() {
    var codsuc = $('#select2-1').val();
    if (codsuc != "" && !familias.hasOwnProperty(codsuc)) {
        var params = {"sucursal": codsuc, "operacion": "familias"};
        $.ajax({
            type: "POST",
            url: urlServerPage,
            data: params,
            success: function(data) {
                if (data.exito == true) {
                    //console.log(JSON.stringify(data));
                    if (data.exito == true) {
                        familias[codsuc] = data.familias;
                    };
                }
                else {
                    console.log(data);
                }
            },
            error: function(data) {
                console.log(JSON.stringify(data));
            }
        });
    }
}

$(document).on('click', '#btnBuscarEmpleado', function(e) {
    e.preventDefault();
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        var params = {"sucursal": codsuc, "operacion": "empleados"};
        $.ajax({
            type: "POST",
            url: urlServerPage,
            data: params,
            success: function(data) {
                if (data.exito == true) {
                    //console.log(JSON.stringify(data));
                    if (data.exito == true) {
                        $("#bootgrid-empleados-comision").bootgrid("clear");
                        for (var i = 0; i < data.empleados.length; i++) {
                            var empleado = data.empleados[i];
                            $("#bootgrid-empleados-comision").bootgrid().bootgrid("append", [{
                                "emp_codemp": empleado['codemp'],
                                "emp_nomemp": empleado['nomemp'],
                                "emp_ape1emp": empleado['ape1emp'],
                                "emp_dniemp": empleado['dniemp'],
                                "emp_data": JSON.stringify(empleado)
                            }]);
                        };
                        //Show Modal
                        $('#modal_empleados').modal('show');
                    };
                }
                else {
                    console.log(data);
                }
            },
            error: function(data) {
                console.log(JSON.stringify(data));
            }
        });
    }
});

function limpiarDatosCabecera() {
    $("#empleado_codigo").val("");
    $("#empleado_nombre").val("");
    $("#empleado_comision").html("");
    $("#comision_general").val("");
    $("#comision_familias").val("");
    $("#comision_articulos").val("");
}

function deshabilitarDatosCabecera() {
    var comision = $('#comision_general').val();
    if (comision != "") {
        $("#empleado_comision").editable({
            type: 'text',
            title: 'Comision',
            value: $.number(comision, 2, '.', ''),
            display: function (value, response) {
                $(this).text($.number(parseFloat($('#comision_general').val()), 2, ',', '.')+" %");
            },
            success: function (response, value) {
                var comision = !isNaN(parseFloat(value))?parseFloat(value):0;
                comision = (comision>100)?100:comision;
                comision = (comision<0)?0:comision;
                $('#comision_general').val(comision);
            }
        }).
        on('shown', function(ev, editable) {
            setTimeout(function() {
                editable.input.$input.val($('#comision_general').val());
                editable.input.$input.select();
            },0);
        });
    }
}

function resetearTablaFamilias() {
    $.each($('#comision_familia > tbody > tr'), function(i, row) {
        $(row).remove();
    });
    resetearTablaArticulos();
}

function resetearTablaArticulos() {
    $.each($('#comision_articulo > tbody > tr'), function(i, row) {
        $(row).remove();
    });
}

function resetearVista(){
    limpiarDatosCabecera();
    deshabilitarDatosCabecera();
    resetearTablaFamilias();
}

$(document).on('click', '#btnCancelar', function(e) {
    e.preventDefault();
    swal({
        title: 'Estas seguro?',
        text: 'Si cancelas esta operacion no se guardaran los datos que has ingresado!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'No, me arrepenti',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Si, cancelarlo',
        closeOnConfirm: false
    },
    function () {
        swal('Cancelado!', 'Tus datos has sido cancelados.', 'success');
        resetearVista();
    });
});

$(document).on('click', '.familia', function(e) {
    $('#comision_familia > tbody > tr').removeClass('fila-resaltada');
    $(this).closest('tr').addClass('fila-resaltada');
    var codsuc = $('#select2-1').val();
    var codfam = $(this).find('.codfam').val();
    resetearTablaArticulos();
    $.each(familias[codsuc][codfam]['articulos'], function(index, articulo) {
        adicionarArticulo(articulo);
    });
});

function adicionarFamilia(familia) {
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        //console.log(JSON.stringify(familia));
        var comision = 0;
        var comision_familias = JSON.parse($('#comision_familias').val());
        if (comision_familias.hasOwnProperty(familia.codfam1)) {
            comision = comision_familias[familia.codfam1];
        }
        var id_row = 'familia-'+familia.codfam1;
        var row = '';
        row += '<tr id="'+id_row+'">';
        row += '<td class="familia cursor-puntero">' +
            familia.desfam1 +
            '<input type="hidden" class="codfam" value="' + familia.codfam1 + '"/>' +
            '</td>';
        row += '<td class="comision text-right">'+
            '<a href="#" class="comfam">' + $.number(comision, 2, ',', '.') + " %" + '</a>' +
            '<input type="hidden" class="comfam1" value="' + $.number(comision, 2, '.', '') + '"/>' +
            '</td>';
        row += '</tr>';
        if ($('#comision_familia > tbody > tr').length > 0) {
            $('#comision_familia > tbody:last-child').append(row);
        }
        else {
            $('#comision_familia > tbody').html(row);
        }

        $('.comfam').editable({
            type: 'text',
            title: 'Comision',
            value: $.number(comision, 2, '.', ''),
            display: function (value, response) {
                value = $(this).siblings('.comfam1').val();
                $(this).text($.number(value, 2, ',', '.')+" %");
            },
            success: function (response, value) {
                var comision = !isNaN(parseFloat(value))?parseFloat(value):0;
                comision = (comision>100)?100:comision;
                comision = (comision<0)?0:comision;
                $(this).siblings('.comfam1').val(comision);
            }
        }).
        on('shown', function(ev, editable) {
            setTimeout(function() {
                var value = editable.input.$input.val();
                var comision = !isNaN(parseFloat(value))?parseFloat(value):0;
                comision = (comision>100)?100:comision;
                comision = (comision<0)?0:comision;
                editable.input.$input.val(comision);
                editable.input.$input.select();
            },0);
        });
    }
}

function adicionarArticulo(articulo) {
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        //console.log(JSON.stringify(articulo));
        var comision = 0;
        var comision_articulos = JSON.parse($('#comision_articulos').val());
        if (comision_articulos.hasOwnProperty(articulo.codart)) {
            comision = comision_articulos[articulo.codart];
        }
        var id_row = 'articulo-'+articulo.codart;
        var row = '';
        row += '<tr id="'+id_row+'">';
        row += '<td class="articulo">' +
            articulo.desart +
            '<input type="hidden" class="codart" value="' + articulo.codart + '"/>' +
            '</td>';
        row += '<td class="comision text-right">'+
            '<a href="#" class="comart">' + $.number(comision, 2, ',', '.') + " %" + '</a>' +
            '<input type="hidden" class="comarticulo" value="' + $.number(comision, 2, '.', '') + '"/>' +
            '</td>';
        row += '</tr>';
        if ($('#comision_articulo > tbody > tr').length > 0) {
            $('#comision_articulo > tbody:last-child').append(row);
        }
        else {
            $('#comision_articulo > tbody').html(row);
        }

        $('.comart').editable({
            type: 'text',
            title: 'Comision',
            value: $.number(comision, 2, '.', ''),
            display: function (value, response) {
                value = $(this).siblings('.comarticulo').val();
                $(this).text($.number(value, 2, ',', '.')+" %");
            },
            success: function (response, value) {
                var comision = !isNaN(parseFloat(value))?parseFloat(value):0;
                comision = (comision>100)?100:comision;
                comision = (comision<0)?0:comision;
                $(this).siblings('.comarticulo').val(comision);
            }
        }).
        on('shown', function(ev, editable) {
            setTimeout(function() {
                var value = editable.input.$input.val();
                var comision = !isNaN(parseFloat(value))?parseFloat(value):0;
                comision = (comision>100)?100:comision;
                comision = (comision<0)?0:comision;
                editable.input.$input.val(comision);
                editable.input.$input.select();
            },0);
        });
    }
}

$(document).on('click', '#btnGuardar', function(e) {
    e.preventDefault();
    var codsuc = $('#select2-1').val();
    var codemp = $('#empleado_codigo').val();
    if (codsuc != "" && codemp != "") {
        guardarComisiones();
    }
});

function guardarComisiones() {
    var codsuc = $('#select2-1').val();
    var comisiones = {};
    comisiones.codsuc = codsuc;
    comisiones.codemp = $('#empleado_codigo').val();
    comisiones.comision = parseFloat($('#comision_general').val());
    comisiones.familias = [];
    $.each($('#comision_familia > tbody > tr'), function(i, row) {
        var comision = {};
        comision.codfam1 = $(this).find('.familia .codfam').val();
        comision.comision = parseFloat($(this).find('.comision .comfam1').val());
        comisiones.familias.push(comision);
    });
    comisiones.articulos = [];
    $.each($('#comision_articulo > tbody > tr'), function(i, row) {
        var comision = {};
        comision.codart = $(this).find('.articulo .codart').val();
        comision.comision = parseFloat($(this).find('.comision .comarticulo').val());
        comisiones.articulos.push(comision);
    });
    var params = {"sucursal": codsuc, "operacion": "guardar_comisiones", "comisiones": JSON.stringify(comisiones)};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(data) {
            // console.log(JSON.stringify(data));
            if (data.exito == true) {
                var comision_articulos = JSON.parse($('#comision_articulos').val());
                for (i=0; i<comisiones.articulos.length; i++) {
                    var comision = comisiones.articulos[i];
                    comision_articulos[comision.codart] = comision.comision;
                }
                $('#comision_articulos').val(JSON.stringify(comision_articulos));
                swal('Comisiones Actualizadas!', '', 'success');
            }
            else {
                console.log(JSON.stringify(data));
            }
        },
        error: function(data) {
            console.log(JSON.stringify(data));
        }
    });
}

$('#select2-1').select2();