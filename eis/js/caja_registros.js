var urlServerPage = ruta.concat("/servidor/php/caja_registros.php");

$(function() {
    $(formValidation);
    usuarioSucursal(inicializar);
    $('#select2-1').select2();
});

function isCodEmpRequired() {
    var mandatory = false;
    if ($('#codmov').val() != "") {
        var json = $("#codmov option:selected").attr('data-json');
        var movimiento = JSON.parse(json);
        mandatory = (movimiento.solemp != null && movimiento.solemp == 1);
    }
    return mandatory;
}

function isCodProRequired() {
    var mandatory = false;
    if ($('#codmov').val() != "") {
        var json = $("#codmov option:selected").attr('data-json');
        var movimiento = JSON.parse(json);
        mandatory = (movimiento.solpro != null && movimiento.solpro == 1);
    }
    return mandatory;
}

function isCodArtRequired() {
    var mandatory = false;
    if ($('#codmov').val() != "") {
        var json = $("#codmov option:selected").attr('data-json');
        var movimiento = JSON.parse(json);
        mandatory = (movimiento.solart != null && movimiento.solart == 1);
    }
    return mandatory;
}

function isCodCajRequired() {
    var mandatory = false;
    if ($('#codmov').val() != "") {
        var json = $("#codmov option:selected").attr('data-json');
        var movimiento = JSON.parse(json);
        mandatory = (movimiento.solcaj != null && movimiento.solcaj == 1);
    }
    return mandatory;
}

function formValidation() {
    if (!$.fn.validate) return;
    $('#formulario-caja-registro').validate({
        errorPlacement: errorPlacementInput,
        errorClass: "my-error-class",
        // Form rules
        rules: {
            codmov: {
                required: true
            },
            codforpag: {
                required: true
            },
            codemp: {
                required: function() {
                    return isCodEmpRequired();
                }
            },
            codpro: {
                required: function() {
                    return isCodProRequired();
                }
            },
            codart: {
                required: function() {
                    return isCodArtRequired();
                }
            },
            codcajdes: {
                required: function() {
                    return isCodCajRequired();
                }
            },
            impmov: {
                required: true,
                digits: true
            }
        }
    });
}

var sucursal_data = [];
function inicializar() {
    guardarSucursal($("#select2-1").val());//Guardar la Sucursal con la que se esta trabajando
    for (var a=0; a<sucursales.length; a++) {
        sucursal_data[sucursales[a]['codsuc']] = {
            moneda: sucursales[a]['moneda']
        };
    }
    cargarData();
}

var cajas = [];
var formas_pago = [];
var empleados = [];
var proveedores = [];
var articulos = [];
function cargarData() {
    var dataToSend = {
        "opcion": "inicializar",
        "sucursal": $("#select2-1").val()
    };
    // console.log("DATA TO SEND: "+JSON.stringify(dataToSend));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: dataToSend,
        success: function (data) {
            if (data.exito == true) {
                // console.log("CON EXITO: " + JSON.stringify(data.cajas));
                cajas = data.cajas;
                formas_pago = data.formas_pago;
                empleados = data.empleados;
                proveedores = data.proveedores;
                articulos = data.articulos;
                mostrarCajas();
            }
            else {
                console.log("SIN EXITO: " + JSON.stringify(data));
            }
        },
        error: function (respuesta) {
            console.log("ERROR: " + JSON.stringify(respuesta));
        }
    });
}

function mostrarCajas() {
    // Cajas
    $('#pestanas').text(" ");
    $('#pestanas_contenido').text(" ");
    for (var i = 0; i < cajas.length; i++) {
        var caja = cajas[i];
        mostrarCaja(caja, i);
    }
}

function mostrarCaja(caja, index) {
    // Crear la pestana
    var pestana =
        '<li class="nav-item">' +
            '<a class="nav-link '+(index==0?'active':'')+'" id="'+caja['codcaj']+'-tab" data-toggle="tab" data-index="'+index+'" href="#'+caja['codcaj']+'" role="tab" aria-controls="home" aria-selected="true">'+caja['nomcaj']+'</a>' +
        '</li>';
    $('#pestanas').append(pestana);

    // Agregar contenido a la pestana
    var contenido =
        '<div class="tab-pane pt-2 fade '+(index==0?'show active':'')+'" id="'+caja['codcaj']+'" role="tabpanel" aria-labelledby="'+caja['codcaj']+'-tab">' +
            '<div class="row dashbord-resumen">' +
                '<div class="col-lg-4">' +
                    '<div class="cardbox text-white bg-gradient-success b0">' +
                        '<div class="cardbox-body p-0">' +
                            '<div class="row">' +
                                '<div class="col-1 ml-0 dashbord-icono">' +
                                    '<i class="ion-plus-round"></i>' +
                                '</div>' +
                                '<div class="col-10 text-right">' +
                                    '<div id="'+caja['codcaj']+'-caja-cobros-format" class="text-bold dashbord-data">' +
                                        sucursal_data[$('#select2-1').val()]['moneda']+' '+$.number(0, 2, ',', '.' ) +
                                    '</div>' +
                                    '<p class="text-bold">Ingresos</p>' +
                                    '<input type="hidden" id="'+caja['codcaj']+'-caja-cobros" value="0">' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="col-lg-4">' +
                    '<div class="cardbox text-white bg-gradient-danger b0">' +
                        '<div class="cardbox-body p-0">' +
                            '<div class="row">' +
                                '<div class="col-1 ml-0 dashbord-icono">' +
                                    '<i class="ion-minus-round"></i>' +
                                '</div>' +
                                '<div class="col-10 text-right">' +
                                    '<div id="'+caja['codcaj']+'-caja-resto-format" class="text-bold dashbord-data">' +
                                        sucursal_data[$('#select2-1').val()]['moneda']+' '+$.number(0, 2, ',', '.' ) +
                                    '</div>' +
                                    '<p class="text-bold">Egresos</p>' +
                                    '<input type="hidden" id="'+caja['codcaj']+'-caja-resto" value="0">' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="col-lg-4">' +
                    '<div class="cardbox text-white bg-gradient-info b0">' +
                        '<div class="cardbox-body p-0">' +
                            '<div class="row">' +
                                '<div class="col-1 ml-0 dashbord-icono">' +
                                    '<i class="ion-navicon-round"></i>' +
                                '</div>' +
                                '<div class="col-10 text-right">' +
                                    '<div id="'+caja['codcaj']+'-caja-total-format" class="text-bold dashbord-data">' +
                                        sucursal_data[$('#select2-1').val()]['moneda']+' '+$.number(0, 2, ',', '.' ) +
                                    '</div>' +
                                    '<p class="text-bold">Saldo Actual</p>' +
                                    '<input type="hidden" id="'+caja['codcaj']+'-caja-total" value="0">' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="cardbox-heading text-bold">Movimientos de caja desde el dia ' +
                caja['fecultcie'] + ' a las ' + caja['horultcie'] +
            '</div>' +
            '<div class="table-responsive">' +
                '<table id="'+caja['codcaj']+'-caja-movimientos" class="table table-bordered table-sm">' +
                    '<thead class="thead-inverse">' +
                        '<tr>' +
                            '<th class="text-center">Fecha</th>' +
                            '<th class="text-center">Clase</th>' +
                            '<th class="text-center d-none d-md-table-cell">Tipo</th>' +
                            '<th class="text-center">Forma Pago</th>' +
                            '<th class="text-left d-none d-md-table-cell">Observacion</th>' +
                            '<th class="text-right">Importe</th>' +
                        '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    '</tbody>' +
                '</table>' +
            '</div>' +
        '</div>';
    $('#pestanas_contenido').append(contenido);

    // Agregar movimientos registrados previamente
    for (var m=0; m<caja['movimientos'].length; m++) {
        var movimiento = caja['movimientos'][m];
        agregarMovimiento(movimiento);
    }
}

function cargarCombos() {
    // Formas de Pago
    $('#codforpag').text(" ");
    $('#codforpag').append('<option value="" selected></option>');
    for (var i = 0; i < formas_pago.length; i++) {
        $('#codforpag').append('<option value="' + formas_pago[i]['codfp'] + '">'+formas_pago[i]['des']+'</option>');
    }
    $('#codforpag').select2();
    // Empleados
    $('#codemp').text(" ");
    $('#codemp').append('<option value="" selected></option>');
    for (var i = 0; i < empleados.length; i++) {
        $('#codemp').append('<option value="' + empleados[i]['codemp'] + '">'+empleados[i]['nomemp']+((empleados[i]['ape1emp']!=null)?' '+empleados[i]['ape1emp']:'')+'</option>');
    }
    $('#codemp').select2();
    // Proveedores
    $('#codpro').text(" ");
    $('#codpro').append('<option value="" selected></option>');
    for (var i = 0; i < proveedores.length; i++) {
        $('#codpro').append('<option value="' + proveedores[i]['codpro'] + '">'+proveedores[i]['razon']+'</option>');
    }
    $('#codpro').select2();
    // Articulos
    $('#codart').text(" ");
    $('#codart').append('<option value=""selected></option>');
    for (var i = 0; i < articulos.length; i++) {
        $('#codart').append('<option value="' + articulos[i]['codart'] + '">'+articulos[i]['desart']+'</option>');
    }
    $('#codart').select2();
    // Cajas
    $('#codcajdes').text(" ");
    $('#codcajdes').append('<option value=""selected></option>');
    for (var i = 0; i < cajas.length; i++) {
        if (cajas[i]['codcaj'] != $('#codcaj').val()) {
            $('#codcajdes').append('<option value="' + cajas[i]['codcaj'] + '">' + cajas[i]['nomcaj'] + '</option>');
        }
    }
    $('#codcajdes').select2();
    // Importe
    $('#impmov').val('');
    // Observacion
    $('#obsmov').val('');

    $('span.select2.select2-container.select2-container--default').css("width", "100%");

    $('#modal_fila_codemp').hide();
    $('#modal_fila_codpro').hide();
    $('#modal_fila_codart').hide();
    $('#modal_fila_codcajdes').hide();
}

function agregarMovimiento(movimiento) {
    var row =
        '<tr class="'+movimiento['codforpag']+'">' +
        '<td class="text-center">'+movimiento['fecmov_string']+' a las '+movimiento['hormov']+'</td>' +
        '<td class="text-center">'+movimiento['ingegr_string']+'</td>' +
        '<td class="text-center d-none d-md-table-cell">'+movimiento['tipo_string']+'</td>' +
        '<td class="text-center">'+movimiento['forpag_string']+'</td>' +
        '<td class="text-left d-none d-md-table-cell">'+((movimiento['obsmov']!=null)?movimiento['obsmov']:"")+'</td>' +
        '<td class="text-right text-bold '+(movimiento['ingegr']=="I"?'text-success':'text-danger')+'">'+
            '<input type="hidden" class="impmov" value="'+movimiento['impmov']+'">' +
            /*sucursal_data[$('#select2-1').val()]['moneda']+' '+*/movimiento['impmov_string']+
        '</td>' +
        '</tr>';
    if ($('#'+movimiento['codcaj']+'-caja-movimientos > tbody > tr').length > 0) {
        $('#'+movimiento['codcaj']+'-caja-movimientos > tbody:last-child').append(row);
    }
    else {
        $('#'+movimiento['codcaj']+'-caja-movimientos > tbody').html(row);
    }

    var cobros = parseFloat($('#'+movimiento['codcaj']+'-caja-cobros').val());
    var resto = parseFloat($('#'+movimiento['codcaj']+'-caja-resto').val());
    if (movimiento['ingegr'] == "I") {
        cobros += parseFloat(movimiento['impmov']);
    }
    else if (movimiento['ingegr'] == "E") {
        resto += parseFloat(movimiento['impmov']) * -1;
    }
    $('#'+movimiento['codcaj']+'-caja-cobros').val(cobros);
    $('#'+movimiento['codcaj']+'-caja-cobros-format').text(sucursal_data[$('#select2-1').val()]['moneda']+' '+$.number(cobros, 2, ',', '.' ));
    $('#'+movimiento['codcaj']+'-caja-resto').val(resto);
    $('#'+movimiento['codcaj']+'-caja-resto-format').text(sucursal_data[$('#select2-1').val()]['moneda']+' '+$.number(resto, 2, ',', '.' ));
    var total = cobros - resto;
    $('#'+movimiento['codcaj']+'-caja-total').val(total);
    $('#'+movimiento['codcaj']+'-caja-total-format').text(sucursal_data[$('#select2-1').val()]['moneda']+' '+$.number(total, 2, ',', '.' ));
}

$("#select2-1").change(function (event) {
    inicializar();
});

$('#btnIngresar').click(function(e) {
    e.preventDefault();
    var pestana_activa = $('#pestanas > li > a.active');;
    if (pestana_activa.length > 0) {
        var index = pestana_activa.attr('data-index');
        $('#codcaj').val(cajas[index].codcaj);
        $('#ingegr').val("I");
        // Titulo
        $('.modal-title').text('Ingreso de Caja');
        // Movimientos
        $('#codmov').text(" ");
        $('#codmov').append('<option value=""selected></option>');
        for (var i = 0; i < cajas[index].ingresos.length; i++) {
            $('#codmov').append('<option value="' + cajas[index].ingresos[i]['codmov'] + '" data-json=\''+JSON.stringify(cajas[index].ingresos[i])+'\'>' + cajas[index].ingresos[i]['desmov'] + '</option>');
        }
        $('#codmov').select2();
        // Combos
        cargarCombos();
        $('#modal_caja_registro').modal('show');
    }
});

$('#btnSacar').click(function(e) {
    e.preventDefault();
    var pestana_activa = $('#pestanas > li > a.active');;
    if (pestana_activa.length > 0) {
        var index = pestana_activa.attr('data-index');
        $('#codcaj').val(cajas[index].codcaj);
        $('#ingegr').val("E");
        // Titulo
        $('.modal-title').text('Retiro de Caja');
        // Movimientos
        $('#codmov').text(" ");
        $('#codmov').append('<option value=""selected></option>');
        for (var i = 0; i < cajas[index].egresos.length; i++) {
            $('#codmov').append('<option value="' + cajas[index].egresos[i]['codmov'] + '" data-json=\''+JSON.stringify(cajas[index].egresos[i])+'\'>' + cajas[index].egresos[i]['desmov'] + '</option>');
        }
        $('#codmov').select2();
        // Combos
        cargarCombos();

        $('#modal_caja_registro').modal('show');
    }
});

$("#codmov").change(function (event) {
    $('#modal_fila_codemp').hide();
    $('#modal_fila_codpro').hide();
    $('#modal_fila_codart').hide();
    $('#modal_fila_codcajdes').hide();
    $("#codforpag").val("").trigger('change');
    $("#codforpag").removeAttr("readonly");
    $('#codforpag').removeAttr('disabled');
    $("#codemp").val("").trigger('change');
    $("#codemp").removeAttr("readonly");
    $('#codemp').removeAttr('disabled');
    $("#codpro").val("").trigger('change');
    $("#codpro").removeAttr("readonly");
    $('#codpro').removeAttr('disabled');
    $("#codart").val("").trigger('change');
    $("#codart").removeAttr("readonly");
    $('#codart').removeAttr('disabled');
    $("#codcajdes").val("").trigger('change');
    $("#codcajdes").removeAttr("readonly");
    $('#codcajdes').removeAttr('disabled');
    if ($('#codmov').val() != "") {
        var json = $("#codmov option:selected").attr('data-json');

        var movimiento = JSON.parse(json);
        // Tipo
        $('#tipo').val(movimiento.tipo);
        // Forma de Pago
        if (movimiento.codforpag != null) {
            $("#codforpag").val(movimiento.codforpag).trigger('change');
            $("#codforpag").attr("readonly", true);
            $('#codforpag').attr('disabled', true);
        }
        // Empleado
        if (movimiento.solemp != null && movimiento.solemp == 1) {
            $('#modal_fila_codemp').show();
            if (movimiento.codemp != null) {
                $("#codemp").val(movimiento.codemp).trigger('change');
                $("#codemp").attr("readonly", true);
                $('#codemp').attr('disabled', true);
            }
        }
        // Proveedor
        if (movimiento.solpro != null && movimiento.solpro == 1) {
            $('#modal_fila_codpro').show();
            if (movimiento.codpro != null) {
                $("#codpro").val(movimiento.codpro).trigger('change');
                $("#codpro").attr("readonly", true);
                $('#codpro').attr('disabled', true);
            }
        }
        // Articulo
        if (movimiento.solart != null && movimiento.solart == 1) {
            $('#modal_fila_codart').show();
            if (movimiento.codart != null) {
                $("#codart").val(movimiento.codart).trigger('change');
                $("#codart").attr("readonly", true);
                $('#codart').attr('disabled', true);
            }
        }
        // Caja
        if (movimiento.solcaj != null && movimiento.solcaj == 1) {
            $('#modal_fila_codcajdes').show();
            if (movimiento.codcaj != null) {
                $("#codcajdes").val(movimiento.codcaj).trigger('change');
                $("#codcajdes").attr("readonly", true);
                $('#codcajdes').attr('disabled', true);
            }
        }
    }
});

$('#btnGuardar').click(function(e) {
    e.preventDefault();
    if ($('#formulario-caja-registro').valid()) {
        var pestana_activa = $('#pestanas > li > a.active');;
        if (pestana_activa.length > 0) {
            var index = pestana_activa.attr('data-index');
            var valido = true;
            if ($('#ingegr').val() == 'E') {
                var fondo = 0;
                for (f=0; f<cajas[index].fondos.length; f++) {
                    if (cajas[index].fondos[f].codfp == $('#codforpag').val()) {
                        fondo = parseFloat(cajas[index].fondos[f].fondo);
                        break;
                    }
                }
                var total = 0;
                $.each($('#'+cajas[index].codcaj+'-caja-movimientos > tbody > tr.'+$('#codforpag').val()), function() {
                    total += parseFloat($(this).find('.impmov').val());
                });
                var maximo_retiro = total - fondo;
                maximo_retiro = (maximo_retiro<0)?0:maximo_retiro;
                if (maximo_retiro < parseFloat($('#impmov').val())) {
                    valido = false;
                    swal('El monto maximo que puede sacar es '+$.number(maximo_retiro, 2, ',', '.' )+'. Con esta forma de pago debe quedar un fondo de '+$.number(fondo, 2, ',', '.' )+' ', '', 'error');
                }
            }

            if (valido) {
                var dataToSend = new FormData($("#formulario-caja-registro")[0]);
                dataToSend.append('sucursal', $('#select2-1').val());
                dataToSend.append('opcion', 'guardar');
                dataToSend.append('usuario', (session != undefined) ? session : "");
                dataToSend.append('codforpag', $('#codforpag').val());
                if (isCodEmpRequired()) {
                    dataToSend.append('codemp', $('#codemp').val());
                }
                if (isCodProRequired()) {
                    dataToSend.append('codpro', $('#codpro').val());
                }
                if (isCodArtRequired()) {
                    dataToSend.append('codart', $('#codart').val());
                    dataToSend.append('desart', $('#codart :selected').text());
                }
                if (isCodCajRequired()) {
                    dataToSend.append('codcajdes', $('#codcajdes').val());
                }
                // console.log("DATA TO SEND: " + dataToSend);
                // for (var pair of dataToSend.entries()) {
                //     console.log(pair[0]+': '+ pair[1]);
                // }
                $.ajax({
                    type: "POST",
                    url: urlServerPage,
                    data: dataToSend,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data.exito == true) {
                            // console.log("CON EXITO: " + JSON.stringify(data));
                            agregarMovimiento(data.movimiento);
                            if (data.movimiento_inverso != undefined) {
                                agregarMovimiento(data.movimiento_inverso);
                            }
                            $("#modal_caja_registro .close").click();
                            swal('Movimiento Registrado!', '', 'success');
                        }
                        else {
                            swal(data.mensaje, '', 'error');
                            console.log("SIN EXITO: " + JSON.stringify(data));
                        }
                    },
                    error: function (respuesta) {
                        swal(JSON.stringify(respuesta), '', 'error');
                        console.log("ERROR: " + JSON.stringify(respuesta));
                    }
                });
            }
        }
    }
});
