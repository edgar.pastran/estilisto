$(function() {
    inicializar();
});

function inicializar() {
    toastr.clear();
    toastr.options.positionClass = 'toast-top-right';
}

$("#btnIngresarPOST").click(function (event){

    event.preventDefault();

    $datos = $("#formulario-ingresar").serialize();

    serverPage = ruta.concat("/servidor/php/login.php");

    $.ajax({
        type: "POST",
        url: serverPage,
        data: $("#formulario-ingresar").serialize(),
        success: function(data) {
            if (data.exito == true) {
                guardarSession(data.usuario, data.nombre, data.apellido, data.compania, data.administrador, data.foto);
                guardarPantallas(data.pantallas);
                //Re Direccionar
                document.location.href="index.html";
            }
            else{
                toastr['error']('Username or Password is Invalid');
                document.getElementById("name").value = "";
                document.getElementById("password").value = "";
                document.getElementById("name").focus();
            };
        },
        error: function(data) {
            console.log(JSON.stringify(data));
        }
    });
});