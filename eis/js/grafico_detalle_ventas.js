var sucursales = new Array();

$(function() {
    cargarTablas();
    inicializar();
});

function cargarTablas() {
    var grid = $('#bootgrid-ventas-art').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        formatters: {
            commands: function(column, row) {
                return '<button type="button" class="btn-flat btn-sm btn-info mr-2 command-edit" data-row-id="' + row.id + '"><em class="ion-information-circled"></em></button>';
            }
        },
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on('loaded.rs.jquery.bootgrid', function() {
        /* Executes after data is loaded and rendered */
        grid.find('.command-edit').on('click', function() {

            var id = $(this).data('row-id');
            document.getElementById("id").value = id;

            serverPage = ruta.concat("/servidor/php/consul_detalle_ventas_emp.php");
            $.post(serverPage, $("#formulario-reporte").serialize(), function (data) {
                if ( data.exito == true && data.nume_regis_e >= 1 ){
                    var iSuc = 0;

                    for (var i = 0; i < sucursales.length; i++) {
                        if (data.sucursal == sucursales[i]['codsuc']) { iSuc = i;};
                    }

                    if (data.nume_regis_e >= 1) {
                        $('#titulo_ventas_art').text("Ventas Articulo : " + id + " " + data.det_desart[0]);

                        $('#totalinf_art').text('TOTAL '+sucursales[iSuc]['moneda']+' :');
                        $('#totalven_art').text(data.totalven_emp);
                        $('#totalcom_art').text(data.totalcom_emp);
                        $('#totaltickets_art').text(data.totaltickets);

                        $("#bootgrid-det-ventas-art").bootgrid("clear");
                        for (var i = 0; i < data.nume_regis_e; i++) {
                            $("#bootgrid-det-ventas-art").bootgrid().bootgrid("append", [{"commands": "", "id": ""+data.det_codemp[i]+"", "det_nombre_emp": ""+data.det_nombre_emp[i]+"", "tot_subtot_emp": data.tot_subtot_emp[i], "tot_comisi_emp": data.tot_comisi_emp[i], "porc_emp": data.porc_emp[i], "tickets_emp": data.tickets_emp[i]}] );
                        };

                        //Show Modal
                        $('#modal_ventas_art').modal('show');

                    };//No hay Datos
                };
            }); //Fin .post

        }).end().find('.command-delete').on('click', function() {
            console.log('You pressed delete on row: ' + $(this).data('row-id'));
        });
    });

    $('#bootgrid-det-ventas-art').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    });

    var grid2 = $('#bootgrid-ventas-emp').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        formatters: {
            commands: function(column, row) {
                return '<button type="button" class="btn-flat btn-sm btn-info mr-2 command-edit" data-row-id="' + row.id + '"><em class="ion-information-circled"></em></button>';
            }
        },
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on('loaded.rs.jquery.bootgrid', function() {
        /* Executes after data is loaded and rendered */
        grid2.find('.command-edit').on('click', function() {

            var id = $(this).data('row-id');
            document.getElementById("id").value = id;

            serverPage = ruta.concat("/servidor/php/consul_detalle_ventas_art.php");
            $.post(serverPage, $("#formulario-reporte").serialize(), function (data) {
                if ( data.exito == true && data.nume_regis_a >= 1 ){
                    var iSuc = 0;

                    for (var i = 0; i < sucursales.length; i++) {
                        if (data.sucursal == sucursales[i]['codsuc']) { iSuc = i;};
                    }

                    if (data.nume_regis_a >= 1) {

                        $('#titulo_ventas_emp').text("Ventas Empleado : " + id + " " +data.det_nombre_emp[0]);

                        $('#totalinf_emp').text('TOTAL '+sucursales[iSuc]['moneda']+' :');
                        $('#totalven_emp').text(data.totalven_a);
                        $('#totalcan_emp').text(data.totaltickets);

                        $("#bootgrid-det-ventas-emp").bootgrid("clear");
                        for (var i = 0; i < data.nume_regis_a; i++) {
                            $("#bootgrid-det-ventas-emp").bootgrid().bootgrid("append", [{"det_codart": ""+data.det_codart[i]+"", "det_desart": ""+data.det_desart[i]+"", "tot_subart": data.tot_subart[i], "porc_art": data.porc_art[i], "canti_art": data.canti_art[i]}] );
                        };

                        //Show Modal
                        $('#modal_ventas_emp').modal('show');

                    };//No hay Datos
                };
            }); //Fin .post
        }).end().find('.command-delete').on('click', function() {

        });
    });

    $('#bootgrid-det-ventas-emp').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    });
}

function inicializar() {
    serverPage = ruta.concat("/servidor/php/consul_suc_usuario.php");
    $.post(serverPage, {"session":session}, function (data) {

        if (data.exito == true){

            $('#select2-2').text(" ");

            for (var i = 0; i < data.nume_regis; i++) {

                if (sucursal.indexOf(data.sucursales_usuario[i]['codsuc']) >= 0) {

                    $('#select2-2').append('<option value="'+data.sucursales_usuario[i]['codsuc']+'"selected>'+data.sucursales_usuario[i]['codsuc']+' '+data.sucursales_usuario[i]['razemp']+'</option>');

                }else{

                    $('#select2-2').append('<option value="'+data.sucursales_usuario[i]['codsuc']+'">'+data.sucursales_usuario[i]['codsuc']+' '+data.sucursales_usuario[i]['razemp']+'</option>');

                };





                sucursales[i] = new Array();

                sucursales[i]['codsuc'] = data.sucursales_usuario[i]['codsuc'];

                sucursales[i]['razemp'] = data.sucursales_usuario[i]['razemp'];

                sucursales[i]['moneda'] = data.sucursales_usuario[i]['moneda'];

            };

        }else{

            //alert('error');

        };



    }); //Fin .post

    $('#reporte').hide('fast');
    $('#btnNuevoReporte').hide('fast');

    document.getElementById("fecha_ini").value = fecha_1_mes;
    document.getElementById("fecha_fin").value = fecha_actual;

    // Configurar la caja de texto de fecha
    $('#example-datepicker-4')
                .datepicker({
                    format: 'dd/mm/yyyy',
                    autoclose: true,
                    container: '#example-datepicker-container-4'
                });
    // // Configurar combos
    $('#select2-2').select2();
}

$("#btnReporte").click(function (event){

    event.preventDefault();

    getSucursal();

    $datos = $("#formulario-reporte").serialize();



    var valor1 = document.getElementById("fecha_ini").value;

    var valor2 = document.getElementById("fecha_fin").value;



    if( valor1.length == 0 ) {

        //alert('Ingrese fecha desde');

    }else if( valor2  == 0 ){

        //alert('Ingrese fecha fin');

    }else{



        serverPage = ruta.concat("/servidor/php/consul_detalle_ventas.php");

        $.post(serverPage, $("#formulario-reporte").serialize(), function (data) {

            if (data.exito == true && ( data.nume_regis_a >= 1 || data.nume_regis_e >= 1 )){



                $('#formulario').hide('fast');

                $('#reporte').show('fast');

                $('#btnReporte').hide('fast');

                $('#btnNuevoReporte').show('fast');

                var nSuc = 0;

                var iSuc = 0;

                for (var i = 0; i < sucursales.length; i++) {

                    if (document.getElementById("sucursal").value.indexOf(sucursales[i]['codsuc']) >= 0) { iSuc = i; nSuc=nSuc+1;};

                }

                guardarSucursal(document.getElementById("sucursal").value);



                if (data.nume_regis_a >= 1) {

                    var desSuc = sucursales[iSuc]['razemp'];

                    if (nSuc > 1) { desSuc = ""};

                    $('#TituloTable1').text(document.getElementById("sucursal").value +" "+desSuc+ " -- desde : " + data.fecha_ini + " hasta : " + data.fecha_fin);



                    $('#totalinf_a').text('TOTAL '+sucursales[iSuc]['moneda']+' :');



                    $('#totalven_a').text(data.totalven_a);



                    $("#bootgrid-ventas-art").bootgrid("clear");

                    for (var i = 0; i < data.nume_regis_a; i++) {

                        $("#bootgrid-ventas-art").bootgrid().bootgrid("append", [{"commands": "", "id": ""+data.det_codart[i]+"", "det_desart": ""+data.det_desart[i]+"", "tot_subart": data.tot_subart[i], "porc_art": data.porc_art[i], "canti_art": data.canti_art[i]}] );

                    };



                    //GRAFICO 1

                    var datos = new Array();

                    for (var i = 0; i < data.nume_regis_a; i++) {

                        datos[i] = new Array();

                        datos[i][0] = data.det_desart[i]+' '+data.tot_subart[i]+' '+sucursales[iSuc]['moneda'];

                        monto = data.tot_subart[i].replace(',','');monto = monto.replace(',','');monto = monto.replace(',','');

                        monto = monto.replace(',','');

                        datos[i][1] = Number(monto);

                    };



                    guardarSucursal(document.getElementById("sucursal").value);

                    guardarDatos(JSON.stringify(data), sucursales[iSuc]['moneda']);

                    $('#grafico1').text("");

                    $('#grafico1').append('<iframe id="external" frameborder="0" style="width:100%;height:270px" src="graficos/ver_grafico_detalle_articulos.html"></iframe>');



                };



                if (data.nume_regis_e >= 1) {

                    $('#TituloTable2').text(document.getElementById("sucursal").value +" "+desSuc+ " -- desde : " + data.fecha_ini + " hasta : " + data.fecha_fin);



                    $('#totalinf_e').text('TOTAL '+sucursales[iSuc]['moneda']+' :');



                    $('#totalven_e').text(data.totalven_emp);



                    $('#totalcom_e').text(data.totalcom_emp);



                    $('#totaltickets').text(data.totaltickets);



                    $("#bootgrid-ventas-emp").bootgrid("clear");

                    for (var i = 0; i < data.nume_regis_e; i++) {

                        $("#bootgrid-ventas-emp").bootgrid().bootgrid("append", [{"commands": "", "id": ""+data.det_codemp[i]+"", "det_nombre_emp": ""+data.det_nombre_emp[i]+"", "tot_subtot_emp": data.tot_subtot_emp[i], "tot_comisi_emp": data.tot_comisi_emp[i], "porc_emp": data.porc_emp[i], "tickets_emp": data.tickets_emp[i]}] );

                    };



                    //GRAFICO 2

                    $('#grafico2').text("");

                    $('#grafico2').append('<iframe id="external" frameborder="0" style="width:100%;height:270px" src="graficos/ver_grafico_detalle_ventas_e.html"></iframe>');

                };



                $('#totalven').text(sucursales[iSuc]['moneda']+'  '+data.totalven);

                $('#totaltik').text(data.totaltik);

                $('#promeven').text(sucursales[iSuc]['moneda']+'  '+data.promeven);

                $('#prometik').text(data.prometik);



            }else{//No existen Registros

                toastr.options.positionClass = 'toast-bottom-right';

                toastr['warning']('No existen Datos');



            };

        }); //Fin .post

    }; //fin validacion Campos

});


$("#btnNuevoReporte").click(function (event){

    $('#formulario').show('fast');

    $('#btnReporte').show('fast');

    $('#btnNuevoReporte').hide('fast');

    //$('#tabla1').text(" ");

    $('#reporte').hide('fast');

});


