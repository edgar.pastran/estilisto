var urlServerPage = ruta.concat("/servidor/php/act_cod_empleados.php");

$(function() {
    $.fn.editable.defaults.mode = 'inline';

    usuarioSucursal(inicializarDatosSucursal);

    // Configurar modal de empleados
    $('#bootgrid-empleados-cambio-codigo').bootgrid({
        css: {
            icon: 'icon',
            iconColumns: 'ion-ios-list-outline',
            iconDown: 'ion-chevron-down',
            iconRefresh: 'ion-refresh',
            iconSearch: 'ion-search',
            iconUp: 'ion-chevron-up',
            dropDownMenuItems: 'dropdown-menu dropdown-menu-right'
        },
        rowCount: [/*-1, */5, 10, 25, 50, 100],
        selection: true,
        multiSelect: false,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    }).
    on("selected.rs.jquery.bootgrid", function(e, rows) {
        var tipo_codigo = $('#tipo_codigo').val();
        limpiarDatosCabecera();
        // Cargar los datos de la cabecera
        var empleado = JSON.parse(rows[0].emp_data);
        $('#empleado_codigo'+tipo_codigo).val(empleado.codemp);
        $('#empleado_nombre'+tipo_codigo).val(empleado.codemp+" "+empleado.nomemp+((empleado.ape1emp != null && empleado.ape1emp.length)>0?" "+empleado.ape1emp:""));

        // Cerrar el modal
        $("#modal_empleados .close").click();

    }).
    on("deselected.rs.jquery.bootgrid", function(e, rows) {
    });

});

$(document).on('change', '#select2-1', function(e) {
    e.preventDefault();
    inicializarDatosSucursal();
});

function inicializarDatosSucursal() {
    resetearVista();
}

$(document).on('click', '#btnBuscarEmpleadoAnterior', function(e) {
    e.preventDefault();
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        $('#tipo_codigo').val('_anterior');
        cargarModalEmpleados();
    }
});

$(document).on('click', '#btnBuscarEmpleadoActual', function(e) {
    e.preventDefault();
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        $('#tipo_codigo').val('_actual');
        cargarModalEmpleados();
    }
});

function cargarModalEmpleados() {
    var codsuc = $('#select2-1').val();
    var params = {"sucursal": codsuc, "operacion": "empleados"};
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(data) {
            if (data.exito == true) {
                //console.log(JSON.stringify(data));
                if (data.exito == true) {
                    $("#bootgrid-empleados-cambio-codigo").bootgrid("clear");
                    for (var i = 0; i < data.empleados.length; i++) {
                        var empleado = data.empleados[i];
                        $("#bootgrid-empleados-cambio-codigo").bootgrid().bootgrid("append", [{
                            "emp_codemp": empleado['codemp'],
                            "emp_nomemp": empleado['nomemp'],
                            "emp_ape1emp": empleado['ape1emp'],
                            "emp_dniemp": empleado['dniemp'],
                            "emp_vender": empleado['vender_format'],
                            "emp_obsoleto": empleado['obsoleto_format'],
                            "emp_data": JSON.stringify(empleado)
                        }]);
                    };
                    //Show Modal
                    $('#modal_empleados').modal('show');
                };
            }
            else {
                console.log(data);
            }
        },
        error: function(data) {
            console.log(JSON.stringify(data));
        }
    });
}

function limpiarDatosCabecera() {
    var tipo_codigo = $("#tipo_codigo").val();
    if (tipo_codigo != "") {
        $("#empleado_codigo"+tipo_codigo).val("");
        $("#empleado_nombre"+tipo_codigo).val("");
    }
    else {
        $("#empleado_codigo_anterior").val("");
        $("#empleado_nombre_anterior").val("");
        $("#empleado_codigo_actual").val("");
        $("#empleado_nombre_actual").val("");
    }
}

function resetearVista(){
    $('#tipo_codigo').val("");
    limpiarDatosCabecera();
    $('#eliminar_codigo_anterior').prop('checked', false);
}

$(document).on('click', '#btnCancelar', function(e) {
    e.preventDefault();
    swal({
        title: 'Estas seguro?',
        text: 'Si cancelas esta operacion no se guardaran los datos que has ingresado!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'No, me arrepenti',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Si, cancelarlo',
        closeOnConfirm: false
    },
    function () {
        resetearVista();
        swal('Cancelado!', 'Tus datos has sido cancelados.', 'success');
    });
});

$(document).on('click', '#btnGuardar', function(e) {
    e.preventDefault();
    var codsuc = $('#select2-1').val();
    var codempant = $('#empleado_codigo_anterior').val();
    var codempact = $('#empleado_codigo_actual').val();
    if (codsuc != "" && codempant != "" && codempact != "") {
        swal({
            title: 'Estas seguro?',
            text: 'Si guardas el cambio de codigo no podras reversar la operacion!',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'No, me arrepenti',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Si, guardarlo',
            closeOnConfirm: false
        },
        function () {
            procesarCambioCodigo();
        });
    }
});

function procesarCambioCodigo() {
    var codsuc = $('#select2-1').val();
    var datos_cambio_codigo = {};
    datos_cambio_codigo.codigo_anterior = $('#empleado_codigo_anterior').val();
    datos_cambio_codigo.codigo_actual = $('#empleado_codigo_actual').val();
    datos_cambio_codigo.eliminar_anterior = $('#eliminar_codigo_anterior').is(':checked');
    var params = {"sucursal": codsuc, "operacion": "procesar_cambio_codigo", "datos_cambio_codigo": JSON.stringify(datos_cambio_codigo)};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(data) {
            // console.log(JSON.stringify(data));
            if (data.exito == true) {
                resetearVista();
                swal('Codigos Actualizados!', '', 'success');
            }
            else {
                console.log(JSON.stringify(data));
            }
        },
        error: function(data) {
            console.log(JSON.stringify(data));
        }
    });
}

$('#select2-1').select2();