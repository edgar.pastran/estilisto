var sucursales = new Array();

$(function() {
    inicializar();
});

function inicializar() {
    serverPage = ruta.concat("/servidor/php/consul_suc_usuario.php");
    $.post(serverPage, {"session": session}, function (data) {

        if (data.exito == true) {

            $('#select2-2').text(" ");

            for (var i = 0; i < data.nume_regis; i++) {

                if (sucursal.indexOf(data.sucursales_usuario[i]['codsuc']) >= 0) {

                    $('#select2-2').append('<option value="' + data.sucursales_usuario[i]['codsuc'] + '"selected>' + data.sucursales_usuario[i]['codsuc'] + ' ' + data.sucursales_usuario[i]['razemp'] + '</option>');

                } else {

                    $('#select2-2').append('<option value="' + data.sucursales_usuario[i]['codsuc'] + '">' + data.sucursales_usuario[i]['codsuc'] + ' ' + data.sucursales_usuario[i]['razemp'] + '</option>');

                }
                ;


                sucursales[i] = new Array();

                sucursales[i]['codsuc'] = data.sucursales_usuario[i]['codsuc'];

                sucursales[i]['razemp'] = data.sucursales_usuario[i]['razemp'];

                sucursales[i]['moneda'] = data.sucursales_usuario[i]['moneda'];

            }
            ;

        } else {

            //alert('error');

        }
        ;


    }); //Fin .post

    var ano = fecha.getFullYear();
    var ano_a = 0;
    for (var i = ano - 0; i >= ano - 4; i--) {
        ano_a = i-1;
        //$('#eje_anterior').append('<option value="' + ano_a + '">' + ano_a + '</option>');
        $('#eje_actual').append('<option value="' + i + '">' + i + '</option>');
    }
    ;

    $('#reporte').hide('fast');
    $('#btnNuevoReporte').hide('fast');
}

function imprimir_fila(i,monto,indicador) {
    var fila_imprimir = "";
    var icono = "<em class='ion-arrow-graph-up-right text-success mr-1 icon-lg text-success'></em>";

    if (-1 < monto && monto < 1) {
        fila_imprimir = "<td></td>";
    }else if (indicador == true) {
        if (monto < 0) { icono = "<em class='ion-arrow-graph-down-right text-danger mr-1 icon-lg text-success'></em>"}
        fila_imprimir = "<td class='text-bold bg-grey-50' align='right'>"+icono+monto+"</td>";
    }else{
        fila_imprimir = "<td align='right'>"+monto+"</td>";
    }
    return formatNumber.new(fila_imprimir);
}

$("#btnReporte").click(function (event){

    event.preventDefault();

    getSucursal();

    $('#eje_anterior').val($('#eje_actual').val() - 1);

    $datos = $("#formulario-reporte").serialize();


    serverPage = ruta.concat("/servidor/php/resumen_pyg.php");

    $.post(serverPage, $("#formulario-reporte").serialize(), function (data) {

        if (data.exito == true ){

            $('#formulario').hide('fast');
            $('#reporte').show('fast');
            $('#btnReporte').hide('fast');
            $('#btnNuevoReporte').show('fast');

            $('#TituloPagina').hide('fast');

            var nSuc = 0;

            var iSuc = 0;

            for (var i = 0; i < sucursales.length; i++) {

                if (document.getElementById("sucursal").value.indexOf(sucursales[i]['codsuc']) >= 0) { iSuc = i; nSuc=nSuc+1;};

            }
            guardarSucursal(document.getElementById("sucursal").value);

            var desSuc = sucursales[iSuc]['razemp'];

            if (nSuc > 1) { desSuc = ""};

            $('#TituloTable1').text(document.getElementById("sucursal").value +" "+desSuc+ " -- Ejercicio : " + $('#eje_actual').val());
            var mes ="";
            var acumulado=0;
            var total_acumulado=0;
            var beneficio="";
            var ventas=0;
            var compras=0;

            $("#tbody1").text("");

            for (var i = 1; i < 12; i++) {
                data.tot_ven_mes[0][0] = data.tot_ven_mes[0][0] + data.tot_ven_mes[0][i]
            }
            var filas = [$('#eje_anterior').val(), $('#eje_actual').val(), "Var%", "AvrTrim", "AvrT%"];
                //INGRESOS
                var fila ="<tr class='bg-grey-50 text-bold'>"+
                "<td class='text-bold'>INGRESOS</td>"+
                imprimir_fila(i,data.tot_ven_mes[0][0].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[1][0].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[1][1].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[1][2].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[1][3].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[1][4].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[1][5].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[1][6].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[1][7].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[1][8].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[1][9].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[1][10].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[1][11].toFixed(0),"X")+
                imprimir_fila(i,data.tot_ven_mes[1][14].toFixed(0),"X")+
                "</tr>";
                $("#tbody1").append(fila);

                //GASTOS
                fila ="<tr class='bg-grey-50 text-bold'>"+
                "<td class='text-bold'>EGRESOS</td>"+
                "<td></td>"+
                "<td></td>"+
                "<td></td>"+
                "<td></td>"+
                "<td></td>"+
                "<td></td>"+
                "<td></td>"+
                "<td></td>"+
                "<td></td>"+
                "<td></td>"+
                "<td></td>"+
                "<td></td>"+
                "<td></td>"+
                "</tr>";
                $("#tbody1").append(fila);

                for (var i = 0; i < data.total_mat_gas; i++) {
                    if (data.tot_gas_mes[i][14] > 0) {
                        fila ="<tr>"+
                        "<td align='right'>"+data.tot_gas_mes[i][13]+"</td>"+
                        "<td></td>"+
                        imprimir_fila(i,data.tot_gas_mes[i][0].toFixed(0),"X")+
                        imprimir_fila(i,data.tot_gas_mes[i][1].toFixed(0),"X")+
                        imprimir_fila(i,data.tot_gas_mes[i][2].toFixed(0),"X")+
                        imprimir_fila(i,data.tot_gas_mes[i][3].toFixed(0),"X")+
                        imprimir_fila(i,data.tot_gas_mes[i][4].toFixed(0),"X")+
                        imprimir_fila(i,data.tot_gas_mes[i][5].toFixed(0),"X")+
                        imprimir_fila(i,data.tot_gas_mes[i][6].toFixed(0),"X")+
                        imprimir_fila(i,data.tot_gas_mes[i][7].toFixed(0),"X")+
                        imprimir_fila(i,data.tot_gas_mes[i][8].toFixed(0),"X")+
                        imprimir_fila(i,data.tot_gas_mes[i][9].toFixed(0),"X")+
                        imprimir_fila(i,data.tot_gas_mes[i][10].toFixed(0),"X")+
                        imprimir_fila(i,data.tot_gas_mes[i][11].toFixed(0),"X")+
                        imprimir_fila(i,data.tot_gas_mes[i][14].toFixed(0),"X")+
                        "</tr>";
                        $("#tbody1").append(fila);
                    }
                }

                //Total GASTOS
                fila ="<tr class='bg-grey-50 text-bold'>"+
                "<td class='text-bold'>Total EGRESOS</td>"+
                "<td></td>"+
                imprimir_fila(i,data.tot_margen[0][0].toFixed(0),"X")+
                imprimir_fila(i,data.tot_margen[0][1].toFixed(0),"X")+
                imprimir_fila(i,data.tot_margen[0][2].toFixed(0),"X")+
                imprimir_fila(i,data.tot_margen[0][3].toFixed(0),"X")+
                imprimir_fila(i,data.tot_margen[0][4].toFixed(0),"X")+
                imprimir_fila(i,data.tot_margen[0][5].toFixed(0),"X")+
                imprimir_fila(i,data.tot_margen[0][6].toFixed(0),"X")+
                imprimir_fila(i,data.tot_margen[0][7].toFixed(0),"X")+
                imprimir_fila(i,data.tot_margen[0][8].toFixed(0),"X")+
                imprimir_fila(i,data.tot_margen[0][9].toFixed(0),"X")+
                imprimir_fila(i,data.tot_margen[0][10].toFixed(0),"X")+
                imprimir_fila(i,data.tot_margen[0][11].toFixed(0),"X")+
                imprimir_fila(i,data.tot_margen[0][14].toFixed(0),"X")+
                "</tr>";
                $("#tbody1").append(fila);

                // //MARGEN
                fila ="<tr class='bg-grey-50 text-bold'>"+
                "<td class='text-bold'>MARGEN</td>"+
                "<td></td>"+
                imprimir_fila(i,data.tot_margen[1][0].toFixed(0),true)+
                imprimir_fila(i,data.tot_margen[1][1].toFixed(0),true)+
                imprimir_fila(i,data.tot_margen[1][2].toFixed(0),true)+
                imprimir_fila(i,data.tot_margen[1][3].toFixed(0),true)+
                imprimir_fila(i,data.tot_margen[1][4].toFixed(0),true)+
                imprimir_fila(i,data.tot_margen[1][5].toFixed(0),true)+
                imprimir_fila(i,data.tot_margen[1][6].toFixed(0),true)+
                imprimir_fila(i,data.tot_margen[1][7].toFixed(0),true)+
                imprimir_fila(i,data.tot_margen[1][8].toFixed(0),true)+
                imprimir_fila(i,data.tot_margen[1][9].toFixed(0),true)+
                imprimir_fila(i,data.tot_margen[1][10].toFixed(0),true)+
                imprimir_fila(i,data.tot_margen[1][11].toFixed(0),true)+
                imprimir_fila(i,data.tot_margen[1][14].toFixed(0),true)+
                "</tr>";
                $("#tbody1").append(fila);

                
                // //DiasxMes
                fila ="<tr>"+
                "<td class='text-bold'><i>DiasxMes</i></td>"+
                "<td></td>"+
                imprimir_fila(i,data.tot_total_dias[1][0].toFixed(0),"X")+
                imprimir_fila(i,data.tot_total_dias[1][1].toFixed(0),"X")+
                imprimir_fila(i,data.tot_total_dias[1][2].toFixed(0),"X")+
                imprimir_fila(i,data.tot_total_dias[1][3].toFixed(0),"X")+
                imprimir_fila(i,data.tot_total_dias[1][4].toFixed(0),"X")+
                imprimir_fila(i,data.tot_total_dias[1][5].toFixed(0),"X")+
                imprimir_fila(i,data.tot_total_dias[1][6].toFixed(0),"X")+
                imprimir_fila(i,data.tot_total_dias[1][7].toFixed(0),"X")+
                imprimir_fila(i,data.tot_total_dias[1][8].toFixed(0),"X")+
                imprimir_fila(i,data.tot_total_dias[1][9].toFixed(0),"X")+
                imprimir_fila(i,data.tot_total_dias[1][10].toFixed(0),"X")+
                imprimir_fila(i,data.tot_total_dias[1][11].toFixed(0),"X")+
                "</tr>";
                $("#tbody1").append(fila);
                
                // //TicketsxMes
                fila ="<tr>"+
                "<td class='text-bold'><i>TicketsxMes</i></td>"+
                "<td></td>"+
                imprimir_fila(i,data.tot_tik_mes[1][0].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[1][1].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[1][2].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[1][3].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[1][4].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[1][5].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[1][6].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[1][7].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[1][8].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[1][9].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[1][10].toFixed(0),"X")+
                imprimir_fila(i,data.tot_tik_mes[1][11].toFixed(0),"X")+
                "</tr>";
                $("#tbody1").append(fila);
                
                // //Tickets/Dia
                fila ="<tr>"+
                "<td class='text-bold'><i>Tickets/Dia</i></td>"+
                "<td></td>"+
                imprimir_fila(i,data.tot_tik_dia_avg[1][0].toFixed(2),"X")+
                imprimir_fila(i,data.tot_tik_dia_avg[1][1].toFixed(2),"X")+
                imprimir_fila(i,data.tot_tik_dia_avg[1][2].toFixed(2),"X")+
                imprimir_fila(i,data.tot_tik_dia_avg[1][3].toFixed(2),"X")+
                imprimir_fila(i,data.tot_tik_dia_avg[1][4].toFixed(2),"X")+
                imprimir_fila(i,data.tot_tik_dia_avg[1][5].toFixed(2),"X")+
                imprimir_fila(i,data.tot_tik_dia_avg[1][6].toFixed(2),"X")+
                imprimir_fila(i,data.tot_tik_dia_avg[1][7].toFixed(2),"X")+
                imprimir_fila(i,data.tot_tik_dia_avg[1][8].toFixed(2),"X")+
                imprimir_fila(i,data.tot_tik_dia_avg[1][9].toFixed(2),"X")+
                imprimir_fila(i,data.tot_tik_dia_avg[1][10].toFixed(2),"X")+
                imprimir_fila(i,data.tot_tik_dia_avg[1][11].toFixed(2),"X")+
                "</tr>";
                $("#tbody1").append(fila);
                
                // //Valor/Ticket
                fila ="<tr>"+
                "<td class='text-bold'><i>Valor/Ticket</i></td>"+
                "<td></td>"+
                imprimir_fila(i,data.tot_ven_tik_avg[1][0].toFixed(2),"X")+
                imprimir_fila(i,data.tot_ven_tik_avg[1][1].toFixed(2),"X")+
                imprimir_fila(i,data.tot_ven_tik_avg[1][2].toFixed(2),"X")+
                imprimir_fila(i,data.tot_ven_tik_avg[1][3].toFixed(2),"X")+
                imprimir_fila(i,data.tot_ven_tik_avg[1][4].toFixed(2),"X")+
                imprimir_fila(i,data.tot_ven_tik_avg[1][5].toFixed(2),"X")+
                imprimir_fila(i,data.tot_ven_tik_avg[1][6].toFixed(2),"X")+
                imprimir_fila(i,data.tot_ven_tik_avg[1][7].toFixed(2),"X")+
                imprimir_fila(i,data.tot_ven_tik_avg[1][8].toFixed(2),"X")+
                imprimir_fila(i,data.tot_ven_tik_avg[1][9].toFixed(2),"X")+
                imprimir_fila(i,data.tot_ven_tik_avg[1][10].toFixed(2),"X")+
                imprimir_fila(i,data.tot_ven_tik_avg[1][11].toFixed(2),"X")+
                "</tr>";
                $("#tbody1").append(fila);
            
            //guardarSucursal(document.getElementById("sucursal").value);
            //guardarDatos(JSON.stringify(data), sucursales[iSuc]['moneda']);
            //$('#grafico1').text("");
            //$('#grafico1').append('<iframe id="external" frameborder="0" style="width:100%;height:416px" src="graficos/ver_resumen_mensual_1.html"></iframe>');
            
            //borrarDatos();
        }else{//No existen Registros

            toastr.options.positionClass = 'toast-bottom-right';
            toastr['warning']('No existen Datos');

        };

    }); //Fin .post

});

$("#btnNuevoReporte").click(function (event){

    $('#formulario').show('fast');

    $('#btnReporte').show('fast');

    $('#btnNuevoReporte').hide('fast');

    //$('#tabla1').text(" ");

    $('#reporte').hide('fast');
    
    $('#TituloPagina').show('fast');
});


// // Configurar combos
$('#select2-2').select2();
