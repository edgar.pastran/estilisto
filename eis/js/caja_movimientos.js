var urlServerPage = ruta.concat("/servidor/php/caja_movimientos.php");

$(function() {
    $(formValidation);
    usuarioSucursal(inicializar);
    cargarCatalogo();
    $('#select2-1').select2();
});

function formValidation() {
    if (!$.fn.validate) return;
    $('#formulario-movimientos-caja').validate({
        errorPlacement: errorPlacementInput,
        errorClass: "my-error-class",
        // Form rules
        rules: {
            desmov: {
                required: true
            },
            ingegr: {
                required: true
            },
            tipo: {
                required: true
            }
        }
    });
}

function limpiar() {
    $("#opcion").val("");
    $("#codmov").val("");
    $("#desmov").val("");
    $("#ingegr").val("");
    $("#tipo").val("");
    $("#codforpag").val("");
    $("#cajas").val("");
    $('.caja').each(function() {
        var id = $(this).attr("id");
        $('#'+id).prop('checked', false);
    });
    $('#solemp').prop('checked', false);
    $("#codemp").val("").trigger('change');
    $('#solpro').prop('checked', false);
    $("#codpro").val("").trigger('change');
    $('#solart').prop('checked', false);
    $("#codart").val("").trigger('change');
    $('#solcaj').prop('checked', false);
    $("#codcaj").val("").trigger('change');
    $('#obsoleto').prop('checked', false);
}

function habilitar(valor) {
    if (valor) {
        $("#desmov").removeAttr("readonly");
        $("#ingegr").removeAttr("readonly");
        $("#ingegr").removeAttr("disabled");
        $("#tipo").removeAttr("readonly");
        $("#tipo").removeAttr("disabled");
        $("#codforpag").removeAttr("readonly");
        $("#codforpag").removeAttr("disabled");
        $('.caja').each(function() {
            var id = $(this).attr("id");
            $("#"+id).removeAttr("disabled");
        });
        $("#solemp").removeAttr("disabled");
        if ($("#solemp").is(":checked")) {
            $("#codemp").removeAttr("readonly");
            $('#codemp').removeAttr('disabled');
        }
        else {
            $("#codemp").attr("readonly", true);
            $('#codemp').attr('disabled', true);
        }
        $("#solpro").removeAttr("disabled");
        if ($("#solpro").is(":checked")) {
            $("#codpro").removeAttr("readonly");
            $('#codpro').removeAttr('disabled');
        }
        else {
            $("#codpro").attr("readonly", true);
            $('#codpro').attr('disabled', true);
        }
        $("#solart").removeAttr("disabled");
        if ($("#solart").is(":checked")) {
            $("#codart").removeAttr("readonly");
            $('#codart').removeAttr('disabled');
        }
        else {
            $("#codart").attr("readonly", true);
            $('#codart').attr('disabled', true);
        }
        $("#solcaj").removeAttr("disabled");
        if ($("#solcaj").is(":checked")) {
            $("#codcaj").removeAttr("readonly");
            $('#codcaj').removeAttr('disabled');
        }
        else {
            $("#codcaj").attr("readonly", true);
            $('#codcaj').attr('disabled', true);
        }
        $("#obsoleto").removeAttr("disabled");
    }
    else {
        $("#desmov").attr("readonly", true);
        $("#ingegr").attr("readonly", true);
        $("#ingegr").attr("disabled", true);
        $("#tipo").attr("readonly", true);
        $("#tipo").attr("disabled", true);
        $("#codforpag").attr("readonly", true);
        $("#codforpag").attr("disabled", true);
        $('.caja').each(function() {
            var id = $(this).attr("id");
            $("#"+id).attr("disabled", true);
        });
        $("#solemp").attr("disabled", true);
        $("#codemp").attr("readonly", true);
        $("#codemp").attr("disabled", true);
        $("#solpro").attr("disabled", true);
        $("#codpro").attr("readonly", true);
        $("#codpro").attr("disabled", true);
        $("#solart").attr("disabled", true);
        $("#codart").attr("readonly", true);
        $("#codart").attr("disabled", true);
        $("#solcaj").attr("disabled", true);
        $("#codcaj").attr("readonly", true);
        $("#codcaj").attr("disabled", true);
        $("#obsoleto").attr("disabled", true);
    }
}

function inicializar() {
    $("#sucursal").val("");
    limpiar();
    cargarCombos();
}

function cargarCombos() {
    var dataToSend = {
        "opcion": "inicializar",
        "sucursal": $("#select2-1").val()
    };
    // console.log("DATA TO SEND: "+JSON.stringify(dataToSend));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: dataToSend,
        success: function (data) {
            if (data.exito == true) {
                // console.log("CON EXITO: " + JSON.stringify(data));
                // Clase
                $('#ingegr').text(" ");
                $('#ingegr').append('<option value="" selected></option>');
                for (var i in data.clase) {
                    $('#ingegr').append('<option value="' + i + '">' + data.clase[i] + '</option>');
                }
                $('#ingegr').attr('disabled', true);
                // Tipo
                $('#tipo').text(" ");
                $('#tipo').append('<option value="" selected></option>');
                for (var j in data.tipo) {
                    $('#tipo').append('<option value="' + j + '">' + data.tipo[j] + '</option>');
                }
                $('#tipo').attr('disabled', true);
                // Forma de Pago
                $('#codforpag').text(" ");
                $('#codforpag').append('<option value="" selected>Cualquiera</option>');
                for (var k=0; k<data.forma_pago.length; k++) {
                    $('#codforpag').append('<option value="'+ data.forma_pago[k]['codfp'] +'">' + data.forma_pago[k]['des'] + '</option>');
                }
                $('#codforpag').attr('disabled', true);
                // Cajas
                $('#contenedor_cajas').text(" ");
                for (var i = 0; i < data.cajas.length; i++) {
                    var chekbox =
                        '<label class="col col-md-2 col-lg-2 m-0 custom-control custom-checkbox">' +
                        '<input class="custom-control-input caja" type="checkbox" disabled="true" id="'+data.cajas[i]['codcaj']+'" name="'+data.cajas[i]['codcaj']+'">' +
                        '<span class="custom-control-indicator"></span>' +
                        '<span class="custom-control-description pl-3">'+data.cajas[i]['nomcaj']+'</span>' +
                        '</label>';
                    $('#contenedor_cajas').append(chekbox);
                }
                $(".caja").click(function(event) {
                    var cajas = "";
                    $('.caja').each(function() {
                        var id = $(this).attr("id");
                        if ($("#"+id).is(":checked")) {
                            cajas += "&"+id;
                        }
                    });
                    cajas += ((cajas!="")?"&":"");
                    $('#cajas').val(cajas);
                });
                // Empleados
                $('#codemp').text(" ");
                $('#codemp').append('<option value=""selected></option>');
                for (var i = 0; i < data.empleados.length; i++) {
                    $('#codemp').append('<option value="' + data.empleados[i]['codemp'] + '">' + data.empleados[i]['nomemp'] + ' ' + data.empleados[i]['ape1emp'] + '</option>');
                }
                $('#codemp').select2();
                $('#codemp').attr('disabled', true);
                // Proveedores
                $('#codpro').text(" ");
                $('#codpro').append('<option value=""selected></option>');
                for (var i = 0; i < data.proveedores.length; i++) {
                    $('#codpro').append('<option value="' + data.proveedores[i]['codpro'] + '">' + data.proveedores[i]['razon'] + '</option>');
                }
                $('#codpro').select2();
                $('#codpro').attr('disabled', true);
                // Articulos
                $('#codart').text(" ");
                $('#codart').append('<option value=""selected></option>');
                for (var i = 0; i < data.articulos.length; i++) {
                    $('#codart').append('<option value="' + data.articulos[i]['codart'] + '">' + data.articulos[i]['desart'] + '</option>');
                }
                $('#codart').select2();
                $('#codart').attr('disabled', true);
                // Cajas
                $('#codcaj').text(" ");
                $('#codcaj').append('<option value=""selected></option>');
                for (var i = 0; i < data.cajas.length; i++) {
                    $('#codcaj').append('<option value="' + data.cajas[i]['codcaj'] + '">' + data.cajas[i]['nomcaj'] + '</option>');
                }
                $('#codcaj').select2();
                $('#codcaj').attr('disabled', true);
            }
            else {
                console.log("SIN EXITO: " + JSON.stringify(data));
            }
        },
        error: function (respuesta) {
            console.log("ERROR: " + JSON.stringify(respuesta));
        }
    });
}

function cargarCatalogo() {
    $('#bootgrid-movimientos-caja').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        selection: true,
        multiSelect: false,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on("selected.rs.jquery.bootgrid", function(e, rows) {
        limpiar();
        var movimiento = JSON.parse(rows[0].conmov_data);
        $("#codmov").val(movimiento.codmov);
        $("#desmov").val(movimiento.desmov);
        $("#ingegr").val(movimiento.ingegr);
        $("#tipo").val(movimiento.tipo);
        $("#codforpag").val(movimiento.codforpag);
        if (movimiento.cajas != null) {
            $("#cajas").val(movimiento.cajas);
            $.each(movimiento.cajas.split('&'), function(i, value) {
                if (value != "") {
                    $('#'+value).prop('checked', true);
                }
            });
        }
        if (movimiento.solemp != null && movimiento.solemp == "1") {
            $('#solemp').prop('checked', true);
            if (movimiento.codemp != null) {
                $('#codemp').val(movimiento.codemp).trigger('change');
            }
        }
        if (movimiento.solpro != null && movimiento.solpro == "1") {
            $('#solpro').prop('checked', true);
            if (movimiento.codpro != null) {
                $('#codpro').val(movimiento.codpro).trigger('change');
            }
        }
        if (movimiento.solart != null && movimiento.solart == "1") {
            $('#solart').prop('checked', true);
            if (movimiento.codart != null) {
                $('#codart').val(movimiento.codart).trigger('change');
            }
        }
        if (movimiento.solcaj != null && movimiento.solcaj == "1") {
            $('#solcaj').prop('checked', true);
            if (movimiento.codcaj != null) {
                $('#codcaj').val(movimiento.codcaj).trigger('change');
            }
        }
        if (movimiento.obsoleto != null && movimiento.obsoleto == "1") {
            $('#obsoleto').prop('checked', true);
        }
        $("#opcion").val("actualizar");

        $("#close-modal").click();

        //Quitar ReadOnly
        habilitar(true);
        // $("#desfam1").removeAttr("readonly");
        // $("#orden").removeAttr("readonly");
        // $("#puntos").removeAttr("readonly");
        // $("#vender").removeAttr("disabled");
        // $("#obsoleto").removeAttr("disabled");
        // $('.remove-image').removeClass('btn-secondary').addClass('btn-danger');
        // $("#btn_remover_imagen").attr("disabled", false);
        // $('.edit-image').removeClass('btn-secondary').addClass('btn-info');
        // $("#imagen_nueva").attr("disabled", false);
    })
    .on("deselected.rs.jquery.bootgrid", function(e, rows) {

    });
}

$("#btn-movimientos").click(function (event) {
    event.preventDefault();
    $("#sucursal").val($("#select2-1").val());
    $("#opcion").val("consulta");

    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: $("#formulario-movimientos-caja").serialize(),
        success: function (data) {
            if (data.exito == true) {
                $("#bootgrid-movimientos-caja").bootgrid("clear");
                for (var i = 0; i < data.movimientos.length; i++) {
                    $("#bootgrid-movimientos-caja").bootgrid().bootgrid("append", [{
                        "conmov_codmov": data.movimientos[i]['codmov'],
                        "conmov_desmov": data.movimientos[i]['desmov'],
                        "conmov_ingegr": data.movimientos[i]['ingegr_string'],
                        "conmov_tipo": data.movimientos[i]['tipo_string'],
                        "conmov_forpag": data.movimientos[i]['forpag_string'],
                        "conmov_data": JSON.stringify(data.movimientos[i])
                    }]);
                }
                //Show Modal
                $('#modal-movimientos-caja').modal('show');
            }
            else {
                console.log("SIN EXITO: " + JSON.stringify(data));
            }
        },
        error: function (respuesta) {
            console.log("ERROR: " + JSON.stringify(respuesta));
        }
    });
});

$("#select2-1").change(function (event) {
    inicializar();
});

$("#solemp").click(function(event) {
    if ($("#solemp").is(":checked")) {
        $("#codemp").removeAttr("readonly");
        $('#codemp').removeAttr('disabled');
    }
    else {
        $("#codemp").val("").trigger('change');
        $("#codemp").attr("readonly", true);
        $('#codemp').attr('disabled', true);
    }
});

$("#solpro").click(function(event) {
    if ($("#solpro").is(":checked")) {
        $("#codpro").removeAttr("readonly");
        $('#codpro').removeAttr('disabled');
    }
    else {
        $("#codpro").val("").trigger('change');
        $("#codpro").attr("readonly", true);
        $('#codpro').attr('disabled', true);
    }
});

$("#solart").click(function(event) {
    if ($("#solart").is(":checked")) {
        $("#codart").removeAttr("readonly");
        $('#codart').removeAttr('disabled');
    }
    else {
        $("#codart").val("").trigger('change');
        $("#codart").attr("readonly", true);
        $('#codart').attr('disabled', true);
    }
});

$("#solcaj").click(function(event) {
    if ($("#solcaj").is(":checked")) {
        $("#codcaj").removeAttr("readonly");
        $('#codcaj').removeAttr('disabled');
    }
    else {
        $("#codcaj").val("").trigger('change');
        $("#codcaj").attr("readonly", true);
        $('#codcaj').attr('disabled', true);
    }
});

function eliminar() {
    if ($("#codmov").val() != "" && $("#opcion").val() != "insertar") {
        swal({
            title: 'Estas seguro?',
            text: 'Si eliminas este movimiento no podras recuperarlo!',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'No, me arrepenti',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Si, eliminarlo',
            closeOnConfirm: false
        },
        function () {
            $("#opcion").val("eliminar");
            guardar();
        });
    }
}

function guardar() {
    $("#sucursal").val($("#select2-1").val());
    if ($("#select2-1").val() != "" && $("#opcion").val() != "") {
        if ($('#formulario-movimientos-caja').valid()) {
            var dataToSend = new FormData($("#formulario-movimientos-caja")[0]);
            // console.log("DATA TO SEND: " + dataToSend);
            // for (var pair of dataToSend.entries()) {
            //     console.log(pair[0]+': '+ pair[1]);
            // }
            $.ajax({
                type: "POST",
                url: urlServerPage,
                data: dataToSend,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.exito == true) {
                        // console.log("CON EXITO: " + JSON.stringify(data));
                        if ($("#opcion").val() == "actualizar") {
                            swal('Movimiento Actualizado!', '', 'success');
                        }
                        else if ($("#opcion").val() == "insertar") {
                            swal('Movimiento Creado!', '', 'success');
                        }
                        else if ($("#opcion").val() == "eliminar") {
                            swal('Movimiento Eliminado!', '', 'success');
                        }
                        cancelar();
                    }
                    else {//alert('error');
                        swal(data.mensaje, '', 'error');
                        console.log("SIN EXITO: " + JSON.stringify(data));
                    }
                },
                error: function (respuesta) {
                    swal(JSON.stringify(respuesta), '', 'error');
                    console.log("ERROR: " + JSON.stringify(respuesta));
                }
            });
        }
    }
}

function cancelar() {
    guardarSucursal($("#select2-1").val());//Guardar la Sucursal con la que se Esta trabajando
    limpiar();
    $("#codmov").attr("readonly", true);
    habilitar(false);

}

function nuevo() {
    if ($("#select2-1").val() != "") {
        limpiar();
        $("#opcion").val("insertar");
        habilitar(true);
    }
    else {
        $("#select2-1").focus();
        swal('Seleccione Sucursal!');

    }
}
