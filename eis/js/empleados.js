var urlServerPage = ruta.concat("/servidor/php/empleados.php");

$(function() {
    usuarioSucursal();
    inicializar();
    cargarCatalogo();
})

function cargarCatalogo() {
    $('#bootgrid-empleadosDM').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        selection: true,
        multiSelect: false,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on("selected.rs.jquery.bootgrid", function(e, rows){
        $("#codemp").val(rows[0].emp_codemp);
        $("#diremp").val(rows[0].emp_diremp);
        $("#nomemp").val(rows[0].emp_nomemp);
        $("#ape1emp").val(rows[0].emp_ape1emp);
        $("#tel1emp").val(rows[0].emp_tel1emp);
        $("#dniemp").val(rows[0].emp_dniemp);
        $("#email").val(rows[0].emp_email);
        $("#select2-sexo").val(rows[0].emp_sexo);
        $("#fecnac").val(rows[0].emp_fecnac);
        if (rows[0].emp_foto != "") {
            $("#foto").attr("src", rows[0].emp_foto);
        }
        $("#vender").val(rows[0].emp_vender); setCheckbox('vender','1');
        $("#obsoleto").val(rows[0].emp_obsoleto); setCheckbox('obsoleto','1');

        $("#opc_empleados").val("actualizar");

        $("#codemp").prop('readonly', true);
        $("#closeMemp").click();

        deshabilitarCampos(false);
    })
    .on("deselected.rs.jquery.bootgrid", function(e, rows){});
}

$("#btn-empleados").click(function (event){
    event.preventDefault();
    $("#suc_empleados").val($("#select2-1").val());
    $("#opc_empleados").val("consulta");

    $.post(urlServerPage, $("#formulario-empleados").serialize(), function (data){
        if ( data.exito == true && data.nume_regis >= 1 ){
            if (data.nume_regis >= 1) {
                $("#bootgrid-empleadosDM").bootgrid("clear");
                for (var i = 0; i < data.nume_regis; i++) {
                    $("#bootgrid-empleadosDM").bootgrid().bootgrid("append", [{
                        "emp_codemp": ""+data.empleados[i]['codemp']+"",
                        "emp_nomemp": ""+data.empleados[i]['nomemp']+"",
                        "emp_ape1emp": ""+data.empleados[i]['ape1emp']+"",
                        "emp_fecnac": ""+data.empleados[i]['fecnac']+"",
                        "emp_diremp": ""+data.empleados[i]['diremp']+"",
                        "emp_tel1emp": ""+data.empleados[i]['tel1emp']+"",
                        "emp_email": ""+data.empleados[i]['email']+"",
                        "emp_dniemp": ""+data.empleados[i]['dniemp']+"",
                        "emp_sexo": ""+data.empleados[i]['sexo']+"",
                        "emp_foto": ""+data.empleados[i]['foto']+"",
                        "emp_vender": ""+data.empleados[i]['vender']+"",
                        "emp_obsoleto": ""+data.empleados[i]['obsoleto']+""
                    }]);
                };
                //Show Modal
                $('#modal_empleados').modal('show');
            };//No hay Datos
        };
    }); //Fin .post
}); //Fin .btn

$("#btn_remover_imagen").click(function (event) {
    if ($("#codemp").val() != "" && $("#opc_empleados").val() != "insertar") {
        $("#opc_empleados").val("remover_imagen");
        guardar();
    }
    else {
        $("#foto").attr("src", ruta.concat("/servidor/images/nothing.png"));
    }
});

$(document).on('click', '#btn-cancelar', function(e) {
    if ( $('#opc_empleados').val() == "insertar" || $('#opc_empleados').val() == "actualizar") {
        e.preventDefault();
        swal({
            title: '¿Estas seguro?',
            text: 'Si cancelas no se guardaran los datos que has ingresado!',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'No, me arrepenti',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Si, cancelarlo',
            closeOnConfirm: false
        },
        function () {
            swal('Cancelado!', 'La Creacion de Empleado ha sido cancelada.', 'success');
            cancelar();
        });
    }
});

$(document).on('click', '#btn-eliminar', function(e) {
    if ($('#codemp').val() != "" && $('#opc_empleados').val() != "insertar") {
        e.preventDefault();
        swal({
            title: '¿Estas seguro?',
            text: 'Si Eliminas se borraran los datos del Empleado!',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'No, me arrepenti',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Si, eliminar',
            closeOnConfirm: false
        },
        function () {
            swal('Eliminado!', 'El Empleado ha sido Eliminado.', 'success');
            eliminar();
        });
    }
});

function eliminar() {
    if ($("#codemp").val() != "" && $("#opc_empleados").val() != "insertar") {
        $("#opc_empleados").val("eliminar");
        guardar();
    }
}

function guardar(){
    if ($('#formulario-empleados').valid() == false) {
        //swal('Complete Campos!', '', 'error');
    }
    else{
        myCheckbox('obsoleto','1');
        myCheckbox('vender','1');
        $("#suc_empleados").val($("#select2-1").val());
        if ($("#select2-1").val() != "" && $("#opc_empleados").val() != "" && $("#codemp").val() != "") {
            $.ajax({
                type: "POST",
                url: urlServerPage,
                data: new FormData($("#formulario-empleados")[0]),
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.exito == true) {
                        if ($("#opc_empleados").val() == "actualizar") {
                            swal('Empleado Actualizado!', '', 'success');
                        }
                        else if ($("#opc_empleados").val() == "insertar") {
                            swal('Empleado Creado!', '', 'success');
                        }
                        else if ($("#opc_empleados").val() == "eliminar") {
                            swal('Empleado Eliminado!', '', 'success');
                        }
                        else if ($("#opc_empleados").val() == "remover_imagen") {
                            swal('Imagen Eliminada!', '', 'success');
                        }
                        cancelar();
                    }
                    else if ($("#opc_empleados").val() == "eliminar") {
                        cancelar();
                    }
                    else {//alert('error');
                        swal(data.mensaje, '', 'error');
                        console.log("SIN EXITO: " + JSON.stringify(data));
                    }
                },
                error: function (respuesta) {
                    swal(JSON.stringify(respuesta), '', 'error');
                    console.log("ERROR: " + JSON.stringify(respuesta));
                }
            });
        };
    };
} //Fin de Funcion

function consultar_empleados(){
    $("#suc_empleados").val($("#select2-1").val());
    var opc = $("#opc_empleados").val();
    if (opc == "insertar") {
        $("#opc_empleados").val("consultar");
        if ($("#select2-1").val() != "" && $("#opc_empleados").val() != "" && $("#codemp").val() != "") {
            $.post(urlServerPage, $("#formulario-empleados").serialize(), function (data) {
                if (data.exito == true){1
                    swal('Empleado YA Existe!', '', 'error');
                    $("#codemp").focus();
                }
                else{//alert('error');
                    $("#codemp").prop('readonly', true);
                };
            }); //Fin .post
        };
    } //Fin del IF
    $("#opc_empleados").val(opc);
} //Fin de Funcion

function consultar_codemp2(){

}

function cancelar(){
    $('#fecnac').
    datepicker({
        endDate : 'now',
        autoclose: true,
        container: '#fecnac-datepicker-container'
    });

    var validator = $( "#formulario-empleados" ).validate();
    validator.resetForm();
    $("#opc_empleados").val("");
    limpiarDatos();

    deshabilitarCampos(true);
    $("#btn-empleados").prop('disabled', false);
} //Fin de Funcion

function nuevo(){

    if ($("#select2-1").val() != "") {
        $("#opc_empleados").val("insertar");
        limpiarDatos();
        $("#vender").val("1"); setCheckbox('vender','1');


        deshabilitarCampos(false);
        $("#codemp").focus();
        $("#btn-empleados").prop('disabled', true);

        var fecha=new Date();
        var fecha_diames=fecha.getDate(); if ( fecha_diames < 10 ) { fecha_diames = '0'+fecha_diames};
        var fecha_mes=fecha.getMonth() +1; if ( fecha_mes < 10 ) { fecha_mes = '0'+fecha_mes};
        var fecha_ano=fecha.getFullYear()-18;
        var fecha_actual = fecha_diames+'/'+fecha_mes+'/'+fecha_ano;
        $("#fecnac").val(fecha_actual);
        $('#fecnac').datepicker('setDate',fecha_actual);
    }else{
        $("#select2-1").focus();
        swal('Seleccione Sucursal!');

    }
} //Fin de Funcion

function limpiarDatos() {
    $("#codemp").val("");
    $("#diremp").val("");
    $("#nomemp").val("");
    $("#ape1emp").val("");
    $("#fecnac").val("");
    $("#select2-sexo").val("M");
    $("#email").val("");
    $("#dniemp").val("");
    $("#tel1emp").val("");
    $("#obsoleto").val(""); setCheckbox('obsoleto','1');
    $("#vender").val(""); setCheckbox('vender','1');

    $("#foto").attr("src", ruta.concat("/servidor/images/nothing.png"));
    $("#btn_remover_imagen").click();
    $("#btn_remover_imagen_nueva").click();
}

function deshabilitarCampos(valor) {
    $("#codemp").prop('readonly', valor);
    $("#diremp").prop('disabled', valor);
    $("#nomemp").prop('disabled', valor);
    $("#ape1emp").prop('disabled', valor);
    $("#fecnac").prop('disabled', valor);
    $("#select2-sexo").prop('disabled', valor);
    $("#email").prop('disabled', valor);
    $("#dniemp").prop('disabled', valor);
    $("#tel1emp").prop('disabled', valor);
    $("#obsoleto").prop('disabled', valor);
    $("#vender").prop('disabled', valor);
    $("#select2-sexo").prop('disabled', valor);

    $("#btn_remover_imagen").prop('disabled', valor);
    $("#imagen_nueva").prop('disabled', valor);

    if (valor) {
        $('.remove-image').removeClass('btn-danger').addClass('btn-secondary');
        $('.edit-image').removeClass('btn-info').addClass('btn-secondary');
    }
    else {
        $('.remove-image').removeClass('btn-secondary').addClass('btn-danger');
        $('.edit-image').removeClass('btn-secondary').addClass('btn-info');
    }
}

function limpiarCamposSucursal() {
    guardarSucursal($("#select2-1").val());//Guardar la Sucursal con la que se Esta trabajando
    cancelar();
}

function inicializar(){
    cancelar();
}

$('#select2-1').select2();
