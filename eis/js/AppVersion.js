/*
 * Validacion de la version de la App
 */

var AppVersion = "1.2.0"
console.log('Buscando version: ');
serverPage = ruta.concat("/servidor/php/version.php");
$.post(serverPage, {"opcion": "consultar"}, function (data) {
        if (data.exito == true) {
            if (AppVersion == data.AppVersion) {
                //alert("Version esta OK");
            }else{
                //swal('Actualización!', 'se requiere de actualizacíon para seguir utilizando la App.', 'success');
                swal({
                    title: 'Actualización!',
                    text: 'Se requiere de actualizacíon para seguir utilizando la App.',
                    type: 'warning',
                    showCancelButton: false,
                    cancelButtonText: 'No,',
                    confirmButtonColor: '#50ad55',
                    confirmButtonText: 'Si, Actualizar',
                    closeOnConfirm: false
                },
                function () {
                    //ir a URL a Actualizar
                    window.location.replace("https://build.phonegap.com/apps/2536280/install/8JsZxb2Eyk8GSEL6Z71g");
                });
                
            }
        }
        else {
            console.log('error');
        };
});

