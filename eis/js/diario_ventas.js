var sucursales = new Array();

$('#bootgrid-basic').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    });



$(function() {
    inicializar();
});

function inicializar() {

    serverPage = ruta.concat("/servidor/php/consul_suc_usuario.php");
    $.post(serverPage, {"session": session}, function (data) {

        if (data.exito == true) {

            $('#select2-2').text(" ");

            for (var i = 0; i < data.nume_regis; i++) {

                if (sucursal.indexOf(data.sucursales_usuario[i]['codsuc']) >= 0) {

                    $('#select2-2').append('<option value="' + data.sucursales_usuario[i]['codsuc'] + '"selected>' + data.sucursales_usuario[i]['codsuc'] + ' ' + data.sucursales_usuario[i]['razemp'] + '</option>');

                } else {

                    $('#select2-2').append('<option value="' + data.sucursales_usuario[i]['codsuc'] + '">' + data.sucursales_usuario[i]['codsuc'] + ' ' + data.sucursales_usuario[i]['razemp'] + '</option>');

                }
                ;


                sucursales[i] = new Array();

                sucursales[i]['codsuc'] = data.sucursales_usuario[i]['codsuc'];

                sucursales[i]['razemp'] = data.sucursales_usuario[i]['razemp'];

                sucursales[i]['moneda'] = data.sucursales_usuario[i]['moneda'];

            }
            ;

        } else {

            //alert('error');

        }
        ;


    }); //Fin .post

    $('#reporte').hide('fast');
    $('#btnNuevoReporte').hide('fast');


    document.getElementById("fecha_ini").value = fecha_1_mes;
    document.getElementById("fecha_fin").value = fecha_actual;

    // Configurar la caja de texto de fecha
    $('#example-datepicker-4')
                .datepicker({
                    format: 'dd/mm/yyyy',
                    autoclose: true,
                    container: '#example-datepicker-container-4'
                });
    // // Configurar combos
    $('#select2-2').select2();

}

$("#btnReporte").click(function (event){

    event.preventDefault();

    getSucursal();

    $datos = $("#formulario-reporte").serialize();



    var valor1 = document.getElementById("fecha_ini").value;

    var valor2 = document.getElementById("fecha_fin").value;

    if( valor1.length == 0 ) {

        //alert('Ingrese fecha desde');

    }else if( valor2  == 0 ){

        //alert('Ingrese fecha fin');

    }else{



        serverPage = ruta.concat("/servidor/php/diario_ventas.php");

        $.post(serverPage, $("#formulario-reporte").serialize(), function (data) {

            if ((data.exito == true) && (data.nume_regis >= 1)){



                $('#formulario').hide('fast');

                $('#reporte').show('fast');

                $('#btnReporte').hide('fast');

                $('#btnNuevoReporte').show('fast');

                var nSuc = 0;

                var iSuc = 0;

                for (var i = 0; i < sucursales.length; i++) {

                    if (document.getElementById("sucursal").value.indexOf(sucursales[i]['codsuc']) >= 0) { iSuc = i; nSuc=nSuc+1;};

                }

                guardarSucursal(document.getElementById("sucursal").value);



                var desSuc = sucursales[iSuc]['razemp'];

                if (nSuc > 1) { desSuc = ""};

                $('#TituloTable1').text(document.getElementById("sucursal").value +" "+desSuc+ " -- desde : " + data.fecha_ini + " hasta : " + data.fecha_fin);



                $("#bootgrid-basic").bootgrid("clear");

                var diasSemana = new Array("Do","Lu","Ma","Mi","Ju","Vi","Sa");

                for (var i = 0; i < data.nume_regis; i++) {

                    var ms = Date.parse(data.ventas[i]['fecfac']);
                    var fecha = new Date(data.ventas[i]['fecfac']); //new Date(ms);
                    var dia = diasSemana[fecha.getUTCDay()];

                    $("#bootgrid-basic").bootgrid().bootgrid("append", [{"tot_dia": dia, "tot_fecha": data.ventas[i]['fecfac'], "tot_ventas": ""+data.ventas[i]['tot_ventas']+"", "tot_tikets": data.ventas[i]['tot_tikets'], "tot_promeven": data.ventas[i]['tot_promeven'], "tot_efectivo": ""+data.tot_efectivo[i]+"", "tot_tarjeta": ""+data.tot_tarjeta[i]+"", "tot_gastos": ""+data.tot_gastos[i]+"", "tot_horcie": ""+data.tot_horcie[i]+""}] );

                };



                $('#totalinf').text('TOTALES  '+sucursales[iSuc]['moneda']+' :');

                $('#totalven').text(data.totalven);

                $('#totaltik').text(data.totaltik);

                $('#promeinf').text('PROMEDIO '+sucursales[iSuc]['moneda']+' :');

                $('#promeven').text(data.promeven);

                $('#prometik').text(data.prometik);

                $('#prometikven').text(data.prometikven);


            }else{//No existen Registros

                toastr.options.positionClass = 'toast-bottom-right';

                toastr['warning']('No existen Datos');

            };

        }); //Fin .post

    }; //fin validacion Campos

});

$("#btnNuevoReporte").click(function (event){

    $('#formulario').show('fast');

    $('#btnReporte').show('fast');

    $('#btnNuevoReporte').hide('fast');

    //$('#tabla1').text(" ");

    $('#reporte').hide('fast');

});

