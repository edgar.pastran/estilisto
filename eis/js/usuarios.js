var urlServerPage = ruta.concat("/servidor/php/usuarios.php");

$(function() {
    cargarCatalogo();
    cargarCatalogoSucursales();
    inicializar();
    consultar_sucursales();
    usuarioGrupos();
});

function cargarCatalogo() {
    $('#bootgrid-usuarios').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        selection: true,
        multiSelect: true,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on("selected.rs.jquery.bootgrid", function(e, rows) {
        $("#idUs").val(rows[0].us_id);
        $("#username").val(rows[0].us_username);

        $("#nombre").val(rows[0].us_nombre);
        $("#apellido").val(rows[0].us_apellido);
        $("#direccion").val(rows[0].us_direccion);
        $("#compania").val(rows[0].us_compania);
        $("#password").val(rows[0].us_password);
        $("#password2").val(rows[0].us_password);
        $("#select2-codgru").val(rows[0].us_codgru);
        $('#select2-codgru').trigger('change');

        $("#administrador").val(rows[0].us_administrador);
        setCheckbox("administrador","X");

        $("#idUs").removeAttr("readonly");
        $("#username").removeAttr("readonly");
        $("#nombre").removeAttr("readonly");
        $("#apellido").removeAttr("readonly");

        $("#direccion").removeAttr("readonly");
        $("#password").removeAttr("readonly");
        $("#password2").removeAttr("readonly");
        $("#compania").removeAttr("readonly");
        $("#administrador").attr("disabled", false);

        $("#select2-codgru").attr("disabled", false);

        $("#opc_usuarios").val("actualizar");

        consultar_accesos();

        $("#closeMus").click();
    })
    .on("deselected.rs.jquery.bootgrid", function(e, rows) {

    });
}

function cargarCatalogoSucursales() {
    $('#bootgrid-sucursal').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        selection: true,
        multiSelect: true,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on("selected.rs.jquery.bootgrid", function(e, rows) {


    })
    .on("deselected.rs.jquery.bootgrid", function(e, rows) {

    });
}

$("#btn-usuario").click(function (event){
    event.preventDefault();
    $("#opc_usuarios").val("consulta");

    $.post(urlServerPage, $("#formulario-usuarios").serialize(), function (data){
        if ( data.exito == true && data.nume_regis >= 1 ){

            $("#bootgrid-usuarios").bootgrid("clear");
            for (var i = 0; i < data.nume_regis; i++) {
                $("#bootgrid-usuarios").bootgrid().bootgrid("append",[{"us_username": ""+data.usuarios[i]['username']+"",
                    "us_nombre": ""+data.usuarios[i]['nombre']+"",
                    "us_apellido": ""+data.usuarios[i]['apellido']+"",
                    "us_administrador": ""+data.usuarios[i]['administrador']+"",
                    "us_administrador": ""+data.usuarios[i]['administrador']+"",
                    "us_direccion": ""+data.usuarios[i]['direccion']+"",
                    "us_compania": ""+data.usuarios[i]['compania']+"",
                    "us_id": ""+data.usuarios[i]['id']+"",
                    "us_password": ""+data.usuarios[i]['password']+"",
                    "us_codgru": ""+data.usuarios[i]['codgru']+""}]);
            };
            //Show Modal
            $('#modal_grupo_usuarios').modal('show');
        };//No hay Datos
    }); //Fin .post

}); //Fin .btn

function eliminar() {
    if ($("#username").val() != "" && $("#opc_usuarios").val() != "insertar") {
        $("#opc_usuarios").val("eliminar");
        guardar();
    }
}

function guardar(){
    //$("#suc_formapago").val($("#select2-1").val());
    if ($("#opc_usuarios").val() != "" && $("#username").val() != "") {
        $.post(urlServerPage, $("#formulario-usuarios").serialize(), function (data) {
            if (data.exito == true){
                if ($("#opc_usuarios").val("insertar")) {
                    swal('USUARIO Creado!', '', 'success');
                }
                if ($("#opc_usuarios").val("eliminar")) {
                    swal('USUARIO Eliminado!', '', 'success');
                }
            }
            else{
                //alert('error');
            }

            if ($("#opc_usuarios").val("actualizar")) {
                actualizar_accesos();
                swal('USUARIO Actualizado!', '', 'success');
            }
            if ($("#opc_usuarios").val("insertar")) {
                $("#idUs").val(data.idUs);
                actualizar_accesos();
            }
            if ($("#opc_usuarios").val("eliminar")) {
                eliminar_accesos();
            }
            cancelar();
        }); //Fin .post
    };
} //Fin de Funcion

function consultar_Usuario(){
    var opc = $("#opc_usuarios").val();
    if (opc == "insertar") {
        $("#opc_usuarios").val("consultar");
        if ($("#opc_usuarios").val() != "" && $("#username").val() != "") {
            $.post(urlServerPage, $("#formulario-usuarios").serialize(), function (data) {
                if (data.exito == true){
                    swal('GRUPO YA Existe!', '', 'error');
                    $("#username").focus();
                }
                else{
                    //alert('error');
                    $("#username").attr("readonly", true);
                }
            }); //Fin .post
        }
    } //Fin del IF
    $("#opc_usuarios").val(opc);
} //Fin de Funcion

function consultar_sucursales(){
    $("#opc_usuarios").val("consulta");
    var urlServerPageAux = ruta.concat("/servidor/php/sucursales.php");

    $.post(urlServerPageAux, $("#formulario-usuarios").serialize(), function (data){
        if ( data.exito == true && data.nume_regis >= 1 ) {
            $("#bootgrid-sucursal").bootgrid("clear");
            for (var i = 0; i < data.nume_regis; i++) {
                $("#bootgrid-sucursal").bootgrid().bootgrid("append",[{"su_codsuc": ""+data.sucursales[i]['codsuc']+"",
                    "su_razemp": ""+data.sucursales[i]['razemp']+"",
                    "su_pais": ""+data.sucursales[i]['pais']+""}]);
            }
        }//No hay Datos
    }); //Fin .post
} //Fin .btn

function limpiar_accesos(){
    var seleccionados = $("#bootgrid-sucursal").bootgrid("getSelectedRows");
    var registros = seleccionados.length;
    for (var i = 0; i < registros; i++) {
        $("#bootgrid-sucursal").bootgrid('deselect', seleccionados[i]['username']);
    };
} //Fin .btn

function consultar_accesos(){
    var opc = $("#opc_usuarios").val();
    $("#opc_usuarios").val("accesos");

    limpiar_accesos();

    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: $("#formulario-usuarios").serialize(),
        success: function(dataAccesos) {
            if ( dataAccesos.exito == true && dataAccesos.nume_regis >= 1 ){
                var seleccion = new Array();
                for (var i = 0; i < dataAccesos.nume_regis; i++) {
                    seleccion[i] = dataAccesos.accesos[i]['codsuc'];
                }
                $("#bootgrid-sucursal").bootgrid('select', seleccion);
            }//No hay Datos
        },
        error: function(data) {
            console.log(JSON.stringify(data));
        }
    });
    $("#opc_usuarios").val(opc);
} //Fin .btn

function actualizar_accesos(){
    var seleccionados = $("#bootgrid-sucursal").bootgrid("getSelectedRows");

    $.post(urlServerPage, {'seleccionados':JSON.stringify(seleccionados),'opcion':'actualizar_accesos','id':$("#idUs").val()}, function (data){

    }); //Fin .post
} //Fin .btn

function eliminar_accesos(){
    var seleccionados = $("#bootgrid-sucursal").bootgrid("getSelectedRows");

    $.post(urlServerPage, {'seleccionados':JSON.stringify(seleccionados),'opcion':'eliminar_accesos','username':$("#username").val()}, function (data){

    }); //Fin .post
} //Fin .btn

function cancelar(){
    inicializar();
    limpiar_accesos();
} //Fin de Funcion

function nuevo(){
    $("#opc_usuarios").val("insertar");
    $("#idUs").val("");
    $("#username").val("");
    $("#nombre").val("");
    $("#apellido").val("");
    $("#direccion").val("");
    $("#compania").val("");
    $("#password").val("");
    $("#password2").val("");
    $("#administrador").val("");
    setCheckbox("administrador","X");

    $("#username").removeAttr("readonly");
    $("#nombre").removeAttr("readonly");
    $("#apellido").removeAttr("readonly");
    $("#direccion").removeAttr("readonly");
    $("#compania").removeAttr("readonly");
    $("#password").removeAttr("readonly");
    $("#password2").removeAttr("readonly");

    $("#administrador").attr("disabled", false);
    $("#username").focus();
    $("#select2-codgru").attr("disabled", false);
    //$("#btn-usuario").attr("disabled", true);
    limpiar_accesos();
} //Fin de Funcion

function inicializar(){
    $("#opc_usuarios").val("");
    $("#idUs").val("");
    $("#username").val("");
    $("#nombre").val("");
    $("#administrador").val("");
    setCheckbox("administrador","X");
    $("#apellido").val("");
    $("#direccion").val("");
    $("#compania").val("");
    $("#password").val("");
    $("#password2").val("");
    $("#select2-codgru").val("");
    $('#select2-codgru').trigger('change');

    $("#username").attr("readonly", true);
    $("#nombre").attr("readonly", true);
    $("#apellido").attr("readonly", true);
    $("#administrador").attr("disabled", true);
    $("#direccion").attr("readonly", true);
    $("#compania").attr("readonly", true);
    $("#password").attr("readonly", true);
    $("#password2").attr("readonly", true);

    $("#btn-usuario").attr("disabled", false);
    $("#select2-codgru").attr("disabled", true);
}