var urlServerPage = ruta.concat("/servidor/php/familias.php");

$(function() {
    usuarioSucursal(inicializar);
    cargarCatalogo();
});

function cargarCatalogo() {
    $('#bootgrid-familias').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        selection: true,
        multiSelect: false,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on("selected.rs.jquery.bootgrid", function(e, rows) {
        $("#codfam1").val(rows[0].fam_codfam1);
        $("#desfam1").val(rows[0].fam_desfam1);
        $("#orden").val(rows[0].fam_orden);
        $("#puntos").val(rows[0].fam_puntos);
        if (rows[0].fam_foto != "") {
            $("#foto").attr("src", rows[0].fam_foto);
        }
        $("#vender").val(rows[0].fam_vender);
        setCheckbox("vender","1");
        $("#obsoleto").val(rows[0].fam_obsoleto);
        setCheckbox("obsoleto","1");

        $("#opc_familia").val("actualizar");

        $("#closeMfam").click();

        //Quitar ReadOnly
        $("#codfam1").attr("readonly" , true);
        $("#desfam1").removeAttr("readonly");
        $("#orden").removeAttr("readonly");
        $("#puntos").removeAttr("readonly");
        $("#vender").removeAttr("disabled");
        $("#obsoleto").removeAttr("disabled");
        $('.remove-image').removeClass('btn-secondary').addClass('btn-danger');
        $("#btn_remover_imagen").attr("disabled", false);
        $('.edit-image').removeClass('btn-secondary').addClass('btn-info');
        $("#imagen_nueva").attr("disabled", false);
    })
    .on("deselected.rs.jquery.bootgrid", function(e, rows) {

    });
}

$("#btn-familias").click(function (event) {
    event.preventDefault();
    $("#suc_familia").val($("#select2-1").val());
    $("#opc_familia").val("consulta");

    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: $("#formulario-familia").serialize(),
        success: function (data) {
            if (data.exito == true && data.nume_regis >= 1) {
                if (data.nume_regis >= 1) {
                    $("#bootgrid-familias").bootgrid("clear");
                    for (var i = 0; i < data.nume_regis; i++) {
                        $("#bootgrid-familias").bootgrid().bootgrid("append", [{
                            "fam_codfam1": "" + data.familias[i]['codfam1'] + "",
                            "fam_desfam1": "" + data.familias[i]['desfam1'] + "",
                            "fam_orden": data.familias[i]['orden'],
                            "fam_puntos": data.familias[i]['puntos'],
                            "fam_vender": "" + data.familias[i]['vender'] + "",
                            "fam_obsoleto": "" + data.familias[i]['obsoleto'] + "",
                            "fam_foto": "" + data.familias[i]['foto'] + ""
                        }]);
                    }
                    ;
                    //Show Modal
                    $('#modal_familias').modal('show');
                }
                ;//No hay Datos
            }
            else {
                console.log("SIN EXITO: " + JSON.stringify(data));
            }
        },
        error: function (respuesta) {
            console.log("ERROR: " + JSON.stringify(respuesta));
        }
    });
}); //Fin .btn

$("#btn_remover_imagen").click(function (event) {
    if ($("#codfam1").val() != "" && $("#opc_familia").val() != "insertar") {
        $("#opc_familia").val("remover_imagen");
        guardar();
    }
    else {
        $("#foto").attr("src", ruta.concat("/servidor/images/nothing.png"));
    }
});

function eliminar() {
    if ($("#codfam1").val() != "" && $("#opc_familia").val() != "insertar") {
        $("#opc_familia").val("eliminar");
        guardar();
    }
}

function guardar() {
    $("#suc_familia").val($("#select2-1").val());
    if ($("#select2-1").val() != "" && $("#opc_familia").val() != "" && $("#codfam1").val() != "") {
        $.ajax({
            type: "POST",
            url: urlServerPage,
            data: new FormData($("#formulario-familia")[0]),
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.exito == true) {
                    if ($("#opc_familia").val() == "actualizar") {
                        swal('Familia Actualizada!', '', 'success');
                    }
                    else if ($("#opc_familia").val() == "insertar") {
                        swal('Familia Creada!', '', 'success');
                    }
                    else if ($("#opc_familia").val() == "eliminar") {
                        swal('Familia Eliminada!', '', 'success');
                    }
                    else if ($("#opc_familia").val() == "remover_imagen") {
                        swal('Imagen Eliminada!', '', 'success');
                    }
                    cancelar();
                }
                else {//alert('error');
                    swal(data.mensaje, '', 'error');
                    console.log("SIN EXITO: " + JSON.stringify(data));
                }
            },
            error: function (respuesta) {
                swal(JSON.stringify(respuesta), '', 'error');
                console.log("ERROR: " + JSON.stringify(respuesta));
            }
        });
    }
    ;
} //Fin de Funcion

function consultar_familia() {
    $("#suc_familia").val($("#select2-1").val());
    var opc = $("#opc_familia").val();
    if (opc == "insertar") {
        $("#opc_familia").val("consultar");
        if ($("#select2-1").val() != "" && $("#opc_familia").val() != "" && $("#codfam1").val() != "") {
            $.post(urlServerPage, $("#formulario-familia").serialize(), function (data) {
                if (data.exito == true) {
                    swal('Familia YA Existe!', '', 'error');
                    $("#codfam1").focus();
                }
                else {
                    //alert('error');
                    $("#codfam1").attr("readonly", true);
                }
            }); //Fin .post
        }
    } //Fin del IF
    $("#opc_familia").val(opc);
} //Fin de Funcion

function cancelar() {
    guardarSucursal($("#select2-1").val());//Guardar la Sucursal con la que se Esta trabajando
    $("#opc_familia").val("");
    $("#codfam1").val("");
    $("#desfam1").val("");
    $("#orden").val("");
    $("#puntos").val("");
    $("#vender").val("");
    setCheckbox("vender", "1");
    $("#obsoleto").val("");
    setCheckbox("obsoleto", "1");
    $("#foto").attr("src", ruta.concat("/servidor/images/nothing.png"));
    $("#btn_remover_imagen").click();
    $("#btn_remover_imagen_nueva").click();

    $("#codfam1").attr("readonly", true);
    $("#desfam1").attr("readonly", true);
    $("#orden").attr("readonly", true);
    $("#puntos").attr("readonly", true);
    $("#vender").attr("disabled", true);
    $("#obsoleto").attr("disabled", true);

    $('.remove-image').removeClass('btn-danger').addClass('btn-secondary');
    $("#btn_remover_imagen").attr("disabled", true);
    $('.edit-image').removeClass('btn-info').addClass('btn-secondary');
    $("#imagen_nueva").attr("disabled", true);
} //Fin de Funcion

function nuevo() {
    if ($("#select2-1").val() != "") {
        $("#opc_familia").val("insertar");
        $("#codfam1").val("");
        $("#desfam1").val("");
        $("#orden").val("");
        $("#puntos").val("");
        $("#vender").val("");
        setCheckbox("vender", "1");
        $("#obsoleto").val("");
        setCheckbox("obsoleto", "1");

        $("#codfam1").removeAttr("readonly");
        $("#codfam1").focus();
        $("#desfam1").removeAttr("readonly");
        $("#orden").removeAttr("readonly");
        $("#puntos").removeAttr("readonly");
        $("#vender").removeAttr("disabled");
        $("#obsoleto").removeAttr("disabled");

        $('.remove-image').removeClass('btn-secondary').addClass('btn-danger');
        $("#btn_remover_imagen").attr("disabled", false);
        $('.edit-image').removeClass('btn-secondary').addClass('btn-info');
        $("#imagen_nueva").attr("disabled", false);
    }
    else {
        $("#select2-1").focus();
        swal('Seleccione Sucursal!');

    }
} //Fin de Funcion

function inicializar() {
    $("#suc_familia").val("");
    $("#opc_familia").val("");
    $("#codfam1").val("");
    $("#desfam1").val("");
    $("#orden").val("");
    $("#puntos").val("");
    $("#vender").val("");
    setCheckbox("vender", "1");
    $("#obsoleto").val("");
    setCheckbox("obsoleto", "1");
    $("#foto").attr("src", ruta.concat("/servidor/images/nothing.png"));

    $('.remove-image').removeClass('btn-danger').addClass('btn-secondary');
    $("#btn_remover_imagen").attr("disabled", true);
    $('.edit-image').removeClass('btn-info').addClass('btn-secondary');
    $("#imagen_nueva").attr("disabled", true);
}

$('#select2-1').select2();