var urlServerPage = ruta.concat("/servidor/php/proveedores.php");

$(function() {
    usuarioSucursal(formapagos);
    cargarCatalogo();
    inicializar();
});

function cargarCatalogo() {
    $('#bootgrid-proveedoresDM').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        selection: true,
        multiSelect: false,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on("selected.rs.jquery.bootgrid", function(e, rows) {
        $("#codpro").val(rows[0].pro_codpro);
        $("#razon").val(rows[0].pro_razon);
        $("#tel2pro").val(rows[0].pro_tel2pro);
        $("#tel1pro").val(rows[0].pro_tel1pro);
        $("#tippro").val(rows[0].pro_tippro);
        $("#mailpro").val(rows[0].pro_mailpro);
        $("#nifpro").val(rows[0].pro_nifpro);
        $("#percon").val(rows[0].pro_percon);
        $("#select2-pais").val(rows[0].pro_pais); $('#select2-pais').trigger('change');
        $("#codfp").val(rows[0].pro_codfp);
        $("#obsoleto").val(rows[0].pro_obsoleto); setCheckbox('obsoleto','1');

        $("#opcion").val("actualizar");

        $("#closeMpro").click();
        deshabilitarCampos(false);
        $("#codpro").prop('readonly', true);

    })
    .on("deselected.rs.jquery.bootgrid", function(e, rows) {

    });
}

$("#btn-empleados").click(function (event){
    event.preventDefault();
    $("#codsuc").val($("#select2-1").val());
    $("#opcion").val("consulta");

    $.post(urlServerPage, $("#formulario-proveedor").serialize(), function (data){
        if ( data.exito == true && data.nume_regis >= 1 ){

            if (data.nume_regis >= 1) {
                $("#bootgrid-proveedoresDM").bootgrid("clear");
                for (var i = 0; i < data.nume_regis; i++) {

                    $("#bootgrid-proveedoresDM").bootgrid().bootgrid("append", [{"pro_codpro": ""+data.proveedores[i]['codpro']+"", "pro_razon": ""+data.proveedores[i]['razon']+"", "pro_nifpro": ""+data.proveedores[i]['nifpro']+"", "pro_tel2pro": ""+data.proveedores[i]['tel2pro']+"", "pro_tel1pro": ""+data.proveedores[i]['tel1pro']+"", "pro_mailpro": ""+data.proveedores[i]['mailpro']+"", "pro_percon": ""+data.proveedores[i]['percon']+"", "pro_pais": ""+data.proveedores[i]['pais']+"", "pro_tippro": ""+data.proveedores[i]['tippro']+"", "pro_codfp": ""+data.proveedores[i]['forpagpro']+"", "pro_obsoleto": ""+data.proveedores[i]['obsoleto']+""}] );

                };
                //Show Modal
                $('#modal_proveedores').modal('show');
            };//No hay Datos
        };
    }); //Fin .post
}); //Fin .btn

function eliminar() {
    $("#opcion").val("eliminar");
    guardar();
}

function guardar(){
    if ($('#formulario-proveedor').valid() == false) {
        //swal('Complete Campos!', '', 'error');
    }else{
        myCheckbox('obsoleto','1');
        $("#codsuc").val($("#select2-1").val());
        if ($("#select2-1").val() != "" && $("#opcion").val() != "") {
            $.post(urlServerPage, $("#formulario-proveedor").serialize(), function (data) {
                if (data.exito == true){
                    if ($("#opcion").val() == "actualizar") {
                        swal('PROVEEDOR Actualizado!', '', 'success');
                    }
                    if ($("#opcion").val() == "insertar") {
                        swal('PROVEEDOR Creado!', '', 'success');
                    }
                    if ($("#opcion").val() == "eliminar") {
                        swal('PROVEEDOR Eliminado!', '', 'success');
                    }

                    cancelar();

                }
                else{//alert('error');
                    if ($("#opcion").val() == "eliminar") {
                        cancelar();
                    }
                };
            }); //Fin .post
        };
    };
} //Fin de Funcion

function consultar_proveedor(){
    $("#codsuc").val($("#select2-1").val());
    var opc = $("#opcion").val();
    if (opc == "insertar") {
        $("#opcion").val("consultar");
        if ($("#select2-1").val() != "" && $("#opcion").val() != "" && $("#codpro").val() != "") {
            $.post(urlServerPage, $("#formulario-proveedor").serialize(), function (data) {
                if (data.exito == true){1
                    swal('PROVEEDOR YA Existe!', '', 'error');
                    $("#codpro").focus();
                }
                else{//alert('error');
                    $("#codpro").prop('readonly', true);
                }
            }); //Fin .post
        }
    } //Fin del IF
    $("#opcion").val(opc);
} //Fin de Funcion

$(document).on('click', '#btn-cancelar', function(e) {
    if ( $('#opcion').val() == "insertar" || $('#opcion').val() == "actualizar") {
        e.preventDefault();
        swal({
                title: '¿Estas seguro?',
                text: 'Si cancelas no se guardaran los datos que has ingresado!',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'No, me arrepenti',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Si, cancelarlo',
                closeOnConfirm: false
            },
            function () {
                swal('Cancelado!', 'La Creacion de Proveedor ha sido cancelada.', 'success');
                cancelar();
            });
    }
});

$(document).on('click', '#btn-eliminar', function(e) {
    if ($('#codpro').val() != "" && $('#opcion').val() != "insertar") {
        e.preventDefault();
        swal({
                title: '¿Estas seguro?',
                text: 'Si Eliminas se borraran los datos del Proveedor!',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'No, me arrepenti',
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Si, eliminar',
                closeOnConfirm: false
            },
            function () {
                swal('Eliminado!', 'El Proveedor ha sido Eliminado.', 'success');
                eliminar();
            });
    }
});

function cancelar(){
    // Configurar la caja de texto de fecha
    var validator = $( "#formulario-proveedor" ).validate();
    validator.resetForm();

    guardarSucursal($("#select2-1").val());//Guardar la Sucursal con la que se Esta trabajando
    limpiarDatos();

    deshabilitarCampos(true);

    $("#btn-empleados").prop('disabled', false);
} //Fin de Funcion

function nuevo(){
    if ($("#select2-1").val() != "") {
        $("#opcion").val("insertar");
        limpiarDatos();

        deshabilitarCampos(false);
        $("#codfp").val("EFECTIVO");
        $("#codpro").focus();

    }else{
        $("#select2-1").focus();
        swal('Seleccione Sucursal!');

    }
} //Fin de Funcion

function limpiarDatos() {
    $("#tippro").val("");
    $("#codpro").val("");
    $("#percon").val("");
    $("#razon").val("");
    $("#tel2pro").val("");
    $("#mailpro").val("");
    $("#nifpro").val("");
    $("#tel1pro").val("");
    $("#select2-pais").val(""); $('#select2-pais').trigger('change');
    $("#codfp").val("");
    $("#obsoleto").val(""); setCheckbox('obsoleto','1');
}

function deshabilitarCampos(valor) {
    $("#codpro").prop('readonly', valor);
    $("#percon").prop('disabled', valor);
    $("#razon").prop('disabled', valor);
    $("#tel2pro").prop('disabled', valor);
    $("#mailpro").prop('disabled', valor);
    $("#nifpro").prop('disabled', valor);
    $("#tel1pro").prop('disabled', valor);
    $("#select2-pais").prop('disabled', valor);
    $("#codfp").prop('disabled', valor);
    $("#obsoleto").prop('disabled', valor);
}

function limpiarCamposSucursal() {
    guardarSucursal($("#select2-1").val());//Guardar la Sucursal con la que se Esta trabajando
    cancelar();
    formapagos('EFECTIVO');
}

function inicializar(){
    cancelar();
}

$('#select2-1').select2();
