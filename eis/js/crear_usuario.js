$(function() {
    $(formWizard);
});

function formWizard() {

    if (!$.fn.steps) return;
    if (!$.fn.validate) return;

    var form = $('#form-crear-usuario');

    form.validate({
        errorPlacement: errorPlacementInput,
        rules: {
            confirm: {
                equalTo: '#password'
            }
        }
    });

    form.children('div').steps({
        headerTag: 'h4',
        bodyTag: 'fieldset',
        transitionEffect: 'slideLeft',
        onStepChanging: function(/*event, currentIndex, newIndex*/) {
            form.validate().settings.ignore = ':disabled,:hidden';
            return form.valid();
        },
        onFinishing: function(/*event, currentIndex*/) {
            form.validate().settings.ignore = ':disabled';
            return form.valid();
        },
        onFinished: function(/*event, currentIndex*/) {
            //Fin y Grabar
            serverPage = ruta.concat("/servidor/php/crear_usuario.php");
            $.post(serverPage, $("#form-crear-usuario").serialize(), function (data) {
                if (data.exito == true) {


                    swal('Usuario Creado!', '', 'success');
                }else{
                    swal('email Invalidao!', '', 'error');

                    document.getElementById("email").value = "";
                    document.getElementById("password").value = "";
                    document.getElementById("confirm").value = "";
                    document.getElementById("confirm").focus();
                };
            });
        }
    });

}