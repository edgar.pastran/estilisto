var urlServerPage = ruta.concat("/servidor/php/tickets.php");

$(function() {
    $.fn.editable.defaults.mode = 'inline';

    usuarioSucursal(inicializarDatosSucursal);

    $('.modal').on('shown.bs.modal', function (e) {
        $('body').addClass('modal-open');
    });
});

$(document).on('change', '#select2-1', function(e) {
    e.preventDefault();
    inicializarDatosSucursal();
    $('#ticket_container').hide();
    $('#dashboard_container').show();
});

function inicializarDatosSucursal() {
    cargarDatosSucursal();
    prepararNuevoTicket();
    cargarFormasDePago();
    cargarEmpleados();
    cargarModalClientes();
    cargarModalServicios();
    cargarModalEmpleados();
}

function prepararNuevoTicket() {
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        var params = {"sucursal": codsuc, "operacion": "proximo_ticket"};
        $.post(urlServerPage, params, function (data) {
            if (data.exito == true) {
                // $('#ticket_numero').html(data.numero_ticket);
                $('#ticket_observaciones').val("");
                if (sucursal_data.hasOwnProperty(codsuc)) {
                    $('#ticket_timezone').val(sucursal_data[codsuc].timezone);
                    $('#cliente_codigo').val(sucursal_data[codsuc].cliente.codigo);
                    $('#cliente_dni').val(sucursal_data[codsuc].cliente.dni);
                    $('#cliente_nombre').val(sucursal_data[codsuc].cliente.fullname);
                    $('#cliente_forma_pago').val(sucursal_data[codsuc].cliente.forma_pago);
                    $('#cliente_permite_deuda').val(sucursal_data[codsuc].cliente.permite_deuda);
                    $('#empleado_codigo').val(sucursal_data[codsuc].empleado.codigo);
                    $('#empleado_nombre').val(sucursal_data[codsuc].empleado.fullname);
                    cargarFechaHora();
                    resetearLineas();
                }
            }
            else {
                console.log('error');
            }
        });
    }
}

function cargarFechaHora() {
    var timezone = $('#ticket_timezone').val();
    var fecha = moment.tz(new Date(), timezone).format('DD/MM/YYYY');
    var hora = moment.tz(new Date(), timezone).format('hh:mm A');
    $('#ticket_fecha').html(fecha);
    $('#ticket_fecha_editable').html(fecha);
    $('#ticket_hora').html(hora);
    $('#ticket_hora_editable').html(hora);
    //setTimeout("cargarFechaHora()", 1000 * 60);
    cargarDashboardSucursal();
    configurarVistaPorSucursal();
}

function resetearLineas() {
    $.each($('#ticket_lineas > tbody > tr'), function(i, row) {
        $(row).remove();
    });
    calcularTotal();
}

function calcularTotal() {
    var codsuc = $('#select2-1').val();
    var total_base_imponible = 0;
    var total_impuesto = 0;
    $.each($('#ticket_lineas > tbody > tr'), function(i, row) {
        var cantidad = parseInt($(this).find('.quantity > .canart').val());
        var base_imponible = parseFloat($(this).find('.impuesto > .basimpart').val());
        var impuesto = parseFloat($(this).find('.impuesto > .monimpart').val());
        total_base_imponible += parseFloat(base_imponible) * parseInt(cantidad);
        total_impuesto += parseFloat(impuesto) * parseInt(cantidad);
    });
    $('#base_imponible_ticket_format').html(sucursal_data[codsuc].moneda+" "+$.number( total_base_imponible, 2, ',', '.' ));
    $('#impuesto_ticket_format').html(sucursal_data[codsuc].moneda+" "+$.number( total_impuesto, 2, ',', '.' ));
    var total = total_base_imponible + total_impuesto;
    total = parseFloat($.number( total, 2, '.', '' ));
    $('#total_ticket').val(total);
    $('#total_ticket_format').html(sucursal_data[codsuc].moneda+" "+$.number( total, 2, ',', '.' ));
}

var sucursal_data = [];
function cargarDatosSucursal() {
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        if (!sucursal_data.hasOwnProperty(codsuc)) {
            var params = {"sucursal": codsuc, "operacion": "sucursal_data"};
            $.post(urlServerPage, params, function (data) {
                if (data.exito == true) {
                    sucursal_data[codsuc] = {
                        timezone: data.sucursal.timezone,
                        moneda: data.sucursal.moneda,
                        impuesto_incluido: data.sucursal.impuesto_incluido,
                        pedir_cliente: data.sucursal.pedir_cliente,
                        codigo_alternativo_cliente: data.sucursal.codigo_alternativo_cliente,
                        modificar_fecha: data.sucursal.modificar_fecha,
                        empleado_linea: data.sucursal.empleado_linea,
                        no_empleado_crear: data.sucursal.no_empleado_crear,
                        ocultar_totales_diarios: data.sucursal.ocultar_totales_diarios,
                        segundos_mostrar_totales_diarios: data.sucursal.segundos_mostrar_totales_diarios,
                        cliente: data.cliente,
                        empleado: data.empleado
                    };
                }
                else {
                    console.log(data);
                }
            });
        }
    }
}

var dashboards = [];
function cargarDashboardSucursal() {
    $('#dashboard_container').html('');
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        if (!dashboards.hasOwnProperty(codsuc)) {
            var url = ruta.concat("/servidor/views/dashboard_tickets.php");
            var params = {"codsuc": codsuc, "fecha": $('#ticket_fecha').html()};
            $.post(url, params, function (data) {
                $('#dashboard_container').html(data);
                $('#'+codsuc+'-dashboard-fecha').html($('#ticket_fecha').html());
                ocultarTotales();
                dashboards[codsuc] = $('#dashboard_container').html();
            });
        }
        else {
            $('#dashboard_container').html(dashboards[codsuc]);
            var params = {"sucursal": codsuc, "operacion": "dashboard_data", "fecha": $('#ticket_fecha').html()};
            $.ajax({
                type: "POST",
                url: urlServerPage,
                data: params,
                success: function(data) {
                    if (data.exito == true) {
                        //console.log(JSON.stringify(data));
                        for (i=0; i<data.data_empleados.length; i++) {
                            var empleado = data.data_empleados[i];
                            $('#'+codsuc+'-'+empleado.codemp+'-tickets').html(empleado.tickets);
                            $('#'+codsuc+'-'+empleado.codemp+'-tickets').attr("title", empleado.tickets+" Tickets");
                            $('#'+codsuc+'-'+empleado.codemp+'-ventas').html(empleado.ventas);
                            $('#'+codsuc+'-'+empleado.codemp+'-ventas').attr("title", (empleado.moneda!=""?empleado.moneda+" ":"")+empleado.ventas);
                            if (parseInt(empleado.tickets) > 0) {
                                $('#'+codsuc+'-'+empleado.codemp+'-data').show();
                            }
                            else {
                                $('#'+codsuc+'-'+empleado.codemp+'-data').hide();
                            }
                        }
                        $('#'+codsuc+'-dashboard-fecha').html($('#ticket_fecha').html());
                        $('#'+codsuc+'-dashboard-ventas').html(data.total_ventas);
                        $('#'+codsuc+'-dashboard-tickets').html(data.total_tickets);
                        ocultarTotales();
                        dashboards[codsuc] = $('#dashboard_container').html();
                    }
                    else {
                        console.log(data);
                    }
                },
                error: function(data) {
                    console.log(JSON.stringify(data));
                }
            });
        }
    }
}

function ocultarTotales() {
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        if (sucursal_data[codsuc].ocultar_totales_diarios == 'X') {
            $('#' + codsuc + '-dashboard-ventas').addClass("invisible");
            $('#' + codsuc + '-dashboard-tickets').addClass("invisible");
            $('#' + codsuc + '-dashboard-tarjeta-ventas').addClass("cursor-puntero");
            $('#' + codsuc + '-dashboard-tarjeta-tickets').addClass("cursor-puntero");
            if (sucursal_data[codsuc].segundos_mostrar_totales_diarios > 0) {
                $('#' + codsuc + '-dashboard-tarjeta-ventas').click(function () {
                    $('#' + codsuc + '-dashboard-ventas').removeClass("invisible");
                    var setInvisible = function () {
                        $('#' + codsuc + '-dashboard-ventas').addClass("invisible");
                    }
                    setTimeout(setInvisible, sucursal_data[codsuc].segundos_mostrar_totales_diarios * 1000);
                });
                $('#' + codsuc + '-dashboard-tarjeta-tickets').click(function () {
                    $('#' + codsuc + '-dashboard-tickets').removeClass("invisible");
                    var setInvisible = function () {
                        $('#' + codsuc + '-dashboard-tickets').addClass("invisible");
                    }
                    setTimeout(setInvisible, sucursal_data[codsuc].segundos_mostrar_totales_diarios * 1000);
                });
            }
            else {
                $('#' + codsuc + '-dashboard-tarjeta-ventas').click(function () {
                    ($('#' + codsuc + '-dashboard-ventas').hasClass("invisible")) ? $('#' + codsuc + '-dashboard-ventas').removeClass("invisible") : $('#' + codsuc + '-dashboard-ventas').addClass("invisible");
                });
                $('#' + codsuc + '-dashboard-tarjeta-tickets').click(function () {
                    ($('#' + codsuc + '-dashboard-tickets').hasClass("invisible")) ? $('#' + codsuc + '-dashboard-tickets').removeClass("invisible") : $('#' + codsuc + '-dashboard-tickets').addClass("invisible");
                });
            }
        }
    }
}

function configurarVistaPorSucursal() {
    var codsuc = $('#select2-1').val();
    if (codsuc != "" && sucursal_data.hasOwnProperty(codsuc)) {
        if (sucursal_data[codsuc].modificar_fecha == 'X') {
            $('#ticket_fecha_editable').editable({
                type: 'combodate',
                title: 'Fecha',
                value: $('#ticket_fecha_editable').html(),
                format: "DD/MM/YYYY",
                viewformat: "DD/MM/YYYY",
                template: "DD/MM/YYYY",
                success: function (response, value) {
                    $('#ticket_fecha').html(value.format('DD/MM/YYYY'));
                }
            });
            $('#ticket_fecha_editable').editable('setValue', $('#ticket_fecha_editable').html(), true);
            $('#ticket_fecha_editable').removeClass('editable-unsaved');

            $('#ticket_hora_editable').editable({
                type: 'combodate',
                title: 'Hora',
                value: $('#ticket_hora_editable').html(),
                format: "hh:mm A",
                viewformat: "hh:mm A",
                template: "hh:mm A",
                success: function (response, value) {
                    $('#ticket_hora').html(value.format('hh:mm A'));
                }
            });
            $('#ticket_hora_editable').editable('setValue', $('#ticket_hora_editable').html(), true);
            $('#ticket_hora_editable').removeClass('editable-unsaved');

            $('#ticket_fecha').hide();
            $('#ticket_hora').hide();
            $('#ticket_fecha_editable').show();
            $('#ticket_hora_editable').show();
        }
        else {
            $('#ticket_fecha_editable').hide();
            $('#ticket_hora_editable').hide();
            $('#ticket_fecha').show();
            $('#ticket_hora').show();
        }
    }
}

var formas_pago = {};
function cargarFormasDePago() {
    var codsuc = $('#select2-1').val();
    if (codsuc != "" && !formas_pago.hasOwnProperty(codsuc)) {
        var params = {"sucursal": codsuc, "operacion": "formas_pago"};
        $.post(urlServerPage, params, function (data) {
            if (data.exito == true) {
                formas_pago[codsuc] = [];
                for (i=0; i<data.formas_pago.length; i++) {
                    var forma_pago = {
                        value: data.formas_pago[i].codfp,
                        text: data.formas_pago[i].des
                    }
                    formas_pago[codsuc].push(forma_pago);
                }
            }
            else {
                console.log(data);
            }
        });
    }
}

var empleados = {};
function cargarEmpleados() {
    var codsuc = $('#select2-1').val();
    if (codsuc != "" && !empleados.hasOwnProperty(codsuc)) {
        var params = {"sucursal": codsuc, "operacion": "empleados"};
        $.post(urlServerPage, params, function (data) {
            if (data.exito == true) {
                empleados[codsuc] = [];
                for (i=0; i<data.empleados.length; i++) {
                    var empleado = {
                        value: data.empleados[i].codemp,
                        text: data.empleados[i].fullname
                    }
                    empleados[codsuc].push(empleado);
                }
            }
            else {
                console.log(data);
            }
        });
    }
}

var modal_clientes = {};
function cargarModalClientes() {
    var codsuc = $('#select2-1').val();
    if (codsuc != "" && !modal_servicios.hasOwnProperty(codsuc)) {
        var url = ruta.concat("/servidor/views/modal_clientes.php");
        $.post(url, {"codsuc": codsuc}, function (data) {
            modal_clientes[codsuc] = data;
        });
    }
}

var modal_servicios = {};
function cargarModalServicios() {
    var codsuc = $('#select2-1').val();
    if (codsuc != "" && !modal_servicios.hasOwnProperty(codsuc)) {
        var url = ruta.concat("/servidor/views/modal_servicios.php");
        $.post(url, {"codsuc": codsuc}, function (data) {
            modal_servicios[codsuc] = data;
        });
    }
}

var modal_empleados = {};
function cargarModalEmpleados() {
    var codsuc = $('#select2-1').val();
    if (codsuc != "" && !modal_empleados.hasOwnProperty(codsuc)) {
        var url = ruta.concat("/servidor/views/modal_empleados.php");
        $.post(url, {"codsuc": codsuc}, function (data) {
            modal_empleados[codsuc] = data;
        });
    }
}

$(document).on('click', '#btnBuscarCliente', function(e) {
    e.preventDefault();
    var codsuc = $('#select2-1').val();
    if (codsuc == "") {
        swal('Error', 'Escoja una sucursal primero', 'error');
    }
    else if (sucursal_data[codsuc].codigo_alternativo_cliente == "X") {
        $('#codigo_alternativo_cliente').val("");
        $('#modal_codigo_alternativo_clientes').modal('show');
    }
    else {
        $('#btnBuscarCliente2').click();
    }
});

$(document).on('click', '#btnBuscarCliente2', function(e) {
    e.preventDefault();
    var codsuc = $('#select2-1').val();
    $("#modal_codigo_alternativo_clientes .close").click();
    if (modal_clientes.hasOwnProperty(codsuc)) {
        $('#modal_clientes').find('.modal-body').html(modal_clientes[codsuc]);
        $('#modal_clientes').modal('show');
    }
    else {
        cargarModalClientes();
        $('#modal_clientes').find('.modal-body').html(modal_clientes[codsuc]);
        $('#modal_clientes').modal('show');
    }
});

$(document).on('click', '#btnBuscarCodigoAlternativo', function(e) {
    e.preventDefault();
    buscarClientePorCodigoAlternativo();
});

$(document).on('keypress', '#codigo_alternativo_cliente', function(e) {
    var key = e.keyCode;
    if ($.inArray(key, [13]) !== -1)  {
        buscarClientePorCodigoAlternativo();
    }
});

function buscarClientePorCodigoAlternativo() {
    var codsuc = $('#select2-1').val();
    if (codsuc != "") {
        var codigo_alternativo = $('#codigo_alternativo_cliente').val();
        var params = {"sucursal": codsuc, "codigo_alternativo": codigo_alternativo, "operacion": "cliente_codigo_alternativo"};
        $.post(urlServerPage, params, function (data) {
            if (data.exito == true) {
                $('#cliente_codigo').val(data.cliente.codigo);
                $('#cliente_dni').val(data.cliente.dni);
                $('#cliente_nombre').val(data.cliente.fullname);
                $('#cliente_forma_pago').val(data.cliente.forma_pago);
                $('#cliente_permite_deuda').val(data.cliente.permite_deuda);
                $("#modal_codigo_alternativo_clientes .close").click();
            }
            else {
                swal('Error', 'Cliente no encontradoo', 'error');
            }
        });
    }
}

$(document).on('click', '#btnBuscarEmpleado', function(e) {
    e.preventDefault();
    var codsuc = $('#select2-1').val();
    if (codsuc == "") {
        swal('Error', 'Escoja una sucursal primero', 'error');
    }
    else {
        $('#modal_empleados_opener').val('cabecera');
        if (modal_empleados.hasOwnProperty(codsuc)){
            $('#modal_empleados').find('.modal-body').html(modal_empleados[codsuc]);
            $('#modal_empleados').modal('show');
        }
        else {
            cargarModalEmpleados();
            $('#modal_empleados').find('.modal-body').html(modal_empleados[codsuc]);
        }
    }
});

var linea_factura = {};
$(document).on('click', '#btnAgregarLinea', function(e) {
    e.preventDefault();
    var codsuc = $('#select2-1').val();
    if (codsuc == "") {
        swal('Error', 'Escoja una sucursal primero', 'error');
    }
    else {
        linea_factura['codsuc'] = codsuc;
        if (modal_servicios.hasOwnProperty(codsuc)){
            //$('#modal_servicios').find('.modal-body').load($(this).attr('href')+"?codsuc="+codsuc);
            //$('#modal_servicios').removeData();
            $('#modal_servicios').find('.modal-body').html(modal_servicios[codsuc]);
        }
        else {
            cargarModalServicios();
            $('#modal_servicios').find('.modal-body').html(modal_servicios[codsuc]);
        }
    }
});

$(document).on('click', '#btnGuardarTicketLinea', function(e) {
    e.preventDefault();
    var id_linea = $('#editar_ticket_linea_id').val();
    var cantidad = $('#canart_editar').val();
    var empleado = $('#codemp_editar').val();
    var precio = $('#preart_editar').val();
    var descuento_porcentaje = $('#pordesart_editar').val();
    var descuento_monto = $('#mondesart_editar').val();
    var impuesto_porcentaje = $('#porimpart_editar').val();
    var impuesto_monto = $('#monimpart_editar').val();
    var base_imponible = $('#basimpart_editar').val();
    var subtotal = $('#subtotal_editar').val();

    $('#'+id_linea).find('.quantity .canart').val(cantidad);
    $('#'+id_linea+' .cantidad').editable('option','value',cantidad);
    $('#'+id_linea).find('.employee .codemp').val(empleado);
    $('#'+id_linea+' .empleado').editable('option','value',empleado);
    $('#'+id_linea).find('.price .preart').val(precio);
    $('#'+id_linea+' .preven').editable('option','value',precio);
    $('#'+id_linea).find('.descuento .pordesart').val(descuento_porcentaje);
    $('#'+id_linea+' .desven').editable('option','value',descuento_porcentaje);
    $('#'+id_linea).find('.descuento .mondesart').val(descuento_monto);
    $('#'+id_linea).find('.impuesto .porimpart').val(impuesto_porcentaje);
    $('#'+id_linea).find('.impuesto .monimpart').val(impuesto_monto);
    $('#'+id_linea).find('.impuesto .basimpart').val(base_imponible);
    $('#'+id_linea).find('.price .subtotal').val(subtotal);
    $('#'+id_linea+' .subtotal_line').html($('#editar_ticket_linea_moneda').val()+" "+$.number( subtotal, 2, ',', '.' ));
    $('#modal_ticket_linea .close').click();
    calcularTotal();
});

$(document).on('click', '#btnCancelarTicket', function(e) {
    e.preventDefault();
    if ($('#ticket_container').is(':visible')) {
        swal({
            title: 'Estas seguro?',
            text: 'Si cancelas este ticket no se guardaran los datos que has ingresado!',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'No, me arrepenti',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Si, cancelarlo',
            closeOnConfirm: false
        },
        function () {
            swal('Cancelado!', 'Tu ticket ha sido cancelado.', 'success');
            resetearVista();
        });
    }
});

function resetearVista() {
    inicializarDatosSucursal();
    $('#ticket_container').hide();
    $('#dashboard_container').show();
}

$(document).on('click', '#btnCrearTicket', function(e) {
    e.preventDefault();
    if ($('#ticket_container').is(':visible')) {
        swal('Error', 'Hay un ticket abierto, si quiere crear uno nuevo cancele el anterior primero', 'error');
    }
    else {
        var codsuc = $('#select2-1').val();
        if (codsuc != "" && sucursal_data.hasOwnProperty(codsuc)) {
            $('#dashboard_container').hide();
            if (sucursal_data[codsuc].no_empleado_crear != 'X') {
                $('#btnBuscarEmpleado').click();
            }
            if (sucursal_data[codsuc].pedir_cliente == 'X') {
                $('#btnBuscarCliente').click();
            }
            $('#ticket_container').show();
        }
    }
});

$(document).on('click', '#btnCobrarTicket', function(e) {
    e.preventDefault();
    var codsuc = $('#select2-1').val();
    var lineas = $('#ticket_lineas > tbody > tr').length;
    if (lineas <= 0) {
        swal('Error', 'Debe incluir servicios al ticket', 'error');
    }
    else {
        var total = parseFloat($('#total_ticket').val());
        $('#cobranza_total').html(sucursal_data[codsuc].moneda+" "+$.number(total, 2, ',', '.' ));

        var forma_pago_defecto = $('#cliente_forma_pago').val();
        if (forma_pago_defecto=="" && formas_pago[codsuc][0].length>0) {
            forma_pago_defecto = formas_pago[codsuc][0];
        }
        $('#codigo_forma_pago_1').val(forma_pago_defecto);
        $('#cobranza_descripcion_forma_pago_1').editable({
            type: 'select',
            title: 'Forma de Pago 1',
            value: forma_pago_defecto,
            source: formas_pago[codsuc],
            success: function (response, value) {
                $('#codigo_forma_pago_1').val(value);
            }
        });
        $('#cobranza_descripcion_forma_pago_1').editable('option','source',formas_pago[codsuc]);
        $('#cobranza_descripcion_forma_pago_1').editable('option','value',forma_pago_defecto);
        $('#cobranza_descripcion_forma_pago_1').removeClass('editable-unsaved');

        $('#cobranza_monto_forma_pago_1').html(total);
        $('#total_forma_pago_1').val(total);
        $('#cobranza_monto_forma_pago_1').editable({
            type: 'text',
            title: 'Efectivo',
            value: total,
            display: function (value, response) {
                $(this).text(/*sucursal_data[codsuc].moneda+" "+*/$.number(value, 2, ',', '.'));
            },
            success: function (response, value) {
                var total_forma_pago_1 = !isNaN(parseFloat(value))?parseFloat(value):0;
                var total_forma_pago_2 = parseFloat($('#total_forma_pago_2').val());
                var cobrado = total_forma_pago_1 + total_forma_pago_2;
                var cambio = -1 * (total - cobrado) ;
                $('#total_forma_pago_1').val(total_forma_pago_1);
                $('#total_cobrado').val(cobrado);
                $('#cobranza_cobrado').html(/*sucursal_data[codsuc].moneda+" "+*/$.number(cobrado, 2, ',', '.'));
                $('#cobranza_cambio').html(/*sucursal_data[codsuc].moneda+" "+*/$.number(cambio, 2, ',', '.'));
            }
        });
        $('#cobranza_monto_forma_pago_1').editable('option','value',total);
        $('#cobranza_monto_forma_pago_1').removeClass('editable-unsaved');

        var forma_pago_secundaria = forma_pago_defecto;
        for (i=0; i<formas_pago[codsuc].length; i++) {
            var nueva_forma_pago = formas_pago[codsuc][i].value;
            if (forma_pago_defecto != nueva_forma_pago) {
                forma_pago_secundaria = nueva_forma_pago;
                break;
            }
        }
        $('#codigo_forma_pago_2').val(forma_pago_secundaria);
        $('#cobranza_descripcion_forma_pago_2').editable({
            type: 'select',
            title: 'Forma de Pago 2',
            value: forma_pago_secundaria,
            source: formas_pago[codsuc],
            success: function (response, value) {
                $('#codigo_forma_pago_2').val(value);
            }
        });
        $('#cobranza_descripcion_forma_pago_2').editable('option','source',formas_pago[codsuc]);
        $('#cobranza_descripcion_forma_pago_2').editable('option','value',forma_pago_secundaria);
        $('#cobranza_descripcion_forma_pago_2').removeClass('editable-unsaved');

        $('#cobranza_monto_forma_pago_2').html(0);
        $('#total_forma_pago_2').val(0);
        $('#cobranza_monto_forma_pago_2').editable({
            type: 'text',
            title: 'Tarjeta',
            value: 0,
            display: function (value, response) {
                $(this).text(/*sucursal_data[codsuc].moneda+" "+*/$.number(value, 2, ',', '.'));
            },
            success: function (response, value) {
                var total_forma_pago_2 = !isNaN(parseFloat(value))?parseFloat(value):0;
                var total_forma_pago_1 = parseFloat($('#total_forma_pago_1').val());
                var cobrado = total_forma_pago_1 + total_forma_pago_2;
                var cambio = -1 * (total - cobrado);
                $('#total_forma_pago_2').val(total_forma_pago_2);
                $('#total_cobrado').val(cobrado);
                $('#cobranza_cobrado').html(/*sucursal_data[codsuc].moneda+" "+*/$.number(cobrado, 2, ',', '.'));
                $('#cobranza_cambio').html(/*sucursal_data[codsuc].moneda+" "+*/$.number(cambio, 2, ',', '.'));
            }
        });
        $('#cobranza_monto_forma_pago_2').editable('option','value',0);
        $('#cobranza_monto_forma_pago_2').removeClass('editable-unsaved');

        $('#cobranza_cobrado').html(/*sucursal_data[codsuc].moneda+" "+*/$.number(total, 2, ',', '.' ));
        $('#total_cobrado').val(total);
        $('#cobranza_cambio').html(/*sucursal_data[codsuc].moneda+" "+*/$.number(0, 2, ',', '.' ));
        $('#modal_cobranza').modal('show');
    }
});

$(document).on('click', '#btnCobrarConfirmado', function(e) {
    if (!($('#btnCobrarConfirmado').hasClass("btn-secondary"))) {
        $('#btnCobrarConfirmado').addClass("btn-secondary");
        var total = parseFloat($('#total_ticket').val());
        var cobrado = parseFloat($('#total_cobrado').val());
        var permite_deuda = $('#cliente_permite_deuda').val()=='1';
        if ((total <= cobrado) ||(permite_deuda)) {
            guardarTicket();
        }
        else {
            swal('Error', 'El monto cobrado debe ser mayor o igual al total del ticket', 'error');
            $('#btnCobrarConfirmado').removeClass("btn-secondary");
        }
    }
});

function guardarTicket() {
    var codsuc = $('#select2-1').val();
    var ticket = {};
    ticket.codsuc = codsuc;
    ticket.fecfac = $('#ticket_fecha').html();
    ticket.hora = $('#ticket_hora').html();
    ticket.codcli = $('#cliente_codigo').val();
    ticket.obsfac = $('#ticket_observaciones').val();
    ticket.codemp = $('#empleado_codigo').val();
    ticket.impcob1 = parseFloat($('#total_forma_pago_1').val());
    ticket.forpag1 = $('#codigo_forma_pago_1').val();
    ticket.impcob2 = parseFloat($('#total_forma_pago_2').val());
    ticket.forpag2 = $('#codigo_forma_pago_2').val();
    ticket.impcam = parseFloat($('#total_cobrado').val()) - parseFloat($('#total_ticket').val());
    ticket.totimpbas = 0;
    ticket.totimpdto = 0;
    ticket.totimpiva = 0;
    ticket.totfac = parseFloat($('#total_ticket').val());
    ticket.lineas_array = [];
    $.each($('#ticket_lineas > tbody > tr'), function(i, row) {
        var linea = {};
        linea.codart = $(this).find('.article .codart').val();
        linea.desart = $(this).find('.article .desart').val();
        linea.preven = parseFloat($(this).find('.price .preart').val());
        linea.cant = parseInt($(this).find('.quantity .canart').val());
        linea.subtot = parseFloat($(this).find('.price .subtotal').val());
        linea.descuento = parseFloat($(this).find('.descuento .pordesart').val());
        linea.impdescuento = parseFloat($(this).find('.descuento .mondesart').val()) * linea.cant;
        linea.impbas = parseFloat($(this).find('.impuesto .basimpart').val()) * linea.cant;
        linea.iva = parseFloat($(this).find('.impuesto .porimpart').val());
        linea.impiva = parseFloat($(this).find('.impuesto .monimpart').val()) * linea.cant;
        linea.codemp = $(this).find('.employee .codemp').val();
        ticket.totimpbas += linea.impbas;
        ticket.totimpdto += linea.impdescuento;
        ticket.totimpiva += linea.impiva;
        ticket.lineas_array.push(linea);
    });
    ticket.usucre = (session != undefined)?session:"";
    ticket.usumod = (session != undefined)?session:"";
    var params = {"sucursal": codsuc, "operacion": "guardar_ticket", "ticket": JSON.stringify(ticket)};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(data) {
            if (data.exito == true) {
                swal('Cobrado!', 'El ticket ha sido cobrado.', 'success');
                $("#modal_cobranza .close").click();
                resetearVista();
            }
            else {
                console.log(JSON.stringify(data));
            }
            $('#btnCobrarConfirmado').removeClass("btn-secondary");
        },
        error: function(data) {
            console.log(JSON.stringify(data));
            $('#btnCobrarConfirmado').removeClass("btn-secondary");
        }
    });
}

$('#select2-1').select2();
