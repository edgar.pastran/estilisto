var sucursales = new Array();

$('#bootgrid-basic').bootgrid({
        navigation:0,   // Turn of paging.
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    });

$(function() {
    inicializar();
});

function inicializar() {
    serverPage = ruta.concat("/servidor/php/consul_suc_usuario.php");
    $.post(serverPage, {"session": session}, function (data) {

        if (data.exito == true) {

            $('#select2-2').text(" ");

            for (var i = 0; i < data.nume_regis; i++) {

                if (sucursal.indexOf(data.sucursales_usuario[i]['codsuc']) >= 0) {

                    $('#select2-2').append('<option value="' + data.sucursales_usuario[i]['codsuc'] + '"selected>' + data.sucursales_usuario[i]['codsuc'] + ' ' + data.sucursales_usuario[i]['razemp'] + '</option>');

                } else {

                    $('#select2-2').append('<option value="' + data.sucursales_usuario[i]['codsuc'] + '">' + data.sucursales_usuario[i]['codsuc'] + ' ' + data.sucursales_usuario[i]['razemp'] + '</option>');

                }
                ;


                sucursales[i] = new Array();

                sucursales[i]['codsuc'] = data.sucursales_usuario[i]['codsuc'];

                sucursales[i]['razemp'] = data.sucursales_usuario[i]['razemp'];

                sucursales[i]['moneda'] = data.sucursales_usuario[i]['moneda'];

            }
            ;

        } else {

            //alert('error');

        }
        ;


    }); //Fin .post

    var ano = fecha.getFullYear();
    for (var i = ano - 0; i >= ano - 4; i--) {

        $('#ejercicio').append('<option value="' + i + '">' + i + '</option>');

    }
    ;

    $('#reporte').hide('fast');
    $('#btnNuevoReporte').hide('fast');
}

$("#btnReporte").click(function (event){

    event.preventDefault();

    getSucursal();

    $datos = $("#formulario-reporte").serialize();



    serverPage = ruta.concat("/servidor/php/resumen_anual.php");

    $.post(serverPage, $("#formulario-reporte").serialize(), function (data) {

        if (data.exito == true && (data.totalven >= 1 || data.totalcom >= 1 || data.totalben >= 1)){



            $('#formulario').hide('fast');

            $('#reporte').show('fast');

            $('#btnReporte').hide('fast');

            $('#btnNuevoReporte').show('fast');

            var nSuc = 0;

            var iSuc = 0;

            for (var i = 0; i < sucursales.length; i++) {

                if (document.getElementById("sucursal").value.indexOf(sucursales[i]['codsuc']) >= 0) { iSuc = i; nSuc=nSuc+1;};

            }

            guardarSucursal(document.getElementById("sucursal").value);



            if (data.nume_regis >= 1) {

                var desSuc = sucursales[iSuc]['razemp'];

                if (nSuc > 1) { desSuc = ""};

                $('#TituloTable1').text(document.getElementById("sucursal").value +" "+desSuc+ " -- Ejercicio : " + data.ejercicio + " -- Moneda: " + sucursales[iSuc]['moneda']);





                var mes ="";

                var acumulado=0;

                var total_acumulado=0;

                var beneficio="";

                var ventas=0;

                var compras=0;

                $("#bootgrid-basic").bootgrid("clear");



                total_acumulado = total_acumulado + data.totalben_ant;

                mes="Saldo Ant."; ventas = 0; compras = 0;

                ventas = formatNumber.new(ventas.toFixed(2));

                compras = formatNumber.new(compras.toFixed(2));

                acumulado = formatNumber.new(total_acumulado.toFixed(2));

                beneficio = formatNumber.new(data.totalben_ant.toFixed(2));



                $("#bootgrid-basic").bootgrid().bootgrid("append", [{"mes": ""+mes+"", "ventas": ""+ventas+"", "compras": compras, "beneficio": beneficio, "acumulado": acumulado}]);



                for (var i = 0; i < 12; i++) {

                    if (i==0) {mes="Enero"};

                    if (i==1) {mes="Febrero"};

                    if (i==2) {mes="Marzo"};

                    if (i==3) {mes="Abril"};

                    if (i==4) {mes="Mayo"};

                    if (i==5) {mes="Junio"};

                    if (i==6) {mes="Julio"};

                    if (i==7) {mes="Agosto"};

                    if (i==8) {mes="Septiembre"};

                    if (i==9) {mes="Octubre"};

                    if (i==10) {mes="Noviembre"};

                    if (i==11) {mes="Diciembre"};

                    total_acumulado = total_acumulado + (data.ventas[i] - data.compras[i]);

                    acumulado = formatNumber.new(total_acumulado.toFixed(2));

                    ventas = formatNumber.new(data.ventas[i].toFixed(2));

                    compras = formatNumber.new(data.compras[i].toFixed(2));

                    beneficio = formatNumber.new(data.beneficio[i].toFixed(2));



                    $("#bootgrid-basic").bootgrid().bootgrid("append", [{"mes": ""+mes+"", "ventas": ""+ventas+"", "compras": compras, "beneficio": beneficio, "acumulado": acumulado}]);

                };



                $('#totalinf').text('TOTAL '+data.ejercicio+' '+sucursales[iSuc]['moneda']+' :');

                $('#totalven').text(formatNumber.new(data.totalven.toFixed(2)));

                $('#totalcom').text(formatNumber.new(data.totalcom.toFixed(2)));

                var ben = 0;

                if (data.totalben == 0) {

                    ben = data.totalven - data.totalcom;

                    $('#totalben').text(formatNumber.new(ben.toFixed(2)));

                }else{

                    $('#totalben').text(formatNumber.new(data.totalben.toFixed(2)));

                };



                $('#totalacu').text("");



                guardarSucursal(document.getElementById("sucursal").value);

                guardarDatos(JSON.stringify(data), sucursales[iSuc]['moneda']);

                $('#grafico1').text("");

                $('#grafico1').append('<iframe id="external" frameborder="0" style="width:100%;height:416px" src="graficos/ver_resumen_anual.html"></iframe>');



            };



        }else{//No existen Registros

            toastr.options.positionClass = 'toast-bottom-right';

            toastr['warning']('No existen Datos');

        };

    }); //Fin .post

});

$("#btnNuevoReporte").click(function (event){

    $('#formulario').show('fast');

    $('#btnReporte').show('fast');

    $('#btnNuevoReporte').hide('fast');

    //$('#tabla1').text(" ");

    $('#reporte').hide('fast');

});


// // Configurar combos
$('#select2-2').select2();
