var urlServerPage = ruta.concat("/servidor/php/grupo_usuario.php");

$(function() {
    cargarCatalogo();
    cargarCatalogoPantallas();
    inicializar();
    consultar_pantallas();
});

function cargarCatalogo() {
    $('#bootgrid-grupo_usuarios').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        selection: true,
        multiSelect: true,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on("selected.rs.jquery.bootgrid", function(e, rows) {
        $("#codgru").val(rows[0].gu_codgru);
        $("#desgru").val(rows[0].gu_desgru);
        $("#obsusu").val(rows[0].gu_obsusu);
        $("#ladmin").val(rows[0].gu_ladmin);
        setCheckbox("ladmin","1");

        $("#desgru").removeAttr("readonly");
        $("#obsusu").removeAttr("readonly");
        $("#ladmin").attr("disabled", false);

        $("#opc_grupo_usuario").val("actualizar");

        consultar_accesos();

        $("#closeMgu").click();
    })
    .on("deselected.rs.jquery.bootgrid", function(e, rows) {

    });
}

function cargarCatalogoPantallas() {
    $('#bootgrid-pantallas-grupo').bootgrid({
        css: ioniconCss,
        rowCount: [-1, 10, 25, 50, 100],
        selection: true,
        multiSelect: true,
        rowSelect: true,
        keepSelection: false,
        caseSensitive: false,
        labels: {
            all: "Todos",
            infos: "Mostrando {{ctx.start}} a {{ctx.end}} de {{ctx.total}} registros",
            loading: "Cargando...",
            noResults: "No se encontraron registros",
            refresh: "Actualizar",
            search: "Buscar"
        },
        templates: {
            select: '<label class="mda-checkbox">' +
            '<input name="select" type="{{ctx.type}}" class="{{css.selectBox}}" value="{{ctx.value}}" {{ctx.checked}} />' +
            '<em class="bg-warning"></em>' +
            '</label>',
            // templates for BS4
            actionButton: '<button class="btn btn-secondary" type="button" title="{{ctx.text}}">{{ctx.content}}</button>',
            actionDropDown: '<div class="{{css.dropDownMenu}}"><button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"><span class="{{css.dropDownMenuText}}">{{ctx.content}}</span> <span class="caret"></span></button><ul class="{{css.dropDownMenuItems}}" role="menu"></ul></div>',
            actionDropDownItem: '<li class="dropdown-item"><a href="" data-action="{{ctx.action}}" class="{{css.dropDownItem}} {{css.dropDownItemButton}}">{{ctx.text}}</a></li>',
            actionDropDownCheckboxItem: '<li class="dropdown-item"><label class="{{css.dropDownItem}}"><input name="{{ctx.name}}" type="checkbox" value="1" class="{{css.dropDownItemCheckbox}}" {{ctx.checked}} /> {{ctx.label}}</label></li>',
            paginationItem: '<li class="page-item {{ctx.css}}"><a href="" data-page="{{ctx.page}}" class="page-link {{css.paginationButton}}">{{ctx.text}}</a></li>',
        }
    })
    .on("selected.rs.jquery.bootgrid", function(e, rows) {


    })
    .on("deselected.rs.jquery.bootgrid", function(e, rows) {

    });
}

$("#btn-grupo_usuario").click(function (event){
    event.preventDefault();
    $("#opc_grupo_usuario").val("consulta");

    $.post(urlServerPage, $("#formulario-grupo_usuario").serialize(), function (data){
        if ( data.exito == true && data.nume_regis >= 1 ){

            $("#bootgrid-grupo_usuarios").bootgrid("clear");
            for (var i = 0; i < data.nume_regis; i++) {
                $("#bootgrid-grupo_usuarios").bootgrid().bootgrid("append",[{"gu_codgru": ""+data.grupousuario[i]['codgru']+"",
                    "gu_desgru": ""+data.grupousuario[i]['desgru']+"",
                    "gu_ladmin": ""+data.grupousuario[i]['ladmin']+"",
                    "gu_obsusu": ""+data.grupousuario[i]['obsusu']+""}]);
            };
            //Show Modal
            $('#modal_grupo_usuarios').modal('show');
        };//No hay Datos
    }); //Fin .post

}); //Fin .btn

function eliminar() {
    if ($("#codgru").val() != "" && $("#opc_grupo_usuario").val() != "insertar") {
        $("#opc_grupo_usuario").val("eliminar");
        guardar();
    }
}

function guardar() {
    //$("#suc_formapago").val($("#select2-1").val());
    if ($("#opc_grupo_usuario").val() != "" && $("#codgru").val() != "") {
        $.post(urlServerPage, $("#formulario-grupo_usuario").serialize(), function (data) {
            if (data.exito == true){
                if ($("#opc_grupo_usuario").val("insertar")) {
                    swal('GRUPO Creado!', '', 'success');
                }
                if ($("#opc_grupo_usuario").val("eliminar")) {
                    swal('GRUPO Eliminado!', '', 'success');
                }
            }
            else{
                //alert('error');
            }

            if ($("#opc_grupo_usuario").val() == "actualizar") {
                actualizar_accesos();
                swal('GRUPO Actualizado!', '', 'success');
            }
            if ($("#opc_grupo_usuario").val() == "insertar") {
                actualizar_accesos();
            }
            if ($("#opc_grupo_usuario").val() == "eliminar") {
                eliminar_accesos();
            }
            cancelar();
        }); //Fin .post
    }
} //Fin de Funcion

function consultar_grupoUsuario() {
    var opc = $("opc_grupo_usuario").val();
    if (opc == "insertar") {
        $("#opc_grupo_usuario").val("consultar");
        if ($("#opc_grupo_usuario").val() != "" && $("#codgru").val() != "") {
            $.post(urlServerPage, $("#formulario-grupo_usuario").serialize(), function (data) {
                if (data.exito == true){
                    swal('GRUPO YA Existe!', '', 'error');
                    $("#codgru").focus();
                }
                else{//alert('error');
                    $("#codgru").attr("readonly", true);
                }
            }); //Fin .post
        }
    } //Fin del IF
    $("#opc_grupo_usuario").val(opc);
} //Fin de Funcion

function consultar_pantallas() {
    $("#opc_grupo_usuario").val("consulta");
    var urlServerPageAux = ruta.concat("/servidor/php/pantallas.php");

    $.post(urlServerPageAux, $("#formulario-grupo_usuario").serialize(), function (data){
        if ( data.exito == true && data.nume_regis >= 1 ) {
            $("#bootgrid-pantallas-grupo").bootgrid("clear");
            for (var i = 0; i < data.nume_regis; i++) {
                $("#bootgrid-pantallas-grupo").bootgrid().bootgrid("append",[{"pa_codpan": ""+data.pantallas[i]['codpan']+"",
                    "pa_despan": ""+data.pantallas[i]['despan']+"",
                    "pa_pantalla": ""+data.pantallas[i]['pantalla']+""}]);
            }
        }//No hay Datos
    }); //Fin .post
} //Fin .btn

function limpiar_accesos() {
    var seleccionados = $("#bootgrid-pantallas-grupo").bootgrid("getSelectedRows");
    var registros = seleccionados.length;
    for (var i = 0; i < registros; i++) {
        $("#bootgrid-pantallas-grupo").bootgrid('deselect', seleccionados[i]['codgru']);
    }
} //Fin .btn

function consultar_accesos() {
    var opc = $("#opc_grupo_usuario").val();
    $("#opc_grupo_usuario").val("accesos");

    limpiar_accesos();
    $.post(urlServerPage, $("#formulario-grupo_usuario").serialize(), function (dataAccesos){
        if ( dataAccesos.exito == true && dataAccesos.nume_regis >= 1 ){
            var seleccion = new Array();
            for (var i = 0; i < dataAccesos.nume_regis; i++) {
                seleccion[i] = dataAccesos.accesos[i]['codpan'];
            }
            $("#bootgrid-pantallas-grupo").bootgrid('select', seleccion);
        }//No hay Datos
    }); //Fin .post
    $("#opc_grupo_usuario").val(opc);
} //Fin .btn

function actualizar_accesos() {
    var seleccionados = $("#bootgrid-pantallas-grupo").bootgrid("getSelectedRows");

    $.post(urlServerPage, {
            'seleccionados': JSON.stringify(seleccionados),
            'opcion': 'actualizar_accesos',
            'codgru': $("#codgru").val()
        },
        function (data){
        }
    ); //Fin .post
} //Fin .btn

function eliminar_accesos() {
    var seleccionados = $("#bootgrid-pantallas-grupo").bootgrid("getSelectedRows");

    $.post(urlServerPage, {
            'seleccionados': JSON.stringify(seleccionados),
            'opcion': 'eliminar_accesos',
            'codgru': $("#codgru").val()
        },
        function (data){
        }
    ); //Fin .post
} //Fin .btn

function cancelar() {
    inicializar();
    limpiar_accesos();
} //Fin de Funcion

function nuevo() {
    $("#opc_grupo_usuario").val("insertar");
    $("#codgru").val("");
    $("#desgru").val("");
    $("#obsusu").val("");

    $("#codgru").removeAttr("readonly");
    $("#codgru").focus();
    $("#desgru").removeAttr("readonly");

    $("#obsusu").removeAttr("readonly");
    $("#ladmin").attr("disabled", false);
    setCheckbox("ladmin","1");

    $("#btn-grupo_usuario").attr("disabled", true);
    limpiar_accesos();
} //Fin de Funcion

function inicializar() {
    $("#opc_grupo_usuario").val("");
    $("#codgru").val("");
    $("#desgru").val("");
    $("#ladmin").val("");
    setCheckbox("ladmin","1");
    $("#obsusu").val("");

    $("#codgru").attr("readonly", true);
    $("#desgru").attr("readonly", true);

    $("#obsusu").attr("readonly", true);
    $("#ladmin").attr("disabled", true);
    $("#btn-grupo_usuario").attr("disabled", true);
}
