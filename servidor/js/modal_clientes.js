$(function() {
    $("#tabla_clientes").DataTable({
        "language": {
            "aria": {
                "sortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "emptyTable": "No hay datos disponibles en esta tabla",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "lengthMenu": "Mostrar _MENU_ registros por p&aacute;gina",
            "search": "Buscar:",
            "zeroRecords": "No se encontraron resultados",
            "paginate": {
                "previous": "Anterior",
                "next": "Siguiente",
                "last": "&Uacute;ltimo",
                "first": "Primero"
            }
        },
        "lengthMenu": [10],
        "pageLength": 10
    });

    $('.fila-cliente').click(function() {
        asignarClienteTicket($(this));
    });
});

function asignarClienteTicket(fila) {
    var codigo = fila.attr('id');
    var dni = fila.find(".dni").html();
    var nombre = fila.find(".nombre").html();
    var apellido = fila.find(".apellido").html();
    var forma_pago = fila.find(".forma_pago").val();
    var permite_deuda = fila.find(".permite_deuda").val();
    nombre += (apellido!="")?" "+apellido:"";
    $('#cliente_codigo').val(codigo);
    $('#cliente_dni').val(dni);
    $('#cliente_nombre').val(nombre);
    $('#cliente_forma_pago').val(forma_pago);
    $('#cliente_permite_deuda').val(permite_deuda);
    $("#modal_clientes .close").click();
}