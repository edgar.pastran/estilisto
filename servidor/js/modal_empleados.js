$(function() {
    // Select a service
    $('.custom-button').click(function() {
        var opener = $('#modal_empleados_opener').val();
        if (opener == "modal_servicios") {
            asignarEmpleadoTicket($(this));
            adicionarLineaTicket();
        }
        else if (opener == "cabecera") {
            asignarEmpleadoTicket($(this));
        }
        $("#modal_empleados .close").click();
    });
});

function asignarEmpleadoTicket(boton_empleado) {
    var codigo = boton_empleado.find('.codemp').val();
    var nombre = boton_empleado.find(".fullname").val();
    $('#empleado_codigo').val(codigo);
    $('#empleado_nombre').val(nombre);
    $("#modal_empleados .close").click();
}