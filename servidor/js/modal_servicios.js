$(function() {
    // init Isotope
    var $container = $('.grid.elements');

    $container.isotope({
        itemSelector: '.element-item',
        layoutMode: 'fitRows',
        filter: ':not(.initially-hidden)'
    });

    var PageLoadFilter = ':not(.initially-hidden)';
    $container.isotope({filter: PageLoadFilter});

    // filter functions
    $('div.filters').on('click', 'div.custom-button', function () {
        var filterValue = $(this).attr('data-filter');
        $container.isotope({filter: filterValue});
    });

    // change is-checked class on buttons
    $('.grid').each(function (i, buttonGroup) {
        var $buttonGroup = $(buttonGroup);
        $buttonGroup.on('click', 'div.custom-button', function () {
            $buttonGroup.find('.is-checked').removeClass('is-checked');
            $(this).addClass('is-checked');
        });
    });

    // Select a service
    $('.element-item').click(function() {
        var codsuc = $('#select2-1').val();
        var codart = $(this).find('.codart').val();
        linea_factura['codart'] = codart;
        linea_factura['desart'] = $(this).find('.desart').val();
        linea_factura['preart'] = parseFloat($.number( $(this).find('.preart').val(), 2, '.', '' ));
        linea_factura['preart_format'] = $(this).find('.preart_format').val();
        linea_factura['pordesart'] = 0.00;
        linea_factura['mondesart'] = 0.00;
        linea_factura['pordesart_format'] = '0,00';
        linea_factura['porimpart'] = parseFloat($.number( $(this).find('.porimpart').val(), 2, '.', '' ));
        linea_factura['porimpart_format'] = $(this).find('.porimpart_format').val();
        linea_factura['monimpart'] = parseFloat($.number( $(this).find('.monimpart').val(), 2, '.', '' ));
        linea_factura['basimpart'] = parseFloat($.number( $(this).find('.basimpart').val(), 2, '.', '' ));

        $("#modal_servicios .close").click();

        if (codsuc != "" && sucursal_data.hasOwnProperty(codsuc)) {
            if (sucursal_data[codsuc].empleado_linea == "X") {
                $('#modal_empleados_opener').val('modal_servicios');
                if (modal_empleados.hasOwnProperty(codsuc)){
                    //$('#modal_servicios').find('.modal-body').load($(this).attr('href')+"?codsuc="+codsuc);
                    //$('#modal_servicios').removeData();
                    $('#modal_empleados').find('.modal-body').html(modal_empleados[codsuc]);
                    $('#modal_empleados').modal('show');
                }
                else {
                    cargarModalEmpleados();
                    $('#modal_empleados').find('.modal-body').html(modal_empleados[codsuc]);
                }
            }
            else {
                adicionarLineaTicket();
            }
        }
    });

});

function adicionarLineaTicket() {
    var codsuc = linea_factura['codsuc'];
    var codemp = $('#empleado_codigo').val();
    var fullname =$('#empleado_nombre').val();

    //console.log(JSON.stringify(linea_factura));
    var numero_linea = 1;
    if ($('#ticket_lineas > tbody > tr').length > 0) {
        var id_linea = $('#ticket_lineas > tbody > tr:last-child').attr("id");
        numero_linea = parseInt(id_linea.split('-')[1])+1;
    }
    var row = '';
    row += '<tr id="ticket_linea-'+numero_linea+'">';
    row += '<td class="quantity text-center">' +
        '<a href="#" class="cantidad">1</a>' +
        '<input type="hidden" class="canart" value="1">' +
        '</td>';
    row += '<td class="article">' +
        linea_factura['desart'] +
        '<input type="hidden" class="codart" value="' + linea_factura['codart'] + '">' +
        '<input type="hidden" class="desart" value="' + linea_factura['desart'] + '">' +
        '</td>';
    row += '<td class="employee">' +
        '<a href="#" class="empleado">' + fullname + '</a>' +
        '<input type="hidden" class="codemp" value="' + codemp + '">' +
        '</td>';
    row += '<td class="price text-right d-none d-md-table-cell">' +
        '<a href="#" class="preven">' + linea_factura['preart_format'] + '</a>' +
        '<input type="hidden" class="preart" value="' + linea_factura['preart'] + '">' +
        '<input type="hidden" class="subtotal" value="' + linea_factura['preart'] + '">' +
        '</td>';
    row += '<td class="descuento text-right d-none d-md-table-cell">'+
        '<a href="#" class="desven">' + linea_factura['pordesart_format'] + '</a>' +
        '<input type="hidden" class="pordesart" value="' + linea_factura['pordesart'] + '">' +
        '<input type="hidden" class="mondesart" value="' + linea_factura['mondesart'] + '">' +
        '</td>';
    row += '<td class="subtotal_line text-right">'
        + sucursal_data[codsuc].moneda+" "+linea_factura['preart_format'] +
        '</td>';
    row += '<td class="impuesto text-right d-none d-md-table-cell">'
        + linea_factura['porimpart_format'] + " %"+
        '<input type="hidden" class="porimpart" value="' + linea_factura['porimpart'] + '">' +
        '<input type="hidden" class="monimpart" value="' + linea_factura['monimpart'] + '">' +
        '<input type="hidden" class="basimpart" value="' + linea_factura['basimpart'] + '">' +
        '</td>';
    row += '<td class="text-center">' +
        '<button class="btn btn-sm btn-primary btn-edit-line" type="button" title="Editar Linea"><em class="ion-edit"></em></button>&nbsp;' +
        '<button class="btn btn-sm btn-danger btn-delete-line" type="button" title="Eliminar Linea"><em class="ion-trash-b"></em></button>' +
        '</td>';
    row += '</tr>';
    if ($('#ticket_lineas > tbody > tr').length > 0) {
        $('#ticket_lineas > tbody:last-child').append(row);
    }
    else {
        $('#ticket_lineas > tbody').html(row);
    }
    calcularTotal();

    $('.cantidad').editable({
        type: 'text',
        title: 'Cantidad',
        value: 1,
        emptytext: 1,
        display: function (value, response) {
            value = !isNaN(parseInt(value))?parseInt(value):1;
            $(this).text($.number(value, 0, ',', '.'));
        },
        success: function (response, value) {
            var cantidad = !isNaN(parseInt(value))?parseInt(value):1;
            $(this).siblings('.canart').val(cantidad);
            var precio = parseFloat($(this).parent().siblings('.price').find('.preart').val());
            var descuento_porcentaje = parseFloat($(this).parent().siblings('.descuento').find('.pordesart').val());
            descuento_porcentaje = (descuento_porcentaje>100)?100:descuento_porcentaje;
            var descuento_monto = precio * (descuento_porcentaje / 100);
            var precio_con_descuento = precio - descuento_monto;
            var subtotal = cantidad * precio_con_descuento;
            $(this).parent().siblings('.price').find('.subtotal').val(subtotal);
            $(this).parent().siblings('.subtotal_line').html(sucursal_data[codsuc].moneda+" "+$.number(subtotal, 2, ',', '.'));
            calcularTotal();
        }
    }).
    on('shown', function(ev, editable) {
        setTimeout(function() {
            editable.input.$input.select();
        },0);
    });

    $('.empleado').editable({
        type: 'select',
        title: 'Empleado',
        value: codemp,
        source: empleados[codsuc],
        success: function (response, value) {
            $(this).siblings('.codemp').val(value);
        }
    }).
    on('shown', function(ev, editable) {
        setTimeout(function() {
            editable.input.$input.select();
        },0);
    });

    $('.preven').editable({
        type: 'text',
        title: 'Precio Unitario',
        value: linea_factura['preart'],
        display: function (value, response) {
            $(this).text(sucursal_data[codsuc].moneda+" "+$.number(value, 2, ',', '.'));
        },
        success: function (response, value) {
            var precio = !isNaN(parseFloat(value))?parseFloat(value):0;
            var cantidad = parseInt($(this).parent().siblings('.quantity').find('.canart').val());
            var descuento_porcentaje = parseFloat($(this).parent().siblings('.descuento').find('.pordesart').val());
            descuento_porcentaje = (descuento_porcentaje>100)?100:descuento_porcentaje;
            var descuento_monto = precio * (descuento_porcentaje / 100);
            var precio_con_descuento = precio - descuento_monto;
            var subtotal = cantidad * precio_con_descuento;
            $(this).siblings('.preart').val(precio);
            $(this).siblings('.subtotal').val(subtotal);
            $(this).parent().siblings('.descuento').find('.mondesart').val(descuento_monto);
            $(this).parent().siblings('.subtotal_line').html(sucursal_data[codsuc].moneda+" "+$.number(subtotal, 2, ',', '.'));
            // Calculo de la base imponible y monto del impuesto del articulo
            var impuesto_porcentaje = $(this).parent().siblings('.impuesto').find('.porimpart').val();
            var base_imponible = precio_con_descuento;
            var impuesto_monto = precio_con_descuento * (impuesto_porcentaje / 100);
            if (sucursal_data[codsuc].impuesto_incluido == "X") {
                base_imponible = precio_con_descuento / (1+(impuesto_porcentaje/100));
                impuesto_monto = precio_con_descuento - base_imponible;
            }
            $(this).parent().siblings('.impuesto').find('.monimpart').val(impuesto_monto);
            $(this).parent().siblings('.impuesto').find('.basimpart').val(base_imponible);
            calcularTotal();
        }
    }).
    on('shown', function(ev, editable) {
        setTimeout(function() {
            editable.input.$input.select();
        },0);
    });

    $('.desven').editable({
        type: 'text',
        title: 'Descuento',
        value: linea_factura['pordesart'],
        display: function (value, response) {
            $(this).text($.number(value, 2, ',', '.')+" %");
        },
        success: function (response, value) {
            var descuento_porcentaje = !isNaN(parseFloat(value))?parseFloat(value):0;
            descuento_porcentaje = (descuento_porcentaje>100)?100:descuento_porcentaje;
            var cantidad = parseInt($(this).parent().siblings('.quantity').find('.canart').val());
            var precio = parseFloat($(this).parent().siblings('.price ').find('.preart').val());
            var descuento_monto = precio * (descuento_porcentaje / 100);
            var precio_con_descuento = precio - descuento_monto;
            var subtotal = cantidad * precio_con_descuento;
            $(this).siblings('.pordesart').val(descuento_porcentaje);
            $(this).siblings('.mondesart').val(descuento_monto);
            $(this).parent().siblings('.price').find('.subtotal').val(subtotal);
            $(this).parent().siblings('.subtotal_line').html(sucursal_data[codsuc].moneda+" "+$.number(subtotal, 2, ',', '.'));
            // Calculo de la base imponible y monto del impuesto del articulo
            var impuesto_porcentaje = $(this).parent().siblings('.impuesto').find('.porimpart').val();
            var base_imponible = precio_con_descuento;
            var impuesto_monto = precio_con_descuento * (impuesto_porcentaje / 100);
            if (sucursal_data[codsuc].impuesto_incluido == "X") {
                base_imponible = precio_con_descuento / (1+(impuesto_porcentaje/100));
                impuesto_monto = precio_con_descuento - base_imponible;
            }
            $(this).parent().siblings('.impuesto').find('.monimpart').val(impuesto_monto);
            $(this).parent().siblings('.impuesto').find('.basimpart').val(base_imponible);
            calcularTotal();
        }
    }).
    on('shown', function(ev, editable) {
        setTimeout(function() {
            editable.input.$input.select();
        },0);
    });

    $('.btn-delete-line').click(function () {
        $(this).closest('tr').remove();
        calcularTotal();
    });

    $('.btn-edit-line').click(function () {
        var id_linea = $(this).closest('tr').attr('id');
        $('#editar_ticket_linea_id').val(id_linea);
        $('#editar_ticket_linea_moneda').val(sucursal_data[codsuc].moneda);

        var descripcion = $('#'+id_linea).find('.article .desart').val();
        $('#editar_ticket_linea_descripcion').html(descripcion);

        var cantidad = $('#'+id_linea).find('.quantity .canart').val();
        $('#canart_editar').val(cantidad);
        $('#editar_ticket_linea_cantidad').editable({
            type: 'text',
            title: 'Cantidad',
            value: cantidad,
            display: function (value, response) {
                value = !isNaN(parseInt(value))?parseInt(value):1;
                $(this).text($.number(value, 0, ',', '.'));
            },
            success: function (response, value) {
                var cantidad = !isNaN(parseInt(value))?parseInt(value):1;
                var precio = parseFloat($('#preart_editar').val());
                var descuento_porcentaje = parseFloat($('#pordesart_editar').val());
                descuento_porcentaje = (descuento_porcentaje>100)?100:descuento_porcentaje;
                var descuento_monto = precio * (descuento_porcentaje / 100);
                var precio_con_descuento = precio - descuento_monto;
                var subtotal = cantidad * precio_con_descuento;
                $('#canart_editar').val(cantidad);
                $('#subtotal_editar').val(subtotal);
                $('#editar_ticket_linea_subtotal').html($('#editar_ticket_linea_moneda').val()+" "+$.number(subtotal, 2, ',', '.'));
            }
        }).
        on('shown', function(ev, editable) {
            setTimeout(function() {
                editable.input.$input.select();
            },0);
        });
        $('#editar_ticket_linea_cantidad').editable('option','value',cantidad);
        $('#editar_ticket_linea_cantidad').removeClass('editable-unsaved');

        var empleado = $('#'+id_linea).find('.employee .codemp').val();
        $('#codemp_editar').val(empleado);
        $('#editar_ticket_linea_empleado').editable({
            type: 'select',
            title: 'Empleado',
            value: empleado,
            source: empleados[codsuc],
            success: function (response, value) {
                $('#codemp_editar').val(value);
            }
        });
        $('#editar_ticket_linea_empleado').editable('option','source',empleados[codsuc]);
        $('#editar_ticket_linea_empleado').editable('option','value',empleado);
        $('#editar_ticket_linea_empleado').removeClass('editable-unsaved');

        var precio = $('#'+id_linea).find('.price .preart').val();
        $('#preart_editar').val(precio);
        $('#editar_ticket_linea_precio').editable({
            type: 'text',
            title: 'Precio Unitario',
            value: precio,
            display: function (value, response) {
                $(this).text($('#editar_ticket_linea_moneda').val()+" "+$.number(value, 2, ',', '.'));
            },
            success: function (response, value) {
                var precio = !isNaN(parseFloat(value))?parseFloat(value):0;
                var cantidad = parseInt($('#canart_editar').val());
                var descuento_porcentaje = parseFloat($('#pordesart_editar').val());
                descuento_porcentaje = (descuento_porcentaje>100)?100:descuento_porcentaje;
                var descuento_monto = precio * (descuento_porcentaje / 100);
                var precio_con_descuento = precio - descuento_monto;
                var subtotal = cantidad * precio_con_descuento;
                $('#preart_editar').val(precio);
                $('#subtotal_editar').val(subtotal);
                $('#mondesart_editar').val(descuento_monto);
                $('#editar_ticket_linea_subtotal').html($('#editar_ticket_linea_moneda').val()+" "+$.number(subtotal, 2, ',', '.'));
                // Calculo de la base imponible y monto del impuesto del articulo
                var impuesto_porcentaje = $('#porimpart_editar').val();
                var base_imponible = precio_con_descuento;
                var impuesto_monto = precio_con_descuento * (impuesto_porcentaje / 100);
                if (sucursal_data[codsuc].impuesto_incluido == "X") {
                    base_imponible = precio_con_descuento / (1+(impuesto_porcentaje/100));
                    impuesto_monto = precio_con_descuento - base_imponible;
                }
                $('#monimpart_editar').val(impuesto_monto);
                $('#basimpart_editar').val(base_imponible);
            }
        }).
        on('shown', function(ev, editable) {
            setTimeout(function() {
                editable.input.$input.select();
            },0);
        });
        $('#editar_ticket_linea_precio').editable('option','value',precio);
        $('#editar_ticket_linea_precio').removeClass('editable-unsaved');

        var descuento_porcentaje = $('#'+id_linea).find('.descuento .pordesart').val();
        $('#pordesart_editar').val(descuento_porcentaje);
        var descuento_monto = $('#'+id_linea).find('.descuento .mondesart').val();
        $('#mondesart_editar').val(descuento_monto);
        $('#editar_ticket_linea_descuento').editable({
            type: 'text',
            title: 'Descuento',
            value: descuento_porcentaje,
            display: function (value, response) {
                $(this).text($.number(value, 2, ',', '.')+" %");
            },
            success: function (response, value) {
                var descuento_porcentaje = !isNaN(parseFloat(value))?parseFloat(value):0;
                descuento_porcentaje = (descuento_porcentaje>100)?100:descuento_porcentaje;
                var cantidad = parseInt($('#canart_editar').val());
                var precio = parseFloat($('#preart_editar').val());
                var descuento_monto = precio * (descuento_porcentaje / 100);
                var precio_con_descuento = precio - descuento_monto;
                var subtotal = cantidad * precio_con_descuento;
                $('#pordesart_editar').val(descuento_porcentaje);
                $('#mondesart_editar').val(descuento_monto);
                $('#subtotal_editar').val(subtotal);
                $('#editar_ticket_linea_subtotal').html($('#editar_ticket_linea_moneda').val()+" "+$.number(subtotal, 2, ',', '.'));
                // Calculo de la base imponible y monto del impuesto del articulo
                var impuesto_porcentaje = $('#porimpart_editar').val();
                var base_imponible = precio_con_descuento;
                var impuesto_monto = precio_con_descuento * (impuesto_porcentaje / 100);
                if (sucursal_data[codsuc].impuesto_incluido == "X") {
                    base_imponible = precio_con_descuento / (1+(impuesto_porcentaje/100));
                    impuesto_monto = precio_con_descuento - base_imponible;
                }
                $('#monimpart_editar').val(impuesto_monto);
                $('#basimpart_editar').val(base_imponible);
            }
        }).
        on('shown', function(ev, editable) {
            setTimeout(function() {
                editable.input.$input.select();
            },0);
        });
        $('#editar_ticket_linea_descuento').editable('option','value',descuento_porcentaje);
        $('#editar_ticket_linea_descuento').removeClass('editable-unsaved');

        var subtotal = $('#'+id_linea).find('.price .subtotal').val();
        $('#subtotal_editar').val(subtotal);
        $('#editar_ticket_linea_subtotal').html($('#editar_ticket_linea_moneda').val()+" "+$.number(subtotal, 2, ',', '.'));

        var impuesto_porcentaje = $('#'+id_linea).find('.impuesto .porimpart').val();
        $('#porimpart_editar').val(impuesto_porcentaje);
        var impuesto_monto = $('#'+id_linea).find('.impuesto .monimpart').val();
        $('#monimpart_editar').val(impuesto_monto);
        var base_imponible = $('#'+id_linea).find('.impuesto .basimpart').val();
        $('#basimpart_editar').val(base_imponible);

        $('#modal_ticket_linea').modal('show');
    });

    linea_factura = {};
}

