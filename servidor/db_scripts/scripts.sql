/**************************************************************************
 * CAMBIOS EN LA BD RELACIONADOS CON LOS TICKETS DE VENTA - INICIO
 **************************************************************************/
/*
Agregar columna zonhor en la tabla EMPRESA
 */
ALTER TABLE `EMPRESA` ADD `zonhor` VARCHAR( 50 ) AFTER `pais`;
/*
Agregar columna impcomision en la tabla FACLIN
 */
ALTER TABLE `FACLIN` ADD `impcomision` DECIMAL(16,4) AFTER `comision`;
/*
Agregar columna impdescuento en la tabla FACLIN
 */
ALTER TABLE `FACLIN` ADD `impdescuento` DECIMAL(16,4) AFTER `descuento`;
/*
Agregar columna impbas en la tabla FACLIN
 */
ALTER TABLE `FACLIN` ADD `impbas` DECIMAL(16,4) AFTER `impdescuento`;
/*
Agregar columna iva en la tabla FACLIN
 */
ALTER TABLE `FACLIN` ADD `iva` DECIMAL(4,2) AFTER `impbas`;
/*
Agregar columna impiva en la tabla FACLIN
 */
ALTER TABLE `FACLIN` ADD `impiva` DECIMAL(16,4) AFTER `iva`;
/*
Agregar columna totlin en la tabla FACLIN
 */
ALTER TABLE `FACLIN` ADD `totlin` DECIMAL(16,4) AFTER `taniva`;
/**************************************************************************
 * CAMBIOS EN LA BD RELACIONADOS CON LOS TICKETS DE VENTA - FIN
 **************************************************************************/

/**************************************************************************
 * CAMBIOS EN LA BD RELACIONADOS CON LAS FACTURAS DE COMPRA - INICIO
 **************************************************************************/
/*
Agregar columna comprar en la tabla ARTICULOS
 */
ALTER TABLE `ARTICULOS` ADD `comprar` TINYINT(1) DEFAULT 1 AFTER `vender`;
/*
Agregar columna idconceptocompras en la tabla EMPRESA
 */
ALTER TABLE `EMPRESA` ADD `idconceptocompras` DECIMAL(5,0) AFTER `nomodcobrados`;
/*
Agregar columna proveedorcompras en la tabla EMPRESA
 */
ALTER TABLE `EMPRESA` ADD `proveedorcompras` VARCHAR(15) AFTER `idconceptocompras`;
/*
Agregar columna anulada en la tabla FACPROC
 */
ALTER TABLE `FACPROC` ADD `anulada` TINYINT(1) AFTER `contab`;
/*
Agregar columna impdescuento en la tabla FACPROL
 */
ALTER TABLE `FACPROL` ADD `impdescuento` DECIMAL(16,4) AFTER `descuento`;
/*
Agregar columna impbas en la tabla FACPROL
 */
ALTER TABLE `FACPROL` ADD `impbas` DECIMAL(16,4) AFTER `impdescuento`;
/*
Agregar columna iva en la tabla FACPROL
 */
ALTER TABLE `FACPROL` ADD `iva` DECIMAL(4,2) AFTER `impbas`;
/*
Agregar columna impiva en la tabla FACPROL
 */
ALTER TABLE `FACPROL` ADD `impiva` DECIMAL(16,4) AFTER `iva`;
/*
Agregar columna totlin en la tabla FACPROL
 */
ALTER TABLE `FACPROL` ADD `totlin` DECIMAL(16,4) AFTER `taniva`;
/**************************************************************************
 * CAMBIOS EN LA BD RELACIONADOS CON LAS FACTURAS DE COMPRA - FIN
 **************************************************************************/

/**************************************************************************
 * CAMBIOS EN LA BD RELACIONADOS CON LAS COMISIONES DE LOS EMPLEADOS - INICIO
 **************************************************************************/
/*
Agregar columna comprar en la tabla ARTICULOS
 */
ALTER TABLE `EMPLEADOS` CHANGE COLUMN `comision` `comision` DECIMAL(4,2) NULL DEFAULT NULL AFTER `obsemp`;
/**************************************************************************
* CAMBIOS EN LA BD RELACIONADOS CON LAS COMISIONES DE LOS EMPLEADOS - FIN
**************************************************************************/

/**************************************************************************
 * CAMBIOS EN LA BD RELACIONADOS CON EL USUARIO DE CREACION Y MODIFICACION - INICIO
 **************************************************************************/
/*
Agregar columna usucre en la tabla FACCAB
 */
ALTER TABLE `FACCAB` ADD `usucre` VARCHAR(255) NULL DEFAULT NULL;
/*
Agregar columna feccre en la tabla FACCAB
 */
ALTER TABLE `FACCAB` ADD `feccre` DATE NULL DEFAULT NULL;
/*
Agregar columna horcre en la tabla FACCAB
 */
ALTER TABLE `FACCAB` ADD `horcre` VARCHAR(5) NULL DEFAULT NULL;
/*
Agregar columna usumod en la tabla FACPROC
 */
ALTER TABLE `FACCAB` ADD `usumod` VARCHAR(255) NULL DEFAULT NULL;
/*
Agregar columna fecmod en la tabla FACCAB
 */
ALTER TABLE `FACCAB` ADD `fecmod` DATE NULL DEFAULT NULL;
/*
Agregar columna hormod en la tabla FACCAB
 */
ALTER TABLE `FACCAB` ADD `hormod` VARCHAR(5) NULL DEFAULT NULL;
/*
Agregar columna usucre en la tabla FACPROC
 */
ALTER TABLE `FACPROC` ADD `usucre` VARCHAR(255) NULL DEFAULT NULL;
/*
Agregar columna feccre en la tabla FACPROC
 */
ALTER TABLE `FACPROC` ADD `feccre` DATE NULL DEFAULT NULL;
/*
Agregar columna horcre en la tabla FACPROC
 */
ALTER TABLE `FACPROC` ADD `horcre` VARCHAR(5) NULL DEFAULT NULL;
/*
Agregar columna usumod en la tabla FACPROC
 */
ALTER TABLE `FACPROC` ADD `usumod` VARCHAR(255) NULL DEFAULT NULL;
/*
Agregar columna fecmod en la tabla FACPROC
 */
ALTER TABLE `FACPROC` ADD `fecmod` DATE NULL DEFAULT NULL;
/*
Agregar columna hormod en la tabla FACPROC
 */
ALTER TABLE `FACPROC` ADD `hormod` VARCHAR(5) NULL DEFAULT NULL;
/**************************************************************************
* CAMBIOS EN LA BD RELACIONADOS CON EL USUARIO DE CREACION Y MODIFICACION - FIN
**************************************************************************/

/**************************************************************************
 * CAMBIOS EN LA BD RELACIONADOS CON ACUMULACION DE PUNTOS POR VENTAS - INICIO
 **************************************************************************/
/*
Agregar columna puntos en la tabla FACLIN
 */
ALTER TABLE `FACLIN` ADD `puntos` DECIMAL(9,2) AFTER `obser`;
/**************************************************************************
 * CAMBIOS EN LA BD RELACIONADOS CON ACUMULACION DE PUNTOS POR VENTAS - FIN
 **************************************************************************/

/**************************************************************************
 * CAMBIOS EN LA BD RELACIONADOS CON OCULTAR TOTAL DE VENTAS EN TICKETS - INICIO
 **************************************************************************/
/*
Agregar columna ocutotdiatic en la tabla EMPRESA
 */
ALTER TABLE `EMPRESA` ADD `ocutotdiatic` VARCHAR(1) DEFAULT '' AFTER `noempcrear`;
/*
Agregar columna segmostotdiatic en la tabla FACLIN
 */
ALTER TABLE `EMPRESA` ADD `segmostotdiatic` INT DEFAULT 0 AFTER `ocutotdiatic`;
/**************************************************************************
* CAMBIOS EN LA BD RELACIONADOS CON OCULTAR TOTAL DE VENTAS EN TICKETS - FIN
 **************************************************************************/

/**************************************************************************
 * CAMBIOS EN LA BD RELACIONADOS CON IMAGEN DE USUARIOS - INICIO
 **************************************************************************/
/*
Agregar columna foto en la tabla USUARIOS
 */
ALTER TABLE `USUARIOS` ADD `foto` VARCHAR(254) AFTER `apellido`;
/**************************************************************************
* CAMBIOS EN LA BD RELACIONADOS CON IMAGEN DE USUARIOS - FIN
 **************************************************************************/

/**************************************************************************
 * CAMBIOS EN LA BD RELACIONADOS CON FACTURAS DE COMPRAS - INICIO
 **************************************************************************/
/*
Cambiar columna numalb en la tabla FACPROC
 */
ALTER TABLE `FACPROC` CHANGE COLUMN `numalb` `numalb` VARCHAR(20) DEFAULT NULL AFTER `ejealb`;
/**************************************************************************
* CAMBIOS EN LA BD RELACIONADOS CON FACTURAS DE COMPRAS - FIN
 **************************************************************************/

/**************************************************************************
 * CAMBIOS EN LA BD RELACIONADOS CON GESTION DE CAJAS - INICIO
 **************************************************************************/
/*
Creacion de la tabla CAJA
 */
CREATE TABLE CAJA
(
  codsuc               VARCHAR(8) NOT NULL,
  codcaj               INT NOT NULL AUTO_INCREMENT,
  tipo                 VARCHAR(1) NOT NULL COMMENT 'C - Caja de Cobro, P - Caja Principal',
  nomcaj               VARCHAR(100) NOT NULL,
  fondo                DOUBLE(16,4),
  PRIMARY KEY (codcaj),
  INDEX codsuc (codsuc),
  INDEX codcaj (codcaj)
);
/*
Creacion de la tabla CONMOV (Configuracion de Movimientos)
 */
CREATE TABLE CONMOV
(
  codsuc               VARCHAR(8) NOT NULL,
  codmov               INT NOT NULL AUTO_INCREMENT,
  desmov               VARCHAR(100) NOT NULL,
  ingegr               VARCHAR(1) NOT NULL COMMENT 'I - INGRESO, E - EGRESO',
  tipo                 VARCHAR(1) NOT NULL COMMENT 'C - CONTABLE, E - ESTADISTICO',
  cajas                VARCHAR(250),
  solemp               TINYINT(1),
  codemp               VARCHAR(15),
  solpro               TINYINT(1),
  codpro               VARCHAR(15),
  solart               TINYINT(1),
  codart               VARCHAR(15),
  obsoleto             TINYINT(1) DEFAULT 0,
  PRIMARY KEY (codmov),
  INDEX codsuc (codsuc),
  INDEX codmov (codmov)
);
/*
Creacion de la tabla MOVCAJ (Movimientos de Caja)
 */
CREATE TABLE MOVCAJ
(
  codsuc               VARCHAR(8) NOT NULL,
  codcaj               INT NOT NULL,
  nummov               INT NOT NULL AUTO_INCREMENT,
  fecmov               DATE NOT NULL,
  hormov               VARCHAR(5) NOT NULL,
  ingegr               VARCHAR(1) NOT NULL COMMENT 'I - INGRESO, E - EGRESO',
  tipo                 VARCHAR(1) NOT NULL COMMENT 'V - VENTA, C - COMPRA, M - MANUAL',
  serfac               VARCHAR(4),
  ejefac               DECIMAL(4,0),
  numfac               DECIMAL(10,0),
  serfacp              VARCHAR(4),
  ejefacp              DECIMAL(6,0),
  numfacp              DECIMAL(12,0),
  codmov               INT,
  codemp               VARCHAR(15),
  codpro               VARCHAR(15),
  codart               VARCHAR(15),
  impmov               DECIMAL(16,4) NOT NULL,
  obsmov               VARCHAR(250),
  PRIMARY KEY (nummov),
  INDEX codsuc (codsuc),
  INDEX codcaj (codcaj),
  INDEX nummov (nummov)
);
/*
Agregar columna usucre en la tabla CIECAB
 */
ALTER TABLE `CIECAB` ADD `usucre` VARCHAR(255) DEFAULT NULL;
/*
Agregar columna feccre en la tabla CIECAB
 */
ALTER TABLE `CIECAB` ADD `feccre` DATE DEFAULT NULL;
/*
Agregar columna horcre en la tabla CIECAB
 */
ALTER TABLE `CIECAB` ADD `horcre` VARCHAR(5) DEFAULT NULL;
/*
Agregar columna codcajven en la tabla EMPRESA
 */
ALTER TABLE `EMPRESA` ADD `codcajven` INT DEFAULT NULL AFTER `impincventas`;
/*
Agregar columna codcajcom en la tabla EMPRESA
 */
ALTER TABLE `EMPRESA` ADD `codcajcom` INT DEFAULT NULL AFTER `impinccompras`;
/*
Agregar columna codcajcom en la tabla EMPRESA
 */
ALTER TABLE `EMPRESA` ADD `codcajcom` INT DEFAULT NULL AFTER `impinccompras`;
/*
Agregar columna razpro en la tabla FACPROC
 */
ALTER TABLE `FACPROC` ADD `razpro` VARCHAR(100) DEFAULT NULL AFTER `codpro`;
/*
Agregar columna codforpag en la tabla CONMOV
 */
ALTER TABLE `CONMOV` ADD `codforpag` VARCHAR(10) DEFAULT NULL AFTER `tipo`;
/*
Agregar columna codforpag en la tabla MOVCAJ
 */
ALTER TABLE `MOVCAJ` ADD `codforpag` VARCHAR(10) NOT NULL AFTER `tipo`;
/*
Agregar columna codcaj en la tabla CIECAB
 */
ALTER TABLE `CIECAB` ADD `codcaj` INT DEFAULT NULL AFTER `numcie`;
/*
Agregar columna ultmov en la tabla CIECAB
 */
ALTER TABLE `CIECAB` ADD `ultmov` INT DEFAULT NULL AFTER `codcaj`;
/*
Modificar columna tipo en la tabla MOVCAJ
 */
ALTER TABLE `MOVCAJ` MODIFY `tipo` VARCHAR(1) NOT NULL COMMENT 'V - VENTA, C - COMPRA, M - MANUAL, A - AJUSTE';
/*
Agregar columna numcie en la tabla MOVCAJ
 */
ALTER TABLE `MOVCAJ` ADD `numcie` INT DEFAULT NULL AFTER `tipo`;
/*
Agregar columna solcaj en la tabla CONMOV
*/
ALTER TABLE `CONMOV` ADD `solcaj` TINYINT(1) DEFAULT NULL AFTER `codart`;
/*
Agregar columna codcaj en la tabla CONMOV
*/
ALTER TABLE `CONMOV` ADD `codcaj` INT DEFAULT NULL AFTER `solcaj`;
/*
Agregar columna codcajdes en la tabla MOVCAJ
*/
ALTER TABLE `MOVCAJ` ADD `codcajdes` INT DEFAULT NULL AFTER `codart`;
/*
Creacion de la tabla CAJFORPAG
 */
CREATE TABLE CAJFORPAG
(
  codsuc               VARCHAR(8) NOT NULL,
  codcaj               INT NOT NULL,
  codfp                VARCHAR(10) NOT NULL,
  fondo                DOUBLE(16,4),
  PRIMARY KEY (codsuc, codcaj, codfp)
);
/*
Agregar columna traautfoncie en la tabla CAJA
*/
ALTER TABLE `CAJA` ADD `traautfoncie` TINYINT(1) DEFAULT NULL;
/*
Agregar columna codcajdescie en la tabla CAJA
*/
ALTER TABLE `CAJA` ADD `codcajdescie` INT DEFAULT NULL AFTER `traautfoncie`;
/*
Modificar columna tipo en la tabla MOVCAJ
 */
ALTER TABLE `MOVCAJ` MODIFY `tipo` VARCHAR(1) NOT NULL COMMENT 'V - VENTA, C - COMPRA, M - MANUAL, A - AJUSTE, T - TRASPASO AUTOMATICO';
/**************************************************************************
 * CAMBIOS EN LA BD RELACIONADOS CON GESTION DE CAJAS - FIN
 **************************************************************************/