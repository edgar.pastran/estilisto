/**************************************************************************
 * DATOS RELACIONADOS CON GESTION DE CAJAS - INICIO
 **************************************************************************/
/*
Agregar registros en la tabla PANTALLAS
 */
INSERT INTO PANTALLAS (codpan, despan, pantalla) VALUES ('CAJAS', 'Cajas', 'cajas.html');
INSERT INTO PANTALLAS (codpan, despan, pantalla) VALUES ('CAJA_CIERRE', 'Cierre de Caja', 'caja_cierre.html');
INSERT INTO PANTALLAS (codpan, despan, pantalla) VALUES ('CAJA_MOVIMIENTOS', 'Movimentos de Caja', 'caja_movimientos.html');
INSERT INTO PANTALLAS (codpan, despan, pantalla) VALUES ('CAJA_REGISTROS', 'Registro de Movimientos de Caja', 'caja_registros.html');
INSERT INTO PANTALLAS (codpan, despan, pantalla) VALUES ('SUBMENU_CAJAS', 'Sub Menu Cajas', 'SUBMENU_CAJAS');
/*
Agregar registros en la tabla ACCESOS
 */
INSERT INTO ACCESOS (codgru, codpan) VALUES ('Administrador', 'CAJAS');
INSERT INTO ACCESOS (codgru, codpan) VALUES ('Administrador', 'CAJA_CIERRE');
INSERT INTO ACCESOS (codgru, codpan) VALUES ('Administrador', 'CAJA_MOVIMIENTOS');
INSERT INTO ACCESOS (codgru, codpan) VALUES ('Administrador', 'CAJA_REGISTROS');
INSERT INTO ACCESOS (codgru, codpan) VALUES ('Administrador', 'SUBMENU_CAJAS');
/**************************************************************************
 * DATOS RELACIONADOS CON GESTION DE CAJAS - FIN
 **************************************************************************/
