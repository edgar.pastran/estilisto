<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array('exito' => false);
    if (isset($_POST['sucursal'])) {
        $sucursal = $_POST['sucursal'];

        if (isset($_POST['opcion'])) {
            require_once("config/Config.php");
            $conexion = new Conexion();

            $subdirectorio_imagenes = 'familia';
            $nombre_campo_imagen = 'imagen_nueva';

            $opcion = $_POST['opcion'];
            if ($opcion == "consulta") {
                $sql =
                    "SELECT FAM.*, EMP.codemp ".
                    "FROM FAMILIA1 FAM, EMPRESA EMP ".
                    "WHERE FAM.codsuc = EMP.codsuc ".
                    "AND FAM.codsuc = '$sucursal'";
                $familias = $conexion->consulta($sql);
                for ($i=0; $i<count($familias); $i++) {
                    $codigo_grupo_empresas = $familias[$i]['codemp'];
                    $codigo_sucursal = $familias[$i]['codsuc'];
                    $foto = $URL_BASE_PATH.'servidor/images/nothing.png';
                    if (isset($familias[$i]['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$familias[$i]['foto'])) {
                        $foto = $URL_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$familias[$i]['foto'];
                    }
                    $familias[$i]['foto'] = $foto;
                }

                if (count($familias) > 1){
                    $respuesta = array( 'exito' => true, 'familias' => $familias, 'nume_regis' => count($familias));
                }
                else{
                    $respuesta = array('exito' => false, 'sucursal' => $sucursal);
                }
            }
            elseif ($opcion == "consultar") {
                $codfam1	= (isset($_POST["codfam1"]))?$_POST['codfam1']:"";

                $sql =
                    "SELECT * ".
                    "FROM FAMILIA1 ".
                    "WHERE codsuc = '$sucursal' ".
                    "AND codfam1 = '$codfam1'";
                $datos = $conexion->consulta($sql);

                if(count($datos)) {
                    $row = $datos[0];

                    $respuesta = array(
                        'exito' => true, 'codsuc' => $row['codsuc'],
                        'codfam1' => $row['codfam1'], 'desfam1' => $row['desfam1'], 'orden' => $row['orden'],
                        'puntos' => $row['puntos'], 'foto' => $row['foto'], 'vender' => $row['vender'],
                        'obsoleto' => $row['obsoleto']
                    );
                }
                else{
                    $respuesta = array('exito' => false, 'sucursal' => $sucursal);
                }
            }
            elseif ($opcion == "actualizar") {
                $codfam1	= (isset($_POST["codfam1"]))?$_POST['codfam1']:"";
                $desfam1	= (isset($_POST["desfam1"]))?$_POST['desfam1']:"";
                $orden	= (isset($_POST["orden"]))?$_POST['orden']:"";
                $puntos	= (isset($_POST["puntos"]))?$_POST['puntos']:"";
                $vender	= (isset($_POST["vender"]))?$_POST['vender']:"";
                $obsoleto	= (isset($_POST["obsoleto"]))?$_POST['obsoleto']:"";
                $foto		= "";

                $sql =
                    "SELECT FAM.foto, EMP.codemp ".
                    "FROM FAMILIA1 FAM, EMPRESA EMP ".
                    "WHERE FAM.codsuc = EMP.codsuc ".
                    "AND FAM.codsuc = '$sucursal' ".
                    "AND FAM.codfam1 = '".$codfam1."'";
                $familia_data = $conexion->consulta($sql);
                if (count($familia_data) > 0) {
                    $respuesta["exito"] = true;

                    require_once("lib/fileUploader.php");

                    $codigo_grupo_empresas = $familia_data[0]['codemp'];
                    $foto_anterior = isset($familia_data[0]['foto'])?$familia_data[0]['foto']:"";
                    if (isset($_FILES[$nombre_campo_imagen]['name']) && $_FILES[$nombre_campo_imagen]['name'] != '') {
                        $directorio_imagenes = $DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$sucursal.'/';
                        $respuesta = FileUploader::upload_image($nombre_campo_imagen, $directorio_imagenes, $foto_anterior, true);
                        if ($respuesta["exito"]) {
                            $foto = $respuesta["file_name"];
                        }
                    }
                    if ($respuesta["exito"]) {
                        $sql =
                            "UPDATE FAMILIA1 SET ".
                            "desfam1 = '$desfam1', ".
                            "orden = '$orden', ".
                            "puntos = '$puntos', ".
                            (($foto != "")?"foto = '$foto', ":"").
                            "vender = '$vender', ".
                            "obsoleto = '$obsoleto' ".
                            "WHERE codsuc = '$sucursal' ".
                            "AND codfam1='$codfam1'";
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                    }
                }
                else {
                    $respuesta = array('exito' => false, 'mensaje' => 'No se encontro la familia');
                }
            }
            elseif ($opcion == "insertar") {
                $codfam1	= (isset($_POST["codfam1"]))?$_POST['codfam1']:"";
                $desfam1	= (isset($_POST["desfam1"]))?$_POST['desfam1']:"";
                $orden	= (isset($_POST["orden"]))?$_POST['orden']:"";
                $puntos	= (isset($_POST["puntos"]))?$_POST['puntos']:"";
                $vender	= (isset($_POST["vender"]))?$_POST['vender']:"";
                $obsoleto	= (isset($_POST["obsoleto"]))?$_POST['obsoleto']:"";
                $foto		= "";

                $sql =
                    "SELECT EMP.codemp ".
                    "FROM EMPRESA EMP ".
                    "WHERE EMP.codsuc = '$sucursal'";
                $empresa_data = $conexion->consulta($sql);
                if (count($empresa_data) > 0) {
                    require_once("lib/fileUploader.php");

                    $codigo_grupo_empresas = $empresa_data[0]['codemp'];
                    if (isset($_FILES[$nombre_campo_imagen]['name']) && $_FILES[$nombre_campo_imagen]['name'] != '') {
                        $directorio_imagenes = $DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$sucursal.'/';
                        $respuesta = FileUploader::upload_image($nombre_campo_imagen, $directorio_imagenes, "", true);
                        if ($respuesta["exito"]) {
                            $foto = $respuesta["file_name"];
                        }
                    }
                }
                $sql =
                    "INSERT INTO FAMILIA1 ".
                    "(codsuc, codfam1, desfam1, orden, puntos, ".(($foto != "")?"foto, ":"")."vender, obsoleto) ".
                    "VALUES ".
                    "('$sucursal','$codfam1','$desfam1','$orden','$puntos',".(($foto != "")?"'$foto',":"")."'$vender','$obsoleto')";
                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
            }
            elseif ($opcion == "eliminar") {
                $codfam1	= (isset($_POST["codfam1"]))?$_POST['codfam1']:"";

                $sql =
                    "SELECT FAM.*, EMP.codemp ".
                    "FROM FAMILIA1 FAM, EMPRESA EMP ".
                    "WHERE FAM.codsuc = EMP.codsuc ".
                    "AND FAM.codsuc = '$sucursal' ".
                    "AND FAM.codfam1 = '".$codfam1."'";
                $familias = $conexion->consulta($sql);
                if (count($familias)) {
                    // Si existe un archivo asociado a la familia se borra
                    $familia = $familias[0];
                    $codigo_grupo_empresas = $familia['codemp'];
                    $codigo_sucursal = $familia['codsuc'];
                    if (isset($familia['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$familia['foto'])) {
                        unlink($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$familia['foto']);
                    }

                    // Se elimina la familia
                    $sql =
                        "DELETE FROM FAMILIA1 ".
                        "WHERE codsuc = '$sucursal' ".
                        "AND codfam1='$codfam1'";
                    $mensaje = $conexion->sentencia($sql);
                    $exito = strpos($mensaje, "Exito") !== false;
                    $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                }
            }
            elseif ($opcion == "remover_imagen") {
                $codfam1	= (isset($_POST["codfam1"]))?$_POST['codfam1']:"";

                $sql =
                    "SELECT FAM.*, EMP.codemp ".
                    "FROM FAMILIA1 FAM, EMPRESA EMP ".
                    "WHERE FAM.codsuc = EMP.codsuc ".
                    "AND FAM.codsuc = '$sucursal' ".
                    "AND FAM.codfam1 = '".$codfam1."'";
                $familias = $conexion->consulta($sql);
                if (count($familias)) {
                    // Si existe un archivo asociado a la familia se borra
                    $familia = $familias[0];
                    $codigo_grupo_empresas = $familia['codemp'];
                    $codigo_sucursal = $familia['codsuc'];
                    $respuesta = array( 'exito' => false);
                    if (isset($familia['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$familia['foto'])) {
                        unlink($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$familia['foto']);
                    }

                    $sql =
                        "UPDATE FAMILIA1 SET ".
                        "foto = NULL ".
                        "WHERE codsuc = '$sucursal' ".
                        "AND codfam1 = '$codfam1'";
                    $mensaje = $conexion->sentencia($sql);
                    $exito = strpos($mensaje, "Exito") !== false;
                    $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                }
            }
        }
    }
    echo json_encode($respuesta, true);
}
catch (Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
