<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

function movimiento_format($movimiento) {
    // Clase
    if ($movimiento['ingegr'] == "I") {
        $movimiento['ingegr_string'] = "Ingreso";
    }
    elseif ($movimiento['ingegr'] == "E") {
        $movimiento['ingegr_string'] = "Egreso";
    }
    // Tipo
    if ($movimiento['tipo'] == "V") {
        $movimiento['tipo_string'] = "Venta";
        if (isset($movimiento['numfac'])) {
            $movimiento['tipo_string'] .= " (Ticket # ".$movimiento['numfac'].")";
        }
    }
    elseif ($movimiento['tipo'] == "C") {
        $movimiento['tipo_string'] = "Compra";
        if (isset($movimiento['numfacp'])) {
            $movimiento['tipo_string'] .= " (Factura # ".$movimiento['numfacp'].")";
        }
    }
    elseif ($movimiento['tipo'] == "M") {
        $movimiento['tipo_string'] = "Manual";
        if (isset($movimiento['desmov'])) {
            $movimiento['tipo_string'] = $movimiento['desmov'];
        }
    }
    elseif ($movimiento['tipo'] == "A") {
        $movimiento['tipo_string'] = "Ajuste";
    }
    elseif ($movimiento['tipo'] == "T") {
        $movimiento['tipo_string'] = "Traspaso Automatico";
    }
    elseif ($movimiento['tipo'] == "TC") {
        $movimiento['tipo_string'] = "Teorico en Cierre";
    }
    $movimiento['fecmov_string'] = DateTime::createFromFormat('Y-m-d', $movimiento['fecmov'])->format('d-m-Y');
    if ($movimiento['ingegr'] == "E" && $movimiento['tipo'] != "TC") {
        $movimiento['impmov'] *= -1;
    }
    $movimiento['impmov_string'] = number_format($movimiento['impmov'], 2, ',', '.');
    return $movimiento;
}

try {//Controlar siempre el error
    $respuesta = array('exito' => false);
    if (isset($_POST['sucursal'])) {
        $sucursal = $_POST['sucursal'];

        if (isset($_POST['opcion'])) {
            require_once("config/Config.php");
            $conexion = new Conexion();

            $opcion = $_POST['opcion'];
            if ($opcion == "inicializar") {
                $data = $conexion->consulta("SELECT zonhor FROM EMPRESA where codsuc='".$sucursal."'");
                if (count($data)) {
                    // Fecha y Hora de la Sucursal
                    @date_default_timezone_set($data[0]['zonhor']);
                }
                // Cajas
                $sql =
                    "SELECT codcaj, nomcaj FROM CAJA ".
                    "WHERE codsuc = '".$sucursal."' ".
                    "ORDER BY codcaj ASC";
                $cajas = $conexion->consulta($sql);
                for ($i=0; $i<count($cajas); $i++) {
                    $ultimo_movimiento_cerrado = 0;
                    $fecha_ultimo_movimiento_cerrado = "1900-01-01";
                    $hora_ultimo_movimiento_cerrado = "00:00";
                    $ultimo_cierre = 0;
                    $sql =
                        "SELECT numcie, ultmov ".
                        "FROM CIECAB ".
                        "WHERE codsuc = '".$sucursal."' ".
                        "AND codcaj = '".$cajas[$i]['codcaj']."' ".
                        "ORDER BY numcie DESC ".
                        "LIMIT 0,1";
                    $data_ultimo_cierre = $conexion->consulta($sql);
                    if (isset($data_ultimo_cierre)) {
                        if (isset($data_ultimo_cierre[0]['ultmov'])) {
                            $ultimo_movimiento_cerrado = $data_ultimo_cierre[0]['ultmov'];
                            $ultimo_cierre = $data_ultimo_cierre[0]['numcie'];
                            $sql =
                                "SELECT fecmov, hormov ".
                                "FROM MOVCAJ ".
                                "WHERE codsuc = '".$sucursal."' ".
                                "AND codcaj = '".$cajas[$i]['codcaj']."' ".
                                "AND nummov = '".$ultimo_movimiento_cerrado."'";
                            $data_ultimo_movimiento_cerrado = $conexion->consulta($sql);
                            if (isset($data_ultimo_movimiento_cerrado)) {
                                $fecha_ultimo_movimiento_cerrado = $data_ultimo_movimiento_cerrado[0]['fecmov'];
                                $hora_ultimo_movimiento_cerrado = $data_ultimo_movimiento_cerrado[0]['hormov'];
                            }
                        }
                    }
                    $cajas[$i]['fecultcie'] = date_format(date_create($fecha_ultimo_movimiento_cerrado), "d-m-Y");
                    $cajas[$i]['horultcie'] = $hora_ultimo_movimiento_cerrado;

                    // Montos por cada forma de pago
                    // Montos teoricos del ultimo cierre
                    $sql =
                        "SELECT codfp AS codforpag, impteo AS impmov ".
                        "FROM CIELIN ".
                        "WHERE codsuc = '".$sucursal."' ".
                        "AND numcie = '".$ultimo_cierre."'";
                    $lineas_ultimo_cierre = $conexion->consulta($sql);
                    for ($l=0; $l<count($lineas_ultimo_cierre); $l++) {
                        $lineas_ultimo_cierre[$l]['codcaj'] = $cajas[$i]['codcaj'];
                        $lineas_ultimo_cierre[$l]['tipo'] = 'TC';
                        $lineas_ultimo_cierre[$l]['fecmov'] = $fecha_ultimo_movimiento_cerrado;
                        $lineas_ultimo_cierre[$l]['hormov'] = $hora_ultimo_movimiento_cerrado;
                        $lineas_ultimo_cierre[$l]['obsmov'] = 'Teorico en Cierre # '.$ultimo_cierre;
                        if ($lineas_ultimo_cierre[$l]['impmov'] >= 0) {
                            $lineas_ultimo_cierre[$l]['ingegr'] = 'I';
                        }
                        else {
                            $lineas_ultimo_cierre[$l]['ingegr'] = 'E';
                        }
                    }

                    // Movimientos posteriores al ultimo cierre
                    $sql =
                        "SELECT M.*, C.desmov ".
                        "FROM MOVCAJ M ".
                        "LEFT JOIN CONMOV C ON M.codsuc = C.codsuc AND M.codmov = C.codmov ".
                        "WHERE M.codsuc = '".$sucursal."' ".
                        "AND M.codcaj = '".$cajas[$i]['codcaj']."' ".
                        "AND M.nummov > ".$ultimo_movimiento_cerrado." ".
                        "ORDER BY M.fecmov, M.hormov ASC";
                    $movimientos_data = $conexion->consulta($sql);
                    $movimientos = array_merge($lineas_ultimo_cierre, $movimientos_data);
                    for ($m=0; $m<count($movimientos); $m++) {
                        $movimientos[$m] = movimiento_format($movimientos[$m]);
                        // Forma de Pago
                        $movimientos[$m]['forpag_string'] = "Cualquiera";
                        if (isset($movimientos[$m]['codforpag'])) {
                            $data = $conexion->consulta("SELECT des FROM FORPAG WHERE codsuc='".$sucursal."' AND codfp='".$movimientos[$m]['codforpag']."'");
                            if (count($data)) {
                                $movimientos[$m]['forpag_string'] = $data[0]['des'];
                            }
                        }
                    }
                    $cajas[$i]["movimientos"] = $movimientos;

                    // Movimientos de Ingreso
                    $sql =
                        "SELECT codmov, desmov, ingegr, tipo, codforpag, solemp, codemp, solpro, codpro, solart, codart, solcaj, codcaj ".
                        "FROM CONMOV ".
                        "WHERE codsuc = '".$sucursal."' ".
                        "AND cajas LIKE '%&".$cajas[$i]['codcaj']."&%' ".
                        "AND (obsoleto = 0 OR obsoleto IS NULL) ".
                        "AND ingegr = 'I' ";
                        "ORDER BY desmov ASC";
                    $cajas[$i]["ingresos"] = $conexion->consulta($sql);

                    // Movimientos de Egreso
                    $sql =
                        "SELECT codmov, desmov, ingegr, tipo, codforpag, solemp, codemp, solpro, codpro, solart, codart, solcaj, codcaj ".
                        "FROM CONMOV ".
                        "WHERE codsuc = '".$sucursal."' ".
                        "AND cajas LIKE '%&".$cajas[$i]['codcaj']."&%' ".
                        "AND (obsoleto = 0 OR obsoleto IS NULL) ".
                        "AND ingegr = 'E' ";
                    "ORDER BY desmov ASC";
                    $cajas[$i]["egresos"] = $conexion->consulta($sql);

                    // Fondo en esta Caja por Forma de Pago
                    $sql =
                        "SELECT codfp, fondo FROM CAJFORPAG ".
                        "WHERE codsuc = '".$sucursal."' ".
                        "AND codcaj = '".$cajas[$i]['codcaj']."'";
                    $cajas[$i]["fondos"] = $conexion->consulta($sql);
                }
                // Formas de Pago
                $sql =
                    "SELECT codfp, des FROM FORPAG ".
                    "WHERE vender = 1 ".
                    "AND codsuc = '".$sucursal."' ".
                    "ORDER BY codfp ASC";
                $formas_pago = $conexion->consulta($sql);
                // Empleados
                $sql =
                    "SELECT codemp, nomemp, ape1emp FROM EMPLEADOS ".
                    "WHERE (obsoleto = 0 OR obsoleto IS NULL) ".
                    "AND codsuc = '".$sucursal."' ".
                    "ORDER BY nomemp ASC";
                $empleados = $conexion->consulta($sql);
                // Proveedores
                $sql =
                    "SELECT codpro, razon FROM PROVEEDOR ".
                    "WHERE (obsoleto = 0 OR obsoleto IS NULL) ".
                    "AND codsuc='".$sucursal."' ".
                    "ORDER BY razon ASC";
                $proveedores = $conexion->consulta($sql);
                // Articulos
                $sql =
                    "SELECT codart, desart FROM ARTICULOS ".
                    "WHERE (obsoleto = 0 OR obsoleto IS NULL) ".
                    "AND codsuc = '".$sucursal."' ".
                    "ORDER BY desart ASC";
                $articulos = $conexion->consulta($sql);
                $respuesta = array( 'exito' => true, 'cajas' => $cajas, 'formas_pago' => $formas_pago, 'empleados' => $empleados, 'proveedores' => $proveedores, 'articulos' => $articulos);
            }
            elseif ($opcion == "guardar") {
                $data = $conexion->consulta("SELECT zonhor FROM EMPRESA where codsuc='".$sucursal."'");
                if (count($data)) {
                    // Fecha y Hora de la Sucursal
                    @date_default_timezone_set($data[0]['zonhor']);
                }
                $movimiento = array(
                    "codsuc" => $sucursal,
                    "codcaj" => (isset($_POST["codcaj"]) && $_POST["codcaj"]!="")?$_POST['codcaj']:"",
                    "fecmov" => date('Y-m-d'),
                    "hormov" => date('H:i'),
                    "ingegr" => (isset($_POST["ingegr"]) && $_POST["ingegr"]!="")?$_POST['ingegr']:"",
                    "tipo"   => "M",
                    "codforpag" => (isset($_POST["codforpag"]) && $_POST["codforpag"]!="")?$_POST['codforpag']:"",
                    "codmov" => (isset($_POST["codmov"]) && $_POST["codmov"]!="")?$_POST['codmov']:null,
                    "codemp" => (isset($_POST["codemp"]) && $_POST["codemp"]!="")?$_POST['codemp']:null,
                    "codpro" => (isset($_POST["codpro"]) && $_POST["codpro"]!="")?$_POST['codpro']:null,
                    "codart" => (isset($_POST["codart"]) && $_POST["codart"]!="")?$_POST['codart']:null,
                    "codcajdes" => (isset($_POST["codcajdes"]) && $_POST["codcajdes"]!="")?$_POST['codcajdes']:null,
                    "impmov" => (isset($_POST["impmov"]) && $_POST["impmov"]!="")?$_POST['impmov']:0,
                    "obsmov" => (isset($_POST["obsmov"]) && $_POST["obsmov"]!="")?$_POST['obsmov']:""
                );
                $mensaje = $conexion->insertar_desde_objeto($movimiento, "MOVCAJ");
                $exito = (strpos($mensaje, "Exito") !== false);
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                if ($exito) {
                    $sql =
                        "SELECT desmov ".
                        "FROM CONMOV ".
                        "WHERE codsuc = '".$sucursal."' ".
                        "AND codmov = '".$_POST["codmov"]."' ";
                    $movimiento_data = $conexion->consulta($sql);
                    if (isset($movimiento_data)) {
                        $movimiento["desmov"] = $movimiento_data[0]["desmov"];
                    }
                    $movimiento = movimiento_format($movimiento);
                    // Forma de Pago
                    $movimiento['forpag_string'] = "Cualquiera";
                    if (isset($movimiento['codforpag'])) {
                        $data = $conexion->consulta("SELECT des FROM FORPAG WHERE codsuc='".$sucursal."' AND codfp='".$movimiento['codforpag']."'");
                        if (count($data)) {
                            $movimiento['forpag_string'] = $data[0]['des'];
                        }
                    }
                    $respuesta['movimiento'] = $movimiento;

                    // Registrar el ingreso en la otra caja si es un traspaso de saldo
                    if ($movimiento["ingegr"] == 'E' && isset($_POST["tipo"]) && $_POST["tipo"] == "E" && isset($_POST["codcajdes"]) && $_POST["codcajdes"] != "") {
                        // Nombre de Caja
                        $nombre_caja = "";
                        $data = $conexion->consulta("SELECT nomcaj FROM CAJA WHERE codsuc='".$sucursal."' AND codcaj='".$movimiento["codcaj"]."'");
                        if (count($data)) {
                            $nombre_caja = $data[0]['nomcaj'];
                        }
                        $movimiento = array(
                            "codsuc" => $sucursal,
                            "codcaj" => $_POST["codcajdes"],
                            "fecmov" => date('Y-m-d'),
                            "hormov" => date('H:i'),
                            "ingegr" => "I",
                            "tipo"   => "M",
                            "codforpag" => (isset($_POST["codforpag"]) && $_POST["codforpag"]!="")?$_POST['codforpag']:"",
                            "codmov" => (isset($_POST["codmov"]) && $_POST["codmov"]!="")?$_POST['codmov']:null,
                            "codemp" => (isset($_POST["codemp"]) && $_POST["codemp"]!="")?$_POST['codemp']:null,
                            "codpro" => (isset($_POST["codpro"]) && $_POST["codpro"]!="")?$_POST['codpro']:null,
                            "codart" => (isset($_POST["codart"]) && $_POST["codart"]!="")?$_POST['codart']:null,
                            "codcajdes" => null,
                            "impmov" => (isset($_POST["impmov"]) && $_POST["impmov"]!="")?$_POST['impmov']:0,
                            "obsmov" => "Traspaso de Saldo (Desde ".$nombre_caja.")"
                        );
                        $mensaje = $conexion->insertar_desde_objeto($movimiento, "MOVCAJ");
                        $exito = (strpos($mensaje, "Exito") !== false);
                        $respuesta['exito'] = $exito;
                        $respuesta['mensaje'] = $mensaje;
                        if ($exito) {
                            $sql =
                                "SELECT desmov ".
                                "FROM CONMOV ".
                                "WHERE codsuc = '".$sucursal."' ".
                                "AND codmov = '".$movimiento["codmov"]."' ";
                            $movimiento_data = $conexion->consulta($sql);
                            if (isset($movimiento_data)) {
                                $movimiento["desmov"] = $movimiento_data[0]["desmov"];
                            }
                            $movimiento = movimiento_format($movimiento);
                            // Forma de Pago
                            $movimiento['forpag_string'] = "Cualquiera";
                            if (isset($movimiento['codforpag'])) {
                                $data = $conexion->consulta("SELECT des FROM FORPAG WHERE codsuc='".$sucursal."' AND codfp='".$movimiento['codforpag']."'");
                                if (count($data)) {
                                    $movimiento['forpag_string'] = $data[0]['des'];
                                }
                            }
                            $respuesta['movimiento_inverso'] = $movimiento;
                        }
                    }
                    // Registrar como una factura de compra si es un Egreso Contable
                    elseif ($movimiento["ingegr"] == 'E' && isset($_POST["tipo"]) && $_POST["tipo"] == "C") {
                        if (isset($movimiento["codpro"]) && isset($movimiento["codart"])) {
                            // Serie
                            $serie = null;
                            $zona_horaria = null;
                            $data = $conexion->consulta("SELECT zonhor, serie, impinccompras FROM EMPRESA where codsuc='".$sucursal."'");
                            if (count($data)) {
                                $serie = $data[0]['serie'];
                                $zona_horaria = $data[0]['zonhor'];
                            }
                            // Ejercicio
                            $ejercicio = date("Y");
                            // Numero de Factura
                            $numero = 1;
                            $data = $conexion->consulta("SELECT MAX(numfacp) as ultima_factura FROM FACPROC WHERE codsuc='".$sucursal."' AND ejefacp='".$ejercicio."'");
                            if (count($data)) {
                                $numero += intval($data[0]['ultima_factura']);
                            }
                            // Nombre de Caja
                            $nombre_caja = "";
                            $data = $conexion->consulta("SELECT nomcaj FROM CAJA WHERE codsuc='".$sucursal."' AND codcaj='".$movimiento["codcaj"]."'");
                            if (count($data)) {
                                $nombre_caja = $data[0]['nomcaj'];
                            }
                            if (isset($zona_horaria) && isset($serie) && isset($ejercicio) && isset($numero)) {
                                $factura = array (
                                    "codsuc"  => $sucursal,
                                    "serfacp" => $serie,
                                    "ejefacp" => $ejercicio,
                                    "numfacp" => $numero,
                                    "fecfacp" => $movimiento["fecmov"],
                                    "codpro"  => $movimiento["codpro"],
                                    "obsfac"  => "Egreso Contable (".$nombre_caja.") - ".$movimiento["obsmov"],
                                    "totimpbas" => $movimiento["impmov"],
                                    "totfacp" => $movimiento["impmov"],
                                    "lineas"  => (1 * 5),
                                    "usucre"  => (isset($_POST["usuario"]) && $_POST["usuario"]!="")?$_POST['usuario']:"",
                                    "feccre"  => date("Y-m-d"),
                                    "horcre"  => date("H:i"),
                                    "usumod"  => (isset($_POST["usuario"]) && $_POST["usuario"]!="")?$_POST['usuario']:"",
                                    "fecmod"  => date("Y-m-d"),
                                    "hormod"  => date("H:i")
                                );
                                // Registro de la factura
                                $mensaje = $conexion->insertar_desde_objeto($factura, "FACPROC");
                                $exito = strpos($mensaje, "Exito") !== false;
                                if ($exito) {
                                    $linea = array(
                                        "codsuc"  => $sucursal,
                                        "serfacp" => $serie,
                                        "ejefacp" => $ejercicio,
                                        "numfacp" => $numero,
                                        "linfacp" => (1 * 5),
                                        "codart"  => $movimiento["codart"],
                                        "desart"  => (isset($_POST["desart"]) && $_POST["desart"]!="")?$_POST['desart']:"",
                                        "preven"  => $movimiento["impmov"],
                                        "canser"  => 1,
                                        "subtot"  => $movimiento["impmov"],
                                        "impbas"  => $movimiento["impmov"],
                                        "totlin"  => $movimiento["impmov"]
                                    );
                                    // Registro de la linea en la factura
                                    $mensaje = $conexion->insertar_desde_objeto($linea, "FACPROL");
                                    $exito = strpos($mensaje, "Exito") !== false;
                                }
                            }
                        }

                        $respuesta['exito'] = $exito;
                        $respuesta['mensaje'] = $mensaje;
                    }
                }
            }
        }
    }
    echo json_encode($respuesta, true);
}
catch (Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
