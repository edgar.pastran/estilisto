<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array('exito' => false);
    if (isset($_POST['sucursal'])) {
        $sucursal = $_POST['sucursal'];

        if (isset($_POST['opcion'])) {
            require_once("config/Config.php");
            $conexion = new Conexion();

            $subdirectorio_imagenes = 'empleado';
            $nombre_campo_imagen = 'imagen_nueva';

            $opcion = $_POST['opcion'];
            if ($opcion == "consulta") {
                $sql =
                    "SELECT EMPL.*, EMP.codemp AS codempresa ".
                    "FROM EMPLEADOS EMPL, EMPRESA EMP ".
                    "WHERE EMPL.codsuc = EMP.codsuc ".
                    "AND EMPL.codsuc = '$sucursal' ".
                    "ORDER BY EMPL.obsoleto ASC, EMPL.codemp ASC";
                $empleados = $conexion->consulta($sql);
                for ($i=0; $i<count($empleados); $i++) {
                    $codigo_grupo_empresas = $empleados[$i]['codempresa'];
                    $codigo_sucursal = $empleados[$i]['codsuc'];
                    $foto = $URL_BASE_PATH.'servidor/images/nothing.png';
                    if (isset($empleados[$i]['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'./*$codigo_sucursal.'/'.*/$empleados[$i]['foto'])) {
                        $foto = $URL_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'./*$codigo_sucursal.'/'.*/$empleados[$i]['foto'];
                    }
                    $empleados[$i]['foto'] = $foto;
                }

                if (count($empleados) > 0){
                    $respuesta = array('exito' => true, 'empleados' => $empleados, 'nume_regis' => count($empleados));
                }
                else{
                    $respuesta = array('exito' => false, 'sucursal' => $sucursal);
                }
            }
            else if ($opcion == "consultar") {
                $codemp	= (isset($_POST["codemp"]))?$_POST['codemp']:"";

                $sql =
                    "SELECT * ".
                    "FROM EMPLEADOS ".
                    "WHERE codsuc = '$sucursal' ".
                    "AND codemp = '$codemp'";
                $datos = $conexion->consulta($sql);

                if (count($datos)) {
                    $row = $datos[0];
                    $respuesta = array(
                        'exito' => true, 'codsuc' => $row['codsuc'],
                        'codemp' => $row['codemp'], 'nomemp' => $row['nomemp'], 'foto' => $row['foto'],
                        'ape1emp' => $row['ape1emp'], 'tel1emp' => $row['tel1emp'],
                        'dniemp' => $row['dniemp'], 'sexo' => $row['sexo'], 'email' => $row['email'],
                        'fecnac' => $row['fecnac'], 'diremp' => $row['diremp'],
                        'vender' => $row['vender'], 'obsoleto' => $row['obsoleto']
                    );
                }
                else{
                    $respuesta = array('exito' => false, 'sucursal' => $sucursal);
                }
            }
            else if ($opcion == "actualizar") {
                $codemp	= (isset($_POST["codemp"]))?$_POST['codemp']:"";
                $nomemp	= (isset($_POST["nomemp"]))?$_POST['nomemp']:"";
                $ape1emp= (isset($_POST["ape1emp"]))?$_POST['ape1emp']:"";
                $foto	= (isset($_POST["foto"]))?$_POST['foto']:"";
                $tel1emp= (isset($_POST["tel1emp"]))?$_POST['tel1emp']:"";
                $dniemp	= (isset($_POST["dniemp"]))?$_POST['dniemp']:"";
                $sexo	= (isset($_POST["sexo"]))?$_POST['sexo']:"";
                $email	= (isset($_POST["email"]))?$_POST['email']:"";
                if (isset($_POST["fecnac"])) {
                    $fecnac	= $_POST['fecnac'];
                    $dia = substr($fecnac,0,2);
                    $mes1 = substr($fecnac,3,2);
                    $anio = substr($fecnac,6,4);
                    $fecnac=$anio."/".$mes1."/".$dia;
                }
                else {
                    $fecnac = "";
                }
                $diremp	 = (isset($_POST["diremp"]))?$_POST['diremp']:"";
                $codemp2 = (isset($_POST["codemp2"]))?$_POST['codemp2']:"";
                $vender	 = (isset($_POST["vender"]))?$_POST['vender']:"";
                $obsoleto= (isset($_POST["obsoleto"]))?$_POST['obsoleto']:"";
                $razon = $nomemp." ".$ape1emp;

                $sql =
                    "SELECT EMPL.foto, EMP.codemp AS codempresa ".
                    "FROM EMPLEADOS EMPL, EMPRESA EMP ".
                    "WHERE EMPL.codsuc = EMP.codsuc ".
                    "AND EMPL.codsuc = '".$sucursal."' ".
                    "AND EMPL.codemp = '".$codemp."'";
                $empleado_data = $conexion->consulta($sql);
                if (count($empleado_data) > 0) {
                    $respuesta["exito"] = true;

                    require_once("lib/fileUploader.php");

                    $codigo_grupo_empresas = $empleado_data[0]['codempresa'];
                    $foto_anterior = isset($empleado_data[0]['foto'])?$empleado_data[0]['foto']:"";
                    if (isset($_FILES[$nombre_campo_imagen]['name']) && $_FILES[$nombre_campo_imagen]['name'] != '') {
                        $directorio_imagenes = $DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'/*.$sucursal.'/'*/;
                        $respuesta = FileUploader::upload_image($nombre_campo_imagen, $directorio_imagenes, $foto_anterior, true);
                        if ($respuesta["exito"]) {
                            $foto = $respuesta["file_name"];
                        }
                    }
                    if ($respuesta["exito"]) {
                        $sql =
                            "UPDATE EMPLEADOS SET ".
                            "nomemp = '$nomemp', ".
                            "ape1emp = '$ape1emp', ".
                            (($foto != "")?"foto = '$foto', ":"").
                            "tel1emp = '$tel1emp', ".
                            "dniemp = '$dniemp', ".
                            "sexo = '$sexo', ".
                            "email = '$email', ".
                            "fecnac = '$fecnac', ".
                            "diremp = '$diremp', ".
                            "vender = '$vender', ".
                            "obsoleto = '$obsoleto' ".
                            "WHERE codsuc='$sucursal' ".
                            "AND codemp='$codemp'";
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        if ($exito) {
                            $sql =
                                "UPDATE PROVEEDOR SET ".
                                "razon = '$razon', ".
                                "tel1pro = '$tel1emp', ".
                                "nifpro = '$dniemp', ".
                                "mailpro = '$email', ".
                                "dirpro = '$diremp', ".
                                "tippro = 'EMPLEADO' ".
                                "WHERE codsuc = '$sucursal' ".
                                "AND codpro='$codemp'";
                            $mensaje = $conexion->sentencia($sql);
                            $exito = strpos($mensaje, "Exito") !== false;
                        }
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                    }
                }
                else {
                    $respuesta = array('exito' => false, 'mensaje' => 'No se encontro el empleado');
                }
            }
            else if ($opcion == "insertar") {
                $codemp	= (isset($_POST["codemp"]))?$_POST['codemp']:"";
                $nomemp	= (isset($_POST["nomemp"]))?$_POST['nomemp']:"";
                $ape1emp= (isset($_POST["ape1emp"]))?$_POST['ape1emp']:"";
                $foto	= (isset($_POST["foto"]))?$_POST['foto']:"";
                $tel1emp= (isset($_POST["tel1emp"]))?$_POST['tel1emp']:"";
                $dniemp	= (isset($_POST["dniemp"]))?$_POST['dniemp']:"";
                $sexo	= (isset($_POST["sexo"]))?$_POST['sexo']:"";
                $email	= (isset($_POST["email"]))?$_POST['email']:"";
                if (isset($_POST["fecnac"])) {
                    $fecnac	= $_POST['fecnac'];
                    $dia = substr($fecnac,0,2);
                    $mes1 = substr($fecnac,3,2);
                    $anio = substr($fecnac,6,4);
                    $fecnac=$anio."/".$mes1."/".$dia;
                }
                else {
                    $fecnac = "";
                }
                $diremp	 = (isset($_POST["diremp"]))?$_POST['diremp']:"";
                $razon = $nomemp." ".$ape1emp;

                $sql =
                    "SELECT EMP.codemp ".
                    "FROM EMPRESA EMP ".
                    "WHERE EMP.codsuc = '$sucursal'";
                $empresa_data = $conexion->consulta($sql);
                if (count($empresa_data) > 0) {
                    require_once("lib/fileUploader.php");

                    $codigo_grupo_empresas = $empresa_data[0]['codemp'];
                    if (isset($_FILES[$nombre_campo_imagen]['name']) && $_FILES[$nombre_campo_imagen]['name'] != '') {
                        $directorio_imagenes = $DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'/*.$sucursal.'/'*/;
                        $respuesta = FileUploader::upload_image($nombre_campo_imagen, $directorio_imagenes, "", true);
                        if ($respuesta["exito"]) {
                            $foto = $respuesta["file_name"];
                        }
                    }
                }

                $sql =
                    "INSERT INTO EMPLEADOS ".
                    "(codsuc, codemp, nomemp, foto, ape1emp, tel1emp, dniemp, sexo, email, fecnac, diremp) ".
                    "VALUES ".
                    "('$sucursal','$codemp','$nomemp','$foto','$ape1emp','$tel1emp','$dniemp','$sexo','$email','$fecnac','$diremp')";
                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                if ($exito) {
                    $sql =
                        "SELECT * FROM PROVEEDOR ".
                        "WHERE codsuc='$sucursal' ".
                        "AND codpro='$codemp'";
                    $datos = $conexion->consulta($sql);
                    if (count($datos)) {
                        $sql =
                            "UPDATE PROVEEDOR SET ".
                            "razon = '$razon', ".
                            "tel1pro = '$tel1emp', ".
                            "nifpro = '$dniemp', ".
                            "mailpro = '$email', ".
                            "dirpro = '$diremp', ".
                            "tippro = 'EMPLEADO' ".
                            "WHERE codsuc = '$sucursal' ".
                            "AND codpro='$codemp'";
                    }
                    else {
                        $sql =
                            "INSERT INTO PROVEEDOR (codsuc, codpro, razon, nifpro, tel1pro, mailpro, dirpro, tippro) ".
                            "VALUES ".
                            "('$sucursal','$codemp','$razon','$dniemp','$tel1emp','$email','$diremp','EMPLEADO')";
                    }
                    $mensaje = $conexion->sentencia($sql);
                    $exito = strpos($mensaje, "Exito") !== false;
                }
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
            }
            else if ($opcion == "eliminar") {
                $codemp	= (isset($_POST["codemp"]))?$_POST['codemp']:"";

                $sql =
                    "SELECT EMPL.*, EMP.codemp ".
                    "FROM EMPLEADOS EMPL, EMPRESA EMP ".
                    "WHERE EMPL.codsuc = EMP.codsuc ".
                    "AND EMPL.codsuc = '".$sucursal."' ".
                    "AND EMPL.codemp = '".$codemp."'";
                $empleados = $conexion->consulta($sql);
                if (count($empleados)) {
                    /******** VERIFICAR SI TIENE TICKET O FACTURA ***************/
                    $ventas = $conexion->consulta("select * from FACLIN where codsuc='$sucursal' and codemp='$codemp' LIMIT 1");
                    if(count($ventas) == 0){ /******** NO TIENE VENTAS ***************/
                        $eliminar='Sin Ventas';
                        $compras = $conexion->consulta("select * from FACPROC where codsuc='$sucursal' and codpro='$codemp' LIMIT 1");
                        if (count($compras) == 0) {/******** NO TIENE COMPRAS ***************/
                            $eliminar='true';
                        }
                        else {
                            $eliminar='Tiene Compras';
                        }
                    }
                    else{
                        $eliminar='Tiene VENTAS';
                    }

                    if ($eliminar=='true') {
                        // Si existe un archivo asociado se borra
                        $empleado = $empleados[0];
                        $codigo_grupo_empresas = $empleado['codemp'];
                        $codigo_sucursal = $empleado['codsuc'];
                        if (isset($empleado['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'./*$codigo_sucursal.'/'.*/$empleado['foto'])) {
                            unlink($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'./*$codigo_sucursal.'/'.*/$empleado['foto']);
                        }

                        // Se elimina
                        $sql =
                            "DELETE FROM PROVEEDOR ".
                            "WHERE codsuc = '$sucursal' ".
                            "AND codpro = '$codemp'";
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        if ($exito) {
                            $sql =
                                "DELETE FROM EMPLEADOS ".
                                "WHERE codsuc = '$sucursal' ".
                                "AND codemp = '$codemp'";
                            $mensaje = $conexion->sentencia($sql);
                            $exito = strpos($mensaje, "Exito") !== false;
                        }
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje, 'eliminar' => $eliminar);
                    }
                    else{
                        $sql =
                            "UPDATE PROVEEDOR SET ".
                            "obsoleto = '1' ".
                            "WHERE codsuc = '$sucursal' ".
                            "AND codpro='$codemp'";
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        if ($exito) {
                            $sql =
                                "UPDATE EMPLEADOS SET ".
                                "obsoleto = '1' ".
                                "WHERE codsuc = '$sucursal' ".
                                "AND codemp='$codemp'";
                            $mensaje = $conexion->sentencia($sql);
                            $exito = strpos($mensaje, "Exito") !== false;
                        }
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje, 'eliminar' => $eliminar);
                    }
                }
            }
            else if ($opcion == "remover_imagen") {
                $codemp	= (isset($_POST["codemp"]))?$_POST['codemp']:"";

                $sql =
                    "SELECT EMPL.*, EMP.codemp ".
                    "FROM EMPLEADOS EMPL, EMPRESA EMP ".
                    "WHERE EMPL.codsuc = EMP.codsuc ".
                    "AND EMPL.codsuc = '$sucursal' ".
                    "AND EMPL.codemp = '".$codemp."'";
                $empleados = $conexion->consulta($sql);
                if (count($empleados)) {
                    // Si existe un archivo asociado se borra
                    $empleado = $empleados[0];
                    $codigo_grupo_empresas = $empleado['codemp'];
                    $codigo_sucursal = $empleado['codsuc'];
                    $respuesta = array( 'exito' => false);
                    if (isset($empleado['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'/*.$codigo_sucursal.'/'*/.$empleado['foto'])) {
                        unlink($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'/*.$codigo_sucursal.'/'*/.$empleado['foto']);
                    }

                    $sql =
                        "UPDATE EMPLEADOS SET ".
                        "foto = NULL ".
                        "WHERE codsuc = '$sucursal' ".
                        "AND codemp = '$codemp'";
                    $mensaje = $conexion->sentencia($sql);
                    $exito = strpos($mensaje, "Exito") !== false;
                    $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                }
            }
        }
    }
    echo json_encode($respuesta, true);
}
catch (Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
