<?php //Prueba de Coneccion y Formulario
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");
//sleep(2);
try {//Controlar siempre el error
	$fecha_ini= '';
	$fecha_fin= '';

	$fecha_desde= '';
	$fecha_hasta= '';

	$ejercicio= '';
	$sucursal= '';

	if (empty($_GET['sucursal']) || empty($_GET['fecha_ini']) || empty($_GET['fecha_fin'])) {	
		if (empty($_GET['sucursal']) ) {throw new Exception('Sucursal Missing.');}
		if (empty($_GET['fecha_ini'])) {throw new Exception('fecha_ini Missing.');}
		if (empty($_GET['fecha_fin'])) { throw new Exception('fecha_fin Missing.');}
	}else
	{
		$fecha_ini=$_GET['fecha_ini'];
		$fecha_fin=$_GET['fecha_fin'];
		$sucursal=$_GET['sucursal'];

		$dia = substr($fecha_ini,0,2);
		$mes1 = substr($fecha_ini,3,2);
		$anio = substr($fecha_ini,6,4);
		$fecha_desde=$anio."/".$mes1."/".$dia;

		$dia = substr($fecha_fin,0,2);
		$mes1 = substr($fecha_fin,3,2);
		$anio = substr($fecha_fin,6,4);
		$fecha_hasta=$anio."/".$mes1."/".$dia;

		$ejercicio = stripslashes($ejercicio);
		$sucursal = stripslashes($sucursal);


		//------DECLARACION DE VARIABLES--------
		$tot_ventas    = array();
		$tot_fecha     = array();
		$tot_tikets    = array();

		$totalcom  = 0;
		$totalven  = 0;
		$totalben  = 0;
		$totaltik  = 0;

		$promecom  = 0;
		$promeven  = 0;
		$promeben  = 0;
		$prometik  = 0;

		//------ VARIABLES CIERRE DE CAJA--------
		$tot_efectivo  = array();
		$tot_tarjeta   = array();
		$tot_gastos    = array();
		$tot_horcie    = array();

		$totalefe  = 0;
		$totaltar  = 0;
		$totalgas  = 0;

		$promeefe  = 0;
		$prometar  = 0;
		$promegas  = 0;

		//------

		include('config.php');
		//------------ BUSQUEDA DE LAS VENTAS ------------ 
		$sql = "SELECT * FROM FACCAB WHERE codsuc = '$sucursal' and fecfac BETWEEN '$fecha_desde' and '$fecha_hasta' ORDER BY codsuc DESC, ejefac DESC, fecfac DESC";
		$result = mysql_query($sql);
		$nume_regis=mysql_num_rows($result);

		//--declaracion de Variables para el diario de ventas--
		$contador=-1;
		$entradas =-1;
		$ventas_d=0;
		$tickets_d=0;
		$fecha_d=' ';
		$sucursal_d=' ';
		$ejercicio_d=' ';

		$reg1=0;
		for ($offset=$reg1; $offset<$nume_regis; $offset++) {
			mysql_data_seek($result, $offset);
			$row=mysql_fetch_array($result);


			if (($sucursal_d <> $row['codsuc']) || ($ejercicio_d <> $row['ejefac'])  || ($fecha_d <> $row['fecfac'])){
				$contador = $contador+1;
				$sucursal_d = $row['codsuc'];
				$ejercicio_d = $row['ejefac'];
				$fecha_d = $row['fecfac'];
				$tot_tikets[$contador] = 0;

				//------------ Modificacion para CIEERE DE CAJA -------
				$tot_efectivo[$contador] = 0;
				$tot_tarjeta[$contador] = 0;
				$tot_gastos[$contador] = 0;
				$tot_horcie[$contador] = ' ';

			}else{

			}

			if ( $entradas == $contador) {
				$tot_ventas[$contador] = $tot_ventas[$contador] + $row['totfac'];
				$tot_tikets[$contador] = $tot_tikets[$contador]+1;
				$tot_fecha[$contador]  = $row['fecfac'];
			}else{
				$tot_ventas[$contador] = $row['totfac'];
				$tot_tikets[$contador] = 1;
				$entradas = $entradas+1;
				$tot_fecha[$contador]  = $row['fecfac'];
			}

			$totalven  = $totalven + $row['totfac'];
			$totaltik  = $totaltik + 1;

		}

		$nume_regan = count($tot_ventas);
        $reg1=0;

		//------------ BUSQUEDA DE CIERRE DE CAJA ------------ 
		
		$sql2 = "SELECT CIECAB.numcie, CIECAB.feccie, CIECAB.horcie, CIECAB.obscie, CIELIN.codfp, CIELIN.des, CIELIN.imprea FROM CIELIN INNER JOIN CIECAB ON CIELIN.codsuc = CIECAB.codsuc AND CIELIN.numcie = CIECAB.numcie
		WHERE CIELIN.codsuc = '$sucursal' and CIECAB.feccie BETWEEN '$fecha_desde' and '$fecha_hasta' order by CIELIN.codsuc DESC, CIECAB.feccie DESC, CIECAB.numcie ASC";
		$result2 = mysql_query($sql2);
		$nume_regis2=mysql_num_rows($result2);

		$seguir =0;
		$contador2 =-1;

		$consulta    = array();
		for ($offset=$reg1; $offset<$nume_regis2; $offset++) {
			mysql_data_seek($result2, $offset);
			$row2=mysql_fetch_array($result2);

			$consulta[$offset] = array();
            $consulta[$offset]['numcie'] = $row2['numcie'];
            $consulta[$offset]['feccie'] = $row2['feccie'];
            $consulta[$offset]['codfp'] = $row2['codfp'];
            $consulta[$offset]['imprea'] = $row2['imprea'];
	    }
         //------------ Modificacion para CIEERE DE CAJA -------
        for ($offset=$reg1; $offset<$nume_regan; $offset++) {
            if ($nume_regis2 > 0) {
	            $seguir=0;

	            $numcie_a = ' ';
	            $feccie_a = ' ';

				while ($seguir <= 0 and $contador2 < $nume_regis2) {
					$contador2 = $contador2 + 1;
					if ($contador2 == $nume_regis2) {
						//$seguir = 1;
					}else{
						mysql_data_seek($result2, $contador2);
						$row2=mysql_fetch_array($result2);

						if (($feccie_a == $row2['feccie']) AND ($numcie_a != $row2['numcie'])) {
							$tot_efectivo[$offset] = 0;
							$tot_tarjeta[$offset] = 0;
							$tot_gastos[$offset] = 0;
						}
						$numcie_a = $row2['numcie'];
						$feccie_a = $row2['feccie'];

						if ($tot_fecha[$offset] < $row2['feccie']) {
							//no Hacer Nada
						}elseif ($tot_fecha[$offset] == $row2['feccie']) {
							
							$tot_horcie[$offset] = $row2['horcie'];
							if ($row2['codfp'] == 'EFECTIVO') {
								$tot_efectivo[$offset] = $row2['imprea'];
							}
							if ($row2['codfp'] == 'TARJETA') {
								$tot_tarjeta[$offset] = $row2['imprea'];
							}
							if ($row2['codfp'] == 'GASTOS') {
								$tot_gastos[$offset] = $row2['imprea'];
							}
							//$seguir = 1; // salir del ciclo si solo si es la misma Fecha y Cambia el num Cier de la Misma Sucurcursal

						}elseif ($tot_fecha[$offset] > $row2['feccie']) {
							$seguir = 1; // salir del ciclo
							$contador2 = $contador2 - 1;
						}
					}
				}
			}
        };

        //Dar Formato a Numeros
        for ($offset=$reg1; $offset<$nume_regan; $offset++) {
            $tot_ventas[$offset] = number_format($tot_ventas[$offset],2, '.', ',');

            $totalefe  = $totalefe + $tot_efectivo[$offset];
			$totaltar  = $totaltar + $tot_tarjeta[$offset];
			$totalgas  = $totalgas + $tot_gastos[$offset];

			$tot_efectivo[$offset] = number_format($tot_efectivo[$offset],2, '.', ',');
            $tot_tarjeta[$offset] = number_format($tot_tarjeta[$offset],2, '.', ',');
            $tot_gastos[$offset] = number_format($tot_gastos[$offset],2, '.', ',');
        };
        

        //------Calculo de los promedios---------
		$nume_regis = count($tot_fecha);
		if ($totalven > 0){
			$promeven = $totalven / $nume_regis;
		}
		if ($totaltik > 0){
			$prometik = $totaltik / $nume_regis;
		}

		if ($totalefe > 0){
			$promeefe = $totalefe / $nume_regis;
		}
		if ($totaltar > 0){
			$prometar = $totaltar / $nume_regis;
		}

		if ($totalgas > 0){
			$promegas = $totalgas / $nume_regis;
		}


		$totalven = number_format($totalven,2, '.', ',');
        $prometik = number_format($prometik,2, '.', ',');
		$promeven = number_format($promeven,2, '.', ',');

		$promeefe = number_format($promeefe,2, '.', ',');
		$prometar = number_format($prometar,2, '.', ',');
		$promegas = number_format($promegas,2, '.', ',');

		//Se declara que esta es una aplicacion que genera un JSON
		echo json_encode(array( 'exito' => true, 'nume_regis' => $nume_regis, 'sucursal' => $sucursal, 'fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin,
			'tot_ventas' => $tot_ventas, 'tot_tikets' => $tot_tikets, 'tot_fecha' => $tot_fecha, 'totalven' => $totalven, 'promeven' => $promeven,
			'totaltik' => $totaltik, 'prometik' => $prometik, 'tot_efectivo' => $tot_efectivo, 'tot_tarjeta' => $tot_tarjeta,
			'tot_gastos' => $tot_gastos, 'tot_horcie' => $tot_horcie, 'promeefe' => $promeefe, 'prometar' => $prometar, 'promegas' => $promegas,
			'totalefe' => $totalefe, 'totaltar' => $totaltar, 'totalgas' => $totalgas,
			'consulta' => $consulta
			));
		}

		mysql_close($connection); // Closing Connection

} catch(Exception $e) {//Controlar siempre el error.
	$data = $e->getMessage();
	echo json_encode($data);
}

?>
