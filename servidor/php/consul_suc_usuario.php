<?php //
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array( 'exito' => false, 'sucursales_usuario' => array(), 'nume_regis' => 0);
    if (isset($_POST['session'])) {
    	require_once("config/Config.php");
		$conexion = new Conexion();

		$login_session = $_POST['session'];
		$data = $conexion->consulta("SELECT id FROM USUARIOS WHERE username='$login_session'");
		if (count($data)) {
			$id = $data[0]['id'];

			$sql =
			"SELECT EMPRESA.codsuc, EMPRESA.razemp, EMPRESA.domemp, EMPRESA.pobemp, EMPRESA.proemp, EMPRESA.pais, EMPRESA.nifemp, EMPRESA.telefono, EMPRESA.moneda AS moneda, EMPRESA.webemp ".
			"FROM EMPRESA ".
			"INNER JOIN USUARIO_EMPRESA ON EMPRESA.codsuc = USUARIO_EMPRESA.codsuc WHERE USUARIO_EMPRESA.id = '$id' ORDER BY codsuc ASC";
			$sucursales_usuario = $conexion->consulta($sql);
			$respuesta = array('exito' => true, 'sucursales_usuario' => $sucursales_usuario, 'nume_regis' => count($sucursales_usuario));
		}
    }
    echo json_encode($respuesta, true);
}
catch(Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>