<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array('exito' => false);
    if (isset($_POST['sucursal'])) {
        $sucursal = $_POST['sucursal'];
        $subdirectorio_imagenes = 'articulo';
        $nombre_campo_imagen = 'imagen_nueva';

        if (isset($_POST['opcion'])) {
            require_once("config/Config.php");
            $conexion = new Conexion();

            $opcion = $_POST['opcion'];
            if ($opcion == "consulta") {
                $sql =
                    "SELECT ARTICULOS.codsuc, ARTICULOS.codart, ARTICULOS.desart, ARTICULOS.tipart, ARTICULOS.codartdos, ARTICULOS.familia1, ARTICULOS.pvpa, ARTICULOS.ivaart, ARTICULOS.orden, ARTICULOS.puntos, ARTICULOS.foto, ARTICULOS.vender, ARTICULOS.comprar, ARTICULOS.obsoleto, EMP.codemp, FAMILIA1.desfam1, FAMILIA1.codfam1 ".
                    "FROM EMPRESA EMP, ".
                    "ARTICULOS INNER JOIN FAMILIA1 ON ARTICULOS.codsuc = FAMILIA1.codsuc AND ARTICULOS.familia1 = FAMILIA1.codfam1 ".
                    "WHERE ARTICULOS.codsuc = EMP.codsuc ".
                    "AND ARTICULOS.codsuc = '$sucursal' ".
                    "ORDER BY obsoleto ASC, codart ASC";
                $articulos = $conexion->consulta($sql);
                for ($i=0; $i<count($articulos); $i++) {
                    $codigo_grupo_empresas = $articulos[$i]['codemp'];
                    $codigo_sucursal = $articulos[$i]['codsuc'];
                    $foto = $URL_BASE_PATH.'servidor/images/nothing.png';
                    if (isset($articulos[$i]['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$articulos[$i]['foto'])) {
                        $foto = $URL_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$articulos[$i]['foto'];
                    }
                    $articulos[$i]['foto'] = $foto;
                }

                if (count($articulos) > 1){
                    $respuesta = array('exito' => true, 'articulos' => $articulos, 'nume_regis' => count($articulos));
                }
                else{
                    $respuesta = array('exito' => false, 'sucursal' => $sucursal);
                }
            }
            else if ($opcion == "consultar") {
                $codart	= (isset($_POST["codart"]))?$_POST['codart']:"";

                // SQL query to fetch information of registerd users and finds user match.
                $sql =
                    "SELECT * ".
                    "FROM ARTICULOS ".
                    "WHERE codsuc = '$sucursal' ".
                    "AND codart = '$codart'";
                $datos = $conexion->consulta($sql);

                if (count($datos)) {
                    $row = $datos[0];
                    $respuesta = array(
                        'exito' => true, 'codsuc' => $row['codsuc'],
                        'codart' => $row['codart'],
                        'desart' => utf8_encode($row['desart']),
                        'tipart' => $row['tipart'],
                        'codartdos' => $row['codartdos'],
                        'ivaart' => $row['ivaart'],
                        'familia1' => $row['familia1'],
                        'pvpa' => $row['pvpa'],
                        'puntos' => $row['puntos'],
                        'orden' => $row['orden'],
                        'foto' => utf8_encode($row['foto']),
                        'vender' => $row['vender'],
                        'comprar' => $row['comprar'],
                        'obsoleto' => $row['obsoleto']
                    );
                }
                else{
                    $respuesta = array('exito' => false, 'sucursal' => $sucursal);
                }
            }
            else if ($opcion == "impuestos") {
                $sql =
                    "SELECT codsuc, impuesto1, impuesto2 ".
                    "FROM EMPRESA ".
                    "WHERE codsuc = '$sucursal'";
                $datos = $conexion->consulta($sql);

                if (count($datos)) {
                    $row = $datos[0];
                    $respuesta = array(
                        'exito' => true, 'codsuc' => $row['codsuc'],
                        'impuesto1' => $row['impuesto1'],
                        'impuesto2' => $row['impuesto2'],
                        'nume_regis' => count($datos)
                    );
                }
                else{
                    $respuesta = array('exito' => false, 'sucursal' => $sucursal);
                }
            }
            else if ($opcion == "actualizar") {
                $codart	= (isset($_POST["codart"]))?$_POST['codart']:"";
                $desart	= (isset($_POST["desart"]))?$_POST['desart']:"";
                $tipart	= (isset($_POST["tipart"]))?$_POST['tipart']:"";
                $codartdos = (isset($_POST["codartdos"]))?$_POST['codartdos']:"";
                $ivaart	= (isset($_POST["ivaart"]))?$_POST['ivaart']:"";
                $familia1  = (isset($_POST["familia1"]))?$_POST['familia1']:"";
                $pvpa	= (isset($_POST["pvpa"]))?$_POST['pvpa']:"";
                $puntos	= (isset($_POST["puntos"]))?$_POST['puntos']:"";
                $orden	= (isset($_POST["orden"]))?$_POST['orden']:"";
                $foto	= (isset($_POST["foto"]))?$_POST['foto']:"";
                $vender	= (isset($_POST["vender"]))?$_POST['vender']:"";
                $comprar= (isset($_POST["comprar"]))?$_POST['comprar']:"";
                $obsoleto= (isset($_POST["obsoleto"]))?$_POST['obsoleto']:"";

                $sql =
                    "SELECT ART.foto, EMP.codemp ".
                    "FROM ARTICULOS ART, EMPRESA EMP ".
                    "WHERE ART.codsuc = EMP.codsuc ".
                    "AND ART.codsuc = '$sucursal' ".
                    "AND ART.codart = '".$codart."'";
                $articulo_data = $conexion->consulta($sql);
                if (count($articulo_data) > 0) {
                    $respuesta["exito"] = true;

                    require_once("lib/fileUploader.php");

                    $codigo_grupo_empresas = $articulo_data[0]['codemp'];
                    $foto_anterior = isset($articulo_data[0]['foto'])?$articulo_data[0]['foto']:"";
                    if (isset($_FILES[$nombre_campo_imagen]['name']) && $_FILES[$nombre_campo_imagen]['name'] != '') {
                        $directorio_imagenes = $DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$sucursal.'/';
                        $respuesta = FileUploader::upload_image($nombre_campo_imagen, $directorio_imagenes, $foto_anterior, true);
                        if ($respuesta["exito"]) {
                            $foto = $respuesta["file_name"];
                        }
                    }
                    if ($respuesta["exito"]) {
                        $sql =
                            "UPDATE ARTICULOS SET ".
                            "desart = '$desart', ".
                            "tipart = '$tipart', ".
                            "codartdos = '$codartdos', ".
                            "ivaart = '$ivaart', ".
                            "familia1 = '$familia1', ".
                            "pvpa = '$pvpa', ".
                            "puntos = '$puntos', ".
                            "orden = '$orden', ".
                            (($foto != "")?"foto = '$foto', ":"").
                            "vender = '$vender', ".
                            "comprar = '$comprar', ".
                            "obsoleto = '$obsoleto' ".
                            "WHERE codsuc = '$sucursal' ".
                            "AND codart='$codart'";
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                    }
                }
                else {
                    $respuesta = array('exito' => false, 'mensaje' => 'No se encontro el articulo');
                }
            }
            else if ($opcion == "insertar") {
                $codart	= (isset($_POST["codart"]))?$_POST['codart']:"";
                $desart	= (isset($_POST["desart"]))?$_POST['desart']:"";
                $tipart	= (isset($_POST["tipart"]))?$_POST['tipart']:"";
                $codartdos = (isset($_POST["codartdos"]))?$_POST['codartdos']:"";
                $ivaart	= (isset($_POST["ivaart"]))?$_POST['ivaart']:"";
                $familia1  = (isset($_POST["familia1"]))?$_POST['familia1']:"";
                $pvpa	= (isset($_POST["pvpa"]))?$_POST['pvpa']:"";
                $puntos	= (isset($_POST["puntos"]))?$_POST['puntos']:"";
                $orden	= (isset($_POST["orden"]))?$_POST['orden']:"";
                $foto	= (isset($_POST["foto"]))?$_POST['foto']:"";
                $vender	= (isset($_POST["vender"]))?$_POST['vender']:"";
                $comprar= (isset($_POST["comprar"]))?$_POST['comprar']:"";

                $sql =
                    "SELECT EMP.codemp ".
                    "FROM EMPRESA EMP ".
                    "WHERE EMP.codsuc = '$sucursal'";
                $empresa_data = $conexion->consulta($sql);
                if (count($empresa_data) > 0) {
                    require_once("lib/fileUploader.php");

                    $codigo_grupo_empresas = $empresa_data[0]['codemp'];
                    if (isset($_FILES[$nombre_campo_imagen]['name']) && $_FILES[$nombre_campo_imagen]['name'] != '') {
                        $directorio_imagenes = $DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$sucursal.'/';
                        $respuesta = FileUploader::upload_image($nombre_campo_imagen, $directorio_imagenes, "", true);
                        if ($respuesta["exito"]) {
                            $foto = $respuesta["file_name"];
                        }
                    }
                }
                $sql =
                    "INSERT into ARTICULOS ".
                    "(codsuc, codart, desart, tipart, codartdos, ivaart, familia1, pvpa, puntos, orden, foto, vender, comprar) ".
                    "VALUES ".
                    "('$sucursal','$codart','$desart','$tipart','$codartdos','$ivaart','$familia1','$pvpa','$puntos','$orden','$foto', '$vender','$comprar')";
                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
            }
            else if ($opcion == "eliminar") {
                $codart	= (isset($_POST["codart"]))?$_POST['codart']:"";

                $sql =
                    "SELECT ART.*, EMP.codemp ".
                    "FROM ARTICULOS ART, EMPRESA EMP ".
                    "WHERE ART.codsuc = EMP.codsuc ".
                    "AND ART.codsuc = '$sucursal' ".
                    "AND ART.codart = '".$codart."'";
                $articulos = $conexion->consulta($sql);
                if (count($articulos)) {
                    /******** VERIFICAR SI TIENE TICKET O FACTURA ***************/
                    $ventas = $conexion->consulta("select * from FACLIN where codsuc='$sucursal' and codart='$codart' LIMIT 1");
                    if(count($ventas) == 0){ /******** NO TIENE VENTAS VALIDAR COMPRAS ***************/
                        $compras = $conexion->consulta("select * from FACPROL where codsuc='$sucursal' and codart='$codart' LIMIT 1");
                        if(count($compras) == 0){ /******** NO TIENE COMPRAS ***************/
                            $eliminar='true';
                        }
                        else{
                            $eliminar='Tiene Compras';
                        }
                    }
                    else{
                        $eliminar='Tiene VENTAS';
                    }

                    if ($eliminar=='true') {
                        // Si existe un archivo asociado se borra
                        $articulo = $articulos[0];
                        $codigo_grupo_empresas = $articulo['codemp'];
                        $codigo_sucursal = $articulo['codsuc'];
                        if (isset($articulo['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$articulo['foto'])) {
                            unlink($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$articulo['foto']);
                        }

                        // Se elimina
                        $sql =
                            "DELETE FROM ARTICULOS ".
                            "WHERE codsuc = '$sucursal' ".
                            "AND codart = '$codart'";
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje, 'eliminar' => $eliminar);
                    }
                    else{
                        $sql =
                            "UPDATE ARTICULOS ".
                            "SET obsoleto='1' ".
                            "WHERE codsuc = '$sucursal' ".
                            "AND codart = '$codart'";
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje, 'eliminar' => $eliminar);
                    }
                }
            }
            else if ($opcion == "remover_imagen") {
                $codart	= (isset($_POST["codart"]))?$_POST['codart']:"";

                $sql =
                    "SELECT ART.*, EMP.codemp ".
                    "FROM ARTICULOS ART, EMPRESA EMP ".
                    "WHERE ART.codsuc = EMP.codsuc ".
                    "AND ART.codsuc = '$sucursal' ".
                    "AND ART.codart = '".$codart."'";
                $articulos = $conexion->consulta($sql);
                if (count($articulos)) {
                    // Si existe un archivo asociado se borra
                    $articulo = $articulos[0];
                    $codigo_grupo_empresas = $articulo['codemp'];
                    $codigo_sucursal = $articulo['codsuc'];
                    $respuesta = array( 'exito' => false);
                    if (isset($articulo['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$articulo['foto'])) {
                        unlink($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$articulo['foto']);
                    }

                    $sql =
                        "UPDATE ARTICULOS SET ".
                        "foto = NULL ".
                        "WHERE codsuc = '$sucursal' ".
                        "AND codart = '$codart'";
                    $mensaje = $conexion->sentencia($sql);
                    $exito = strpos($mensaje, "Exito") !== false;
                    $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                }
            }
        }
    }
    echo json_encode($respuesta, true);
}
catch (Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
