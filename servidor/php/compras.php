<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");


try {//Controlar siempre el error
	$respuesta = array( 'exito' => false);
    if (isset($_POST['sucursal'])) {
    	$codigo_sucursal = $_POST['sucursal'];
        if (isset($_POST['operacion'])) {
            require_once("config/Config.php");
            $conexion = new Conexion();

            $operacion = $_POST['operacion'];
            if ($operacion == "consulta" && isset($_POST['ejercicio'])) {
            	$ejercicio = ($_POST['ejercicio']!="")?$_POST['ejercicio']:date("Y");
            	$meses = (isset($_POST['meses']))?$_POST['meses']:0;
            	$mes = (is_numeric($meses))?(intval(date('m'))-intval($meses)):1;
            	if ($ejercicio < date("Y")) {
            	    $mes = (is_numeric($meses))?(12-intval($meses)):1;
                }
            	$fecha_minima = $ejercicio."-".$mes."-01";
                $sql =
                    "SELECT FP.*, CO.desconcep, PR.razon ".
					"FROM FACPROC FP ".
                    "LEFT JOIN PROVEEDOR PR ON (PR.codsuc = FP.codsuc AND PR.codpro = FP.codpro) ".
                    "LEFT JOIN CONCEPTOS CO ON (CO.codsuc = FP.codsuc AND CO.idconcepto = FP.idconcepto) ".
                    "WHERE (FP.anulada = 0 OR FP.anulada IS NULL) ".
					"AND FP.codsuc = '".$codigo_sucursal."' ".
                    "AND FP.ejefacp = ".$ejercicio." ".
                    "AND FP.fecfacp >= '".$fecha_minima."' ".
                    //"AND FP.idconcepto IS NOT NULL ". // Evitar las facturas generadas por un movimiento de caja (Egreso Contable)
					"ORDER BY FP.ejefacp DESC, FP.serfacp DESC, FP.numfacp DESC";
                $facturas = $conexion->consulta($sql);
                for ($i=0; $i<count($facturas); $i++) {
                	$facturas[$i]["fecfacp_format"] = DateTime::createFromFormat('Y-m-d', $facturas[$i]["fecfacp"])->format('d/m/Y');
                	$facturas[$i]["numalb"] = isset($facturas[$i]["numalb"])?$facturas[$i]["numalb"]:"";
                    $facturas[$i]["totfacp_format"] = number_format($facturas[$i]["totfacp"], 2, ",", ".");
                    $sql2 =
                        "SELECT FL.* ".
                        "FROM FACPROL FL ".
                        "WHERE FL.codsuc = '".$facturas[$i]["codsuc"]."' ".
                        "AND FL.ejefacp = '".$facturas[$i]["ejefacp"]."' ".
                        "AND FL.serfacp = '".$facturas[$i]["serfacp"]."' ".
                        "AND FL.numfacp = '".$facturas[$i]["numfacp"]."' ".
                        "ORDER BY FL.linfacp ASC";
                    $lineas = $conexion->consulta($sql2);
                    for ($j=0; $j<count($lineas); $j++) {
                        // Monto del descuento
                        $lineas[$j]['impdescuento'] = (($lineas[$j]['canser'] > 0)?($lineas[$j]['impdescuento']/$lineas[$j]['canser']):0);
                        // Monto del impuesto
                        $lineas[$j]['impiva'] = (($lineas[$j]['canser'] > 0)?($lineas[$j]['impiva']/$lineas[$j]['canser']):0);
                        // Base imponible
                        if ($lineas[$j]['canser'] > 0) {
                            if (isset($lineas[$j]['impbas'])) {
                                $lineas[$j]['impbas'] = $lineas[$j]['impbas']/$lineas[$j]['canser'];
                            }
                            else {
                                $lineas[$j]['impbas'] = $lineas[$j]['subtot']/$lineas[$j]['canser'];
                            }
                        }
                        else {
                            $lineas[$j]['impbas'] = 0;
                        }
                    }
                    $facturas[$i]["lineas_array"] = $lineas;
				}
                $respuesta = array( 'exito' => true, 'facturas' => $facturas);
            }
			elseif ($operacion == "sucursal_data") {
                $sucursal = array();
                $sql =
                    "SELECT e.zonhor, e.moneda, e.idconceptocompras, e.proveedorcompras, e.impinccompras ".
                    "FROM EMPRESA e ".
                    "WHERE e.codsuc = '".$codigo_sucursal."' ";
                $data = $conexion->consulta($sql);
                if (count($data)) {
                    $timezone = 'America/Bogota';
                    if (isset($data[0]['zonhor'])) {
                        $timezone = $data[0]['zonhor'];
                    }
                    $sucursal = array(
                        "timezone" => $timezone,
                        "moneda" => (isset($data[0]['moneda']))?$data[0]['moneda']:"",
                        "idconcepto_compras" => (isset($data[0]['idconceptocompras']))?$data[0]['idconceptocompras']:"",
                        "proveedor_compras" => (isset($data[0]['proveedorcompras']))?$data[0]['proveedorcompras']:"",
                        "impuesto_incluido" => $data[0]['impinccompras']
                    );
                }
                $respuesta = array('exito' => true, 'sucursal' => $sucursal);
            }
			elseif ($operacion == "ejercicios") {
                $sql =
                    "SELECT DISTINCT(ejefacp) FROM FACPROC ".
                    "WHERE codsuc='".$codigo_sucursal."' ".
					"ORDER BY ejefacp DESC";
                $data = $conexion->consulta($sql);
                $ejercicios = array();
                foreach ($data as $record) {
                	array_push($ejercicios, $record["ejefacp"]);
				}
                $anno = date("Y");
                if (!in_array($anno, $ejercicios)) {
                	$ejercicios = array_merge(array($anno), $ejercicios);
				}
                $respuesta = array('exito' => true, 'ejercicios' => $ejercicios);
            }
            elseif ($operacion == "proveedores") {
                $sql =
                    "SELECT codpro, razon FROM PROVEEDOR ".
                    "WHERE (obsoleto = 0 OR obsoleto IS NULL) ".
                    "AND codsuc='".$codigo_sucursal."'";
                $proveedores = $conexion->consulta($sql);
                $respuesta = array('exito' => true, 'proveedores' => $proveedores);
			}
			elseif ($operacion == "conceptos") {
                $sql =
                    "SELECT idconcepto, desconcep FROM CONCEPTOS ".
                    "WHERE codsuc='".$codigo_sucursal."'";
                $conceptos = $conexion->consulta($sql);
                $respuesta = array('exito' => true, 'conceptos' => $conceptos);
            }
			elseif ($operacion == "impuestos") {
                $sql =
                    "SELECT impuesto1, impuesto2 FROM EMPRESA ".
                    "WHERE codsuc='".$codigo_sucursal."'";
                $data = $conexion->consulta($sql);
                $impuestos = array();
                if (count($data)) {
                	$impuesto1 = array(
                		"value" => (isset($data[0]['impuesto1'])?$data[0]['impuesto1']:0),
						"text" => (isset($data[0]['impuesto1'])?$data[0]['impuesto1']:0)
					);
                	array_push($impuestos, $impuesto1);
                    $impuesto2 = array(
                        "value" => (isset($data[0]['impuesto2'])?$data[0]['impuesto2']:0),
                        "text" => (isset($data[0]['impuesto2'])?$data[0]['impuesto2']:0)
                    );
                    array_push($impuestos, $impuesto2);
				}
                $respuesta = array('exito' => true, 'impuestos' => $impuestos);
            }
			elseif ($operacion == "articulos") {
                $impuestos = $conexion->consulta("SELECT impuesto1, impuesto2, impinccompras FROM EMPRESA WHERE codsuc='".$codigo_sucursal."'");
                $sql =
                    "SELECT codart, desart, coste, ivaart FROM ARTICULOS ".
                    "WHERE (obsoleto = 0 OR obsoleto IS NULL) ".
                    "AND codsuc = '".$codigo_sucursal."' ".
					"AND comprar = 1 ".
                    "ORDER BY codart ASC";
                $articulos = $conexion->consulta($sql);
                for ($i=0; $i<count($articulos); $i++) {
                    $articulos[$i]['canser'] = 1;
                	// Calculo del descuento
                    $articulos[$i]['descuento'] = 0;
                    $articulos[$i]['impdescuento'] = 0;
                    // Calculo del porcentaje de impuesto aplicado al articulo
                    $impuesto_porcentaje = 0;
                    if (isset($articulos[$i]['ivaart'])) {
                        if ($articulos[$i]['ivaart']=='1') {
                            $impuesto_porcentaje = isset($impuestos[0]['impuesto1'])?$impuestos[0]['impuesto1']:0;
                        }
						elseif ($articulos[$i]['ivaart']=='2') {
                            $impuesto_porcentaje = isset($impuestos[0]['impuesto2'])?$impuestos[0]['impuesto2']:0;
                        }
                    }
                    $articulos[$i]['iva'] = $impuesto_porcentaje;
                    // Calculo de la base imponible y monto del impuesto del articulo
                    $coste = $articulos[$i]['coste'];
                    $base_imponible = $coste;
                    $impuesto_monto = $coste * ($impuesto_porcentaje / 100);
                    if (isset($impuestos[0]['impinccompras']) && $impuestos[0]['impinccompras']=='X') {
                        $base_imponible = $coste / (1+($impuesto_porcentaje/100));
                        $impuesto_monto = $coste - $base_imponible;
                    }
                    $articulos[$i]['preven'] = $coste;
                    $articulos[$i]['subtot'] = ($articulos[$i]['canser'] * $coste) - $articulos[$i]['impdescuento'];
                    $articulos[$i]['impbas'] = $base_imponible;
                    $articulos[$i]['impiva'] = $impuesto_monto;
                }
                $respuesta = array('exito' => true, 'articulos' => $articulos);
            }
            elseif ($operacion == "proxima_factura" && isset($_POST['ejercicio'])) {
                $numero_factura = 1;
                $ejercicio = $_POST['ejercicio'];
                $anno = ($ejercicio!="")?$ejercicio:date("Y");
                $data = $conexion->consulta("SELECT MAX(numfacp) as ultima_factura FROM FACPROC WHERE codsuc='".$codigo_sucursal."' AND ejefacp='".$anno."'");
                if (count($data)) {
                    $numero_factura += intval($data[0]['ultima_factura']);
                }
                $respuesta = array('exito' => true, 'numero_factura' => $numero_factura, 'ejercicio' =>$anno);
            }
            elseif ($operacion == "insertar" && isset($_POST['factura'])) {
                $factura = json_decode($_POST['factura']);
                // Conversion de la fecha y hora en la factura
                $factura->fecfacp = DateTime::createFromFormat('d/m/Y', $factura->fecfacp)->format('Y-m-d');
                // Busqueda de la serie en la factura
                // y otros datos configurados en la empresa
                $impuesto_incluido_compras = false;
                $caja_compras = null;
                $factura->serfacp = null;
                $data = $conexion->consulta("SELECT zonhor, serie, impinccompras, codcajcom FROM EMPRESA where codsuc='".$factura->codsuc."'");
                if (count($data)) {
                    // Fecha y Hora de Creacion y Modifcacion
                    @date_default_timezone_set($data[0]['zonhor']);
                    $factura->feccre = date("Y-m-d");
                    $factura->horcre = date("H:i");
                    $factura->fecmod = date("Y-m-d");
                    $factura->hormod = date("H:i");
                    $factura->serfacp = $data[0]['serie'];
                    if ((isset($data[0]['impinccompras'])) && (strtoupper($data[0]['impinccompras']) == "X")) {
                        $impuesto_incluido_compras = true;
                    }
                    if ((isset($data[0]['codcajcom'])) && ($data[0]['codcajcom'] != null)) {
                        $caja_compras = $data[0]['codcajcom'];
                    }
                }
                // Calculo del total de lineas
                $factura->lineas = 5 * count($factura->lineas_array);
                // Calculo del siguiente numero de ticket
                $factura->numfacp = 1;
                $data = $conexion->consulta("SELECT MAX(numfacp) as ultima_factura FROM FACPROC where codsuc='".$factura->codsuc."' AND ejefacp='".$factura->ejefacp."'");
                if (count($data)) {
                    $factura->numfacp += intval($data[0]['ultima_factura']);
                }
                // Registro de la factura
                $mensaje = $conexion->insertar_desde_objeto($factura, "FACPROC");
                $exito = strpos($mensaje, "Exito") !== false;
                if ($exito) {
                    $id_linea = 1;
                    foreach ($factura->lineas_array as $linea) {
                        if ($exito) {
                            // Asignacion de los datos de la factura en la linea
                            $linea->codsuc = $factura->codsuc;
                            $linea->serfacp = $factura->serfacp;
                            $linea->ejefacp = $factura->ejefacp;
                            $linea->numfacp = $factura->numfacp;
                            $linea->linfacp = 5 * $id_linea;
                            // Calculo del Total de la Linea
                            $linea->totlin = $linea->subtot;
                            if (!$impuesto_incluido_compras) {
                                $linea->totlin = $linea->subtot + $linea->impiva;
                            }
                            // Registro de la linea de la factura
                            $mensaje = $conexion->insertar_desde_objeto($linea, "FACPROL");
                            $exito = $exito && (strpos($mensaje, "Exito") !== false);
                            $id_linea++;
                        }
                    }
                }
                if ($exito) {
                    /*
                    // Registrar la compra como un Movimiento de Egreso en la caja
                    if ($caja_compras == null) {
                        // Buscar una caja de cobro de la sucursal
                        $sql =
                            "SELECT codcaj " .
                            "FROM CAJA " .
                            "WHERE codsuc = '" . $ticket->codsuc . "' " .
                            "AND tipo = 'C'";
                        $data = $conexion->consulta($sql);
                        if (count($data)) {
                            $caja_compras = $data[0]['codcaj'];
                        }
                    }
                    if ($caja_compras != null) {
                        // Registrar la compra en la caja
                        $movimiento_caja = array(
                            "codsuc" => $factura->codsuc,
                            "codcaj" => $caja_compras,
                            "fecmov" => $factura->feccre,
                            "hormov" => $factura->horcre,
                            "ingegr" => 'E',
                            "tipo"   => 'C',
                            "serfacp" => $factura->serfacp,
                            "ejefacp" => $factura->ejefacp,
                            "numfacp" => $factura->numfacp,
                            "codpro" => $factura->codpro,
                            "impmov" => $factura->totfacp,
                            "obsmov" => $factura->obsfac
                        );
                        $mensaje = $conexion->insertar_desde_objeto($movimiento_caja, "MOVCAJ");
                        $exito = strpos($mensaje, "Exito") !== false;
                    }
                    */
                }
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
            }
            elseif ($operacion == "actualizar" && isset($_POST['factura'])) {
                $factura = json_decode($_POST['factura']);
                // Conversion de la fecha y hora en la factura
                $factura->fecfacp = DateTime::createFromFormat('d/m/Y', $factura->fecfacp)->format('Y-m-d');
                // Busqueda de la serie en la factura
                // y otros datos configurados en la empresa
                $impuesto_incluido_compras = false;
                $factura->serfacp = null;
                $data = $conexion->consulta("SELECT zonhor, serie, impinccompras FROM EMPRESA where codsuc='".$factura->codsuc."'");
                if (count($data)) {
                    // Fecha y Hora de Modifcacion
                    @date_default_timezone_set($data[0]['zonhor']);
                    $factura->fecmod = date("Y-m-d");
                    $factura->hormod = date("H:i");
                    $factura->serfacp = $data[0]['serie'];
                    if ((isset($data[0]['impinccompras'])) && (strtoupper($data[0]['impinccompras']) == "X")) {
                        $impuesto_incluido_compras = true;
                    }
                }
                // Calculo del total de lineas
                $factura->lineas = 5 * count($factura->lineas_array);
                // Actualizacion de la factura
                $campos_clave = array("codsuc", "serfacp", "ejefacp", "numfacp");
                $mensaje = $conexion->actualizar_desde_objeto($factura, $campos_clave, "FACPROC");
                $exito = strpos($mensaje, "Exito") !== false;
                if ($exito) {
                    // Eliminacion de las lineas anteriores de la factura
                    $campos_clave = array("codsuc", "serfacp", "ejefacp", "numfacp");
                    $mensaje = $conexion->eliminar_desde_objeto($factura, $campos_clave, "FACPROL");
                    $exito = strpos($mensaje, "Exito") !== false;
                    if ($exito) {
                        $id_linea = 1;
                        foreach ($factura->lineas_array as $linea) {
                            if ($exito) {
                                // Asignacion de los datos de la factura en la linea
                                $linea->codsuc = $factura->codsuc;
                                $linea->serfacp = $factura->serfacp;
                                $linea->ejefacp = $factura->ejefacp;
                                $linea->numfacp = $factura->numfacp;
                                $linea->linfacp = 5 * $id_linea;
                                // Calculo del Total de la Linea
                                $linea->totlin = $linea->subtot;
                                if (!$impuesto_incluido_compras) {
                                    $linea->totlin = $linea->subtot + $linea->impiva;
                                }
                                // Registro de la linea de la factura
                                $mensaje = $conexion->insertar_desde_objeto($linea, "FACPROL");
                                $exito = $exito && (strpos($mensaje, "Exito") !== false);
                                $id_linea++;
                            }
                        }
                    }
                }
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
            }
            elseif ($operacion == "eliminar" && isset($_POST['factura'])) {
                $factura = json_decode($_POST['factura']);
                $factura_anulada = array(
                    "codsuc" => $factura->codsuc,
                    "serfacp" => null,
                    "ejefacp" => $factura->ejefacp,
                    "numfacp" => $factura->numfacp,
                    "anulada" => 1
                );
                // Busqueda de la serie en la factura
                $data = $conexion->consulta("SELECT zonhor, serie, impinccompras FROM EMPRESA where codsuc='".$factura->codsuc."'");
                if (count($data)) {
                    // Fecha y Hora de Modifcacion
                    @date_default_timezone_set($data[0]['zonhor']);
                    $factura_anulada["fecmod"] = date("Y-m-d");
                    $factura_anulada["hormod"] = date("H:i");
                    $factura_anulada["serfacp"] = $data[0]['serie'];
                }
                // Eliminacion logica de la factura
                $campos_clave = array("codsuc", "serfacp", "ejefacp", "numfacp");
                $mensaje = $conexion->actualizar_desde_objeto($factura_anulada, $campos_clave, "FACPROC");
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
            }
        }
    }
    echo json_encode($respuesta, true);
}
catch(Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
