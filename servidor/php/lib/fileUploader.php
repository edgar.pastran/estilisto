<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 22/06/2018
 * Time: 04:39 PM
 */

require_once("utilities.php");
class FileUploader {

    private static $MAXIMUM_FILE_SIZE_IN_KB = 500 * 1024;
    private static $RANDOM_FILE_NAME_LENGTH = 8;
    private static $IMAGE_EXTENSION_ALLOWED = array("JPG", "PNG", "JPEG", "GIF");
    private static $IMAGE_MAXIMUM_WIDTH = 128;
    private static $IMAGE_MAXIMUM_HEIGHT = 128;

    public static function upload_file($field_name, $target_dir, $file_name, $random_file_name=false) {
        $result = array(
            "exito" => false,
            "mensaje" => "",
            "file_name" => ""
        );
        // Create the directory if it does not exist
        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0777, true);
        }
        // Check file size
        $target_file = $target_dir . basename($_FILES[$field_name]["name"]);
        $image_file_extension = strtoupper(pathinfo($target_file,PATHINFO_EXTENSION));
        if ($_FILES[$field_name]["size"] > (self::$MAXIMUM_FILE_SIZE_IN_KB) &&
            !in_array($image_file_extension, self::$IMAGE_EXTENSION_ALLOWED)) {
            $result["mensaje"] = "Lo siento, tu archivo es muy grande. Debe ser menor o igual a ".self::$MAXIMUM_FILE_SIZE_IN_KB." KB";
            return $result;
        }
        $target_file = $target_dir . $file_name;
        // Remove if other file with same name already exists
        if ($file_name != "" && file_exists($target_file)) {
            unlink( $target_file );
        }
        // Check if the file will have a random name
        if ($random_file_name) {
            $file_name = Utilities::random_word(self::$RANDOM_FILE_NAME_LENGTH);
            $file_name .= ".".$image_file_extension;
            $target_file = $target_dir . $file_name;
        }
        // If everything is ok, try to upload file
        if (move_uploaded_file($_FILES[$field_name]["tmp_name"], $target_file)) {
            $result["exito"] = true;
            $result["mensaje"] = "El archivo ". basename( $_FILES[$field_name]["name"]). " fue cargado satisfactoriamente.";
            $result["file_name"] = $file_name;
        }
        else {
            $result["mensaje"] = "Lo siento, hubo un error cargando tu archivo.";
        }
        return $result;
    }

    public static function upload_image($field_name, $target_dir, $file_name, $random_file_name=false) {
        $result = array(
            "exito" => false,
            "mensaje" => "",
            "file_name" => ""
        );
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES[$field_name]["tmp_name"]);
            if($check === false) {
                $result["mensaje"] = "El archivo no es una imagen.";
                return $result;
            }
        }
        $target_file = $target_dir . basename($_FILES[$field_name]["name"]);
        $image_file_extension = strtoupper(pathinfo($target_file,PATHINFO_EXTENSION));
        // Allow certain file formats
        if (!in_array($image_file_extension, self::$IMAGE_EXTENSION_ALLOWED)) {
            $result["mensaje"] = "Lo siento, solo son permitidos archivos con extension ".implode(", ", self::$IMAGE_EXTENSION_ALLOWED).".";
            return $result;
        }
        // if everything is ok, try to upload file
        $result = self::upload_file($field_name, $target_dir, $file_name, $random_file_name);
        if ($result["exito"]) {
            $file_name = $result["file_name"];
            // Ckeck if it's necessary resize the image
            if ($_FILES[$field_name]["size"] > (self::$MAXIMUM_FILE_SIZE_IN_KB)) {
                $target_file = $target_dir.$file_name;
                self::resize_image($target_file, self::$IMAGE_MAXIMUM_WIDTH, self::$IMAGE_MAXIMUM_HEIGHT);
            }

            // Remove if other image with same name but another extension still exists
            if (!$random_file_name) {
                $file_name = explode(".", $file_name)[0];
                foreach (self::$IMAGE_EXTENSION_ALLOWED as $extension) {
                    if ($image_file_extension !== $extension) {
                        $target_file = $target_dir . $file_name . "." . $extension;
                        if (file_exists($target_file)) {
                            unlink($target_file);
                        }
                    }
                }
            }
        }
        return $result;
    }

    private static function resize_image($file_name, $max_width, $max_height) {
        list($original_width, $original_height) = getimagesize($file_name);

        $width = $original_width;
        $height = $original_height;

        # taller
        if ($height > $max_height) {
            $width = ($max_height / $height) * $width;
            $height = $max_height;
        }

        # wider
        if ($width > $max_width) {
            $height = ($max_width / $width) * $height;
            $width = $max_width;
        }

        $source_image = imagecreatefromstring(file_get_contents($file_name));
        $destiny_image = imagecreatetruecolor($width,$height);
        imagecopyresampled($destiny_image,$source_image,0,0,0,0,$width,$height,$original_width,$original_height);
        imagedestroy($source_image);
        imagepng($destiny_image,$file_name); // adjust format as needed
        imagedestroy($destiny_image);
    }

    public static function copy_image($source_image, $target_dir) {
        // Create the directory if it does not exist
        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0777, true);
        }
        // Check if exists the file
        if (file_exists($source_image) && is_file($source_image)) {
            $target_image = $target_dir.DIRECTORY_SEPARATOR.basename($source_image);
            return copy($source_image, $target_image);
        }
        return false;
    }

}
?>