<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");


try {//Controlar siempre el error
    $respuesta = array( 'exito' => false);
    if (isset($_POST['sucursal'])) {
        $codigo_sucursal = $_POST['sucursal'];
        if (isset($_POST['operacion'])) {
            require_once("config/Config.php");
            $conexion = new Conexion();

            $operacion = $_POST['operacion'];
            if ($operacion == "empleados") {
                $sql =
                    "SELECT codemp, nomemp, ape1emp, dniemp, comision FROM EMPLEADOS ".
                    "WHERE (obsoleto = 0 OR obsoleto IS NULL) ".
                    "AND vender = 1 ".
                    "AND codsuc='".$codigo_sucursal."'";
                $empleados = $conexion->consulta($sql);
                for ($i=0; $i<count($empleados); $i++) {
                    $sql =
                        "SELECT codfam1, comision FROM EMPFAM ".
                        "WHERE codsuc = '".$codigo_sucursal."' ".
                        "AND codemp = '".$empleados[$i]['codemp']."'";
                    $comisiones = $conexion->consulta($sql);
                    $empleados[$i]['comision_familias'] = array();
                    foreach ($comisiones as $comision) {
                        $empleados[$i]['comision_familias'][$comision['codfam1']] = $comision['comision'];
                    }
                    $sql =
                        "SELECT codart, comision FROM EMPART ".
                        "WHERE codsuc = '".$codigo_sucursal."' ".
                        "AND codemp = '".$empleados[$i]['codemp']."'";
                    $comisiones = $conexion->consulta($sql);
                    $empleados[$i]['comision_articulos'] = array();
                    foreach ($comisiones as $comision) {
                        $empleados[$i]['comision_articulos'][$comision['codart']] = $comision['comision'];
                    }
                }
                $respuesta = array('exito' => true, 'empleados' => $empleados);
            }
            elseif ($operacion == "familias") {
                $sql =
                    "SELECT codfam1, desfam1 FROM FAMILIA1 ".
                    "WHERE vender=1 AND ".
                    "(obsoleto = 0 OR obsoleto IS NULL) ".
                    "AND codsuc='".$codigo_sucursal."' ".
                    "ORDER BY orden ASC";
                $familias = $conexion->consulta($sql);
                $data = array();
                for ($i=0; $i<count($familias); $i++) {
                    $sql =
                        "SELECT codart, desart FROM ARTICULOS ".
                        "WHERE vender = 1 ".
                        "AND (obsoleto = 0 OR obsoleto IS NULL) ".
                        "AND codsuc='".$codigo_sucursal."' ".
                        "AND familia1 = '".$familias[$i]['codfam1']."'".
                        "ORDER BY orden ASC";
                    $articulos = $conexion->consulta($sql);
                    $data[$familias[$i]['codfam1']] = $familias[$i];
                    $data[$familias[$i]['codfam1']]['articulos'] = $articulos;
                }
                $respuesta = array('exito' => true, 'familias' => $data);
            }
            elseif ($operacion == "guardar_comisiones" && isset($_POST['comisiones'])) {
                $comisiones = json_decode($_POST['comisiones']);
                // Actualizacion de la comision general
                $campos_clave = array("codsuc", "codemp");
                $mensaje = $conexion->actualizar_desde_objeto($comisiones, $campos_clave, "EMPLEADOS");
                $exito = strpos($mensaje, "Exito") !== false;
                if ($exito) {
                    // Actualizacion de la comision por familias
                    foreach ($comisiones->familias as $comision_familia) {
                        if ($exito) {
                            // Asignacion de los datos del empleado y sucursal
                            $comision_familia->codsuc = $comisiones->codsuc;
                            $comision_familia->codemp = $comisiones->codemp;
                            // Eliminacion de la comision anterior
                            $campos_clave = array("codsuc", "codemp", "codfam1");
                            $mensaje = $conexion->eliminar_desde_objeto($comision_familia, $campos_clave, "EMPFAM");
                            $exito = $exito && (strpos($mensaje, "Exito") !== false);
                            // Insercion de la comision actual
                            if ($exito && $comision_familia->comision > 0) {
                                $mensaje = $conexion->insertar_desde_objeto($comision_familia, "EMPFAM");
                                $exito = $exito && (strpos($mensaje, "Exito") !== false);
                            }
                        }
                    }
                    // Actualizacion de la comision por articulos
                    foreach ($comisiones->articulos as $comision_articulo) {
                        if ($exito) {
                            // Asignacion de los datos del empleado y sucursal
                            $comision_articulo->codsuc = $comisiones->codsuc;
                            $comision_articulo->codemp = $comisiones->codemp;
                            // Eliminacion de la comision anterior
                            $campos_clave = array("codsuc", "codemp", "codart");
                            $mensaje = $conexion->eliminar_desde_objeto($comision_articulo, $campos_clave,"EMPART");
                            $exito = $exito && (strpos($mensaje, "Exito") !== false);
                            // Insercion de la comision actual
                            if ($exito && $comision_articulo->comision > 0) {
                                $mensaje = $conexion->insertar_desde_objeto($comision_articulo, "EMPART");
                                $exito = $exito && (strpos($mensaje, "Exito") !== false);
                            }
                        }
                    }
                }
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
            }
        }
    }
    echo json_encode($respuesta, true);
}
catch(Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
