<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

//session_start(); // Starting Session
$error=''; // Variable To Store Error Message

if (empty($_POST['username']) || empty($_POST['password'])) {
	$error = "Username or Password is invalid";
	echo json_encode(array('exito' => false, 'usuario' => '', 'password' => ''));
}
else
{
    require_once("config/Config.php");
    $conexion = new Conexion();

	// Define $username and $password
	$username = $_POST['username'];
	$password = $_POST['password'];
	$nombre = "";
	$apellido ="";
	$compania = "";
	$administrador = "";
	$direccion = "";
	$foto = "";
	$pantallas = "";

	$username = stripslashes($username);
	$password = stripslashes($password);

	$sql =
        "SELECT * ".
        "FROM USUARIOS ".
        "WHERE username = '".$username."' ".
		"AND password = '$password'";
	$datos = $conexion->consulta($sql);

	if(count($datos)){
		$row = $datos[0];
		//$_SESSION['login_user']=$username; // Initializing Session
		//Se declara que esta es una aplicacion que genera un JSON
		$nombre = $row['nombre'];
		$apellido =$row['apellido'];
		$compania = $row['compania'];
		$administrador = $row['administrador'];
		$direccion = $row['direccion'];
        $foto = $URL_BASE_PATH.'servidor/images/user.jpg';
        if (isset($row['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/usuario/'.$row['foto'])) {
            $foto = $URL_BASE_PATH.'servidor/images/usuario/'.$row['foto'];
        }

		//-- Cargar datos de Pantallas de Acceso --
		$sql =
			"SELECT PANTALLAS.codpan, PANTALLAS.pantalla, PANTALLAS.despan ".
			"FROM PANTALLAS ".
			"INNER JOIN ACCESOS ON PANTALLAS.codpan = ACCESOS.codpan ".
			"INNER JOIN USUARIOS ON ACCESOS.codgru = USUARIOS.codgru ".
			"WHERE USUARIOS.username = '$username'";
		$datos = $conexion->consulta($sql);

		for ($offset=0; $offset<count($datos); $offset++) {
			$row = $datos[$offset];
			$pantallas  = $pantallas.$row['pantalla'].",";
		};

		echo json_encode(array('exito' => true, 'usuario' => $username, 'password' => $password,
			'nombre' => $nombre, 'apellido' => $apellido, 'compania' => $compania,
			'administrador' => $administrador, 'direccion' => $direccion, 'foto' => $foto, 'pantallas' => $pantallas));
	}
	else{
		//Se declara que esta es una aplicacion que genera un JSON
		echo json_encode(array('exito' => false, 'usuario' => $username, 'password' => $password));
	}
}
?>
