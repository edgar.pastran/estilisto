<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array('exito' => false);
    if (isset($_POST['sucursal'])) {
        $sucursal = $_POST['sucursal'];

        if (isset($_POST['opcion'])) {
            require_once("config/Config.php");
            $conexion = new Conexion();

            $opcion = $_POST['opcion'];
            if ($opcion == "inicializar") {
                // Clase (Ingreso / Egreso)
                $clase = array(
                    "I" => "Ingreso",
                    "E" => "Egreso"
                );
                // Tipo (Contable / Estadistico)
                $tipo = array(
                    "C" => "Contable",
                    "E" => "Estadistico"
                );
                // Formas de Pago
                $sql =
                    "SELECT codfp, des FROM FORPAG ".
                    "WHERE codsuc = '".$sucursal."' ".
                    "ORDER BY codfp ASC";
                $formas_pago = $conexion->consulta($sql);
                // Cajas
                $sql =
                    "SELECT codcaj, nomcaj FROM CAJA ".
                    "WHERE codsuc = '".$sucursal."' ".
                    "ORDER BY codcaj ASC";
                $cajas = $conexion->consulta($sql);
                // Empleados
                $sql =
                    "SELECT codemp, nomemp, ape1emp FROM EMPLEADOS ".
                    "WHERE (obsoleto = 0 OR obsoleto IS NULL) ".
                    "AND codsuc = '".$sucursal."' ".
                    "ORDER BY nomemp ASC";
                $empleados = $conexion->consulta($sql);
                // Proveedores
                $sql =
                    "SELECT codpro, razon FROM PROVEEDOR ".
                    "WHERE (obsoleto = 0 OR obsoleto IS NULL) ".
                    "AND codsuc='".$sucursal."' ".
                    "ORDER BY razon ASC";
                $proveedores = $conexion->consulta($sql);
                // Articulos
                $sql =
                    "SELECT codart, desart FROM ARTICULOS ".
                    "WHERE (obsoleto = 0 OR obsoleto IS NULL) ".
                    "AND codsuc = '".$sucursal."' ".
                    "ORDER BY desart ASC";
                $articulos = $conexion->consulta($sql);
                $respuesta = array( 'exito' => true, 'clase' => $clase, 'tipo' => $tipo, 'forma_pago' => $formas_pago, 'cajas' => $cajas, 'empleados' => $empleados, 'proveedores' => $proveedores, 'articulos' => $articulos);
            }
            elseif ($opcion == "consulta") {
                $sql =
                    "SELECT * ".
                    "FROM CONMOV ".
                    "WHERE codsuc = '$sucursal'";
                $movimientos = $conexion->consulta($sql);
                for ($i=0; $i<count($movimientos); $i++) {
                    // Ingreso /Egreso
                    $movimientos[$i]['ingegr_string'] = "";
                    if ($movimientos[$i]['ingegr'] == "I") {
                        $movimientos[$i]['ingegr_string'] = "Ingreso";
                    }
                    elseif ($movimientos[$i]['ingegr'] == "E") {
                        $movimientos[$i]['ingegr_string'] = "Egreso";
                    }
                    // Tipo
                    $movimientos[$i]['tipo_string'] = "";
                    if ($movimientos[$i]['tipo'] == "C") {
                        $movimientos[$i]['tipo_string'] = "Contable";
                    }
                    elseif ($movimientos[$i]['tipo'] == "E") {
                        $movimientos[$i]['tipo_string'] = "Estadistico";
                    }
                    // Forma de Pago
                    $movimientos[$i]['forpag_string'] = "Cualquiera";
                    if (isset($movimientos[$i]['codforpag'])) {
                        $data = $conexion->consulta("SELECT des FROM FORPAG WHERE codsuc='".$sucursal."' AND codfp='".$movimientos[$i]['codforpag']."'");
                        if (count($data)) {
                            $movimientos[$i]['forpag_string'] = $data[0]['des'];
                        }
                    }
                }
                $respuesta = array( 'exito' => true, 'movimientos' => $movimientos);
            }
            elseif ($opcion == "actualizar") {
                if (isset($_POST['codmov'])) {
                    $movimiento = array(
                        "codsuc" => $sucursal,
                        "codmov" => $_POST['codmov'],
                        "desmov" => (isset($_POST["desmov"]) && $_POST["desmov"]!="")?$_POST['desmov']:"",
                        "ingegr" => (isset($_POST["ingegr"]) && $_POST["ingegr"]!="")?$_POST['ingegr']:"",
                        "tipo"   => (isset($_POST["tipo"])   && $_POST["tipo"]!="")?$_POST['tipo']:"",
                        "codforpag" => (isset($_POST["codforpag"]) && $_POST["codforpag"]!="")?$_POST['codforpag']:null,
                        "cajas"  => (isset($_POST["cajas"])  && $_POST["cajas"]!="")?$_POST['cajas']:null,
                        "solemp" => (isset($_POST["solemp"]) && $_POST["solemp"]!="0")?"1":"0",
                        "codemp" => (isset($_POST["codemp"]) && $_POST["codemp"]!="")?$_POST['codemp']:null,
                        "solpro" => (isset($_POST["solpro"]) && $_POST["solpro"]!="0")?"1":"0",
                        "codpro" => (isset($_POST["codpro"]) && $_POST["codpro"]!="")?$_POST['codpro']:null,
                        "solart" => (isset($_POST["solart"]) && $_POST["solart"]!="0")?"1":"0",
                        "codart" => (isset($_POST["codart"]) && $_POST["codart"]!="")?$_POST['codart']:null,
                        "solcaj" => (isset($_POST["solcaj"]) && $_POST["solcaj"]!="0")?"1":"0",
                        "codcaj" => (isset($_POST["codcaj"]) && $_POST["codcaj"]!="")?$_POST['codcaj']:null,
                        "obsoleto" => (isset($_POST["obsoleto"]) && $_POST["obsoleto"]!="0")?$_POST['obsoleto']:"0"
                    );
                    $campos_clave = array("codmov");
                    $mensaje = $conexion->actualizar_desde_objeto($movimiento, $campos_clave, "CONMOV");
                    $exito = (strpos($mensaje, "Exito") !== false);
                    $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                }
            }
            elseif ($opcion == "insertar") {
                $movimiento = array(
                    "codsuc" => $sucursal,
                    "desmov" => (isset($_POST["desmov"]) && $_POST["desmov"]!="")?$_POST['desmov']:"",
                    "ingegr" => (isset($_POST["ingegr"]) && $_POST["ingegr"]!="")?$_POST['ingegr']:"",
                    "tipo"   => (isset($_POST["tipo"])   && $_POST["tipo"]!="")?$_POST['tipo']:"",
                    "codforpag" => (isset($_POST["codforpag"]) && $_POST["codforpag"]!="")?$_POST['codforpag']:null,
                    "cajas"  => (isset($_POST["cajas"])  && $_POST["cajas"]!="")?$_POST['cajas']:null,
                    "solemp" => (isset($_POST["solemp"]) && $_POST["solemp"]!="0")?"1":"0",
                    "codemp" => (isset($_POST["codemp"]) && $_POST["codemp"]!="")?$_POST['codemp']:null,
                    "solpro" => (isset($_POST["solpro"]) && $_POST["solpro"]!="0")?"1":"0",
                    "codpro" => (isset($_POST["codpro"]) && $_POST["codpro"]!="")?$_POST['codpro']:null,
                    "solart" => (isset($_POST["solart"]) && $_POST["solart"]!="0")?"1":"0",
                    "codart" => (isset($_POST["codart"]) && $_POST["codart"]!="")?$_POST['codart']:null,
                    "solcaj" => (isset($_POST["solcaj"]) && $_POST["solcaj"]!="0")?"1":"0",
                    "codcaj" => (isset($_POST["codcaj"]) && $_POST["codcaj"]!="")?$_POST['codcaj']:null,
                    "obsoleto" => (isset($_POST["obsoleto"]) && $_POST["obsoleto"]!="0")?$_POST['obsoleto']:"0"
                );
                $mensaje = $conexion->insertar_desde_objeto($movimiento, "CONMOV");
                $exito = (strpos($mensaje, "Exito") !== false);
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
            }
            elseif ($opcion == "eliminar") {
                if (isset($_POST['codmov'])) {
                    $movimiento = array(
                        "codsuc" => $sucursal,
                        "codmov" => $_POST['codmov']
                    );
                    $campos_clave = array("codmov");
                    $mensaje = $conexion->eliminar_desde_objeto($movimiento, $campos_clave, "CONMOV");
                    $exito = (strpos($mensaje, "Exito") !== false);
                    $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                }
            }
        }
    }
    echo json_encode($respuesta, true);
}
catch (Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
