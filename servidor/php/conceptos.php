<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array('exito' => false);
    if (isset($_POST['sucursal'])) {
        $sucursal = $_POST['sucursal'];

        if (isset($_POST['opcion'])) {
            require_once("config/Config.php");
            $conexion = new Conexion();

            $opcion = $_POST['opcion'];
            if ($opcion == "consulta") { // Cunsulta de los que existen en la Sucursal
                $sql =
					"SELECT * ".
					"FROM CONCEPTOS ".
					"WHERE codsuc='$sucursal'";
                $conceptos = $conexion->consulta($sql);
                if(count($conceptos)) {
                    $respuesta = array( 'exito' => true, 'conceptos' => $conceptos, 'nume_regis' => count($conceptos));
                }
                else{
                    $respuesta = array('exito' => false, 'sucursal' => $sucursal);
                }
            }
            else if ($opcion == "consultar") { // Consultar uno en Especifico
                $idconcepto	= (isset($_POST["idconcepto"]))?$_POST['idconcepto']:"";

                $sql =
					"SELECT * ".
					"FROM CONCEPTOS ".
					"WHERE codsuc = '$sucursal' ".
					"AND idconcepto = '$idconcepto'";
                $concepto = $conexion->consulta($sql);

                if(count($conceptos)) {
                	$row = $concepto[0];
                    $respuesta = array('exito' => true, 'codsuc' => $row['codsuc'], 'idconcepto' => $row['idconcepto'], 'desconcep' => $row['desconcep']);
                }
                else{
                    //Se declara que esta es una aplicacion que genera un JSON
                    $respuesta = array('exito' => false, 'sucursal' => $sucursal);
                }
            }
            else if ($opcion == "actualizar") {
                $idconcepto	= (isset($_POST["idconcepto"]))?$_POST['idconcepto']:"";
                $desconcep	= (isset($_POST["desconcepconcep"]))?$_POST['desconcepconcep']:"";

                // SQL query to fetch information of registerd users and finds user match.
                $sql =
					"UPDATE CONCEPTOS SET ".
					"desconcep = '$desconcep' ".
					"WHERE codsuc = '$sucursal' ".
					"AND idconcepto = '$idconcepto'";
                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje, 'sucursal' => $sucursal);
            }
            else if ($opcion == "insertar") {
                $idconcepto	= (isset($_POST["idconcepto"]))?$_POST['idconcepto']:"";
                $desconcep	= (isset($_POST["desconcepconcep"]))?$_POST['desconcepconcep']:"";

                // SQL query to fetch information of registerd users and finds user match.
                $sql =
					"INSERT INTO CONCEPTOS ".
					"(codsuc, idconcepto, desconcep) VALUES ".
					"('$sucursal','$idconcepto','$desconcep')";
                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje, 'sucursal' => $sucursal);
            }
            else if ($opcion == "eliminar") {
                $idconcepto	= (isset($_POST["idconcepto"]))?$_POST['idconcepto']:"";

                // SQL query to fetch information of registerd users and finds user match.
                $sql =
					"DELETE FROM CONCEPTOS ".
					"WHERE codsuc = '$sucursal' ".
					"AND idconcepto = '$idconcepto'";
                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje, 'sucursal' => $sucursal);
            }
        }
    }
    echo json_encode($respuesta, true);
}
catch (Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
