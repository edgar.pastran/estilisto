<?php //

header('Content-type: application/json');

header("Access-Control-Allow-Origin: *");



try {//Controlar siempre el error

	$fecha_ini= '';
	$fecha_fin= '';

	$fecha_desde= '';
	$fecha_hasta= '';

	$ejercicio= '';
	$sucursal= '';



	if (empty($_POST['sucursal']) || empty($_POST['fecha_ini']) || empty($_POST['fecha_fin'])) {	

		if (empty($_POST['sucursal']) ) {throw new Exception('Sucursal Missing.');}
		if (empty($_POST['fecha_ini'])) {throw new Exception('fecha_ini Missing.');}
		if (empty($_POST['fecha_fin'])) {throw new Exception('fecha_fin Missing.');}

	}else{

		$fecha_ini=$_POST['fecha_ini'];
		$fecha_fin=$_POST['fecha_fin'];
		$sucursal="(".$_POST['sucursal'].")";

		$dia = substr($fecha_ini,0,2);
		$mes1 = substr($fecha_ini,3,2);
		$anio = substr($fecha_ini,6,4);
		$fecha_desde=$anio."/".$mes1."/".$dia;

		$dia = substr($fecha_fin,0,2);
		$mes1 = substr($fecha_fin,3,2);
		$anio = substr($fecha_fin,6,4);
		$fecha_hasta=$anio."/".$mes1."/".$dia;
		$ejercicio = stripslashes($ejercicio);

		//------DECLARACION DE VARIABLES--------
		$facturas    = array();
		$totfacturas    = array();
		$nume_regis= 0;
		$totalcom=0;

		include('config.php');

		//------------BUSQUEDA DE LAS VENTAS------------ 
		$sql = "SELECT FACPROL.codart, FACPROL.desart, FACPROL.subtot, FACPROC.codsuc, FACPROC.ejefacp, FACPROC.numfacp, FACPROC.fecfacp, FACPROC.numalb, FACPROC.codpro, FACPROC.totimpbas, FACPROC.totimpdto, FACPROC.totimpiva, FACPROC.totfacp, FACPROC.obsfac, PROVEEDOR.razon
				FROM FACPROL
				INNER JOIN FACPROC 
				ON FACPROC.codsuc = FACPROL.codsuc AND FACPROC.serfacp = FACPROL.serfacp AND FACPROC.ejefacp = FACPROL.ejefacp AND FACPROC.numfacp = FACPROL.numfacp
				INNER JOIN PROVEEDOR 
				ON FACPROC.codsuc = PROVEEDOR.codsuc AND FACPROC.codpro = PROVEEDOR.codpro
				WHERE FACPROC.codsuc IN $sucursal and (FACPROC.anulada<>'1' or FACPROC.anulada is NULL) and FACPROC.fecfacp BETWEEN '$fecha_desde' and '$fecha_hasta' ORDER BY FACPROC.fecfacp DESC, FACPROC.codsuc ASC";
		$result = mysql_query($sql);
		$nume_regis=mysql_num_rows($result);

		$contador=-1;
		$entradas =-1;

		$sucursal_d=' ';
		$ejercicio_d=' ';
		$fecha_d=' ';
		$numfacp_d=' ';

		$reg1=0;
		for ($offset=$reg1; $offset<$nume_regis; $offset++) {
			mysql_data_seek($result, $offset);
			$row=mysql_fetch_array($result);

			if (($sucursal_d <> $row['codsuc']) || ($ejercicio_d <> $row['ejefacp'])  || ($fecha_d <> $row['fecfacp']) || ($numfacp_d <> $row['numfacp'])){ 

				$contador = $contador+1;

				$sucursal_d = $row['codsuc'];
				$ejercicio_d = $row['ejefacp'];
				$fecha_d = $row['fecfacp'];
				$numfacp_d = $row['numfacp'];

			}else{

			}


			if ( $entradas == $contador) { //Pertenese al mismo documento
				$facturas[$contador]['detalle']   = $facturas[$contador]['detalle']." -- ".$row['codart']." - ".utf8_encode($row['desart']);;;
			}else{ //Entrada Nueva
				$entradas = $entradas+1;
				$facturas[$contador]['codsuc']   = $row['codsuc'];
				$facturas[$contador]['numfacp']   = $row['numfacp'];
				$facturas[$contador]['fecfacp']   = $row['fecfacp'];
				$facturas[$contador]['numalb']   = $row['numalb'];
				$facturas[$contador]['codpro']   = $row['codpro']." - ".utf8_encode($row['razon']);
				$facturas[$contador]['totimpbas']   = $row['totimpbas'];
				$facturas[$contador]['totimpdto']   = $row['totimpdto'];
				$facturas[$contador]['totimpiva']   = $row['totimpiva'];
				$facturas[$contador]['totfacp']   = $row['totfacp'];
				$facturas[$contador]['obsfac']   = $row['obsfac'];
				$facturas[$contador]['detalle']   = $row['codart']." - ".utf8_encode($row['desart']);;
			}		

		}

		$nume_regis = count($facturas);

        $reg1=0;
        $offset=$reg1;
        for ($offset=$reg1; $offset<$nume_regis; $offset++) {
        	$totalcom = $totalcom + $facturas[$offset]['totfacp'];

            $facturas[$offset]['totimpbas'] = number_format($facturas[$offset]['totimpbas'],2, '.', ',');
            $facturas[$offset]['totimpdto'] = number_format($facturas[$offset]['totimpdto'],2, '.', ',');
            $facturas[$offset]['totimpiva'] = number_format($facturas[$offset]['totimpiva'],2, '.', ',');
            $facturas[$offset]['totfacp'] = number_format($facturas[$offset]['totfacp'],2, '.', ',');

            $facturas[$offset]['obsfac'] = utf8_encode($facturas[$offset]['obsfac']);

        };

        $totalcom = number_format($totalcom,2, '.', ',');

		//Se declara que esta es una aplicacion que genera un JSON

		echo json_encode(array( 'exito' => true, 'nume_regis' => $nume_regis, 'sucursal' => $sucursal, 'facturas' => $facturas, 'totalcom' => $totalcom));

	}//Fin Validacion de POST

} catch(Exception $e) {//Controlar siempre el error.

	$data = $e->getMessage();

	echo json_encode($data);

}





?>

