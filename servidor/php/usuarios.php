<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array('exito' => false);

    if (isset($_POST['opcion'])) {
        require_once("config/Config.php");
        $conexion = new Conexion();

        $opcion = $_POST['opcion'];
        if ($opcion == "consulta") {
            // SQL query to fetch information of registerd users and finds user match.
            $sql =
                "SELECT * ".
                "FROM USUARIOS ".
                "WHERE username <> ''";
            $usuarios = $conexion->consulta($sql);

            if (count($usuarios)) {
                $respuesta = array('exito' => true, 'usuarios' => $usuarios, 'nume_regis' => count($usuarios));
            }
            else{
                $respuesta = array('exito' => false, 'id' => $id);
            }
        }
        else if ($opcion == "accesos") {
            $id	= (isset($_POST["id"]))?$_POST['id']:"";

            // SQL query to fetch information of registerd users and finds user match.
            $sql =
                "SELECT * ".
                "FROM USUARIO_EMPRESA ".
                "WHERE id = '$id'";
            $datos = $conexion->consulta($sql);

            if (count($datos)) {
                $respuesta = array('exito' => true, 'accesos' => $datos, 'nume_regis' => count($datos));
            }
            else{
                $respuesta = array('exito' => false, 'id' => $id);
            }
        }
        else if ($opcion == "consultar") {
            $id	= (isset($_POST["id"]))?$_POST['id']:"";
            $username = (isset($_POST["username"]))?$_POST['username']:"";

            // SQL query to fetch information of registerd users and finds user match.
            $sql =
                "SELECT * ".
                "FROM USUARIOS ".
                "WHERE username = '$username'";
            $datos = $conexion->consulta($sql);
            if (count($datos)) {
                $row = $datos[0];

                $respuesta = array(
                    'exito' => true, 'id' => $row['id'], 'nombre' => $row['nombre'],
                    'administrador' => $row['administrador'], 'apellido' => $row['apellido'],
                    'username' => $row['username'], 'password' => $row['password'],
                    'direccion' => $row['direccion'], 'compania' => $row['compania'],
                    'codgru' => $row['codgru']
                );
            }
            else{
                $respuesta = array('exito' => false, 'id' => $id);
            }
        }
        else if ($opcion == "actualizar") {
            $id	= (isset($_POST["id"]))?$_POST['id']:"";
            $nombre	  = (isset($_POST["nombre"]))?$_POST['nombre']:"";
            $apellido = (isset($_POST["apellido"]))?$_POST['apellido']:"";
            $username = (isset($_POST["username"]))?$_POST['username']:"";
            $password = (isset($_POST["password"]))?$_POST['password']:"";
            $direccion= (isset($_POST["direccion"]))?$_POST['direccion']:"";
            $compania = (isset($_POST["compania"]))?$_POST['compania']:"";
            $administrador	= (isset($_POST["administrador"]))?$_POST['administrador']:"";
            $codgru	= (isset($_POST["codgru"]))?$_POST['codgru']:"";

            // SQL query to fetch information of registerd users and finds user match.
            $sql =
                "UPDATE USUARIOS SET ".
                "nombre = '$nombre', ".
                "apellido = '$apellido', ".
                "username = '$username', ".
                "password = '$password', ".
                "direccion = '$direccion', ".
                "compania = '$compania', ".
                "administrador = '$administrador', ".
                "codgru = '$codgru' ".
                "WHERE id='$id'";

            $mensaje = $conexion->sentencia($sql);
            $exito = strpos($mensaje, "Exito") !== false;
            $respuesta = array('exito' => $exito, 'id' => $id);
        }
        else if ($opcion == "actualizar_accesos") {
            //Eliminar los Accesos
            $seleccionados = (isset($_POST["seleccionados"]))?json_decode($_POST['seleccionados']):"";
            $id	= (isset($_POST["id"]))?$_POST['id']:"";

            $sql =
                "DELETE FROM ".
                "USUARIO_EMPRESA ".
                "WHERE id = '$id'";
            $mensaje = $conexion->sentencia($sql);

            //Insertar Nuevos Accesos
            $nume_regis = count($seleccionados);
            for ($i=0; $i<$nume_regis; $i++) {
                $codsuc = $seleccionados[$i];
                $sql =
                    "INSERT INTO USUARIO_EMPRESA ".
                    "(id, codsuc) ".
                    "VALUES ".
                    "('$id','$codsuc')";
                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'id' => $id);
            }
        }
        else if ($opcion == "eliminar_accesos") {
            $id	= (isset($_POST["id"]))?$_POST['id']:"";

            //Eliminar los Accesos
            $sql =
                "DELETE FROM ACCESOS ".
                "WHERE id = '$id'";
            $mensaje = $conexion->sentencia($sql);
            $exito = strpos($mensaje, "Exito") !== false;
            $respuesta = array('exito' => $exito, 'id' => $id);
        }
        else if ($opcion == "insertar") {
            $username = (isset($_POST["username"]))?$_POST['username']:"";
            $nombre	  = (isset($_POST["nombre"]))?$_POST['nombre']:"";
            $apellido = (isset($_POST["apellido"]))?$_POST['apellido']:"";
            $direccion= (isset($_POST["direccion"]))?$_POST['direccion']:"";
            $administrador	= (isset($_POST["administrador"]))?$_POST['administrador']:"";
            $password = (isset($_POST["password"]))?$_POST['password']:"";
            $compania = (isset($_POST["compania"]))?$_POST['compania']:"";
            $codgru	= (isset($_POST["codgru"]))?$_POST['codgru']:"";

            $sql =
                "SELECT MAX(id) AS id ".
                "FROM USUARIOS ".
                "WHERE id <> '0'";
            $datos = $conexion->consulta($sql);

            $id = 1;
            if (count($datos)) {
                $id += $datos[0]['id'];
            }

            $sql =
                "INSERT INTO USUARIOS ".
                "(id, username, nombre, apellido, direccion, administrador, password, compania, codgru) ".
                "VALUES ".
                "('$id','$username','$nombre','$apellido','$direccion','$administrador','$password','$compania','$codgru')";

            $mensaje = $conexion->sentencia($sql);
            $exito = strpos($mensaje, "Exito") !== false;
            $respuesta = array('exito' => $exito, 'idUs' => $id);
        }
        else if ($opcion == "eliminar") {
            $id	= (isset($_POST["id"]))?$_POST['id']:"";

            $sql =
                "DELETE FROM USUARIOS WHERE id = '$id'";

            $mensaje = $conexion->sentencia($sql);
            $exito = strpos($mensaje, "Exito") !== false;
            $respuesta = array('exito' => $exito, 'idUs' => $id);
        }
    }
    echo json_encode($respuesta, true);
}
catch (Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
