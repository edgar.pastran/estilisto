<?php //
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");
//header("Content-Type: text/html;charset=utf-8");

$fecha_ini= '';
$fecha_fin= '';

$fecha_desde= '';
$fecha_hasta= '';

$ejercicio= '';
$sucursal= '';

//------DECLARACION DE VARIABLES--------
$moneda = " ";

$det_codart   = array();
$det_desart   = array();
$tot_subart   = array();
$codart=' ';

$tot_compras    = array();

$tot_ventas    = array();
$tot_fecha     = array();
$tot_tikets    = array();

$totalcom  = 0;
$totalven  = 0;
$totalben  = 0;

$nume_regis_a = 0;
$porc_art = array();
$canti_art = array();
$totalven_a = 0;
$nume_regis_e = 0;
$nume_regis = 0;

$moneda = " ";

$det_codemp     = array();
$det_nombre_emp = array();
$tot_subtot_emp = array();
$tot_comisi_emp = array();
$codemp=' ';
$numfac=' ';

$totalven_emp  = 0;
$totalcom_emp  = 0;
$total_utilidad  = 0;
$totaltickets  = 0;
$porc_emp = array();
$tickets_emp = array();

try {//Controlar siempre el error
	if (empty($_POST['sucursal']) || empty($_POST['fecha_ini']) || empty($_POST['fecha_fin'])) {

	}
	else
	{
		$fecha_ini=$_POST['fecha_ini'];
		$fecha_fin=$_POST['fecha_fin'];
		$sucursal=$_POST['sucursal'];
		$id=$_POST['id'];

		$dia = substr($fecha_ini,0,2);
		$mes1 = substr($fecha_ini,3,2);
		$anio = substr($fecha_ini,6,4);
		$fecha_desde=$anio."/".$mes1."/".$dia;

		$dia = substr($fecha_fin,0,2);
		$mes1 = substr($fecha_fin,3,2);
		$anio = substr($fecha_fin,6,4);
		$fecha_hasta=$anio."/".$mes1."/".$dia;

		$ejercicio = stripslashes($ejercicio);
		$sucursal="(".$_POST['sucursal'].")";
	
		include('config.php');
		//------------ BUSQUEDA DE LAS VENTAS de ARTICULO por EMPLEADOS ------------ 
		$sql = "SELECT EMPLEADOS.codemp, FACLIN.numfac, EMPLEADOS.nomemp, EMPLEADOS.ape1emp, FACLIN.codart, FACLIN.desart, FACLIN.cant, FACLIN.subtot, FACLIN.comision
		FROM FACLIN INNER JOIN FACCAB 
		ON FACLIN.codsuc = FACCAB.codsuc AND FACLIN.ejefac = FACCAB.ejefac
		AND FACLIN.numfac = FACCAB.numfac
		INNER JOIN EMPLEADOS
		ON FACLIN.codsuc = EMPLEADOS.codsuc AND FACLIN.codemp = EMPLEADOS.codemp
		WHERE FACLIN.codsuc IN $sucursal AND FACCAB.fecfac BETWEEN '$fecha_desde' AND '$fecha_hasta' AND FACLIN.codemp = '$id' order by FACLIN.codart ASC";

		$result = mysql_query($sql);
		$nume_regis=mysql_num_rows($result);

		//------------VENTAS POR ARTICULO ------------ 
		//--declaracion de Variables para el diario de ventas--
		$contador=-1;
		$entradas =-1;
		$ventas_d=0;
		$tickets_d=0;
		$fecha_d=' ';
		$sucursal_d=' ';
		$ejercicio_d=' ';
		$datos = array(); //Arreglo de datos de la Consulta para Luego Ordenar

		$reg1=0;
		for ($offset=$reg1; $offset<$nume_regis; $offset++) {
			$datos[]=mysql_fetch_assoc($result); //Llenado de Arreglo General para Ordenar
			mysql_data_seek($result, $offset);
			$row=mysql_fetch_array($result);

			if ( $codart != $row['codart'] ){
				$contador = $contador+1;
				$codart = $row['codart'];
			}else{

			}
			if ( $entradas == $contador) {
				$det_codart[$contador] = $row['codart'];
				$det_desart[$contador] = $row['desart'];
				$tot_subart[$contador] = $tot_subart[$contador]+$row['subtot'];
				$canti_art[$contador] = $canti_art[$contador] + $row['cant'];
			}else{
				$det_codart[$contador] = $row['codart'];
				$det_desart[$contador] = $row['desart'];
				$tot_subart[$contador] = $row['subtot'];
				$canti_art[$contador] = intval($row['cant']);
				$entradas = $entradas+1;
			}
			$totalven_a = $totalven_a + $row['subtot'];
			$det_nombre_emp[$contador] = $row['nomemp'];
		}
		

		//Dar Formato a Numeros
		$nume_regis_a = count($det_codart);
	    $reg1=0;
	    $offset=$reg1;
	    for ($offset=$reg1; $offset<$nume_regis_a; $offset++) {
	    	$totaltickets = $totaltickets + $canti_art[$offset];

	    	if ($totalven_a > 0) {$porc_art[$offset] = number_format((($tot_subart[$offset] * 100) / $totalven_a),2, '.', ',');
	    	}else{$porc_art[$offset] = 0;}
	        $tot_subart[$offset] = number_format($tot_subart[$offset],2, '.', ',');
	        $det_desart[$offset] = utf8_encode($det_desart[$offset]);
	    };
		$totalven_a = number_format($totalven_a,2, '.', ',');


		mysql_close($connection); // Closing Connection
}// fin del Validacion de campos
	//Se declara que esta es una aplicacion que genera un JSON

	echo json_encode(array( 'exito' => true, 'nume_regis_a' => $nume_regis_a, 'fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin,
		'sucursal' => $sucursal, 'det_codart' => $det_codart, 
		'det_desart' => $det_desart, 'tot_subart' => $tot_subart, 'totalven_a' => $totalven_a, 'porc_art' => $porc_art,
		'nume_regis_e' => $nume_regis_e, 'det_codemp' => $det_codemp, 'det_nombre_emp' => $det_nombre_emp, 'tot_subtot_emp' => $tot_subtot_emp,
		'tot_comisi_emp' => $tot_comisi_emp, 'totalven_emp' => $totalven_emp, 'totalcom_emp' => $totalcom_emp, 'porc_emp' => $porc_emp,
		'$total_utilidad' => $total_utilidad, 'canti_art' => $canti_art, 'tickets_emp' => $tickets_emp, 'totaltickets' => $totaltickets
		));

} catch(Exception $e) {//Controlar siempre el error.
	$data = $e->getMessage();
	echo json_encode($data);
}


?>
