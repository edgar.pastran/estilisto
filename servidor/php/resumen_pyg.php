<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
	$fecha_ini= '';
	$fecha_fin= '';

	$fecha_desde= '';
	$fecha_hasta= '';

	$ejercicio= '';
	$sucursal= '';

	if (empty($_POST['sucursal']) || empty($_POST['eje_anterior']) || empty($_POST['eje_actual'])) {	
		if (empty($_POST['sucursal']) ) {throw new Exception('Sucursal Missing.');}
		if (empty($_POST['eje_anterior'])) {throw new Exception('eje_anterior Missing.');}
		if (empty($_POST['eje_actual'])) { throw new Exception('eje_actual Missing.');}
	}else{
		$eje_anterior=$_POST['eje_anterior'];
		$eje_actual=$_POST['eje_actual'];
		$sucursal="(".$_POST['sucursal'].")";

		$ejercicio = stripslashes($ejercicio);

		//------DECLARACION DE VARIABLES--------
		$tot_tik_mes    = array();
		$tot_tik_mes[0]    = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$tot_tik_mes[1]    = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$tot_ven_mes[0]	   = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		$tot_ven_mes[1]	   = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

		$tot_ven_tik_avg[0] = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$tot_ven_tik_avg[1] = array(0,0,0,0,0,0,0,0,0,0,0,0);
		
		$tot_tik_dia_avg[0] = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$tot_tik_dia_avg[1] = array(0,0,0,0,0,0,0,0,0,0,0,0);

		$tot_tik_mes[5][0] 	= $eje_anterior;
        $tot_tik_dia_avg[5][0] = $eje_anterior;
        $tot_ven_mes[5][0] = $eje_anterior;
        $tot_ven_tik_avg[5][0] = $eje_anterior;

        $tot_tik_mes[5][1] 	= $eje_actual;
        $tot_tik_dia_avg[5][1] = $eje_actual;
        $tot_ven_mes[5][1] = $eje_actual;
        $tot_ven_tik_avg[5][1] = $eje_actual;

        $tot_total_dias[0]    = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$tot_total_dias[1]    = array(0,0,0,0,0,0,0,0,0,0,0,0);

		$tot_margen[0]    = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);//total gastos por mes
		$tot_margen[1]    = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);//margen por mes de 0 a 11

		$ventas_actual    = array();

		//------
		require_once("config/Config.php");
		$conexion = new Conexion();

		//------------ EJECICIO ANTERIOR ------------ //
		$fecha_desde=$eje_anterior."/01/01";
		$fecha_hasta=$eje_anterior."/12/31";
		

		$sql = "SELECT ejefac, fecfac, codsuc, count(*) AS tot_tikets, sum(totfac) AS tot_ventas, avg(totfac) AS tot_promeven FROM FACCAB WHERE codsuc IN $sucursal and fecfac BETWEEN '$fecha_desde' and '$fecha_hasta' GROUP BY fecfac ASC";
		$datos = $conexion->consulta($sql);

		$nume_regis = count($datos);
		if (count($datos) > 0){
        	//------ Calculo de Totales Generales ----
			for ($i=0; $i<count($datos); $i++) {
        		$mes = substr($datos[$i]['fecfac'],5,2) - 1; //Obtener el Mes de la Fecha
        		
        		$tot_total_dias[0][$mes] = $tot_total_dias[0][$mes] + 1; //Total Dias Año Anterior

        		$tot_tik_mes[0][$mes] = $tot_tik_mes[0][$mes] + $datos[$i]['tot_tikets'];
        		$tot_tik_dia_avg[0][$mes] = $tot_tik_dia_avg[0][$mes] + 1; // Primero el total de dias del Mes
        		$tot_ven_mes[0][$mes] = $tot_ven_mes[0][$mes] + $datos[$i]['tot_ventas'];
        		$tot_ven_tik_avg[0][$mes] = 0; // Primero el total de dias del Mes
        	}
        	//------Calculo de los promedios---------
        	for ($mes=0; $mes<count($tot_tik_mes[0]); $mes++) {

				if ($tot_tik_mes[0][$mes] > 0) { //Promedio de Ticket por dia al Mes
					$tot_tik_dia_avg[0][$mes] = $tot_tik_mes[0][$mes] / $tot_tik_dia_avg[0][$mes];
				}else{
					$tot_tik_dia_avg[0][$mes] = 0;
				};

				if ($tot_ven_mes[0][$mes] > 0) { //Promedio de Venta por Ticker al Mes
					$tot_ven_tik_avg[0][$mes] = $tot_ven_mes[0][$mes] / $tot_tik_mes[0][$mes];
				}else{
					$tot_ven_tik_avg[0][$mes] = 0;
				};
			}
		}

        // ****  ------- EJECICIO ACTUAL   --------  **** //
		$fecha_desde=$eje_actual."/01/01";
		$fecha_hasta=$eje_actual."/12/31";

		$sql = "SELECT ejefac, fecfac, codsuc, count(*) AS tot_tikets, sum(totfac) AS tot_ventas, avg(totfac) AS tot_promeven FROM FACCAB WHERE codsuc IN $sucursal and fecfac BETWEEN '$fecha_desde' and '$fecha_hasta' GROUP BY fecfac ASC";
		$datos = $conexion->consulta($sql);
		

		$nume_regis = count($datos);
		if (count($datos) > 0){
        	//------ Calculo de Totales Generales ----
			for ($i=0; $i<count($datos); $i++) {
        		$mes = substr($datos[$i]['fecfac'],5,2) - 1; //Obtener el Mes de la Fecha

        		$tot_total_dias[1][$mes] = $tot_total_dias[1][$mes] + 1; //Total Dias Año Actual

        		$tot_tik_mes[1][$mes] = $tot_tik_mes[1][$mes] + $datos[$i]['tot_tikets'];
        		$tot_tik_dia_avg[1][$mes] = $tot_tik_dia_avg[1][$mes] + 1; // Primero el total de dias del Mes
        		$tot_ven_mes[1][$mes] = $tot_ven_mes[1][$mes] + $datos[$i]['tot_ventas'];
        		$tot_ven_mes[1][14] = $tot_ven_mes[1][14] + $datos[$i]['tot_ventas'];
        		$tot_ven_tik_avg[1][$mes] = 0; // Primero el total de dias del Mes

        	}
        	
        	for ($mes=0; $mes<count($tot_tik_mes[1]); $mes++) {

        		//------Calculo de los promedios---------
				if ($tot_tik_mes[1][$mes] > 0) { //Promedio de Ticket por dia al Mes
					$tot_tik_dia_avg[1][$mes] = $tot_tik_mes[1][$mes] / $tot_tik_dia_avg[1][$mes];
				}else{
					$tot_tik_dia_avg[1][$mes] = 0;
				};

				if ($tot_ven_mes[1][$mes] > 0) { //Promedio de Venta por Ticker al Mes
					$tot_ven_tik_avg[1][$mes] = $tot_ven_mes[1][$mes] / $tot_tik_mes[1][$mes];
				}else{
					$tot_ven_tik_avg[1][$mes] = 0;
				};

				//------ Calculo de Porcentajes Mensuales ---------
				if (($tot_tik_mes[0][$mes] > 0) and ($tot_tik_mes[1][$mes] > 0)) { //Var %
					$tot_tik_mes[2][$mes] = (($tot_tik_mes[1][$mes]*100)/$tot_tik_mes[0][$mes])-100;
				}else{
					$tot_tik_mes[2][$mes] = 0;
				};

				if (($tot_tik_dia_avg[0][$mes] > 0) and ($tot_tik_dia_avg[1][$mes] > 0)) { //Var %
					$tot_tik_dia_avg[2][$mes] = (($tot_tik_dia_avg[1][$mes]*100)/$tot_tik_dia_avg[0][$mes])-100;
				}else{
					$tot_tik_dia_avg[2][$mes] = 0;
				};

				if (($tot_ven_mes[0][$mes] > 0) and ($tot_ven_mes[1][$mes] > 0)) { //Var %
					$tot_ven_mes[2][$mes] = (($tot_ven_mes[1][$mes]*100)/$tot_ven_mes[0][$mes])-100;
				}else{
					$tot_ven_mes[2][$mes] = 0;
				};

				if (($tot_ven_tik_avg[0][$mes] > 0) and ($tot_ven_tik_avg[1][$mes] > 0)) { //Var %
					$tot_ven_tik_avg[2][$mes] = (($tot_ven_tik_avg[1][$mes]*100)/$tot_ven_tik_avg[0][$mes])-100;
				}else{
					$tot_ven_tik_avg[2][$mes] = 0;
				};
				//------ Dar Formato de Numeros ---------
		        // $tot_tik_mes[1][$mes] 		= number_format($tot_tik_mes[1][$mes],2, '.', ',');
			}

			for ($mes=0; $mes<count($tot_tik_mes[1]); $mes++) {
	        	//------ Valores Trimestrales ---------
				if ($tot_tik_mes[1][$mes] > 0) {
					if ($mes == 0) {
						$tot_tik_mes[3][$mes] = ($tot_tik_mes[0][9] + $tot_tik_mes[0][10]) / 2;
						$tot_tik_dia_avg[3][$mes] = ($tot_tik_dia_avg[0][9] + $tot_tik_dia_avg[0][10]) / 2;
						$tot_ven_mes[3][$mes] = ($tot_ven_mes[0][9] + $tot_ven_mes[0][10]) / 2;
						$tot_ven_tik_avg[3][$mes] = ($tot_ven_tik_avg[0][9] + $tot_ven_tik_avg[0][10]) / 2;

					}elseif ($mes == 1) {
						$tot_tik_mes[3][$mes] = ($tot_tik_mes[0][9] + $tot_tik_mes[0][10] + $tot_tik_mes[1][0]) / 3;
						$tot_tik_dia_avg[3][$mes] = ($tot_tik_dia_avg[0][9] + $tot_tik_dia_avg[0][10] + $tot_tik_dia_avg[1][0]) / 3;
						$tot_ven_mes[3][$mes] = ($tot_ven_mes[0][9] + $tot_ven_mes[0][10] + $tot_ven_mes[1][0]) / 3;
						$tot_ven_tik_avg[3][$mes] = ($tot_ven_tik_avg[0][9] + $tot_ven_tik_avg[0][10] + $tot_ven_tik_avg[1][0]) / 3;
					}elseif ($mes == 2) {
						$tot_tik_mes[3][$mes] = ($tot_tik_mes[0][10] + $tot_tik_mes[1][0] + $tot_tik_mes[1][1]) / 3;
						$tot_tik_dia_avg[3][$mes] = ($tot_tik_dia_avg[0][10] + $tot_tik_dia_avg[1][0] + $tot_tik_dia_avg[1][1]) / 3;
						$tot_ven_mes[3][$mes] = ($tot_ven_mes[0][10] + $tot_ven_mes[1][0] + $tot_ven_mes[1][1]) / 3;
						$tot_ven_tik_avg[3][$mes] = ($tot_ven_tik_avg[0][10] + $tot_ven_tik_avg[1][0] + $tot_ven_tik_avg[1][1]) / 3;
					}else{
						$mes_1 = $mes-1;
						$mes_2 = $mes-2;
						$mes_3 = $mes-3;
						$tot_tik_mes[3][$mes] = ($tot_tik_mes[1][$mes_3] + $tot_tik_mes[1][$mes_2] + $tot_tik_mes[1][$mes_1]) / 3;
						$tot_tik_dia_avg[3][$mes] = ($tot_tik_dia_avg[1][$mes_3] + $tot_tik_dia_avg[1][$mes_2] + $tot_tik_dia_avg[1][$mes_1]) / 3;
						$tot_ven_mes[3][$mes] = ($tot_ven_mes[1][$mes_3] + $tot_ven_mes[1][$mes_2] + $tot_ven_mes[1][$mes_1]) / 3;
						$tot_ven_tik_avg[3][$mes] = ($tot_ven_tik_avg[1][$mes_3] + $tot_ven_tik_avg[1][$mes_2] + $tot_ven_tik_avg[1][$mes_1]) / 3;
					}

				}else{
					$tot_tik_mes[3][$mes] = 0;
					$tot_tik_dia_avg[3][$mes] = 0;
					$tot_ven_mes[3][$mes] = 0;
					$tot_ven_tik_avg[3][$mes] = 0;
				}
				//% Trimestrales
				//------ Calculo de Porcentajes Mensuales ---------
				if (($tot_tik_mes[3][$mes] > 0) and ($tot_tik_mes[1][$mes] > 0)) { //Var %
					$tot_tik_mes[4][$mes] = (($tot_tik_mes[1][$mes]*100)/$tot_tik_mes[3][$mes])-100;
				}else{
					$tot_tik_mes[4][$mes] = 0;
				};

				if (($tot_tik_dia_avg[3][$mes] > 0) and ($tot_tik_dia_avg[1][$mes] > 0)) { //Var %
					$tot_tik_dia_avg[4][$mes] = (($tot_tik_dia_avg[1][$mes]*100)/$tot_tik_dia_avg[3][$mes])-100;
				}else{
					$tot_tik_dia_avg[4][$mes] = 0;
				};

				if (($tot_ven_mes[3][$mes] > 0) and ($tot_ven_mes[1][$mes] > 0)) { //Var %
					$tot_ven_mes[4][$mes] = (($tot_ven_mes[1][$mes]*100)/$tot_ven_mes[3][$mes])-100;
				}else{
					$tot_ven_mes[4][$mes] = 0;
				};

				if (($tot_ven_tik_avg[3][$mes] > 0) and ($tot_ven_tik_avg[1][$mes] > 0)) { //Var %
					$tot_ven_tik_avg[4][$mes] = (($tot_ven_tik_avg[1][$mes]*100)/$tot_ven_tik_avg[3][$mes])-100;
				}else{
					$tot_ven_tik_avg[4][$mes] = 0;
				};
			}


			// ------- Consulta de los Gastos  -------
			$tot_gas_mes = array();
			//------------Buscar Articulos para la Compras------------ 
			$sql = "SELECT codart, desart
			FROM ARTICULOS 
			WHERE codsuc IN $sucursal";
			$datos = $conexion->consulta($sql);
			for ($i=0; $i<count($datos); $i++) {
				$mes = substr($datos[$i]['fecfac'],5,2) - 1; //Obtener el Mes de la Fecha
				$tot_gas_mes[$i][0] = 0;
				$tot_gas_mes[$i][1] = 0;
				$tot_gas_mes[$i][2] = 0;
				$tot_gas_mes[$i][3] = 0;
				$tot_gas_mes[$i][4] = 0;
				$tot_gas_mes[$i][5] = 0;
				$tot_gas_mes[$i][6] = 0;
				$tot_gas_mes[$i][7] = 0;
				$tot_gas_mes[$i][8] = 0;
				$tot_gas_mes[$i][9] = 0;
				$tot_gas_mes[$i][10] = 0;
				$tot_gas_mes[$i][11] = 0;

				$tot_gas_mes[$i][12] = $datos[$i]['codart'];
				$tot_gas_mes[$i][13] = $datos[$i]['desart'];
				$tot_gas_mes[$i][14] = 0;
			}
			$m = count($tot_gas_mes);
			//------------BUSQUEDA DE LAS COMPRAS ARTICULOS------------ 
			$fecha_desde=$eje_actual."/01/01";
			$fecha_hasta=$eje_actual."/12/31";

			$sql = "SELECT FACPROC.fecfacp, FACPROL.codart, FACPROL.desart, FACPROL.subtot
			FROM FACPROL INNER JOIN FACPROC 
			ON FACPROL.codsuc = FACPROC.codsuc AND FACPROL.ejefacp = FACPROC.ejefacp
			AND FACPROL.numfacp = FACPROC.numfacp 
			WHERE FACPROL.subtot > 0 AND (FACPROC.anulada<>'1' or FACPROC.anulada is NULL) AND FACPROL.codsuc IN $sucursal AND FACPROC.fecfacp BETWEEN '$fecha_desde' AND '$fecha_hasta' ORDER BY FACPROC.fecfacp ASC";
			$datos = $conexion->consulta($sql);
        	//------ Calculo de Totales Generales GASTOS ----
			for ($i=0; $i<count($datos); $i++) {
				$mes = substr($datos[$i]['fecfacp'],5,2) - 1; //Obtener el Mes de la Fecha

				for ($x=0; $x<$m; $x++) {//Recorrer la Tabla de Articulos para sumar en el mes
					if ($tot_gas_mes[$x][12] == $datos[$i]['codart']) { //Si coincide el articulo
						$tot_gas_mes[$x][$mes] = $tot_gas_mes[$x][$mes] + $datos[$i]['subtot'];
						$tot_margen[0][$mes] = $tot_margen[0][$mes] + $datos[$i]['subtot'];
						$tot_margen[0][14] = $tot_margen[0][14] + $datos[$i]['subtot'];
						$tot_gas_mes[$x][14] = $tot_gas_mes[$x][14] + $datos[$i]['subtot'];
						$x = $m; //salir del for
					}
				}
			}

			$tot_margen[1][14] = $tot_ven_mes[1][14] - $tot_margen[0][14]; //Calculo de Margen
        	for ($i=0; $i<12; $i++) {
        		$tot_margen[1][$i] = $tot_ven_mes[1][$i] - $tot_margen[0][$i];
        	}

			//Se declara que esta es una aplicacion que genera un JSON
			echo json_encode(array( 'exito' => true, 'tot_tik_mes' => $tot_tik_mes, 'tot_tik_dia_avg' => $tot_tik_dia_avg, 'tot_ven_mes' => $tot_ven_mes, 'tot_ven_tik_avg' => $tot_ven_tik_avg, 'tot_gas_mes' => $tot_gas_mes, 'total_mat_gas' => $m, 'tot_margen' => $tot_margen,
				'tot_total_dias' => $tot_total_dias));		
		}else{
			$respuesta = array('exito' => false, 'sucursal' => $sucursal);
		}
	}
}
catch (Exception $e) {//Controlar siempre el error.
	$data = $e->POSTMessage();
	echo json_encode($data, true);
}
?>

