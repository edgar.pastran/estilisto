<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array('exito' => false);
    if (isset($_POST['sucursal'])) {
        $respuesta['mensaje'] = '0';
        $sucursal = $_POST['sucursal'];

        if (isset($_POST['opcion'])) {
            $respuesta['mensaje'] = '1';
            require_once("config/Config.php");
            $conexion = new Conexion();

            $opcion = $_POST['opcion'];
            if ($opcion == "inicializar") {
                // Formas de pago
                $sql =
                    "SELECT codfp, des FROM FORPAG ".
                    "WHERE vender = 1 ".
                    "AND codsuc = '".$sucursal."' ".
                    "ORDER BY codfp ASC";
                $formas_pago = $conexion->consulta($sql);

                // Cajas
                $sql =
                    "SELECT codcaj, nomcaj, traautfoncie, codcajdescie ".
                    "FROM CAJA ".
                    "WHERE codsuc = '".$sucursal."' ".
                    "ORDER BY codcaj ASC";
                $cajas = $conexion->consulta($sql);
                for ($i=0; $i<count($cajas); $i++) {
                    // Teoricos por Formas de Pago
                    $teoricos = array();
                    for ($j=0; $j<count($formas_pago); $j++) {
                        $teoricos[$formas_pago[$j]['codfp']]['codcaj'] = $cajas[$i]['codcaj'];
                        $teoricos[$formas_pago[$j]['codfp']]['codfp'] = $formas_pago[$j]['codfp'];
                        $teoricos[$formas_pago[$j]['codfp']]['des'] = $formas_pago[$j]['des'];
                        $teoricos[$formas_pago[$j]['codfp']]['fondo'] = 0;
                        $teoricos[$formas_pago[$j]['codfp']]['I'] = 0;
                        $teoricos[$formas_pago[$j]['codfp']]['E'] = 0;
                        // Fondo en esta Caja por Forma de Pago
                        $sql =
                            "SELECT fondo FROM CAJFORPAG ".
                            "WHERE codsuc = '".$sucursal."' ".
                            "AND codcaj = '".$cajas[$i]['codcaj']."' ".
                            "AND codfp = '".$formas_pago[$j]['codfp']."' ";
                        $fondo_caja = $conexion->consulta($sql);
                        if (count($fondo_caja)) {
                            $teoricos[$formas_pago[$j]['codfp']]['fondo'] = floatval($fondo_caja[0]['fondo']);
                        }
                    }

                    $ultimo_movimiento_cerrado = 0;
                    $fecha_ultimo_movimiento_cerrado = "1900-01-01";
                    $hora_ultimo_movimiento_cerrado = "00:00";
                    $ultimo_cierre = 0;
                    $sql =
                        "SELECT numcie, ultmov ".
                        "FROM CIECAB ".
                        "WHERE codsuc = '".$sucursal."' ".
                        "AND codcaj = '".$cajas[$i]['codcaj']."' ".
                        "ORDER BY numcie DESC ".
                        "LIMIT 0,1";
                    $data_ultimo_cierre = $conexion->consulta($sql);
                    if (isset($data_ultimo_cierre)) {
                        if (isset($data_ultimo_cierre[0]['ultmov'])) {
                            $ultimo_movimiento_cerrado = $data_ultimo_cierre[0]['ultmov'];
                            $ultimo_cierre = $data_ultimo_cierre[0]['numcie'];
                            $sql =
                                "SELECT fecmov, hormov ".
                                "FROM MOVCAJ ".
                                "WHERE codsuc = '".$sucursal."' ".
                                "AND codcaj = '".$cajas[$i]['codcaj']."' ".
                                "AND nummov = '".$ultimo_movimiento_cerrado."'";
                            $data_ultimo_movimiento_cerrado = $conexion->consulta($sql);
                            if (isset($data_ultimo_movimiento_cerrado)) {
                                $fecha_ultimo_movimiento_cerrado = $data_ultimo_movimiento_cerrado[0]['fecmov'];
                                $hora_ultimo_movimiento_cerrado = $data_ultimo_movimiento_cerrado[0]['hormov'];
                            }
                        }
                    }
                    $cajas[$i]['fecultcie'] = date_format(date_create($fecha_ultimo_movimiento_cerrado), "d-m-Y");
                    $cajas[$i]['horultcie'] = $hora_ultimo_movimiento_cerrado;

                    if (isset($cajas[$i]['traautfoncie']) && $cajas[$i]['traautfoncie'] == "1" && isset($cajas[$i]['codcajdescie'])) {
                        $data_traspasos = array();
                    }
                    else {
                        // Movimientos de Traspaso de Saldo
                        $sql =
                            "SELECT * ".
                            "FROM CONMOV ".
                            "WHERE codsuc = '".$sucursal."' ".
                            "AND ingegr = 'E' ".
                            "AND solcaj = 1 ".
                            "AND cajas LIKE '%&".$cajas[$i]['codcaj']."&%'";
                        $data_traspasos = $conexion->consulta($sql);
                    }
                    $cajas[$i]['traspasos'] = $data_traspasos;

                    // Montos por cada forma de pago
                    // Montos reales del ultimo cierre
                    $sql =
                        "SELECT codfp, imprea, impteo, impdif ".
                        "FROM CIELIN ".
                        "WHERE codsuc = '".$sucursal."' ".
                        "AND numcie = '".$ultimo_cierre."'";
                    $lineas_ultimo_cierre = $conexion->consulta($sql);
                    $total_teorico = 0;
                    for ($l=0; $l<count($lineas_ultimo_cierre); $l++) {
                        $codforpag = $lineas_ultimo_cierre[$l]['codfp'];
                        $monto_real = $lineas_ultimo_cierre[$l]['imprea'];
                        $total_teorico += $monto_real;
                        if ($monto_real > 0) {
                            $teoricos[$codforpag]['I'] = $monto_real;
                        }
                        else {
                            $teoricos[$codforpag]['E'] = abs($monto_real);
                        }
                    }
                    // Montos de los movimientos posteriores al ultimo cierre
                    $sql =
                        "SELECT MAX(nummov) AS nummov, SUM(impmov) AS total, codforpag, ingegr ".
                        "FROM MOVCAJ ".
                        "WHERE codsuc = '".$sucursal."' ".
                        "AND codcaj = '".$cajas[$i]['codcaj']."' ".
                        "AND tipo != 'A' ". // Excluir los movimientos de ajustes automaticos
                        "AND nummov > '".$ultimo_movimiento_cerrado."' ".
                        "GROUP BY codforpag, ingegr";
                    $total_movimientos = $conexion->consulta($sql);
                    $ultimo_movimiento_por_cerrar = 0;
                    for ($m=0; $m<count($total_movimientos); $m++) {
                        $nummov = $total_movimientos[$m]['nummov'];
                        if ($nummov > $ultimo_movimiento_por_cerrar) {
                            $ultimo_movimiento_por_cerrar = $nummov;
                        }
                        $codforpag = $total_movimientos[$m]['codforpag'];
                        $ingegr = $total_movimientos[$m]['ingegr'];
                        $total = $total_movimientos[$m]['total'];
                        $teoricos[$codforpag][$ingegr] += $total;
                        if ($ingegr == "I") {
                            $total_teorico += $total;
                        }
                        elseif($ingegr == "E") {
                            $total_teorico -= $total;
                        }
                    }
                    $cajas[$i]['ultmov'] = $ultimo_movimiento_por_cerrar;
                    $cajas[$i]['total_teorico'] = $total_teorico;
                    $cajas[$i]['teoricos'] = array();
                    foreach ($teoricos as $teorico) {
                        array_push($cajas[$i]['teoricos'], $teorico);
                    }
                }
                $respuesta = array( 'exito' => true, 'cajas' => $cajas, 'formas_pago' => $formas_pago);
            }
            elseif ($opcion == "guardar" && isset($_POST['cierre'])) {
                $data = $conexion->consulta("SELECT zonhor FROM EMPRESA where codsuc='".$sucursal."'");
                if (count($data)) {
                    // Fecha y Hora de la Sucursal
                    @date_default_timezone_set($data[0]['zonhor']);
                }

                $cierre = json_decode($_POST['cierre']);
                $cierre->codsuc = $sucursal;
                // Calculo del siguiente numero de cierre
                $cierre->numcie = 1;
                $data = $conexion->consulta("SELECT MAX(numcie) as ultimo_cierre FROM CIECAB where codsuc='".$cierre->codsuc."'");
                if (count($data)) {
                    $cierre->numcie += intval($data[0]['ultimo_cierre']);
                }
                // Colocar fecha y hora
                $cierre->feccie = date("Y-m-d");
                $cierre->horcie = date("H:i");
                $cierre->feccre = date("Y-m-d");
                $cierre->horcre = date("H:i");

                // Registro del cierre
                $mensaje = $conexion->insertar_desde_objeto($cierre, "CIECAB");
                $exito = strpos($mensaje, "Exito") !== false;
                if ($exito) {
                    // Conocer la caja destino para los traspados de saldos
                    $caja_destino_traspaso_saldos = null;
                    // Nombre de Caja
                    $nombre_caja_destino = "";
                    $nombre_caja_fuente = "";
                    $data = $conexion->consulta("SELECT traautfoncie, codcajdescie FROM CAJA WHERE codsuc='".$sucursal."' AND codcaj='".$cierre->codcaj."'");
                    if (count($data)) {
                        if (isset($data[0]['traautfoncie']) && $data[0]['traautfoncie']=="1" && isset($data[0]['codcajdescie'])) {
                            $caja_destino_traspaso_saldos = $data[0]['codcajdescie'];
                            // Nombres de Cajas
                            $data = $conexion->consulta("SELECT nomcaj FROM CAJA WHERE codsuc='".$sucursal."' AND codcaj='".$caja_destino_traspaso_saldos."'");
                            if (count($data)) {
                                $nombre_caja_destino = $data[0]['nomcaj'];
                            }
                            $data = $conexion->consulta("SELECT nomcaj FROM CAJA WHERE codsuc='".$sucursal."' AND codcaj='".$cierre->codcaj."'");
                            if (count($data)) {
                                $nombre_caja_fuente = $data[0]['nomcaj'];
                            }
                        }
                    }

                    foreach ($cierre->lineas as $linea) {
                        //Movimiento de Ajuste
                        if ($linea->impdif != 0 && $exito) {
                            $ajuste = array(
                                "codsuc" => $cierre->codsuc,
                                "codcaj" => $cierre->codcaj,
                                "fecmov" => $cierre->feccie,
                                "hormov" => $cierre->horcie,
                                "ingegr" => (($linea->impdif<0)?"E":"I"),
                                "tipo"   => "A",
                                "numcie" => $cierre->numcie,
                                "codforpag" => $linea->codfp,
                                "impmov" => abs($linea->impdif),
                                "obsmov" => (($linea->impdif<0)?"Egreso":"Ingreso")." por Ajuste en el Cierre de Caja # ".$cierre->numcie
                            );
                            $mensaje = $conexion->insertar_desde_objeto($ajuste, "MOVCAJ");
                            $exito = strpos($mensaje, "Exito") !== false;
                        }
                        if ($exito) {
                            $linea->codsuc = $cierre->codsuc;
                            $linea->numcie = $cierre->numcie;
                            // Registro de la linea del cierre
                            $mensaje = $conexion->insertar_desde_objeto($linea, "CIELIN");
                            $exito = $exito && (strpos($mensaje, "Exito") !== false);

                            // Registro del traspaso de saldo si es necesario
                            if ($exito && isset($caja_destino_traspaso_saldos)) {
                                // Buscar el fondo por esta forma de pago
                                $fondo = 0;
                                $data = $conexion->consulta("SELECT fondo FROM CAJFORPAG WHERE codsuc='".$cierre->codsuc."' AND codcaj='".$cierre->codcaj."' AND codfp='".$linea->codfp."'");
                                if (count($data)) {
                                    $fondo = floatval($data[0]['fondo']);
                                }

                                if ($linea->imprea - $fondo > 0) {
                                    // Movimiento de Traspaso
                                    $movimiento_traspaso = array(
                                        "codsuc" => $sucursal,
                                        "codcaj" => $cierre->codcaj,
                                        "fecmov" => date('Y-m-d'),
                                        "hormov" => date('H:i'),
                                        "ingegr" => "E",
                                        "tipo" => "T",
                                        "numcie" => $cierre->numcie,
                                        "codforpag" => $linea->codfp,
                                        "codcajdes" => $caja_destino_traspaso_saldos,
                                        "impmov" => ($linea->imprea - $fondo),
                                        "obsmov" => "Traspaso de Saldo Automatico por Cierre (Hacia " . $nombre_caja_destino . ")"
                                    );
                                    $mensaje = $conexion->insertar_desde_objeto($movimiento_traspaso, "MOVCAJ");
                                    $exito = (strpos($mensaje, "Exito") !== false);
                                    if ($exito) {
                                        // Movimiento Inverso del Traspaso
                                        $movimiento_inverso = array(
                                            "codsuc" => $sucursal,
                                            "codcaj" => $caja_destino_traspaso_saldos,
                                            "fecmov" => date('Y-m-d'),
                                            "hormov" => date('H:i'),
                                            "ingegr" => "I",
                                            "tipo" => "T",
                                            "codforpag" => $linea->codfp,
                                            "codcajdes" => null,
                                            "impmov" => ($linea->imprea - $fondo),
                                            "obsmov" => "Traspaso de Saldo Automatico por Cierre (Desde " . $nombre_caja_fuente . ")"
                                        );
                                        $mensaje = $conexion->insertar_desde_objeto($movimiento_inverso, "MOVCAJ");
                                        $exito = (strpos($mensaje, "Exito") !== false);
                                    }
                                }
                            }
                        }
                    }

                }
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
            }
            elseif ($opcion == "traspasar" && isset($_POST['movimiento'])) {
                $respuesta['mensaje'] = '2';
                $data = $conexion->consulta("SELECT zonhor FROM EMPRESA where codsuc='".$sucursal."'");
                if (count($data)) {
                    // Fecha y Hora de la Sucursal
                    @date_default_timezone_set($data[0]['zonhor']);
                }
                $respuesta['mensaje'] = '3';
                $movimiento = json_decode($_POST['movimiento']);
                $respuesta['mensaje'] = $movimiento;
                $movimiento->fecmov = date('Y-m-d');
                $movimiento->hormov = date('H:i');
                $tipo = $movimiento->tipo;
                $movimiento->tipo = "M";
                $formas_pago = $movimiento->forpag;
                unset($movimiento->forpag);
                foreach ($formas_pago as $forma_pago) {
                    $movimiento->codforpag = $forma_pago->codforpag;
                    $movimiento->impmov = $forma_pago->impmov;
                    $mensaje = $conexion->insertar_desde_objeto($movimiento, "MOVCAJ");
                    $exito = (strpos($mensaje, "Exito") !== false);
                    $respuesta['exito'] = $exito;
                    $respuesta['mensaje'] = $mensaje;
                    if ($exito) {
                        // Registrar el ingreso en la otra caja si es un traspaso de saldo
                        if ($movimiento->ingegr == 'E' && $tipo == "E" && $movimiento->codcajdes != "") {
                            // Nombre de Caja
                            $nombre_caja = "";
                            $data = $conexion->consulta("SELECT nomcaj FROM CAJA WHERE codsuc='".$sucursal."' AND codcaj='".$movimiento->codcaj."'");
                            if (count($data)) {
                                $nombre_caja = $data[0]['nomcaj'];
                            }
                            $movimiento_inverso = array(
                                "codsuc" => $sucursal,
                                "codcaj" => $movimiento->codcajdes,
                                "fecmov" => date('Y-m-d'),
                                "hormov" => date('H:i'),
                                "ingegr" => "I",
                                "tipo"   => "M",
                                "codforpag" => $movimiento->codforpag,
                                "codmov" => $movimiento->codmov,
                                "codcajdes" => null,
                                "impmov" => $movimiento->impmov,
                                "obsmov" => "Traspaso de Saldo (Desde ".$nombre_caja.")"
                            );
                            $mensaje = $conexion->insertar_desde_objeto($movimiento_inverso, "MOVCAJ");
                            $exito = (strpos($mensaje, "Exito") !== false);
                            $respuesta['exito'] = $exito;
                            $respuesta['mensaje'] = $mensaje;
                        }
                    }
                }
            }
        }
    }
    echo json_encode($respuesta, true);
}
catch (Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
