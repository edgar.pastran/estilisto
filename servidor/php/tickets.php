<?php //
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array( 'exito' => false);
    if (isset($_POST['sucursal'])) {
        $codigo_sucursal = $_POST['sucursal'];
        if (isset($_POST['operacion'])) {
            require_once("config/Config.php");
            $conexion = new Conexion();

            $operacion = $_POST['operacion'];
            if ($operacion == "proximo_ticket") {
                $numero_ticket = 1;
                $anno = date("Y");
                $data = $conexion->consulta("SELECT MAX(numfac) as ultima_factura FROM FACCAB WHERE codsuc='".$codigo_sucursal."' AND ejefac='".$anno."'");
                if (count($data)) {
                    $numero_ticket += intval($data[0]['ultima_factura']);
                }
                $respuesta = array('exito' => true, 'numero_ticket' => $numero_ticket);
            }
            elseif ($operacion == "dashboard_data"  && isset($_POST['fecha'])) {
                $fecha = $_POST['fecha'];
                $fecha = DateTime::createFromFormat('d/m/Y', $fecha)->format('Y-m-d');

                // Calculo de totales de sucursal en el dia
                $total_ventas = 0;
                $total_tickets = 0;
                $sql =
                    "SELECT numfac, totfac ".
                    "FROM FACCAB ".
                    "WHERE codsuc = '".$codigo_sucursal."'".
                    "AND fecfac = '".$fecha."'";
                $data = $conexion->consulta($sql);
                $total_tickets = count($data);
                for ($i=0; $i<count($data); $i++) {
                    $total_ventas += $data[$i]['totfac'];
                }

                // Calculo de totales por empleado en el dia
                $sql =
                    "SELECT EMPR.moneda, EMPL.codemp ".
                    "FROM EMPLEADOS EMPL, EMPRESA EMPR ".
                    "WHERE EMPL.codsuc = EMPR.codsuc ".
                    "AND (EMPL.obsoleto = 0 OR EMPL.obsoleto IS NULL) ".
                    "AND EMPL.vender = 1 ".
                    "AND EMPL.codsuc='".$codigo_sucursal."'";
                $empleados = $conexion->consulta($sql);
                $moneda = "";
                for ($i=0; $i<count($empleados); $i++) {
                    // Calculo de Tickets y Ventas del dia
                    // Si la empresa incluye el impuesto en sus montos
                    // se usa la columna subtot sino se usa la columna totlin
                    $columna_en_linea = "subtot";
                    if (!isset($empleados[$i]['impincventas']) || strtoupper($empleados[$i]['impincventas']) != "X") {
                        $columna_en_linea = "totlin";
                    }
                    $empleados[$i]['tickets'] = 0;
                    $empleados[$i]['ventas'] = 0;
                    $sql =
                        "SELECT DISTINCT(FC.numfac) as tickets, SUM(FL.".$columna_en_linea.") as ventas ".
                        "FROM FACLIN FL, FACCAB FC ".
                        "WHERE FL.codsuc = FC.codsuc ".
                        "AND FL.serfac = FC.serfac ".
                        "AND FL.ejefac = FC.ejefac ".
                        "AND FL.numfac = FC.numfac ".
                        "AND FC.codsuc = '".$codigo_sucursal."'".
                        "AND FC.fecfac = '".$fecha."' ".
                        "AND FL.codemp = ".$empleados[$i]['codemp']." ".
                        "GROUP BY FC.numfac";
                    $data = $conexion->consulta($sql);
                    $tickets = count($data);
                    $ventas = 0;
                    $empleados[$i]['tickets'] = number_format($tickets, 0, ',', '.');
                    for ($j=0; $j<count($data); $j++) {
                        $ventas += $data[$j]['ventas'];
                    }
                    $empleados[$i]['ventas'] = number_format($ventas, 2, ',', '.');
                    $moneda = $empleados[$i]['moneda'];
                }
                $respuesta = array(
                    'exito' => true,
                    'data_empleados'=> $empleados,
                    'total_ventas'  => ($moneda!=""?$moneda." ":"").number_format($total_ventas, 2, ',', '.'),
                    'total_tickets' => number_format($total_tickets, 0, ',', '.')
                );
            }
            elseif ($operacion == "sucursal_data") {
                $sucursal = array();
                $cliente = array();
                $empleado = array();
                $sql =
                    "SELECT e.zonhor, e.moneda, e.impincventas, e.pedcliticket, e.codaltcliente, e.permodfecha, e.pedemplinea, e.noempcrear, e.ocutotdiatic, e.segmostotdiatic, ".
                    "c.codcli, c.nomcli, c.ape1cli, c.dnicli, c.forpagcli, c.perdeuda, ".
                    "m.codemp, m.nomemp, m.ape1emp ".
                    "FROM EMPRESA e ".
                    "LEFT JOIN CLIENTES c ON (c.codsuc = e.codsuc AND c.codcli = e.clientetpv) ".
                    "LEFT JOIN EMPLEADOS m ON (m.codsuc = e.codsuc AND m.codemp = e.empleadotpv) ".
                    "WHERE e.codsuc = '".$codigo_sucursal."' ";
                $data = $conexion->consulta($sql);
                if (count($data)) {
                    $timezone = 'America/Bogota';
                    if (isset($data[0]['zonhor'])) {
                        $timezone = $data[0]['zonhor'];
                    }
                    $sucursal = array(
                        "timezone" => $timezone,
                        "moneda" => (isset($data[0]['moneda']))?$data[0]['moneda']:"",
                        "impuesto_incluido" => $data[0]['impincventas'],
                        "pedir_cliente" => $data[0]['pedcliticket'],
                        "codigo_alternativo_cliente" => $data[0]['codaltcliente'],
                        "modificar_fecha" => $data[0]['permodfecha'],
                        "empleado_linea" => $data[0]['pedemplinea'],
                        "no_empleado_crear" => $data[0]['noempcrear'],
                        "ocultar_totales_diarios" => $data[0]['ocutotdiatic'],
                        "segundos_mostrar_totales_diarios" => $data[0]['segmostotdiatic']
                    );
                    $cliente = array(
                        "codigo" => $data[0]['codcli'],
                        "dni" => $data[0]['dnicli'],
                        "fullname" => $data[0]['nomcli'].(isset($data[0]['ape1cli'])?" ".$data[0]['ape1cli']:""),
                        "forma_pago" => isset($data[0]['forpagcli'])?$data[0]['forpagcli']:"",
                        "permite_deuda" => (isset($data[0]['perdeuda']) && $data[0]['perdeuda']==1)?$data[0]['perdeuda']:0

                    );
                    $empleado = array(
                        "codigo" => $data[0]['codemp'],
                        "fullname" => $data[0]['nomemp'].(isset($data[0]['ape1emp'])?" ".$data[0]['ape1emp']:"")
                    );
                }
                $respuesta = array('exito' => true, 'sucursal' => $sucursal, 'cliente' => $cliente, 'empleado' => $empleado);
            }
            elseif ($operacion == "formas_pago") {
                $sql =
                    "SELECT codfp, des FROM FORPAG ".
                    "WHERE vender = 1 ".
                    "AND codsuc='".$codigo_sucursal."'";
                $formas_pago = $conexion->consulta($sql);
                $respuesta = array( 'exito' => true, 'formas_pago' => $formas_pago);
            }
            elseif ($operacion == "empleados") {
                $sql =
                    "SELECT codemp, nomemp, ape1emp FROM EMPLEADOS ".
                    "WHERE (obsoleto = 0 OR obsoleto IS NULL) ".
                    "AND vender = 1 ".
                    "AND codsuc='".$codigo_sucursal."'";
                $empleados = $conexion->consulta($sql);
                for ($i=0; $i<count($empleados); $i++) {
                    $empleados[$i]['fullname'] = $empleados[$i]['nomemp'].(isset($empleados[$i]['ape1emp'])?" ".$empleados[$i]['ape1emp']:"");
                }
                $respuesta = array('exito' => true, 'empleados' => $empleados);
            }
            elseif ($operacion == "cliente_codigo_alternativo" && isset($_POST['codigo_alternativo'])) {
                $codigo_alternativo_cliente = $_POST['codigo_alternativo'];
                $sql =
                    "SELECT codcli, nomcli, ape1cli, dnicli, forpagcli, perdeuda ".
                    "FROM CLIENTES ".
                    "WHERE codcli2 = '".$codigo_alternativo_cliente."'".
                    "AND codsuc IN (".
                        "SELECT codsuc ".
                        "FROM EMPRESA ".
                        "WHERE codemp IN (".
                            "SELECT codemp ".
                            "FROM EMPRESA ".
                            "WHERE codsuc = '".$codigo_sucursal."'".
                        ")".
                    ")";
                $cliente_data = $conexion->consulta($sql);
                if (count($cliente_data)) {
                    $cliente = array(
                        "codigo" => $cliente_data[0]['codcli'],
                        "dni" => $cliente_data[0]['dnicli'],
                        "fullname" => $cliente_data[0]['nomcli'].(isset($cliente_data[0]['ape1cli'])?" ".$cliente_data[0]['ape1cli']:""),
                        "forma_pago" => isset($cliente_data[0]['forpagcli'])?$cliente_data[0]['forpagcli']:"",
                        "permite_deuda" => (isset($cliente_data[0]['perdeuda']) && $cliente_data[0]['perdeuda']==1)?$cliente_data[0]['perdeuda']:0
                    );
                    $respuesta = array('exito' => true, 'cliente' => $cliente);
                }
            }
            elseif ($operacion == "guardar_ticket" && isset($_POST['ticket'])) {
                $ticket = json_decode($_POST['ticket']);
                // Conversion de la fecha y hora en la factura
                $ticket->fecfac = DateTime::createFromFormat('d/m/Y', $ticket->fecfac)->format('Y-m-d');
                $ticket->hora = date('H:i', strtotime($ticket->hora));
                $ticket->ejefac = date("Y", strtotime($ticket->fecfac));
                // Busqueda de la serie en la factura
                // y otros datos configurados en la empresa
                $impuesto_incluido_ventas = false;
                $comision_con_impuesto = false;
                $caja_ventas = null;
                $ticket->serfac = null;
                $ticket->idconcepto = null;
                $data = $conexion->consulta("SELECT zonhor, serie, idconcepto, impincventas, calcomincluido, codcajven FROM EMPRESA where codsuc='".$ticket->codsuc."'");
                if (count($data)) {
                    // Fecha y Hora de Creacion y Modifcacion
                    @date_default_timezone_set($data[0]['zonhor']);
                    $ticket->feccre = date("Y-m-d");
                    $ticket->horcre = date("H:i");
                    $ticket->fecmod = date("Y-m-d");
                    $ticket->hormod = date("H:i");
                    $ticket->serfac = $data[0]['serie'];
                    $ticket->idconcepto = $data[0]['idconcepto'];
                    if ((isset($data[0]['impincventas'])) && (strtoupper($data[0]['impincventas']) == "X")) {
                        $impuesto_incluido_ventas = true;
                    }
                    if ((isset($data[0]['calcomincluido'])) && (strtoupper($data[0]['calcomincluido']) == "X")) {
                        $comision_con_impuesto = true;
                    }
                    if ((isset($data[0]['codcajven'])) && ($data[0]['codcajven'] != null)) {
                        $caja_ventas = $data[0]['codcajven'];
                    }
                }
                // Calculo del total de lineas
                $ticket->lineas = 5 * count($ticket->lineas_array);
                // Calculo del siguiente numero de ticket
                $ticket->numfac = 1;
                $data = $conexion->consulta("SELECT MAX(numfac) as ultima_factura FROM FACCAB where codsuc='".$ticket->codsuc."' AND ejefac='".$ticket->ejefac."'");
                if (count($data)) {
                    $ticket->numfac += intval($data[0]['ultima_factura']);
                }
                // Registro del ticket
                $mensaje = $conexion->insertar_desde_objeto($ticket, "FACCAB");
                $exito = strpos($mensaje, "Exito") !== false;
                if ($exito) {
                    $id_linea = 1;
                    $total_puntos = 0;
                    foreach ($ticket->lineas_array as $linea) {
                        if ($exito) {
                            // Asignacion de los datos de la factura en la linea
                            $linea->codsuc = $ticket->codsuc;
                            $linea->serfac = $ticket->serfac;
                            $linea->ejefac = $ticket->ejefac;
                            $linea->numfac = $ticket->numfac;
                            $linea->linfac = 5 * $id_linea;
                            // Calculo del coste del articulo
                            $linea->coste = null;
                            $data = $conexion->consulta("SELECT coste FROM ARTICULOS WHERE codsuc='".$ticket->codsuc."' AND codart='".$linea->codart."'");
                            if (count($data)) {
                                $linea->coste = $data[0]['coste'];
                            }
                            // Calculo del Total de la Linea
                            $linea->totlin = $linea->subtot;
                            if (!$impuesto_incluido_ventas) {
                                $linea->totlin = $linea->subtot + $linea->impiva;
                            }
                            // Calculo de la comision
                            $linea->comision = 0;
                            $data = $conexion->consulta("SELECT comision FROM EMPART WHERE codsuc='".$ticket->codsuc."' AND codart='".$linea->codart."' AND codemp='".$linea->codemp."'");
                            if (count($data)) {
                                $linea->comision = $data[0]['comision'];
                            }
                            else {
                                $sql =
                                    "SELECT comision FROM EMPFAM ".
                                    "WHERE codsuc='".$ticket->codsuc."' ".
                                    "AND codemp='".$linea->codemp."' ".
                                    "AND codfam1 IN (".
                                    "SELECT familia1 FROM ARTICULOS ".
                                    "WHERE codsuc = '".$ticket->codsuc."' ".
                                    "AND codart='".$linea->codart."'".
                                    ")";
                                $data = $conexion->consulta($sql);
                                if (count($data)) {
                                    $linea->comision = $data[0]['comision'];
                                }
                                else {
                                    $data = $conexion->consulta("SELECT comision FROM EMPLEADOS WHERE codsuc='".$ticket->codsuc."' AND codemp='".$linea->codemp."'");
                                    if (count($data)) {
                                        $linea->comision = $data[0]['comision'];
                                    }
                                }
                            }
                            $linea->impcomision = $linea->totlin * ($linea->comision / 100);
                            if (!$comision_con_impuesto) {
                                $linea->impcomision = ($linea->totlin - $linea->impiva) * ($linea->comision / 100);
                            }
                            // Porcentaje de comision del empleado 1
                            $linea->poremps = 100;
                            // Calculo de los puntos obtenidos
                            $linea->puntos = 0;
                            $data = $conexion->consulta("SELECT puntos FROM ARTICULOS WHERE codsuc='".$ticket->codsuc."' AND codart='".$linea->codart."'");
                            if (count($data) && isset($data[0]['puntos']) && $data[0]['puntos'] >0) {
                                $linea->puntos = $data[0]['puntos'] * $linea->cant;
                            }
                            else {
                                $sql =
                                    "SELECT puntos FROM FAMILIA1 ".
                                    "WHERE codsuc='".$ticket->codsuc."' ".
                                    "AND codfam1 IN (".
                                    "SELECT familia1 FROM ARTICULOS ".
                                    "WHERE codsuc = '".$ticket->codsuc."' ".
                                    "AND codart='".$linea->codart."'".
                                    ")";
                                $data = $conexion->consulta($sql);
                                if (count($data) && isset($data[0]['puntos']) && $data[0]['puntos'] >0) {
                                    $linea->puntos = $data[0]['puntos'] * $linea->cant;
                                }
                            }
                            $total_puntos += $linea->puntos;
                            // Registro de la linea del ticket
                            $mensaje = $conexion->insertar_desde_objeto($linea, "FACLIN");
                            $exito = $exito && (strpos($mensaje, "Exito") !== false);
                            $id_linea++;
                        }
                    }
                    // Actualizar la cantidad de puntos acumulados en la factura
                    $factura_con_puntos = array(
                        "codsuc" => $ticket->codsuc,
                        "serfac" => $ticket->serfac,
                        "ejefac" => $ticket->ejefac,
                        "numfac" => $ticket->numfac,
                        "puntos" => $total_puntos
                    );
                    $campos_clave = array("codsuc", "serfac", "ejefac", "numfac");
                    $mensaje = $conexion->actualizar_desde_objeto($factura_con_puntos, $campos_clave, "FACCAB");
                    $exito = strpos($mensaje, "Exito") !== false;
                    if ($exito) {
                        // Actualizar la cantidad de puntos acumulados por el cliente
                        $puntos_acumulados = $total_puntos;
                        $data = $conexion->consulta("SELECT puntos FROM CLIENTES WHERE codsuc='".$ticket->codsuc."' AND codcli='".$ticket->codcli."'");
                        if ((count($data)) && (isset($data[0]['puntos']))) {
                            $puntos_acumulados = $data[0]['puntos'] + $total_puntos;
                        }
                        $sql =
                            "UPDATE CLIENTES SET ".
                            "puntos = ".$puntos_acumulados." ".
                            "WHERE codsuc = '".$ticket->codsuc."' ".
                            "AND codcli = '".$ticket->codcli."'";
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                    }
                    // Si existe deuda en la factura sumarla al cliente (si es posible)
                    if ($exito && $ticket->impcam <0) {
                        $data = $conexion->consulta("SELECT perdeuda FROM CLIENTES WHERE codsuc='".$ticket->codsuc."' AND codcli='".$ticket->codcli."'");
                        if (count($data)) {
                            if (isset($data[0]['perdeuda']) && ($data[0]['perdeuda'] = 1)) {
                                $sql =
                                    "UPDATE CLIENTES SET ".
                                    "deuda = (deuda + ".abs($this->impcam).") ".
                                    "WHERE codsuc = '".$ticket->codsuc."' ".
                                    "AND codcli = '".$ticket->codcli."'";
                                $mensaje = $conexion->sentencia($sql);
                                $exito = strpos($mensaje, "Exito") !== false;
                            }
                        }
                    }
                    if ($exito) {
                        if ($caja_ventas == null) {
                            // Buscar una caja de cobro de la sucursal
                            $sql =
                                "SELECT codcaj " .
                                "FROM CAJA " .
                                "WHERE codsuc = '" . $ticket->codsuc . "' " .
                                "AND tipo = 'C'";
                            $data = $conexion->consulta($sql);
                            if (count($data)) {
                                $caja_ventas = $data[0]['codcaj'];
                            }
                        }
                        if ($caja_ventas != null) {
                            // Registrar la venta en la caja
                            $movimiento_caja = array(
                                "codsuc" => $ticket->codsuc,
                                "codcaj" => $caja_ventas,
                                "fecmov" => $ticket->feccre,
                                "hormov" => $ticket->horcre,
                                "ingegr" => 'I',
                                "tipo"   => 'V',
                                "codforpag" => $ticket->forpag1,
                                "serfac" => $ticket->serfac,
                                "ejefac" => $ticket->ejefac,
                                "numfac" => $ticket->numfac,
                                "codemp" => $ticket->codemp,
                                "impmov" => $ticket->impcob1,
                                "obsmov" => $ticket->obsfac
                            );
                            $mensaje = $conexion->insertar_desde_objeto($movimiento_caja, "MOVCAJ");
                            $exito = strpos($mensaje, "Exito") !== false;
                            if ($exito) {
                                if (($ticket->impcob2 != null) && ($ticket->impcob2 > 0)) {
                                    $movimiento_caja = array(
                                        "codsuc" => $ticket->codsuc,
                                        "codcaj" => $caja_ventas,
                                        "fecmov" => $ticket->feccre,
                                        "hormov" => $ticket->horcre,
                                        "ingegr" => 'I',
                                        "tipo"   => 'V',
                                        "codforpag" => $ticket->forpag2,
                                        "serfac" => $ticket->serfac,
                                        "ejefac" => $ticket->ejefac,
                                        "numfac" => $ticket->numfac,
                                        "codemp" => $ticket->codemp,
                                        "impmov" => $ticket->impcob2,
                                        "obsmov" => $ticket->obsfac
                                    );
                                    $mensaje = $conexion->insertar_desde_objeto($movimiento_caja, "MOVCAJ");
                                    $exito = strpos($mensaje, "Exito") !== false;
                                }
                            }
                        }
                    }
                }
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
            }
        }
    }
    echo json_encode($respuesta, true);
}
catch(Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
