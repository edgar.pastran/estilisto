<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array('exito' => false);
    if (isset($_POST['sucursal'])) {
        $sucursal = $_POST['sucursal'];

        if (isset($_POST['opcion'])) {
            require_once("config/Config.php");
            $conexion = new Conexion();

            $subdirectorio_imagenes = 'cliente';
            $nombre_campo_imagen = 'imagen_nueva';

            $opcion = $_POST['opcion'];
            if ($opcion == "consulta") {
                $sql =
                    "SELECT CLI.*, EMP.codemp ".
                    "FROM CLIENTES CLI, EMPRESA EMP ".
                    "WHERE CLI.codsuc = EMP.codsuc ".
                    "AND CLI.codsuc = '$sucursal' ".
                    "ORDER BY CLI.obsoleto ASC, CLI.codcli ASC";
                $clientes = $conexion->consulta($sql);
                for ($i=0; $i<count($clientes); $i++) {
                    $codigo_grupo_empresas = $clientes[$i]['codemp'];
                    $codigo_sucursal = $clientes[$i]['codsuc'];
                    $foto = $URL_BASE_PATH.'servidor/images/nothing.png';
                    if (isset($clientes[$i]['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'./*$codigo_sucursal.'/'.*/$clientes[$i]['foto'])) {
                        $foto = $URL_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'./*$codigo_sucursal.'/'.*/$clientes[$i]['foto'];
                    }
                    $clientes[$i]['foto'] = $foto;
                }

                if (count($clientes) > 1){
                    $respuesta = array( 'exito' => true, 'clientes' => $clientes, 'nume_regis' => count($clientes));
                }
                else{
                    //Se declara que esta es una aplicacion que genera un JSON
                    $respuesta = array('exito' => false, 'sucursal' => $sucursal);
                }
            }
            else if ($opcion == "consultar") {
                $codcli	= (isset($_POST["codcli"]))?$_POST['codcli']:"";

                // SQL query to fetch information of registerd users and finds user match.
                $sql =
                    "SELECT * ".
                    "FROM CLIENTES ".
                    "WHERE codsuc = '$sucursal' ".
                    "AND codcli = '$codcli'";
                $datos = $conexion->consulta($sql);

                if (count($datos)) {
                    $row = $datos[0];
                    $respuesta = array(
                        'exito' => true, 'codsuc' => $row['codsuc'],
                        'codcli' => $row['codcli'], 'nomcli' => $row['nomcli'], 'foto' => $row['foto'],
                        'ape1cli' => $row['ape1cli'], 'tel1cli' => $row['tel1cli'],
                        'dnicli' => $row['dnicli'], 'sexo' => $row['sexo'], 'email' => $row['email'],
                        'fecnac' => $row['fecnac'], 'codcli2' => $row['codcli2'],
                        'forpagcli' => $row['forpagcli'], 'puntos' => $row['puntos'],
                        'obsoleto' => $row['obsoleto'], 'actpuntos' => $row['actpuntos']
                    );
                }
                else{
                    $respuesta = array('exito' => false, 'sucursal' => $sucursal);
                }
            }
            else if ($opcion == "actualizar") {
                $codcli	= (isset($_POST["codcli"]))?$_POST['codcli']:"";
                $nomcli	= (isset($_POST["nomcli"]))?$_POST['nomcli']:"";
                $foto   = (isset($_POST["foto"]))?$_POST['foto']:"";
                $ape1cli= (isset($_POST["ape1cli"]))?$_POST['ape1cli']:"";
                $tel1cli= (isset($_POST["tel1cli"]))?$_POST['tel1cli']:"";
                $dnicli	= (isset($_POST["dnicli"]))?$_POST['dnicli']:"";
                $sexo	= (isset($_POST["sexo"]))?$_POST['sexo']:"";
                $email	= (isset($_POST["email"]))?$_POST['email']:"";
                $codcli2= (isset($_POST["codcli2"]))?$_POST['codcli2']:"";
                if (isset($_POST["fecnac"])) {
                    $fecnac	= $_POST['fecnac'];
                    $dia = substr($fecnac,0,2);
                    $mes1 = substr($fecnac,3,2);
                    $anio = substr($fecnac,6,4);
                    $fecnac=$anio."/".$mes1."/".$dia;
                }
                else {
                    $fecnac	= "";
                }
                $codfp	  = (isset($_POST["codfp"]))?$_POST['codfp']:"";
                $obsoleto = (isset($_POST["obsoleto"]))?$_POST['obsoleto']:"";
                $actpuntos= (isset($_POST["actpuntos"]))?$_POST['actpuntos']:"";

                $sql =
                    "SELECT CLI.foto, EMP.codemp ".
                    "FROM CLIENTES CLI, EMPRESA EMP ".
                    "WHERE CLI.codsuc = EMP.codsuc ".
                    "AND CLI.codsuc = '$sucursal' ".
                    "AND CLI.codcli = '".$codcli."'";
                $cliente_data = $conexion->consulta($sql);
                if (count($cliente_data) > 0) {
                    $respuesta["exito"] = true;

                    require_once("lib/fileUploader.php");

                    $codigo_grupo_empresas = $cliente_data[0]['codemp'];
                    $foto_anterior = isset($cliente_data[0]['foto'])?$cliente_data[0]['foto']:"";
                    if (isset($_FILES[$nombre_campo_imagen]['name']) && $_FILES[$nombre_campo_imagen]['name'] != '') {
                        $directorio_imagenes = $DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'/*.$sucursal.'/'*/;
                        $respuesta = FileUploader::upload_image($nombre_campo_imagen, $directorio_imagenes, $foto_anterior, true);
                        if ($respuesta["exito"]) {
                            $foto = $respuesta["file_name"];
                        }
                    }
                    if ($respuesta["exito"]) {
                        $sql =
                            "UPDATE CLIENTES SET ".
                            "nomcli = '$nomcli', ".
                            (($foto != "")?"foto = '$foto', ":"").
                            "ape1cli = '$ape1cli', ".
                            "tel1cli = '$tel1cli', ".
                            "dnicli = '$dnicli', ".
                            "sexo = '$sexo', ".
                            "email = '$email', ".
                            "fecnac = '$fecnac', ".
                            "codcli2 = '$codcli2', ".
                            "forpagcli = '$codfp', ".
                            "obsoleto = '$obsoleto', ".
                            "actpuntos = '$actpuntos' ".
                            "WHERE codsuc = '$sucursal' ".
                            "AND codcli='$codcli'";
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                    }
                }
                else {
                    $respuesta = array('exito' => false, 'mensaje' => 'No se encontro el cliente');
                }
            }
            else if ($opcion == "insertar") {
                $nomcli	= (isset($_POST["nomcli"]))?$_POST['nomcli']:"";
                $foto   = (isset($_POST["foto"]))?$_POST['foto']:"";
                $ape1cli= (isset($_POST["ape1cli"]))?$_POST['ape1cli']:"";
                $tel1cli= (isset($_POST["tel1cli"]))?$_POST['tel1cli']:"";
                $dnicli	= (isset($_POST["dnicli"]))?$_POST['dnicli']:"";
                $sexo	= (isset($_POST["sexo"]))?$_POST['sexo']:"";
                $email	= (isset($_POST["email"]))?$_POST['email']:"";
                $codcli2= (isset($_POST["codcli2"]))?$_POST['codcli2']:"";
                if (isset($_POST["fecnac"])) {
                    $fecnac	= $_POST['fecnac'];
                    $dia = substr($fecnac,0,2);
                    $mes1 = substr($fecnac,3,2);
                    $anio = substr($fecnac,6,4);
                    $fecnac=$anio."/".$mes1."/".$dia;
                }
                else {
                    $fecnac	= "";
                }
                $codfp	  = (isset($_POST["codfp"]))?$_POST['codfp']:"";
                $actpuntos= (isset($_POST["actpuntos"]))?$_POST['actpuntos']:"";

                $sql =
                    "SELECT EMP.codemp ".
                    "FROM EMPRESA EMP ".
                    "WHERE EMP.codsuc = '$sucursal'";
                $empresa_data = $conexion->consulta($sql);
                if (count($empresa_data) > 0) {
                    require_once("lib/fileUploader.php");

                    $codigo_grupo_empresas = $empresa_data[0]['codemp'];
                    if (isset($_FILES[$nombre_campo_imagen]['name']) && $_FILES[$nombre_campo_imagen]['name'] != '') {
                        $directorio_imagenes = $DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'/*.$sucursal.'/'*/;
                        $respuesta = FileUploader::upload_image($nombre_campo_imagen, $directorio_imagenes, "", true);
                        if ($respuesta["exito"]) {
                            $foto = $respuesta["file_name"];
                        }
                    }
                }

                // Nuevo codigo para el cliente
                $codcli = 1;
                $sql =
                    "SELECT MAX(codcli) AS codcli ".
                    "FROM CLIENTES ".
                    "WHERE codcli < '9999990'";
                $data = $conexion->consulta($sql);
                if (count($data) > 0) {
                    $codcli = $data[0]['codcli'] + 1;
                }

                $sql =
                    "INSERT INTO CLIENTES ".
                    "(codsuc, codcli, nomcli, foto, ape1cli, tel1cli, dnicli, sexo, email, fecnac, codcli2, forpagcli, actpuntos) ".
                    "VALUES ".
                    "('$sucursal','$codcli','$nomcli','$foto','$ape1cli','$tel1cli','$dnicli','$sexo','$email','$fecnac','$codcli2','$codfp','$actpuntos')";
                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
            }
            else if ($opcion == "eliminar") {
                $codcli	= (isset($_POST["codcli"]))?$_POST['codcli']:"";

                $sql =
                    "SELECT CLI.*, EMP.codemp ".
                    "FROM CLIENTES CLI, EMPRESA EMP ".
                    "WHERE CLI.codsuc = EMP.codsuc ".
                    "AND CLI.codsuc = '$sucursal' ".
                    "AND CLI.codcli = '".$codcli."'";
                $clientes = $conexion->consulta($sql);
                if (count($clientes)) {
                    /******** VERIFICAR SI TIENE TICKET O FACTURA ***************/
                    $ventas = $conexion->consulta("select * from FACCAB where codsuc='$sucursal' and codcli='$codcli' LIMIT 1");
                    if(count($ventas) == 0){ /******** NO TIENE VENTAS ***************/
                        $eliminar='true';
                    }
                    else{
                        $eliminar='Tiene VENTAS';
                    }

                    if ($eliminar=='true') {
                        // Si existe un archivo asociado se borra
                        $cliente = $clientes[0];
                        $codigo_grupo_empresas = $cliente['codemp'];
                        $codigo_sucursal = $cliente['codsuc'];
                        if (isset($cliente['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'./*$codigo_sucursal.'/'.*/$cliente['foto'])) {
                            unlink($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'./*$codigo_sucursal.'/'.*/$cliente['foto']);
                        }

                        // Se elimina
                        $sql =
                            "DELETE FROM CLIENTES ".
                            "WHERE codsuc = '$sucursal' ".
                            "AND codcli = '$codcli'";
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje, 'eliminar' => $eliminar);
                    }
                    else{
                        $sql =
                            "UPDATE CLIENTES ".
                            "SET obsoleto='1' ".
                            "WHERE codsuc = '$sucursal' ".
                            "AND codcli = '$codcli'";
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje, 'eliminar' => $eliminar);
                    }
                }
            }
            else if ($opcion == "remover_imagen") {
                $codcli	= (isset($_POST["codcli"]))?$_POST['codcli']:"";

                $sql =
                    "SELECT CLI.*, EMP.codemp ".
                    "FROM CLIENTES CLI, EMPRESA EMP ".
                    "WHERE CLI.codsuc = EMP.codsuc ".
                    "AND CLI.codsuc = '$sucursal' ".
                    "AND CLI.codcli = '".$codcli."'";
                $clientes = $conexion->consulta($sql);
                if (count($clientes)) {
                    // Si existe un archivo asociado se borra
                    $cliente = $clientes[0];
                    $codigo_grupo_empresas = $cliente['codemp'];
                    $codigo_sucursal = $cliente['codsuc'];
                    $respuesta = array( 'exito' => false);
                    if (isset($cliente['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'/*.$codigo_sucursal.'/'*/.$cliente['foto'])) {
                        unlink($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'/*.$codigo_sucursal.'/'*/.$cliente['foto']);
                    }

                    $sql =
                        "UPDATE CLIENTES SET ".
                        "foto = NULL ".
                        "WHERE codsuc = '$sucursal' ".
                        "AND codcli = '$codcli'";
                    $mensaje = $conexion->sentencia($sql);
                    $exito = strpos($mensaje, "Exito") !== false;
                    $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                }
            }
        }
    }
    echo json_encode($respuesta, true);
}
catch (Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
