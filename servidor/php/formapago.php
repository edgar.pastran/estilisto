<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array('exito' => false);
    if (isset($_POST['sucursal'])) {
        $sucursal	= $_POST['sucursal'];
        $subdirectorio_imagenes = 'formapago';
        $nombre_campo_imagen = 'imagen_nueva';

        if (isset($_POST['opcion'])) {
            require_once("config/Config.php");
            $conexion = new Conexion();

            $opcion = $_POST['opcion'];
            if ($opcion == "consulta") {
                $sql =
                    "SELECT FP.*, EMP.codemp ".
                    "FROM FORPAG FP, EMPRESA EMP ".
                    "WHERE FP.codsuc = EMP.codsuc ".
                    "AND FP.codsuc = '$sucursal'";
                $formapagos = $conexion->consulta($sql);
                for ($i=0; $i<count($formapagos); $i++) {
                    $codigo_grupo_empresas = $formapagos[$i]['codemp'];
                    $codigo_sucursal = $formapagos[$i]['codsuc'];
                    $foto = $URL_BASE_PATH.'servidor/images/nothing.png';
                    if (isset($formapagos[$i]['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$formapagos[$i]['foto'])) {
                        $foto = $URL_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$formapagos[$i]['foto'];
                    }
                    $formapagos[$i]['foto'] = $foto;
                }
                if (count($formapagos)){
                    $respuesta = array('exito' => true, 'formapagos' => $formapagos, 'nume_regis' => count($formapagos));
                }
            }
            elseif ($opcion == "consultar" && isset($_POST['codfp'])) {
                $codfp	= $_POST['codfp'];

                $sql =
                    "SELECT * ".
                    "FROM FORPAG ".
                    "WHERE codsuc = '$sucursal' ".
                    "AND codfp='$codfp'";
                $datos = $conexion->consulta($sql);
                if (count($datos)) {
                    $respuesta = array('exito' => true, 'datos' => $datos);
                }
            }
            elseif ($opcion == "actualizar" && isset($_POST['codfp'])) {
                $respuesta['mensaje'] = 'No se encontro la forma de pago';

                $codfp	= $_POST['codfp'];
                $des	= isset($_POST['des'])?$_POST['des']:"";
                $serie	= isset($_POST['serie'])?$_POST['serie']:"";
                $vender	= isset($_POST['vender'])?$_POST['vender']:"";
                $foto	= "";

                $sql =
                    "SELECT FP.foto, EMP.codemp ".
                    "FROM FORPAG FP, EMPRESA EMP ".
                    "WHERE FP.codsuc = EMP.codsuc ".
                    "AND FP.codsuc = '$sucursal' ".
                    "AND FP.codfp = '".$codfp."'";
                $formapago_data = $conexion->consulta($sql);
                if (count($formapago_data) > 0) {
                    $respuesta["exito"] = true;

                    require_once("lib/fileUploader.php");

                    $codigo_grupo_empresas = $formapago_data[0]['codemp'];
                    $foto_anterior = isset($formapago_data[0]['foto'])?$formapago_data[0]['foto']:"";
                    if (isset($_FILES[$nombre_campo_imagen]['name']) && $_FILES[$nombre_campo_imagen]['name'] != '') {
                        $directorio_imagenes = $DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$sucursal.'/';
                        $respuesta = FileUploader::upload_image($nombre_campo_imagen, $directorio_imagenes, $foto_anterior, true);
                        if ($respuesta["exito"]) {
                            $foto = $respuesta["file_name"];
                        }
                    }
                    if ($respuesta["exito"]) {
                        $sql =
                            "UPDATE FORPAG SET ".
                            "des = '$des', ".
                            (($foto != "")?"foto = '$foto', ":"").
                            "serie = '$serie', ".
                            "vender = '$vender' ".
                            "WHERE codsuc = '$sucursal' ".
                            "AND codfp='$codfp'";
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                    }
                }
            }
            elseif ($opcion == "insertar" && isset($_POST['codfp'])) {
                $codfp	= $_POST['codfp'];
                $des	= isset($_POST['des'])?$_POST['des']:"";
                $serie	= isset($_POST['serie'])?$_POST['serie']:"";
                $vender	= isset($_POST['vender'])?$_POST['vender']:"";
                $foto	= "";

                $sql =
                    "SELECT EMP.codemp ".
                    "FROM EMPRESA EMP ".
                    "WHERE EMP.codsuc = '$sucursal'";
                $empresa_data = $conexion->consulta($sql);
                if (count($empresa_data) > 0) {
                    require_once("lib/fileUploader.php");

                    $codigo_grupo_empresas = $empresa_data[0]['codemp'];
                    if (isset($_FILES[$nombre_campo_imagen]['name']) && $_FILES[$nombre_campo_imagen]['name'] != '') {
                        $directorio_imagenes = $DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$sucursal.'/';
                        $respuesta = FileUploader::upload_image($nombre_campo_imagen, $directorio_imagenes, "", true);
                        if ($respuesta["exito"]) {
                            $foto = $respuesta["file_name"];
                        }
                    }
                }
                $sql =
                    "INSERT INTO FORPAG ".
                    "(codsuc, codfp, des, ".(($foto != "")?"foto, ":"")."serie, vender) ".
                    "VALUES ".
                    "('$sucursal','$codfp','$des',".(($foto != "")?"'$foto',":"")."'$serie','$vender')";
                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
            }
            elseif ($opcion == "eliminar" && isset($_POST['codfp'])) {
                $codfp	= $_POST['codfp'];

                $sql =
                    "SELECT FP.*, EMP.codemp ".
                    "FROM FORPAG FP, EMPRESA EMP ".
                    "WHERE FP.codsuc = EMP.codsuc ".
                    "AND FP.codsuc = '$sucursal' ".
                    "AND FP.codfp = '".$codfp."'";
                $formapagos = $conexion->consulta($sql);
                if (count($formapagos)) {
                    // Si existe un archivo asociado a la familia se borra
                    $formapago = $formapagos[0];
                    $codigo_grupo_empresas = $formapago['codemp'];
                    $codigo_sucursal = $formapago['codsuc'];
                    if (isset($formapago['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$formapago['foto'])) {
                        unlink($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$formapago['foto']);
                    }

                    // Se elimina la formapago
                    $sql =
                        "DELETE FROM FORPAG ".
                        "WHERE codsuc = '$sucursal' ".
                        "AND codfp = '$codfp'";
                    $mensaje = $conexion->sentencia($sql);
                    $exito = strpos($mensaje, "Exito") !== false;
                    $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                }
            }
            elseif ($opcion == "remover_imagen" && isset($_POST['codfp'])) {
                $codfp	= $_POST['codfp'];
                $sql =
                    "SELECT FP.*, EMP.codemp ".
                    "FROM FORPAG FP, EMPRESA EMP ".
                    "WHERE FP.codsuc = EMP.codsuc ".
                    "AND FP.codsuc = '$sucursal' ".
                    "AND FP.codfp = '".$codfp."'";
                $formapagos = $conexion->consulta($sql);
                if (count($formapagos)) {
                    // Si existe un archivo asociado a la familia se borra
                    $formapago = $formapagos[0];
                    $codigo_grupo_empresas = $formapago['codemp'];
                    $codigo_sucursal = $formapago['codsuc'];
                    $respuesta = array( 'exito' => false);
                    if (isset($formapago['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$formapago['foto'])) {
                        unlink($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$formapago['foto']);
                    }

                    $sql =
                        "UPDATE FORPAG SET ".
                        "foto = NULL ".
                        "WHERE codsuc = '$sucursal' ".
                        "AND codfp = '$codfp'";
                    $mensaje = $conexion->sentencia($sql);
                    $exito = strpos($mensaje, "Exito") !== false;
                    $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                }
            }
        }
    }
    echo json_encode($respuesta, true);
}
catch(Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
