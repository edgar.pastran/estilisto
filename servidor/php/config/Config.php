<?php
$protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
$hostName = $_SERVER['HTTP_HOST'];
$currentPath = $_SERVER['PHP_SELF'];
$dirPath = $_SERVER['DOCUMENT_ROOT'];
$pathInfo = pathinfo($currentPath);
$directories = explode("/", $pathInfo['dirname']);
$root_directory = $directories[1];
$isLocalVersion = (
    ($hostName == "localhost") ||
    (strpos($hostName, "10.") !== false) ||
    (strpos($hostName, "192.") !== false)
);
$DIR_BASE_PATH = $dirPath."/".(($isLocalVersion)?$root_directory."/":"");
$URL_BASE_PATH = $protocol.$hostName."/".(($isLocalVersion)?$root_directory."/":"");

class Conexion {

    private $DB_HOST  = 'localhost';
    private $DB_NAME  = 'stylistt_dev';
    private $USERNAME = 'stylistt_admin';
    private $PASSWORD = '@F~kqMK[Imx4';
    private $conexion = null;

    private function abrir_conexion() {
        $this->conexion = new mysqli($this->DB_HOST, $this->USERNAME, $this->PASSWORD, $this->DB_NAME);
        //chequear conexion
        if ($this->conexion->connect_errno) {
            die ("Connection failed: " .$this->conexion->connect_error);
        }
        $this->conexion->query("SET NAMES 'utf8'");
        return true;
    }

    private function cerrar_conexion() {
        // cerrar conexión de base de datos
        mysqli_close($this->conexion);
    }

    public function consulta($query) {
        $registros = array();
        if ($this->abrir_conexion()) {
            $resultado = $this->conexion->query($query);
            if ($resultado) {
                while($fila = $resultado->fetch_assoc()) {
                    array_push($registros, $fila);
                }
            }
            $this->cerrar_conexion();
        }
        return $registros;
    }

    public function sentencia($sentencia) {
        $respuesta = "";
        if ($this->abrir_conexion()) {
            $resultado = $this->conexion->query($sentencia);
            if ($resultado === TRUE) {
                $respuesta = "Exito: La sentencia se ejecuto satisfactoriamente";
            }
            else {
                $respuesta = "Error: Problemas al ejecutar la sentencia (".$this->conexion->error.")";
            }
            $this->cerrar_conexion();
        }
        else {
            $respuesta = "Error: Problemas al conectarse a la base de datos";
        }
        return $respuesta;
    }

    public function insertar_desde_objeto($objeto, $tabla) {
        $sql = "INSERT INTO ".$tabla;
        $campos = " (";
        $valores = " (";
        foreach($objeto as $campo => $valor) {
            if (!is_array($valor)) {
                $campos .= $campo.",";
                if ($valor == null) {
                    $valores .= "NULL,";
                }
                else if (strpos(strtoupper($valor), "NOW()") !== false) {
                    $valores .= $valor.",";
                }
                else {
                    $valores .= "'".$valor."',";
                }
            }
        }
        $campos = trim($campos, ',').")";
        $valores = trim($valores, ',').")";
        $sql .= $campos." VALUES".$valores;
        return $this->sentencia($sql);
    }

    public function actualizar_desde_objeto($objeto, $campos_clave, $tabla) {
        $sql = "UPDATE ".$tabla." SET";
        $campos = "";
        $clave  = " WHERE";
        foreach($objeto as $campo => $valor) {
            if (!is_array($valor)) {
                if (in_array($campo, $campos_clave)) {
                    $clave .= " ".$campo."='".$valor."' AND";
                }
                else if ($valor == null) {
                    $campos .= " ".$campo."=NULL,";
                }
                else if (strpos(strtoupper($valor), "NOW()") !== false) {
                    $campos .= " ".$campo."=".$valor.",";
                }
                else {
                    $campos .= " ".$campo."='".$valor."',";
                }
            }
        }
        $campos = trim($campos, ',');
        $clave = trim($clave, 'AND');
        $sql .= $campos.$clave;
        return $this->sentencia($sql);
    }

    public function eliminar_desde_objeto($objeto, $campos_clave, $tabla) {
        $sql = "DELETE FROM ".$tabla;
        $clave  = " WHERE";
        foreach($objeto as $campo => $valor) {
            if (!is_array($valor)) {
                if (in_array($campo, $campos_clave)) {
                    $clave .= " ".$campo."='".$valor."' AND";
                }
            }
        }
        $clave = trim($clave, 'AND');
        $sql .= $clave;
        return $this->sentencia($sql);
    }
}
?>