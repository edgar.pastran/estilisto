<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array( 'exito' => false, 'sucursales_usuario' => array(), 'nume_regis' => 0);
    require_once("config/Config.php");
    $conexion = new Conexion();

    if (isset($_POST['sucursal'])) {
        $sucursal = $_POST['sucursal'];
        if (isset($_POST['opcion'])) {
            $opcion = $_POST['opcion'];
            $subdirectorio_imagenes = "empresa";
            $nombre_campo_imagen = 'imagen_nueva';

            if ($opcion == "consulta") { //Consulta de una Sucursal
                $sql =
                    "SELECT * ".
                    "FROM EMPRESA ".
                    "WHERE codsuc = '$sucursal' ";
                $datos = $conexion->consulta($sql);
                if (count($datos)) {
                    $empresa = $datos[0];
                    $codigo_grupo_empresas = $empresa['codemp'];
                    $codigo_sucursal = $empresa['codsuc'];
                    $logo = $URL_BASE_PATH.'servidor/images/nothing.png';
                    if (isset($empresa['imaemp']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$empresa['imaemp'])) {
                        $logo = $URL_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$empresa['imaemp'];
                    }
                    $empresa['imaemp'] = $logo;
                    $empresa['exito'] = true;
                    $respuesta = $empresa;
                }
                else{
                    //Se declara que esta es una aplicacion que genera un JSON
                    $respuesta = array('exito' => false, 'sucursal' => $sucursal);
                }
            }
            else if ($opcion == "empresa") {
                $sql =
                    "SELECT * ".
                    "FROM EMPRESA ".
                    "WHERE codsuc = '".$sucursal."' ";
                $empresa_data = $conexion->consulta($sql);
                if (count($empresa_data) > 0) {
                    $domemp	= (isset($_POST["domemp"]))?$_POST['domemp']:"";
                    $email	= (isset($_POST["email"]))?$_POST['email']:"";
                    $fax	= (isset($_POST["fax"]))?$_POST['fax']:"";
                    $nifemp	= (isset($_POST["nifemp"]))?$_POST['nifemp']:"";
                    $pais	= (isset($_POST["pais"]))?$_POST['pais']:"";
                    $zonhor = (isset($_POST["zona_horaria"]))?$_POST['zona_horaria']:"";
                    $pobemp	= (isset($_POST["pobemp"]))?$_POST['pobemp']:"";
                    $proemp	= (isset($_POST["proemp"]))?$_POST['proemp']:"";
                    $razemp	= (isset($_POST["razemp"]))?$_POST['razemp']:"";
                    $telefono=(isset($_POST["telefono"]))?$_POST['telefono']:"";
                    $webemp = (isset($_POST["webemp"]))?$_POST['webemp']:"";
                    $imaemp = "";

                    $respuesta["exito"] = true;

                    require_once("lib/fileUploader.php");

                    $codigo_grupo_empresas = $empresa_data[0]['codemp'];
                    $logo_anterior = isset($empresa_data[0]['imaemp'])?$empresa_data[0]['imaemp']:"";
                    if (isset($_FILES[$nombre_campo_imagen]['name']) && $_FILES[$nombre_campo_imagen]['name'] != '') {
                        $directorio_imagenes = $DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$sucursal.'/';
                        $respuesta = FileUploader::upload_image($nombre_campo_imagen, $directorio_imagenes, $logo_anterior, true);
                        if ($respuesta["exito"]) {
                            $imaemp = $respuesta["file_name"];
                        }
                    }
                    if ($respuesta["exito"]) {
                        $sql =
                            "UPDATE EMPRESA SET ".
                            "domemp = '$domemp', ".
                            "email = '$email', ".
                            "fax = '$fax', ".
                            (($imaemp != "")?"imaemp = '$imaemp', ":"").
                            "nifemp = '$nifemp', ".
                            "pais = '$pais', ".
                            "zonhor = '$zonhor', ".
                            "pobemp = '$pobemp', ".
                            "proemp = '$proemp', ".
                            "razemp = '$razemp', ".
                            "telefono = '$telefono', ".
                            "webemp = '$webemp' ".
                            "WHERE codsuc ='$sucursal'";
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje, 'sucursal' => $sucursal);
                    }
                }
                else {
                    $respuesta = array('exito' => false, 'mensaje' => 'No se encontro la empresa');
                }
            }
            else if ($opcion == "remover_imagen") {
                $sql =
                    "SELECT * ".
                    "FROM EMPRESA ".
                    "WHERE codsuc = '".$sucursal."'";
                $empresa_data = $conexion->consulta($sql);
                if (count($empresa_data)) {
                    // Si existe un archivo asociado se borra
                    $empresa = $empresa_data[0];
                    $codigo_grupo_empresas = $empresa['codemp'];
                    $codigo_sucursal = $empresa['codsuc'];
                    $respuesta = array( 'exito' => false, 'sql' => $sql);
                    if (isset($empresa['imaemp']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$empresa['imaemp'])) {
                        unlink($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$empresa['imaemp']);
                    }

                    $sql =
                        "UPDATE EMPRESA SET ".
                        "imaemp = NULL ".
                        "WHERE codsuc = '$sucursal'";
                    $mensaje = $conexion->sentencia($sql);
                    $exito = strpos($mensaje, "Exito") !== false;
                    $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                }
            }
            else if ($opcion == "general") {
                // Define Variables
                if (isset($_POST["codemp"])) { $codemp	= $_POST['codemp']; };
                if (isset($_POST["serie"])) { $serie	= $_POST['serie']; };

                if ($codemp == "") { //Asignacion de Grupo Misma Sucursal si esta Vacio
                    $codemp = $sucursal;
                };

                if (isset($_POST["impuesto1"])) { $impuesto1	= $_POST['impuesto1'];};
                if (isset($_POST["re1"])) { $re1	= $_POST['re1'];};
                if (isset($_POST["impuesto2"])) { $impuesto2	= $_POST['impuesto2'];};
                if (isset($_POST["re2"])) { $re2	= $_POST['re2'];};
                if (isset($_POST["moneda"])) { $moneda	= $_POST['moneda'];};

                if (isset($_POST["actarqueo"])) { $actarqueo	= $_POST['actarqueo'];};
                if (isset($_POST["actseguridad"])) { $actseguridad	= $_POST['actseguridad'];};
                if (isset($_POST["actlogininac"])) { $actlogininac	= $_POST['actlogininac'];};
                if (isset($_POST["segundos"])) { $segundos	= $_POST['segundos'];};

                $sql =
                    "UPDATE EMPRESA SET ".
                    "impuesto1='$impuesto1', ".
                    "re1='$re1', ".
                    "impuesto2='$impuesto2', ".
                    "re2='$re2', ".
                    "moneda='$moneda', ".
                    "actarqueo='$actarqueo', ".
                    "actseguridad='$actseguridad', ".
                    "actlogininac='$actlogininac', ".
                    "segundos='$segundos', ".
                    "codemp='$codemp', ".
                    "serie='$serie' ".
                    "WHERE codsuc='$sucursal'";

                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'sucursal' => $sucursal);
            }
            else if ($opcion == "ventas") {
                // Define Variables
                $clientetpv	= (isset($_POST["clientetpv"]))?$_POST['clientetpv']:"";
                $empleadotpv = (isset($_POST["empleadotpv"]))?$_POST['empleadotpv']:'';
                $codfp = (isset($_POST["codfp"]))?$_POST['codfp']:"";
                $idconcepto = (isset($_POST["idconcepto"]))?$_POST['idconcepto']:"";
                $codcajven = (isset($_POST["codcajven"]) && $_POST["codcajven"]!="")?$_POST['codcajven']:"NULL";
                $impincventas = (isset($_POST["impincventas"]))?$_POST['impincventas']:"";
                $calcomincluido	= (isset($_POST["calcomincluido"]))?$_POST['calcomincluido']:"";
                $pedcliticket = (isset($_POST["pedcliticket"]))?$_POST['pedcliticket']:"";
                $codaltcliente = (isset($_POST["codaltcliente"]))?$_POST['codaltcliente']:"";
                $permodfecha = (isset($_POST["permodfecha"]))?$_POST['permodfecha']:"";
                $pedemplinea = (isset($_POST["pedemplinea"]))?$_POST['pedemplinea']:"";
                $noempcrear	= (isset($_POST["noempcrear"]))?$_POST['noempcrear']:"";
                $ocutotdiatic = (isset($_POST["ocutotdiatic"]))?$_POST['ocutotdiatic']:"";
                $segmostotdiatic = (isset($_POST["segmostotdiatic"]))?$_POST['segmostotdiatic']:"";
                $nomodcobrados = (isset($_POST["nomodcobrados"]))?$_POST['nomodcobrados']:"";

                // SQL query to fetch information of registerd users and finds user match.
                $sql =
                    "UPDATE EMPRESA SET ".
                    "clientetpv='$clientetpv', ".
                    "empleadotpv='$empleadotpv', ".
                    "codfp='$codfp', ".
                    "idconcepto='$idconcepto', ".
                    "impincventas='$impincventas', ".
                    "codcajven = $codcajven, ".
                    "calcomincluido='$calcomincluido', ".
                    "pedcliticket='$pedcliticket', ".
                    "codaltcliente='$codaltcliente', ".
                    "permodfecha='$permodfecha', ".
                    "pedemplinea='$pedemplinea', ".
                    "noempcrear='$noempcrear', ".
                    "ocutotdiatic='$ocutotdiatic', ".
                    "segmostotdiatic='$segmostotdiatic', ".
                    "nomodcobrados='$nomodcobrados' ".
                    "WHERE codsuc='$sucursal'";

                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'sucursal' => $sucursal);
            }
            else if ($opcion == "compras") {
                // Define Variables
                $proveedorcompras = (isset($_POST['proveedorcompras']))?"'".$_POST['proveedorcompras']."'":"null";
                $idconceptocompras = (isset($_POST['idconceptocompras']))?"'".$_POST['idconceptocompras']."'":"null";
                $impinccompras = (isset($_POST['impinccompras']))?$_POST['impinccompras']:"";
                $codcajcom = (isset($_POST["codcajcom"]) && $_POST["codcajcom"]!="")?$_POST['codcajcom']:"NULL";

                // SQL query to fetch information of registerd users and finds user match.
                $sql =
                    "UPDATE EMPRESA SET ".
                    "idconceptocompras=".$idconceptocompras.", ".
                    "proveedorcompras=".$proveedorcompras.", ".
                    "impinccompras='".$impinccompras."', ".
                    "codcajcom = $codcajcom ".
                    "WHERE codsuc='$sucursal'";

                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'sucursal' => $sucursal);
            }
            else if ($opcion == "consulta_sucursales_usuario") { //Consulta Varias Sucursales Para Actualizar por un Usuario
                $sql =
                    "SELECT * ".
                    "FROM EMPRESA ".
                    "WHERE codsuc <> ''";
                $empresas = $conexion->consulta($sql);

                if (count($empresas) > 1) {
                    $respuesta = array( 'exito' => true, 'empresas' => $empresas, 'nume_regis' => count($empresas));
                }
                else{
                    $respuesta = array('exito' => false, 'sucursal' => $sucursal);
                }
            }
            else if ($opcion == "consultar_sucursal") { //Consulta de una Sucursal
                $sql =
                    "SELECT * ".
                    "FROM EMPRESA ".
                    "WHERE codsuc = '$sucursal' ";
                $datos = $conexion->consulta($sql);
                if (count($datos)) {
                    $respuesta = array('exito' => true, 'sucursal' => $sucursal);
                }
                else{
                    $respuesta = array('exito' => false, 'sucursal' => $sucursal);
                }
            }
            else if ($opcion == "crear") {
                require_once("lib/fileUploader.php");

                $razemp	= (isset($_POST["razemp"]))?$_POST['razemp']:"";
                $domemp	= (isset($_POST["domemp"]))?$_POST['domemp']:"";
                $pais	= (isset($_POST["pais"]))?$_POST['pais']:"";
                $codemp = (isset($_POST["codemp"]))?$_POST['codemp']:"";
                $codsucN= (isset($_POST["codsucN"]))?$_POST['codsucN']:"";
                if ($codemp == "") { $codemp = $codsucN; };
                $imaemp = "";

                // *** CREAR EMPRESA ASIGNAR NUEVO CODIGO ***
                if ($codsucN == '0') {
                    $sql =
                        "SELECT MAX(codsuc) AS codsuc ".
                        "FROM EMPRESA ".
                        "WHERE codsuc > '100000'".
                        "AND codsuc < 'a'";
                    $data = $conexion->consulta($sql);
                    if (count($data) > 0) {
                        $codsucN = $data[0]['codsuc']+1;
                        if ($codsucN == 1) { $codsucN = 100001;}
                    };
                };

                $sql =
                    "INSERT INTO EMPRESA (codsuc, razemp, domemp, pais, codemp) ".
                    "VALUES ".
                    "('$codsucN','$razemp','$domemp','$pais','$codemp')";
                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);

                // *** Actualizar las otras Tablas ***

                // *** EMPRESA Datos de CONFIGURACION ***
                if ($exito == true) {
                    $sql =
                        "SELECT * ".
                        "FROM EMPRESA ".
                        "WHERE codsuc = '".$sucursal."'";
                    $data = $conexion->consulta($sql);
                    if (count($data) > 0) {
                        $sql =
                            "UPDATE EMPRESA SET ".
                            "serie='".$data[0]['serie']."', ".
                            "impuesto1='".$data[0]['impuesto1']."', ".
                            "re1='".$data[0]['re1']."', ".
                            "impuesto2='".$data[0]['impuesto2']."', ".
                            "re2='".$data[0]['re2']."', ".
                            "moneda='".$data[0]['moneda']."', ".
                            "actarqueo='".$data[0]['actarqueo']."', ".
                            "actseguridad='".$data[0]['actseguridad']."', ".
                            "actlogininac='".$data[0]['actlogininac']."', ".
                            "segundos='".$data[0]['segundos']."', ".
                            "clientetpv='".$data[0]['clientetpv']."', ".
                            "empleadotpv='".$data[0]['empleadotpv']."', ".
                            "codfp='".$data[0]['codfp']."', ".
                            "idconcepto='".$data[0]['idconcepto']."', ".
                            "impincventas='".$data[0]['impincventas']."', ".
                            "calcomincluido='".$data[0]['calcomincluido']."', ".
                            "pedcliticket='".$data[0]['pedcliticket']."', ".
                            "codaltcliente='".$data[0]['codaltcliente']."', ".
                            "permodfecha='".$data[0]['permodfecha']."', ".
                            "pedemplinea='".$data[0]['pedemplinea']."', ".
                            "noempcrear='".$data[0]['noempcrear']."', ".
                            "noempcrear='".$data[0]['noempcrear']."', ".
                            "ocutotdiatic='".$data[0]['ocutotdiatic']."', ".
                            "segmostotdiatic='".$data[0]['segmostotdiatic']."', ".
                            "nomodcobrados='".$data[0]['nomodcobrados']."', ".
                            "idconceptocompras='".$data[0]['idconceptocompras']."', ".
                            "proveedorcompras='".$data[0]['proveedorcompras']."', ".
                            "impinccompras='".$data[0]['impinccompras']."' ".
                            "where codsuc='$codsucN'";

                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        $respuesta = array('exito' => $exito, 'mensaje' => $sql);
                    }
                }

                // *** CONCEPTOS ***
                if ($exito == true) {
                    $sql =
                        "SELECT * ".
                        "FROM CONCEPTOS ".
                        "WHERE codsuc = '".$sucursal."'";
                    $data = $conexion->consulta($sql);
                    if (count($data) > 0) {
                        $sql =
                            "INSERT INTO CONCEPTOS (codsuc, idconcepto, desconcep) ".
                            "VALUES ";
                        $coma = "";
                        for ($i = 0; $i < count($data); $i++) {
                            $idconcepto = $data[$i]['idconcepto'];
                            $desconcep = $data[$i]['desconcep'];
                            if ($i > 0) { $coma = ","; }
                            $sql = $sql.$coma."('$codsucN','$idconcepto','$desconcep')";
                        };
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                    };
                };

                // *** FORMAS DE PAGO ***
                if ($exito == true) {
                    $sql =
                        "SELECT FP.*, EMP.codemp ".
                        "FROM FORPAG FP, EMPRESA EMP ".
                        "WHERE FP.codsuc = EMP.codsuc ".
                        "AND FP.codsuc = '".$sucursal."'";
                    $data = $conexion->consulta($sql);
                    if (count($data) > 0) {
                        $sql =
                            "INSERT INTO FORPAG (codsuc, codfp, des, vender, foto) ".
                            "VALUES ";
                        $coma = "";
                        for ($i = 0; $i < count($data); $i++) {
                            $codfp = $data[$i]['codfp'];
                            $des = $data[$i]['des'];
                            $vender = $data[$i]['vender'];
                            $foto = $data[$i]['foto'];

                            if ($i > 0) { $coma = ","; }
                            $sql = $sql.$coma."('$codsucN','$codfp','$des','$vender','$foto')";

                            /*** IMAGENES DE FORMAS DE PAGO ***/
                            $codigo_grupo_empresas = $data[$i]['codemp'];
                            $subdirectorio_imagenes = 'formapago';
                            $source_image = $DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$sucursal.'/'.$foto;
                            $target_dir = $DIR_BASE_PATH.'servidor/images/'.$codemp.'/'.$subdirectorio_imagenes.'/'.$codsucN.'/';
                            FileUploader::copy_image($source_image, $target_dir);
                        };
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                    };
                };

                // *** FAMILIAS o CATEGORIAS ***
                if ($exito == true) {
                    $sql =
                        "SELECT FAM.*, EMP.codemp ".
                        "FROM FAMILIA1 FAM, EMPRESA EMP ".
                        "WHERE FAM.codsuc = EMP.codsuc ".
                        "AND FAM.codsuc = '".$sucursal."'";
                    $data = $conexion->consulta($sql);
                    if (count($data) > 0) {
                        $sql =
                            "INSERT INTO FAMILIA1 (codsuc, codfam1, desfam1, vender, foto, obsoleto, orden, puntos) ".
                            "VALUES ";
                        $coma = "";
                        for ($i = 0; $i < count($data); $i++) {
                            $codfam1 = $data[$i]['codfam1'];
                            $desfam1 = $data[$i]['desfam1'];
                            $vender = $data[$i]['vender'];
                            $foto = $data[$i]['foto'];
                            $obsoleto = $data[$i]['obsoleto'];
                            $orden = $data[$i]['orden'];
                            $puntos = $data[$i]['puntos'];

                            if ($i > 0) { $coma = ","; }
                            $sql = $sql.$coma."('$codsucN','$codfam1','$desfam1','$vender','$foto','$obsoleto','$orden','$puntos')";

                            /*** IMAGENES DE FAMILIAS ***/
                            $codigo_grupo_empresas = $data[$i]['codemp'];
                            $subdirectorio_imagenes = 'familia';
                            $source_image = $DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$sucursal.'/'.$foto;
                            $target_dir = $DIR_BASE_PATH.'servidor/images/'.$codemp.'/'.$subdirectorio_imagenes.'/'.$codsucN.'/';
                            FileUploader::copy_image($source_image, $target_dir);
                        };
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                    };
                };

                // *** ARTICULOS o SERVICIOS ***
                if ($exito == true) {
                    $sql =
                        "SELECT ART.*, EMP.codemp ".
                        "FROM ARTICULOS ART, EMPRESA EMP ".
                        "WHERE ART.codsuc = EMP.codsuc ".
                        "AND ART.codsuc = '".$sucursal."'";
                    $data = $conexion->consulta($sql);
                    if (count($data) > 0) {
                        $sql =
                            "INSERT INTO ARTICULOS (codsuc, codart, desart, familia1, foto, tipart, pvpa, ivaart, vender, comprar, obsoleto, orden, puntos) ".
                            "VALUES ";
                        $coma = "";
                        for ($i = 0; $i < count($data); $i++) {
                            $codart = $data[$i]['codart'];
                            $desart = $data[$i]['desart'];
                            $familia1 = $data[$i]['familia1'];
                            $foto = $data[$i]['foto'];
                            $tipart = $data[$i]['tipart'];
                            $pvpa = $data[$i]['pvpa'];
                            $ivaart = $data[$i]['ivaart'];
                            $vender = $data[$i]['vender'];
                            $comprar = $data[$i]['comprar'];
                            $obsoleto = $data[$i]['obsoleto'];
                            $orden = $data[$i]['orden'];
                            $puntos = $data[$i]['puntos'];

                            if ($i > 0) { $coma = ","; }
                            $sql = $sql.$coma."('$codsucN','$codart','$desart','$familia1','$foto','$tipart','$pvpa','$ivaart','$vender','$comprar','$obsoleto','$orden','$puntos')";

                            /*** IMAGENES DE ARTICULOS ***/
                            $codigo_grupo_empresas = $data[$i]['codemp'];
                            $subdirectorio_imagenes = 'articulo';
                            $source_image = $DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$sucursal.'/'.$foto;
                            $target_dir = $DIR_BASE_PATH.'servidor/images/'.$codemp.'/'.$subdirectorio_imagenes.'/'.$codsucN.'/';
                            FileUploader::copy_image($source_image, $target_dir);
                        };
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                    };
                };

                // *** CLIENTES ***
                if ($exito == true) {
                    $sql =
                        "INSERT INTO CLIENTES (codsuc, codcli, nomcli) ".
                        "VALUES ".
                        "('$codsucN','9999998','CLIENTE CONTADO (HOMBRE)')".
                        ",('$codsucN','9999999','CLIENTE CONTADO (MUJER)')";

                    $mensaje = $conexion->sentencia($sql);
                    $exito = strpos($mensaje, "Exito") !== false;
                    $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                };

                // *** EMPLEADOS ***
                if ($exito == true) {
                    $sql =
                        "INSERT INTO EMPLEADOS (codsuc, codemp, nomemp) ".
                        "VALUES ".
                        "('$codsucN','9999999','EMPLEADO TEMP.')";

                    $mensaje = $conexion->sentencia($sql);
                    $exito = strpos($mensaje, "Exito") !== false;
                    $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                };

                // *** PROVEEDOR ***
                if ($exito == true) {
                    $sql =
                        "INSERT INTO PROVEEDOR (codsuc, codpro, razon) ".
                        "VALUES ".
                        "('$codsucN','0000000','PROVEEDOR GENERICO')";

                    $mensaje = $conexion->sentencia($sql);
                    $exito = strpos($mensaje, "Exito") !== false;
                    $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                };

                // *** CAJAS ***
                if ($exito == true) {
                    $sql =
                        "SELECT * ".
                        "FROM CAJA ".
                        "WHERE codsuc = '".$sucursal."'";
                    $data = $conexion->consulta($sql);
                    if (count($data) > 0) {
                        $sql =
                            "INSERT INTO CAJA (codsuc, tipo, nomcaj, fondo) ".
                            "VALUES ";
                        $coma = "";
                        for ($i = 0; $i < count($data); $i++) {
                            $tipo = $data[$i]['tipo'];
                            $nomcaj = $data[$i]['nomcaj'];
                            $fondo = isset($data[$i]['fondo'])?$data[$i]['fondo']:'NULL';
                            if ($i > 0) { $coma = ","; }
                            $sql = $sql.$coma."('$codsucN','$tipo','$nomcaj',$fondo)";
                        };
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                    };
                };

                // *** CONFIGURACION DE MOVIMIENTOS DE CAJA ***
                if ($exito == true) {
                    $sql =
                        "SELECT * ".
                        "FROM CONMOV ".
                        "WHERE codsuc = '".$sucursal."'";
                    $data = $conexion->consulta($sql);
                    if (count($data) > 0) {
                        $sql =
                            "INSERT INTO CONMOV (codsuc, desmov, ingegr, tipo, cajas, solemp, codemp, solpro, codpro, solart, codart, obsoleto) ".
                            "VALUES ";
                        $coma = "";
                        for ($i = 0; $i < count($data); $i++) {
                            $desmov = "'".$data[$i]['desmov']."'";
                            $ingegr = "'".$data[$i]['ingegr']."'";
                            $tipo   = "'".$data[$i]['tipo']."'";
                            $cajas  = isset($data[$i]['cajas'])?"'".$data[$i]['cajas']."'":'NULL';
                            $solemp = isset($data[$i]['solemp'])?$data[$i]['solemp']:'NULL';
                            $codemp = isset($data[$i]['codemp'])?"'".$data[$i]['codemp']."'":'NULL';
                            $solpro = isset($data[$i]['solpro'])?$data[$i]['solpro']:'NULL';
                            $codpro = isset($data[$i]['codpro'])?"'".$data[$i]['codpro']."'":'NULL';
                            $solart = isset($data[$i]['solart'])?$data[$i]['solart']:'NULL';
                            $codart = isset($data[$i]['codart'])?"'".$data[$i]['codart']."'":'NULL';
                            $obsoleto = isset($data[$i]['obsoleto'])?$data[$i]['obsoleto']:'NULL';
                            if ($i > 0) { $coma = ","; }
                            $sql = $sql.$coma."('$codsucN',$desmov,$ingegr,$tipo,$cajas,$solemp,$codemp,$solpro,$codpro,$solart,$codart,$obsoleto)";
                        };
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                    };
                };

                /*** USUARIO ACCESO ***
                if ($exito == true) {
                $sql =
                "SELECT * ".
                "FROM ARTICULOS ".
                "WHERE codsuc = '".$sucursal."'";
                $data = $conexion->consulta($sql);
                if (count($data) > 0) {
                $sql =
                "INSERT INTO PROVEEDOR (codsuc, codpro, razon) ".
                "VALUES ".
                "('$codsucN','0000000','PROVEEDOR GENERICO')";

                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                };
                };*/
            }
        }
    }
    elseif (isset($_POST['opcion'])) {
        $opcion = $_POST['opcion'];
        if ($opcion == "eliminar") { //Eliminar Sucursar SI y Solo SI no tiene VENTAS O COMPRAS
            if (isset($_POST["codsucN"])) {
                $codsucN  = $_POST['codsucN'];
                $sucursal = $codsucN;
                $sql =
                    "SELECT * ".
                    "FROM FACCAB ".
                    "WHERE codsuc = '".$sucursal."'";
                $data = $conexion->consulta($sql);

                if (count($data) == 0){
                    $sql =
                        "SELECT * ".
                        "FROM FACPROC ".
                        "WHERE codsuc = '".$sucursal."'";
                    $data = $conexion->consulta($sql);
                    if (count($data) == 0){
                        $sql = "DELETE FROM EMPRESA "."WHERE codsuc = '".$sucursal."'";
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                        if ($exito == true) {
                            $respuesta = array('exito' => true, 'sucursal' => $sucursal, 'mensaje'=>'Empresa Eliminada');

                            $sql = "DELETE FROM ARTICULOS "."WHERE codsuc = '".$sucursal."'";
                            $mensaje = $conexion->sentencia($sql);
                            $exito = strpos($mensaje, "Exito") !== false;
                            if ($exito != true) {
                                $respuesta = array('exito' => false, 'sucursal' => $sucursal, 'mensaje'=>'Error en ARTICULOS');
                            }

                            $sql = "DELETE FROM CLIENTES "."WHERE codsuc = '".$sucursal."'";
                            $mensaje = $conexion->sentencia($sql);
                            $exito = strpos($mensaje, "Exito") !== false;
                            if ($exito != true) {
                                $respuesta = array('exito' => false, 'sucursal' => $sucursal, 'mensaje'=>'Error en CLIENTES');
                            }

                            $sql = "DELETE FROM CONCEPTOS "."WHERE codsuc = '".$sucursal."'";
                            $mensaje = $conexion->sentencia($sql);
                            $exito = strpos($mensaje, "Exito") !== false;
                            if ($exito != true) {
                                $respuesta = array('exito' => false, 'sucursal' => $sucursal, 'mensaje'=>'Error en CONCEPTOS');
                            }

                            $sql = "DELETE FROM EMPLEADOS "."WHERE codsuc = '".$sucursal."'";
                            $mensaje = $conexion->sentencia($sql);
                            $exito = strpos($mensaje, "Exito") !== false;
                            if ($exito != true) {
                                $respuesta = array('exito' => false, 'sucursal' => $sucursal, 'mensaje'=>'Error en EMPLEADOS');
                            }

                            $sql = "DELETE FROM FAMILIA1 "."WHERE codsuc = '".$sucursal."'";
                            $mensaje = $conexion->sentencia($sql);
                            $exito = strpos($mensaje, "Exito") !== false;
                            if ($exito != true) {
                                $respuesta = array('exito' => false, 'sucursal' => $sucursal, 'mensaje'=>'Error en FAMILIA1');
                            }

                            $sql = "DELETE FROM FORPAG "."WHERE codsuc = '".$sucursal."'";
                            $mensaje = $conexion->sentencia($sql);
                            $exito = strpos($mensaje, "Exito") !== false;
                            if ($exito != true) {
                                $respuesta = array('exito' => false, 'sucursal' => $sucursal, 'mensaje'=>'Error en FORPAG');
                            }

                            $sql = "DELETE FROM PROVEEDOR "."WHERE codsuc = '".$sucursal."'";
                            $mensaje = $conexion->sentencia($sql);
                            $exito = strpos($mensaje, "Exito") !== false;
                            if ($exito != true) {
                                $respuesta = array('exito' => false, 'sucursal' => $sucursal, 'mensaje'=>'Error en PROVEEDOR');
                            }

                            $sql = "DELETE FROM CAJA "."WHERE codsuc = '".$sucursal."'";
                            $mensaje = $conexion->sentencia($sql);
                            $exito = strpos($mensaje, "Exito") !== false;
                            if ($exito != true) {
                                $respuesta = array('exito' => false, 'sucursal' => $sucursal, 'mensaje'=>'Error en CAJA');
                            }

                            $sql = "DELETE FROM CONMOV "."WHERE codsuc = '".$sucursal."'";
                            $mensaje = $conexion->sentencia($sql);
                            $exito = strpos($mensaje, "Exito") !== false;
                            if ($exito != true) {
                                $respuesta = array('exito' => false, 'sucursal' => $sucursal, 'mensaje'=>'Error en CONMOV');
                            }
                        }
                    }
                    else{
                        $respuesta = array('exito' => false, 'sucursal' => $sucursal, 'mensaje'=>'Empresa Posee Compras');
                    }

                }
                else{
                    $respuesta = array('exito' => false, 'sucursal' => $sucursal, 'mensaje'=>'Empresa Posee Ventas');
                }
            }
        }
    }
    echo json_encode($respuesta, true);
}
catch(Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>