<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array('exito' => false);

    if (isset($_POST['opcion'])) {
        require_once("config/Config.php");
        $conexion = new Conexion();

        $opcion = $_POST['opcion'];
        if ($opcion == "consulta") {
            $sql =
				"SELECT * ".
				"FROM PANTALLAS ".
				"WHERE codpan <> ''";
            $datos = $conexion->consulta($sql);

            if (count($datos)) {
                $respuesta = array('exito' => true, 'pantallas' => $datos, 'nume_regis' => count($datos));
            }
        }
        else if ($opcion == "consultar") {
            $codpan	= (isset($_POST["codpan"]))?$_POST['codpan']:"";

            $sql =
				"SELECT * ".
				"FROM PANTALLAS ".
				"WHERE codpan = '$codpan'";
            $rows = mysql_num_rows($query);
            $datos = mysql_fetch_row($query);
            $datos = $conexion->consulta($sql);

            if (count($datos)) {
                $row = $datos[0];
                $respuesta = array('exito' => true, 'codpan' => $row['codpan'], 'despan' => $row['despan'], 'pantalla' => $row['pantalla']);
            }
            else{
                $respuesta = array('exito' => false, 'codpan' => $codpan);
            }
        }
        else if ($opcion == "usuario_pantallas") {
            $session = (isset($_POST["session"]))?$_POST['session']:"";

            $sql =
				"SELECT PANTALLAS.codpan, PANTALLAS.pantalla, PANTALLAS.despan ".
				"FROM PANTALLAS ".
				"INNER JOIN ACCESOS ON PANTALLAS.codpan = ACCESOS.codpan ".
				"INNER JOIN USUARIOS ON ACCESOS.codgru = USUARIOS.codgru ".
				"WHERE USUARIOS.username = '$session'";
            $datos = $conexion->consulta($sql);

            if (count($datos)) {
                $respuesta = array('exito' => true, 'pantallas' => $datos, 'nume_regis' => count($datos));
            }
        }
        else if ($opcion == "actualizar") {
            $codpan	= (isset($_POST["codpan"]))?$_POST['codpan']:"";
            $despan	= (isset($_POST["despan"]))?$_POST['despan']:"";
            $pantalla= (isset($_POST["pantalla"]))?$_POST['pantalla']:"";

            $sql =
				"UPDATE PANTALLAS SET ".
				"despan = '$despan', ".
				"pantalla = '$pantalla' ".
				"WHERE codpan = '$codpan'";
            $mensaje = $conexion->sentencia($sql);
            $exito = strpos($mensaje, "Exito") !== false;
            $respuesta = array('exito' => $exito, 'codpan' => $codpan);
        }
        else if ($opcion == "insertar") {
            $codpan	= (isset($_POST["codpan"]))?$_POST['codpan']:"";
            $despan	= (isset($_POST["despan"]))?$_POST['despan']:"";
            $pantalla= (isset($_POST["pantalla"]))?$_POST['pantalla']:"";

            $sql =
				"INSERT INTO PANTALLAS ".
				"(codpan, despan, pantalla) ".
				"VALUES ".
				"('$codpan','$despan','$pantalla')";
            $mensaje = $conexion->sentencia($sql);
            $exito = strpos($mensaje, "Exito") !== false;
            $respuesta = array('exito' => $exito, 'codpan' => $codpan);
        }
        else if ($opcion == "eliminar") {
            $codpan	= (isset($_POST["codpan"]))?$_POST['codpan']:"";

            $sql =
				"DELETE FROM PANTALLAS ".
				"WHERE codpan = '$codpan'";
            $mensaje = $conexion->sentencia($sql);
            $exito = strpos($mensaje, "Exito") !== false;
            $respuesta = array('exito' => $exito, 'codpan' => $codpan);
        }
    }
    echo json_encode($respuesta, true);
}
catch (Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
