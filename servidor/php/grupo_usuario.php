<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array('exito' => false);

	if (isset($_POST['opcion'])) {
		require_once("config/Config.php");
		$conexion = new Conexion();

		$opcion = $_POST['opcion'];
		if ($opcion == "consulta") {
			$sql =
				"SELECT * ".
				"FROM GRUPOS ".
				"WHERE codgru <> ''";
			$grupousuario = $conexion->consulta($sql);
			if (count($grupousuario)) {
				$respuesta = array('exito' => true, 'grupousuario' => $grupousuario, 'nume_regis' => count($grupousuario));
			}
		}
		else if ($opcion == "accesos") {
			$codgru	= (isset($_POST["codgru"]))?$_POST['codgru']:"";

			$sql =
				"SELECT * ".
				"FROM ACCESOS ".
				"WHERE codgru = '$codgru'";
			$datos = $conexion->consulta($sql);

			if (count($datos)) {
				$respuesta = array( 'exito' => true, 'accesos' => $datos, 'nume_regis' => count($datos));
			}
			else{
				$respuesta = array('exito' => false, 'codgru' => $codgru);
			}
		}
		else if ($opcion == "consultar") {
            $codgru	= (isset($_POST["codgru"]))?$_POST['codgru']:"";

			$sql =
				"SELECT * ".
				"FROM GRUPOS ".
				"WHERE codgru = '$codgru'";
			$datos = $conexion->consulta($sql);

			if (count($datos)) {
				$row = $datos[0];
				$respuesta = array('exito' => true, 'codgru' => $row['codgru'], 'desgru' => $row['desgru'],
					'ladmin' => $row['ladmin'], 'obsusu' => $row['obsusu']
				);
			}
			else{
				$respuesta = array('exito' => false, 'codgru' => $codgru);
			}
		}
		else if ($opcion == "actualizar") {
            $codgru	= (isset($_POST["codgru"]))?$_POST['codgru']:"";
            $desgru	= (isset($_POST["desgru"]))?$_POST['desgru']:"";
            $ladmin	= (isset($_POST["ladmin"]))?$_POST['ladmin']:"";
            $obsusu	= (isset($_POST["obsusu"]))?$_POST['obsusu']:"";

			$sql =
				"UPDATE GRUPOS SET ".
				"desgru = '$desgru', ".
				"ladmin = '$ladmin', ".
				"obsusu = '$obsusu' ".
				"WHERE codgru = '$codgru'";
            $mensaje = $conexion->sentencia($sql);
            $exito = strpos($mensaje, "Exito") !== false;
            $respuesta = array('exito' => $exito, 'codgru' => $codgru);
		}
		else if ($opcion == "actualizar_accesos") {
			//Eliminar los Accesos
            $seleccionados = (isset($_POST["seleccionados"]))?json_decode($_POST['seleccionados']):array();
            $codgru	= (isset($_POST["codgru"]))?$_POST['codgru']:"";

			$sql =
				"DELETE FROM ACCESOS ".
				"WHERE codgru = '$codgru'";
            $mensaje = $conexion->sentencia($sql);
            $exito = strpos($mensaje, "Exito") !== false;
            if ($exito) {
                //Insertar Nuevos Accesos
                for ($i = 0; $i < count($seleccionados); $i++) {
                    $codpan = $seleccionados[$i];
                    $sql =
						"INSERT INTO ACCESOS ".
						"(codgru, codpan) ".
						"VALUES ".
						"('$codgru','$codpan')";
                    $mensaje = $conexion->sentencia($sql);
                    $exito = strpos($mensaje, "Exito") !== false;
                }
            }
            $respuesta = array('exito' => $exito, 'codgru' => $codgru);
		}
		else if ($opcion == "eliminar_accesos") {
			//Eliminar los Accesos
            $seleccionados = (isset($_POST["seleccionados"]))?json_decode($_POST['seleccionados']):array();
            $codgru	= (isset($_POST["codgru"]))?$_POST['codgru']:"";

            $sql =
				"DELETE FROM ACCESOS ".
				"WHERE codgru = '$codgru'";
            $mensaje = $conexion->sentencia($sql);
            $exito = strpos($mensaje, "Exito") !== false;
            $respuesta = array('exito' => $exito, 'codgru' => $codgru);
		}
		else if ($opcion == "insertar") {
            $codgru	= (isset($_POST["codgru"]))?$_POST['codgru']:"";
            $desgru	= (isset($_POST["desgru"]))?$_POST['desgru']:"";
            $ladmin	= (isset($_POST["ladmin"]))?$_POST['ladmin']:"";
            $obsusu	= (isset($_POST["obsusu"]))?$_POST['obsusu']:"";

			$sql =
				"INSERT INTO GRUPOS ".
				"(codgru, desgru, ladmin, obsusu) ".
				"VALUES ".
				"('$codgru','$desgru','$ladmin','$obsusu')";
            $mensaje = $conexion->sentencia($sql);
            $exito = strpos($mensaje, "Exito") !== false;
            $respuesta = array('exito' => $exito, 'codgru' => $codgru);
		}
		else if ($opcion == "eliminar") {
            $codgru	= (isset($_POST["codgru"]))?$_POST['codgru']:"";

			$sql =
				"DELETE FROM GRUPOS ".
				"WHERE codgru = '$codgru'";
            $mensaje = $conexion->sentencia($sql);
            $exito = strpos($mensaje, "Exito") !== false;
            $respuesta = array('exito' => $exito, 'codgru' => $codgru);
		}
	}
    echo json_encode($respuesta, true);
}
catch (Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
