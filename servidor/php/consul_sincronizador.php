<?php //
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    require_once("config/Config.php");
    $conexion = new Conexion();

    $grupos = $conexion->consulta("SELECT * FROM SINCRONIZADOR ORDER BY grupo");
    echo json_encode(array( 'exito' => true, 'grupos' => $grupos, 'nume_regis' => count($grupos)));
}
catch(Exception $e) {//Controlar siempre el error.
	$data = $e->getMessage();
	echo json_encode($data);
}

?>
