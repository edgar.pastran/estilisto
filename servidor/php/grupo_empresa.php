<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array('exito' => false, 'empresas' => array(), 'nume_regis' => 0);
    if (isset($_POST['sucursal'])) {
        $sucursal = $_POST['sucursal'];
        if (isset($_POST['opcion'])) {
            require_once("config/Config.php");
            $conexion = new Conexion();

            $opcion = $_POST['opcion'];
            if ($opcion == "consulta") {
                $sql = "select * from GRUPO_EMPRESA where codemp<>''";
                $empresas = $conexion->consulta($sql);

                if (count($empresas) > 0) {
                    $respuesta = array('exito' => true, 'empresas' => $empresas, 'nume_regis' => count($empresas));
                }
            }
            else if ($opcion == "consultar") {
                $codemp = (isset($_POST["codemp"]))?$_POST['codemp']:"";

                $sql = "select * from GRUPO_EMPRESA where codemp='$codemp'";
                $datos = $conexion->consulta($sql);
                if (count($datos) > 0) {
                    $row = $datos[0];
                    $respuesta = array('exito' => true, 'codemp' => $row['codemp'], 'nomgrupo' => $row['nomgrupo']);
                }
            }
            else if ($opcion == "actualizar") {
                $codemp = (isset($_POST["codemp"]))?$_POST['codemp']:"";
                $nomgrupo = (isset($_POST["nomgrupo"]))?$_POST['nomgrupo']:"";

                $sql =
					"UPDATE GRUPO_EMPRESA SET ".
					"nomgrupo='$nomgrupo' ".
					"WHERE codemp='$codemp'";

                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'codemp' => $codemp);
            }
            else if ($opcion == "insertar") {
                $codemp = (isset($_POST["codemp"]))?$_POST['codemp']:"";
                $nomgrupo = (isset($_POST["nomgrupo"]))?$_POST['nomgrupo']:"";

                $sql = "insert into GRUPO_EMPRESA (codemp, nomgrupo) values ('$codemp','$nomgrupo')";

                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'codemp' => $codemp);
            }
            else if ($opcion == "eliminar") {
                $codemp = (isset($_POST["codemp"]))?$_POST['codemp']:"";

                $sql = "delete from GRUPO_EMPRESA where codemp='$codemp'";

                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'codemp' => $codemp);
            }
        }
    }
    echo json_encode($respuesta, true);
}
catch(Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
