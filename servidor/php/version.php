<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array('exito' => false);
    
        //$opcion = $_POST['opcion'];

        require_once("config/Config.php");
        $conexion = new Conexion();

        //if ($opcion == "consultar") {
            $sql =
                "SELECT * 
            FROM VERSION 
            WHERE version IS NOT NULL ";
            $datos = $conexion->consulta($sql);

            if (count($datos)) {
                $row = $datos[0];
                $respuesta = array(
                    'exito' => true, 'AppVersion' => $row['version']
                );
            }
            else{
                $respuesta = array('exito' => false, 'AppVersion' => '0');
            }
        //}
        
    
    echo json_encode($respuesta, true);
}
catch (Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
