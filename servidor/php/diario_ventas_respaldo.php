<?php //Prueba de Coneccion y Formulario
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");
//sleep(2);
try {//Controlar siempre el error
	$fecha_ini= '';
	$fecha_fin= '';

	$fecha_desde= '';
	$fecha_hasta= '';

	$ejercicio= '';
	$sucursal= '';

	if (empty($_POST['sucursal']) || empty($_POST['fecha_ini']) || empty($_POST['fecha_fin'])) {	
		if (empty($_POST['sucursal']) ) {throw new Exception('Sucursal Missing.');}
		if (empty($_POST['fecha_ini'])) {throw new Exception('fecha_ini Missing.');}
		if (empty($_POST['fecha_fin'])) { throw new Exception('fecha_fin Missing.');}
	}else
	{
		$fecha_ini=$_POST['fecha_ini'];
		$fecha_fin=$_POST['fecha_fin'];
		$sucursal=$_POST['sucursal'];

		$dia = substr($fecha_ini,0,2);
		$mes1 = substr($fecha_ini,3,2);
		$anio = substr($fecha_ini,6,4);
		$fecha_desde=$anio."/".$mes1."/".$dia;

		$dia = substr($fecha_fin,0,2);
		$mes1 = substr($fecha_fin,3,2);
		$anio = substr($fecha_fin,6,4);
		$fecha_hasta=$anio."/".$mes1."/".$dia;

		$ejercicio = stripslashes($ejercicio);
		$sucursal = stripslashes($sucursal);


		//------DECLARACION DE VARIABLES--------
		$tot_ventas    = array();
		$tot_fecha     = array();
		$tot_tikets    = array();

		$totalcom  = 0;
		$totalven  = 0;
		$totalben  = 0;
		$totaltik  = 0;

		$promecom  = 0;
		$promeven  = 0;
		$promeben  = 0;
		$prometik  = 0;

		include('config.php');
		//------------ BUSQUEDA DE LAS VENTAS ------------ 
		$sql = "SELECT * FROM FACCAB WHERE codsuc = '$sucursal' and fecfac BETWEEN '$fecha_desde' and '$fecha_hasta' ORDER BY codsuc DESC, ejefac DESC, fecfac DESC";
		$result = mysql_query($sql);
		$nume_regis=mysql_num_rows($result);

		//--declaracion de Variables para el diario de ventas--
		$contador=-1;
		$entradas =-1;
		$ventas_d=0;
		$tickets_d=0;
		$fecha_d=' ';
		$sucursal_d=' ';
		$ejercicio_d=' ';

		$reg1=0;
		for ($offset=$reg1; $offset<$nume_regis; $offset++) {
			mysql_data_seek($result, $offset);
			$row=mysql_fetch_array($result);


			if (($sucursal_d <> $row['codsuc']) || ($ejercicio_d <> $row['ejefac'])  || ($fecha_d <> $row['fecfac'])){
				$contador = $contador+1;
				$sucursal_d = $row['codsuc'];
				$ejercicio_d = $row['ejefac'];
				$fecha_d = $row['fecfac'];
				$tot_tikets[$contador] = 0;
			}else{

			}

			if ( $entradas == $contador) {
				$tot_ventas[$contador] = $tot_ventas[$contador] + $row['totfac'];
				$tot_tikets[$contador] = $tot_tikets[$contador]+1;
				$tot_fecha[$contador]  = $row['fecfac'];
			}else{
				$tot_ventas[$contador] = $row['totfac'];
				$tot_tikets[$contador] = 1;
				$entradas = $entradas+1;
				$tot_fecha[$contador]  = $row['fecfac'];
			}

			$totalven  = $totalven + $row['totfac'];
			$totaltik  = $totaltik + 1;

		}

		//------Calculo de los promedios---------
		$nume_regis = count($tot_fecha);
		if ($totalven > 0){
			$promeven = $totalven / $nume_regis;
		}
		if ($totaltik > 0){
			$prometik = $totaltik / $nume_regis;
		}

		//Dar Formato a Numeros
		
		$nume_regan = count($tot_ventas);
        $reg1=0;
        $offset=$reg1;
        for ($offset=$reg1; $offset<$nume_regan; $offset++) {
            $tot_ventas[$offset] = number_format($tot_ventas[$offset],2, '.', ',');
        };
        $totalven = number_format($totalven,2, '.', ',');
        $promeven = number_format($promeven,2, '.', ',');
        $prometik = number_format($prometik,2, '.', ',');


		//Se declara que esta es una aplicacion que genera un JSON
		echo json_encode(array( 'exito' => true, 'nume_regis' => $nume_regis, 'sucursal' => $sucursal, 'fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin,
			'tot_ventas' => $tot_ventas, 'tot_tikets' => $tot_tikets, 'tot_fecha' => $tot_fecha, 'totalven' => $totalven, 'promeven' => $promeven,
			'totaltik' => $totaltik, 'prometik' => $prometik
			));
		}

		mysql_close($connection); // Closing Connection

} catch(Exception $e) {//Controlar siempre el error.
	$data = $e->getMessage();
	echo json_encode($data);
}

?>
