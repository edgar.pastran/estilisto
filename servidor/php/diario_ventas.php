<?php //Prueba de Coneccion y Formulario
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
	$fecha_ini= '';
	$fecha_fin= '';

	$fecha_desde= '';
	$fecha_hasta= '';

	$ejercicio= '';
	$sucursal= '';

	if (empty($_POST['sucursal']) || empty($_POST['fecha_ini']) || empty($_POST['fecha_fin'])) {	
		if (empty($_POST['sucursal']) ) {throw new Exception('Sucursal Missing.');}
		if (empty($_POST['fecha_ini'])) {throw new Exception('fecha_ini Missing.');}
		if (empty($_POST['fecha_fin'])) { throw new Exception('fecha_fin Missing.');}
	}else{
		$fecha_ini=$_POST['fecha_ini'];
		$fecha_fin=$_POST['fecha_fin'];
		$sucursal="(".$_POST['sucursal'].")";

		$dia = substr($fecha_ini,0,2);
		$mes1 = substr($fecha_ini,3,2);
		$anio = substr($fecha_ini,6,4);
		$fecha_desde=$anio."/".$mes1."/".$dia;

		$dia = substr($fecha_fin,0,2);
		$mes1 = substr($fecha_fin,3,2);
		$anio = substr($fecha_fin,6,4);
		$fecha_hasta=$anio."/".$mes1."/".$dia;

		$ejercicio = stripslashes($ejercicio);
		


		//------DECLARACION DE VARIABLES--------
		$tot_ventas    = array();
		$tot_fecha     = array();
		$tot_tikets    = array();
		$ventas    = array();

		$totalcom  = 0;
		$totalven  = 0;
		$totalben  = 0;
		$totaltik  = 0;
		$totalpromeven  = 0;

		$promecom  = 0;
		$promeven  = 0;
		$promeben  = 0;
		$prometik  = 0;
		$prometikven  = 0;

		//------ VARIABLES CIERRE DE CAJA--------
		$tot_efectivo  = array();
		$tot_tarjeta   = array();
		$tot_gastos    = array();
		$tot_horcie    = array();

		$totalefe  = 0;
		$totaltar  = 0;
		$totalgas  = 0;

		$promeefe  = 0;
		$prometar  = 0;
		$promegas  = 0;

		//------

		require_once("config/Config.php");
        $conexion = new Conexion();
		//------------ BUSQUEDA DE LAS VENTAS ------------ 
		$sql = "SELECT ejefac, fecfac, codsuc, count(*) AS tot_tikets, sum(totfac) AS tot_ventas, avg(totfac) AS tot_promeven FROM FACCAB WHERE codsuc IN $sucursal and fecfac BETWEEN '$fecha_desde' and '$fecha_hasta' GROUP BY fecfac DESC";
        $datos = $conexion->consulta($sql);

        $nume_regis = count($datos);
        if (count($datos) > 0){
        	//------ Calculo de Totales Generales ----
        	for ($i=0; $i<count($datos); $i++) {
	            $totalven  = $totalven + $datos[$i]['tot_ventas'];
				$totaltik  = $totaltik + $datos[$i]['tot_tikets'];
				$totalpromeven  = $totalpromeven + $datos[$i]['tot_promeven'];

				$datos[$i]['tot_ventas'] = number_format($datos[$i]['tot_ventas'],2, '.', ',');
				$datos[$i]['tot_promeven'] = number_format($datos[$i]['tot_promeven'],2, '.', ',');
				//------------ Modificacion para CIEERE DE CAJA -------
				$tot_efectivo[$i] = 0;
				$tot_tarjeta[$i] = 0;
				$tot_gastos[$i] = 0;
				$tot_horcie[$i] = ' ';
	        }

	        //------------ BUSQUEDA DE CIERRE DE CAJA ------------ 
		
			// $sql2 = "SELECT CIECAB.numcie, CIECAB.feccie, CIECAB.horcie, CIECAB.obscie, CIELIN.codfp, CIELIN.des, CIELIN.imprea FROM CIELIN INNER JOIN CIECAB ON CIELIN.codsuc = CIECAB.codsuc AND CIELIN.numcie = CIECAB.numcie
			// WHERE CIELIN.codsuc IN $sucursal and CIECAB.feccie BETWEEN '$fecha_desde' and '$fecha_hasta' order by CIECAB.feccie DESC, CIECAB.numcie ASC";
			// $result2 = mysql_query($sql2);
			// $nume_regis2=mysql_num_rows($result2);

			// $seguir =0;
			// $contador2 =-1;

	  //        //------------ Modificacion para CIEERE DE CAJA -------
	  //       for ($offset=$reg1; $offset<$nume_regan; $offset++) {
	  //           if ($nume_regis2 > 0) {
		 //            $seguir=0;

		 //            $numcie_a = ' ';
		 //            $feccie_a = ' ';

			// 		while ($seguir <= 0 and $contador2 < $nume_regis2) {
			// 			$contador2 = $contador2 + 1;
			// 			if ($contador2 == $nume_regis2) {
			// 				//$seguir = 1;
			// 			}else{
			// 				mysql_data_seek($result2, $contador2);
			// 				$row2=mysql_fetch_array($result2);

			// 				if (($feccie_a == $row2['feccie']) AND ($numcie_a != $row2['numcie'])) {
			// 					$tot_efectivo[$offset] = 0;
			// 					$tot_tarjeta[$offset] = 0;
			// 					$tot_gastos[$offset] = 0;
			// 				}
			// 				$numcie_a = $row2['numcie'];
			// 				$feccie_a = $row2['feccie'];

			// 				if ($tot_fecha[$offset] < $row2['feccie']) {
			// 					//no Hacer Nada
			// 				}elseif ($tot_fecha[$offset] == $row2['feccie']) {
								
			// 					$tot_horcie[$offset] = $row2['horcie'];
			// 					if ($row2['codfp'] == 'EFECTIVO') {
			// 						$tot_efectivo[$offset] = $row2['imprea'];
			// 					}
			// 					if ($row2['codfp'] == 'TARJETA') {
			// 						$tot_tarjeta[$offset] = $row2['imprea'];
			// 					}
			// 					if ($row2['codfp'] == 'GASTOS') {
			// 						$tot_gastos[$offset] = $row2['imprea'];
			// 					}
			// 					//$seguir = 1; // salir del ciclo si solo si es la misma Fecha y Cambia el num Cier de la Misma Sucurcursal

			// 				}elseif ($tot_fecha[$offset] > $row2['feccie']) {
			// 					$seguir = 1; // salir del ciclo
			// 					$contador2 = $contador2 - 1;
			// 				}
			// 			}
			// 		}
			// 	}
	  //       };

        	//------Calculo de los promedios---------
			if ($totalven > 0){
				$promeven = $totalven / $nume_regis;
			}
			if ($totaltik > 0){
				$prometik = $totaltik / $nume_regis;
			}

			if ($totalefe > 0){
				$promeefe = $totalefe / $nume_regis;
			}
			if ($totaltar > 0){
				$prometar = $totaltar / $nume_regis;
			}

			if ($totalgas > 0){
				$promegas = $totalgas / $nume_regis;
			}

			if ($totalpromeven > 0){
				$prometikven = $totalven / $totaltik;
			}

			//------ Dar Formato de Numeros ---------
			$totalven = number_format($totalven,2, '.', ',');
	        $prometik = number_format($prometik,2, '.', ',');
			$promeven = number_format($promeven,2, '.', ',');
			$prometikven = number_format($prometikven,2, '.', ',');

			$promeefe = number_format($promeefe,2, '.', ',');
			$prometar = number_format($prometar,2, '.', ',');
			$promegas = number_format($promegas,2, '.', ',');

			//Se declara que esta es una aplicacion que genera un JSON
			echo json_encode(array( 'exito' => true, 'ventas' => $datos, 'nume_regis' => $nume_regis, 'sucursal' => $sucursal, 'fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin,
				'tot_ventas' => $tot_ventas, 'tot_tikets' => $tot_tikets, 'tot_fecha' => $tot_fecha, 'totalven' => $totalven, 'promeven' => $promeven,
				'totaltik' => $totaltik, 'prometik' => $prometik, 'tot_efectivo' => $tot_efectivo, 'tot_tarjeta' => $tot_tarjeta,
				'tot_gastos' => $tot_gastos, 'tot_horcie' => $tot_horcie, 'promeefe' => $promeefe, 'prometar' => $prometar, 'promegas' => $promegas, 'prometikven'=> $prometikven,
				'totalefe' => $totalefe, 'totaltar' => $totaltar, 'totalgas' => $totalgas
				));
        }
        else{
            $respuesta = array('exito' => false, 'sucursal' => $sucursal);
        }
    }

}
catch (Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>