<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

//session_start(); // Starting Session
$error=''; // Variable To Store Error Message

	include('config.php');

	// Define Variables
	$sucursales    = array();

	$codsuc	= "";
	$razemp	= "";
	$pais = "";
	

	if (isset($_POST["codsuc"])) { $codsuc	= $_POST['codsuc'];};
	if (isset($_POST["razemp"])) { $razemp	= $_POST['razemp'];};

	if (isset($_POST["pais"])) { $pais	= $_POST['pais'];};

	$opcion = $_POST['opcion'];

	if ($opcion == "consulta") {
		// SQL query to fetch information of registerd users and finds user match.
		$query = mysql_query("select * from EMPRESA where codsuc<>''", $connection);
		$nume_regis=mysql_num_rows($query);
		$datos = mysql_fetch_row($query);

		//-- Cargar el Arreglo de Empresas --
		$reg1=0;
		for ($offset=$reg1; $offset<$nume_regis; $offset++) {
			mysql_data_seek($query, $offset);
			$row=mysql_fetch_array($query);

			$sucursales[$offset]['codsuc']   = $row['codsuc'];
			$sucursales[$offset]['razemp']   = utf8_encode($row['razemp']);

			$sucursales[$offset]['pais']   = utf8_encode($row['pais']);
		};

		if($datos != null){
			$nume_regis = count($sucursales);
			// Crear el Objeto JSON
			echo json_encode(array( 'exito' => true, 'sucursales' => $sucursales, 'nume_regis' => $nume_regis ));
		}
		else{
			//Se declara que esta es una aplicacion que genera un JSON
			echo json_encode(array('exito' => false, 'codsuc' => $codsuc));
		}
	}

	if ($opcion == "consultar") {
		// SQL query to fetch information of registerd users and finds user match.
		$query = mysql_query("select * from EMPRESA where codsuc='$codsuc'", $connection);
		$rows = mysql_num_rows($query);
		$datos = mysql_fetch_row($query);

		if($datos != null){
			mysql_data_seek($query, 0);
			$row=mysql_fetch_array($query);

			echo json_encode(array('exito' => true, 'codsuc' => $row['codsuc'], 'razemp' => $row['razemp'], 'pais' => $row['pais']
			));
		}
		else{
			echo json_encode(array('exito' => false, 'codsuc' => $codsuc));
		}
	}

	if ($opcion == "actualizar") {
		// SQL query to fetch information of registerd users and finds user match.
		$query = mysql_query("update EMPRESA set razemp='$razemp', pais='$pais' where codsuc='$codsuc'", $connection);

		if(mysql_affected_rows() >= 1){
			echo json_encode(array('exito' => true, 'codsuc' => $codsuc));
		}
		else{
			$codsuc = mysqli_error($connection); 
			//Se declara que esta es una aplicacion que genera un JSON
			echo json_encode(array('exito' => false, 'codsuc' => $codsuc));
		}
	}

	if ($opcion == "insertar") {
		// SQL query to fetch information of registerd users and finds user match.
		$query = mysql_query("insert into EMPRESA (codsuc, razemp, pais) values ('$codsuc','$razemp','$pais')", $connection);

		if(mysql_affected_rows() >= 1){
			echo json_encode(array('exito' => true, 'codsuc' => $codsuc));
		}
		else{
			//Se declara que esta es una aplicacion que genera un JSON
			echo json_encode(array('exito' => false, 'codsuc' => $codsuc));
		}
	}

	if ($opcion == "eliminar") {
		// SQL query to fetch information of registerd users and finds user match.
		$query = mysql_query("delete from EMPRESA where codsuc='$codsuc'", $connection);

		if(mysql_affected_rows() >= 1){
			echo json_encode(array('exito' => true, 'codsuc' => $codsuc));
		}
		else{
			$codsuc = mysqli_error($connection); 
			//Se declara que esta es una aplicacion que genera un JSON
			echo json_encode(array('exito' => false, 'codsuc' => $codsuc));
		}
	}
	mysql_close($connection); // Closing Connection 

?>
