<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array( 'exito' => false);
    if (isset($_POST['sucursal'])) {
        $codigo_sucursal = $_POST['sucursal'];
        if (isset($_POST['operacion'])) {
            require_once("config/Config.php");
            $conexion = new Conexion();

            $operacion = $_POST['operacion'];
            if ($operacion == "proveedores") {
                $sql =
                    "SELECT codpro, razon, obsoleto ".
                    "FROM PROVEEDOR ".
                    "WHERE codsuc='".$codigo_sucursal."' ".
                    "ORDER BY codpro";
                $proveedores = $conexion->consulta($sql);
                for ($i=0; $i<count($proveedores); $i++) {
                    $proveedores[$i]["obsoleto_format"] = ((isset($proveedores[$i]["obsoleto"]) && $proveedores[$i]["obsoleto"] == "1")?"SI":"NO");
                }
                $respuesta = array('exito' => true, 'proveedores' => $proveedores);
            }
            elseif ($operacion == "procesar_cambio_codigo" && isset($_POST['datos_cambio_codigo'])) {
                $datos_cambio_codigo = json_decode($_POST['datos_cambio_codigo']);
                $codigo_anterior = $datos_cambio_codigo->codigo_anterior;
                $codigo_actual = $datos_cambio_codigo->codigo_actual;
                $eliminar_anterior = $datos_cambio_codigo->eliminar_anterior;
                // Actualizacion en las cabeceras de las facturas de venta
                $sql =
                    "UPDATE FACPROC SET ".
                    "codpro = '".$codigo_actual."' ".
                    "WHERE codsuc = '".$codigo_sucursal."' ".
                    "AND codpro = '".$codigo_anterior."'";
                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                if ($exito && $eliminar_anterior) {
                    // Eliminacion del Resgistro con Codigo Anterior
                    $sql =
                        "DELETE FROM PROVEEDOR ".
                        "WHERE codsuc = '".$codigo_sucursal."' ".
                        "AND codpro = '".$codigo_anterior."'";
                    $mensaje = $conexion->sentencia($sql);
                    $exito = strpos($mensaje, "Exito") !== false;
                }
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
            }
        }
    }
    echo json_encode($respuesta, true);
}
catch(Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
