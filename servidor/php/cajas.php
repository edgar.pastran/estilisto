<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array('exito' => false);
    if (isset($_POST['sucursal'])) {
        $sucursal	= $_POST['sucursal'];

        if (isset($_POST['opcion'])) {
            require_once("config/Config.php");
            $conexion = new Conexion();

            $opcion = $_POST['opcion'];
            if ($opcion == "consulta") {
                $sql =
                    "SELECT * ".
                    "FROM CAJA ".
                    "WHERE codsuc = '$sucursal'";
                $cajas = $conexion->consulta($sql);
                if (count($cajas)){
                    for ($i=0; $i<count($cajas); $i++) {
                        // Descripcion del Tipo
                        $cajas[$i]['tipo_string'] = "";
                        if ($cajas[$i]['tipo'] == "C") {
                            $cajas[$i]['tipo_string'] = "Caja de Cobro";
                        }
                        elseif ($cajas[$i]['tipo'] == "P") {
                            $cajas[$i]['tipo_string'] = "Caja Principal";
                        }
                        $sql =
                            "SELECT * ".
                            "FROM CAJFORPAG ".
                            "WHERE codsuc = '".$sucursal."' ".
                            "AND codcaj = '".$cajas[$i]['codcaj']."'";
                        $formas_pago = $conexion->consulta($sql);
                        $cajas[$i]['formas_pago'] = $formas_pago;
                    }
                    $respuesta = array('exito' => true, 'cajas' => $cajas, 'nume_regis' => count($cajas));
                }
            }
            elseif ($opcion == "consultar_datos") {
                $sql =
                    "SELECT codfp, des FROM FORPAG ".
                    "WHERE vender = 1 ".
                    "AND codsuc = '".$sucursal."' ".
                    "ORDER BY codfp ASC";
                $formas_pago = $conexion->consulta($sql);
                $sql =
                    "SELECT codcaj, nomcaj ".
                    "FROM CAJA ".
                    "WHERE codsuc = '$sucursal'";
                $cajas = $conexion->consulta($sql);
                $respuesta = array('exito' => true, 'formas_pago' => $formas_pago, 'cajas' => $cajas);
            }
            elseif ($opcion == "consultar" && isset($_POST['codcaj'])) {
                $codcaj	= $_POST['codcaj'];

                $sql =
                    "SELECT * ".
                    "FROM CAJA ".
                    "WHERE codsuc = '$sucursal' ".
                    "AND codcaj='$codcaj'";
                $datos = $conexion->consulta($sql);
                if (count($datos)) {
                    $respuesta = array('exito' => true, 'datos' => $datos);
                }
            }
            elseif ($opcion == "actualizar" && isset($_POST['codcaj'])) {
                $respuesta['mensaje'] = 'No se encontro la forma de pago';

                $codcaj	= $_POST['codcaj'];
                $nomcaj	= isset($_POST['nomcaj'])?$_POST['nomcaj']:"";
                $fondo	= isset($_POST['fondo'])?$_POST['fondo']:"";
                $tipo	= isset($_POST['tipo'])?$_POST['tipo']:"";
                $traautfoncie = (isset($_POST['traautfoncie']) && $_POST['traautfoncie']=="1")?$_POST['traautfoncie']:"0";
                $codcajdescie = (isset($_POST['codcajdescie']) && $_POST['codcajdescie']!="")?$_POST['codcajdescie']:"null";

                $sql =
                    "UPDATE CAJA SET ".
                    "nomcaj = '$nomcaj', ".
                    "fondo = '$fondo', ".
                    "tipo = '$tipo', ".
                    "traautfoncie = $traautfoncie, ".
                    "codcajdescie = $codcajdescie ".
                    "WHERE codsuc = '$sucursal' ".
                    "AND codcaj='$codcaj'";
                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                if ($exito && isset($_POST['formas_pago'])) {
                    $formas_pago = json_decode($_POST['formas_pago']);
                    for ($f=0; $f<count($formas_pago); $f++) {
                        if ($exito) {
                            $forma_pago = array(
                                "codsuc" => $sucursal,
                                "codcaj" => $codcaj,
                                "codfp"  => $formas_pago[$f]->codfp,
                                "fondo"  => $formas_pago[$f]->fondo
                            );
                            $sql =
                                "SELECT * ".
                                "FROM CAJFORPAG ".
                                "WHERE codsuc = '".$forma_pago["codsuc"]."' ".
                                "AND codcaj = '".$forma_pago["codcaj"]."' ".
                                "AND codfp = '".$forma_pago["codfp"]."'";
                            if (count($conexion->consulta($sql))) {
                                $campos_clave = array("codsuc", "codcaj", "codfp");
                                $mensaje = $conexion->actualizar_desde_objeto($forma_pago, $campos_clave, "CAJFORPAG");
                            }
                            else {
                                $mensaje = $conexion->insertar_desde_objeto($forma_pago, "CAJFORPAG");
                            }
                            $exito = (strpos($mensaje, "Exito") !== false);
                        }
                    }
                }
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
                    
                
            }
            elseif ($opcion == "insertar") {
                $codcaj	= $_POST['codcaj'];
                $nomcaj	= isset($_POST['nomcaj'])?$_POST['nomcaj']:"";
                $fondo	= isset($_POST['fondo'])?$_POST['fondo']:"";
                $tipo	= isset($_POST['tipo'])?$_POST['tipo']:"";
                $traautfoncie = (isset($_POST['traautfoncie']) && $_POST['traautfoncie']=="1")?$_POST['traautfoncie']:"0";
                $codcajdescie = (isset($_POST['codcajdescie']) && $_POST['codcajdescie']=="")?$_POST['codcajdescie']:"null";

                $sql =
                    "INSERT INTO CAJA ".
                    "(codsuc, nomcaj, fondo, tipo, traautfoncie, codcajdescie) ".
                    "VALUES ".
                    "('$sucursal','$nomcaj','$fondo','$tipo', $traautfoncie, $codcajdescie)";
                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                if ($exito && isset($_POST['formas_pago'])) {
                    $sql =
                        "SELECT MAX(codcaj) AS codcaj ".
                        "FROM CAJA ".
                        "WHERE codsuc = '".$sucursal."'";
                    $caja_data = $conexion->consulta($sql);
                    if (count($caja_data)){
                        $codcaj = $caja_data[0]['codcaj'];
                        $formas_pago = json_decode($_POST['formas_pago']);
                        for ($f=0; $f<count($formas_pago); $f++) {
                            if ($exito) {
                                $forma_pago = array(
                                    "codsuc" => $sucursal,
                                    "codcaj" => $codcaj,
                                    "codfp"  => $formas_pago[$f]->codfp,
                                    "fondo"  => $formas_pago[$f]->fondo
                                );
                                $mensaje = $conexion->insertar_desde_objeto($forma_pago, "CAJFORPAG");
                                $exito = (strpos($mensaje, "Exito") !== false);
                            }
                        }
                    }
                }
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
            }
            elseif ($opcion == "eliminar" && isset($_POST['codcaj'])) {
                $codcaj	= $_POST['codcaj'];
                // Se elimina todos los fondos por forma de pago de la caja
                $sql =
                    "DELETE FROM CAJFORPAG ".
                    "WHERE codsuc = '$sucursal' ".
                    "AND codcaj = '$codcaj'";
                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                if ($exito) {
                    // Se elimina la caja
                    $sql =
                        "DELETE FROM CAJA ".
                        "WHERE codsuc = '$sucursal' ".
                        "AND codcaj = '$codcaj'";
                    $mensaje = $conexion->sentencia($sql);
                    $exito = strpos($mensaje, "Exito") !== false;
                }
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
            }
        }
    }
    echo json_encode($respuesta, true);
}
catch(Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
