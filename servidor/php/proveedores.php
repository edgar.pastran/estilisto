<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array('exito' => false);
    if (isset($_POST['sucursal'])) {
        $sucursal = $_POST['sucursal'];
        $subdirectorio_imagenes = 'formapago';
        $nombre_campo_imagen = 'imagen_nueva';

        if (isset($_POST['opcion'])) {
            require_once("config/Config.php");
            $conexion = new Conexion();

            $opcion = $_POST['opcion'];
            if ($opcion == "consulta") {
                $sql =
					"SELECT * ".
					"FROM PROVEEDOR ".
					"WHERE codsuc = '$sucursal' ".
					"ORDER BY obsoleto ASC, codpro ASC";
                $proveedores = $conexion->consulta($sql);
                if (count($proveedores)) {
                    $respuesta = array('exito' => true, 'proveedores' => $proveedores, 'nume_regis' => count($proveedores));
                }
                else{
                    $respuesta = array('exito' => false, 'sucursal' => $sucursal);
                }
            }
            else if ($opcion == "consultar") {
                $codpro	= (isset($_POST["codpro"]))?$_POST['codpro']:"";

                $sql =
					"SELECT * ".
					"FROM PROVEEDOR ".
					"WHERE codsuc = '$sucursal' ".
					"AND codpro = '$codpro'";
                $datos = $conexion->consulta($sql);

                if (count($datos)) {
                    $row = $datos[0];
                    $respuesta = array(
                    	'exito' => true,
						'codsuc' => $row['codsuc'],
                        'codpro' => $row['codpro'],
                        'razon' => $row['razon'],
                        'tel2pro' => $row['tel2pro'],
                        'tel1pro' => $row['tel1pro'],
                        'nifpro' => $row['nifpro'],
                        'tippro' => $row['tippro'],
                        'mailpro' => $row['mailpro'],
                        'pais' => $row['pais'],
                        'percon' => $row['percon'],
                        'forpagpro' => $row['forpagpro'],
                        'obsoleto' => $row['obsoleto']
                    );
                }
                else{
                    $respuesta = array('exito' => false, 'sucursal' => $sucursal);
                }
            }
            else if ($opcion == "actualizar") {
                $codpro	= (isset($_POST["codpro"]))?$_POST['codpro']:"";
                $razon	= (isset($_POST["razon"]))?$_POST['razon']:"";
                $tel2pro= (isset($_POST["tel2pro"]))?$_POST['tel2pro']:"";
                $tel1pro= (isset($_POST["tel1pro"]))?$_POST['tel1pro']:"";
                $nifpro	= (isset($_POST["nifpro"]))?$_POST['nifpro']:"";
                $tippro	= (isset($_POST["tippro"]))?$_POST['tippro']:"";
                $mailpro= (isset($_POST["mailpro"]))?$_POST['mailpro']:"";
                $pais	= (isset($_POST["pais"]))?$_POST['pais']:"";
                $percon	= (isset($_POST["percon"]))?$_POST['percon']:"";
                $forpagpro	= (isset($_POST["forpagpro"]))?$_POST['forpagpro']:"";
                $obsoleto	= (isset($_POST["obsoleto"]))?$_POST['obsoleto']:"";

                $sql =
					"UPDATE PROVEEDOR SET ".
					"razon = '$razon', ".
					"tel2pro = '$tel2pro', ".
					"tel1pro = '$tel1pro', ".
					"nifpro = '$nifpro', ".
					"tippro = '$tippro', ".
					"mailpro = '$mailpro', ".
					"pais = '$pais', ".
					"percon = '$percon', ".
					"forpagpro = '$forpagpro', ".
					"obsoleto = '$obsoleto' ".
					"WHERE codsuc = '$sucursal' ".
					"AND codpro = '$codpro'";

                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'sucursal' => $sucursal, 'mensaje' => $mensaje);
            }
            else if ($opcion == "insertar") {
                $codpro	= (isset($_POST["codpro"]))?$_POST['codpro']:"";
                $razon	= (isset($_POST["razon"]))?$_POST['razon']:"";
                $tel2pro= (isset($_POST["tel2pro"]))?$_POST['tel2pro']:"";
                $tel1pro= (isset($_POST["tel1pro"]))?$_POST['tel1pro']:"";
                $nifpro	= (isset($_POST["nifpro"]))?$_POST['nifpro']:"";
                $tippro	= (isset($_POST["tippro"]))?$_POST['tippro']:"";
                $mailpro= (isset($_POST["mailpro"]))?$_POST['mailpro']:"";
                $pais	= (isset($_POST["pais"]))?$_POST['pais']:"";
                $percon	= (isset($_POST["percon"]))?$_POST['percon']:"";
                $forpagpro	= (isset($_POST["forpagpro"]))?$_POST['forpagpro']:"";

                $sql =
					"INSERT INTO PROVEEDOR ".
					"(codsuc, codpro, razon, tel2pro, tel1pro, nifpro, tippro, mailpro, pais, percon, forpagpro) ".
					"VALUES ".
					"('$sucursal','$codpro','$razon','$tel2pro','$tel1pro','$nifpro','$tippro','$mailpro','$pais','$percon','$forpagpro')";

                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                $respuesta = array('exito' => $exito, 'sucursal' => $sucursal, 'mensaje' => $mensaje);
            }
            else if ($opcion == "eliminar") {
                $codpro	= (isset($_POST["codpro"]))?$_POST['codpro']:"";
                $tippro	= (isset($_POST["tippro"]))?$_POST['tippro']:"";

                /******** VERIFICAR SI TIENE TICKET O FACTURA ***************/
                $eliminar = '';
                if ($tippro == "EMPLEADO") {
                    $sql =
						"SELECT * ".
						"FROM FACLIN ".
						"WHERE codsuc = '$sucursal' ".
						"AND codemp = '$codpro' ".
						"LIMIT 1";
                    $proveedor_data = $conexion->consulta($sql);
                    $rows = count($proveedor_data);
                }
                else{
                    $rows = 0;
                }

                if($rows == 0){ /******** NO TIENE VENTAS VALIDAR COMPRAS ***************/
                    $eliminar='Sin Ventas';
                    $sql =
						"SELECT * ".
						"FROM FACPROC ".
						"WHERE codsuc = '$sucursal' ".
						"AND codpro = '$codpro' ".
						"LIMIT 1";
                    $proveedor_data = $conexion->consulta($sql);
                    $rows2 = count($proveedor_data);

                    if($rows2 == 0){ /******** NO TIENE COMPRAS ***************/
                        $eliminar='true';
                    }
                    else{
                        $eliminar='Tiene Compras';
                    }
                }
                else{
                    $eliminar='Tiene VENTAS';
                }

                if ($eliminar=='true') {
                    $sql = "delete from EMPLEADOS where codsuc='$sucursal' and codemp='$codpro'";
                    $mensaje = $conexion->sentencia($sql);
                    $exito = strpos($mensaje, "Exito") !== false;
                    if ($exito) {
                        $sql = "delete from PROVEEDOR where codsuc='$sucursal' and codpro='$codpro'";
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
					}
                    $respuesta = array('exito' => $exito, 'sucursal' => $sucursal, 'mensaje' => $mensaje, 'eliminar' => $eliminar);
                }
                else{
                    $sql = "update EMPLEADOS set obsoleto='1' where codsuc='$sucursal' and codemp='$codpro'";
                    $mensaje = $conexion->sentencia($sql);
                    $exito = strpos($mensaje, "Exito") !== false;
                    if ($exito) {
                        $sql = "update PROVEEDOR set obsoleto='1' where codsuc='$sucursal' and codpro='$codpro'";
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
					}
                    $respuesta = array('exito' => $exito, 'sucursal' => $sucursal, 'mensaje' => $mensaje, 'eliminar' => $eliminar);
                }
            }
        }
    }
    echo json_encode($respuesta, true);
}
catch (Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
