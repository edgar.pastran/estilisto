<?php
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
    $respuesta = array( 'exito' => false);
    if (isset($_POST['sucursal'])) {
        $codigo_sucursal = $_POST['sucursal'];
        if (isset($_POST['operacion'])) {
            require_once("config/Config.php");
            $conexion = new Conexion();

            $operacion = $_POST['operacion'];
            if ($operacion == "empleados") {
                $sql =
                    "SELECT codemp, nomemp, ape1emp, dniemp, vender, obsoleto ".
                    "FROM EMPLEADOS ".
                    "WHERE codsuc='".$codigo_sucursal."' ".
                    "ORDER BY codemp";
                $empleados = $conexion->consulta($sql);
                for ($i=0; $i<count($empleados); $i++) {
                    $empleados[$i]["vender_format"] = ((isset($empleados[$i]["vender"]) && $empleados[$i]["vender"] == "1")?"SI":"NO");
                    $empleados[$i]["obsoleto_format"] = ((isset($empleados[$i]["obsoleto"]) && $empleados[$i]["obsoleto"] == "1")?"SI":"NO");
                }
                $respuesta = array('exito' => true, 'empleados' => $empleados);
            }
            elseif ($operacion == "procesar_cambio_codigo" && isset($_POST['datos_cambio_codigo'])) {
                $datos_cambio_codigo = json_decode($_POST['datos_cambio_codigo']);
                $codigo_anterior = $datos_cambio_codigo->codigo_anterior;
                $codigo_actual = $datos_cambio_codigo->codigo_actual;
                $eliminar_anterior = $datos_cambio_codigo->eliminar_anterior;
                // Actualizacion en las cabeceras de las facturas de venta
                $sql =
                    "UPDATE FACCAB SET ".
                    "codemp = '".$codigo_actual."' ".
                    "WHERE codsuc = '".$codigo_sucursal."' ".
                    "AND codemp = '".$codigo_anterior."'";
                $mensaje = $conexion->sentencia($sql);
                $exito = strpos($mensaje, "Exito") !== false;
                if ($exito) {
                    // Actualizacion en las lineas de las facturas de venta 1
                    $sql =
                        "UPDATE FACLIN SET ".
                        "codemp = '".$codigo_actual."' ".
                        "WHERE codsuc = '".$codigo_sucursal."' ".
                        "AND codemp = '".$codigo_anterior."'";
                    $mensaje = $conexion->sentencia($sql);
                    $exito = strpos($mensaje, "Exito") !== false;
                    if ($exito) {
                        // Actualizacion en las lineas de las facturas de venta 2
                        $sql =
                            "UPDATE FACLIN SET ".
                            "codemp2 = '".$codigo_actual."' ".
                            "WHERE codsuc = '".$codigo_sucursal."' ".
                            "AND codemp2 = '".$codigo_anterior."'";
                        $mensaje = $conexion->sentencia($sql);
                        $exito = strpos($mensaje, "Exito") !== false;
                        if ($exito) {
                            // Eliminacion de las comisiones Viejas del Empleado por Familia
                            $sql =
                                "DELETE FROM EMPFAM ".
                                "WHERE codsuc = '".$codigo_sucursal."' ".
                                "AND codemp = '".$codigo_actual."'";
                            $mensaje = $conexion->sentencia($sql);
                            $exito = strpos($mensaje, "Exito") !== false;
                            if ($exito) {
                                // Actualizacion en las comisiones del Empleado por Familia
                                $sql =
                                    "UPDATE EMPFAM SET " .
                                    "codemp = '" . $codigo_actual . "' " .
                                    "WHERE codsuc = '" . $codigo_sucursal . "' " .
                                    "AND codemp = '" . $codigo_anterior . "'";
                                $mensaje = $conexion->sentencia($sql);
                                $exito = strpos($mensaje, "Exito") !== false;
                                if ($exito) {
                                    // Eliminacion de las comisiones Viejas del Empleado por Articulo
                                    $sql =
                                        "DELETE FROM EMPART ".
                                        "WHERE codsuc = '".$codigo_sucursal."' ".
                                        "AND codemp = '".$codigo_actual."'";
                                    $mensaje = $conexion->sentencia($sql);
                                    $exito = strpos($mensaje, "Exito") !== false;
                                    if ($exito) {
                                        // Actualizacion en las comisiones del Empleado por Articulo
                                        $sql =
                                            "UPDATE EMPART SET " .
                                            "codemp = '" . $codigo_actual . "' " .
                                            "WHERE codsuc = '" . $codigo_sucursal . "' " .
                                            "AND codemp = '" . $codigo_anterior . "'";
                                        $mensaje = $conexion->sentencia($sql);
                                        $exito = strpos($mensaje, "Exito") !== false;
                                        // Eliminacion del Resgistro con Codigo Anterior
                                        if ($exito && $eliminar_anterior) {
                                            $sql =
                                                "DELETE FROM EMPLEADOS " .
                                                "WHERE codsuc = '" . $codigo_sucursal . "' " .
                                                "AND codemp = '" . $codigo_anterior . "'";
                                            $mensaje = $conexion->sentencia($sql);
                                            $exito = strpos($mensaje, "Exito") !== false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $respuesta = array('exito' => $exito, 'mensaje' => $mensaje);
            }
        }
    }
    echo json_encode($respuesta, true);
}
catch(Exception $e) {//Controlar siempre el error.
    $data = $e->getMessage();
    echo json_encode($data, true);
}
?>
