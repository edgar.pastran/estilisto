<?php //Prueba de Coneccion y Formulario
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

try {//Controlar siempre el error
	$fecha_ini= '';
	$fecha_fin= '';

	$fecha_desde= '';
	$fecha_hasta= '';

	$ejercicio= '';
	$sucursal= '';

	if (empty($_POST['sucursal']) || empty($_POST['eje_anterior']) || empty($_POST['eje_actual'])) {	
		if (empty($_POST['sucursal']) ) {throw new Exception('Sucursal Missing.');}
		if (empty($_POST['eje_anterior'])) {throw new Exception('eje_anterior Missing.');}
		if (empty($_POST['eje_actual'])) { throw new Exception('eje_actual Missing.');}
	}else{
		$eje_anterior=$_POST['eje_anterior'];
		$eje_actual=$_POST['eje_actual'];
		$sucursal="(".$_POST['sucursal'].")";

		$ejercicio = stripslashes($ejercicio);

		//------DECLARACION DE VARIABLES--------
		$tot_tik_mes    = array();
		$tot_tik_mes[0]    = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$tot_tik_mes[1]    = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$tot_ven_mes[0]	   = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$tot_ven_mes[1]	   = array(0,0,0,0,0,0,0,0,0,0,0,0);

		$tot_ven_tik_avg[0] = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$tot_ven_tik_avg[1] = array(0,0,0,0,0,0,0,0,0,0,0,0);
		
		$tot_tik_dia_avg[0] = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$tot_tik_dia_avg[1] = array(0,0,0,0,0,0,0,0,0,0,0,0);

		$tot_tik_mes[5][0] 	= $eje_anterior;
        $tot_tik_dia_avg[5][0] = $eje_anterior;
        $tot_ven_mes[5][0] = $eje_anterior;
        $tot_ven_tik_avg[5][0] = $eje_anterior;

        $tot_tik_mes[5][1] 	= $eje_actual;
        $tot_tik_dia_avg[5][1] = $eje_actual;
        $tot_ven_mes[5][1] = $eje_actual;
        $tot_ven_tik_avg[5][1] = $eje_actual;

        $tot_total_dias[0]    = array(0,0,0,0,0,0,0,0,0,0,0,0);
		$tot_total_dias[1]    = array(0,0,0,0,0,0,0,0,0,0,0,0);

		$ventas_actual    = array();

		//------
		require_once("config/Config.php");
		$conexion = new Conexion();

		//------------ EJECICIO ANTERIOR ------------ //
		$fecha_desde=$eje_anterior."/01/01";
		$fecha_hasta=$eje_anterior."/12/31";
		

		$sql = "SELECT ejefac, fecfac, codsuc, count(*) AS tot_tikets, sum(totfac) AS tot_ventas, avg(totfac) AS tot_promeven FROM FACCAB WHERE codsuc IN $sucursal and fecfac BETWEEN '$fecha_desde' and '$fecha_hasta' GROUP BY fecfac ASC";
		$datos = $conexion->consulta($sql);

		$nume_regis = count($datos);
		if (count($datos) > 0){
        	//------ Calculo de Totales Generales ----
			for ($i=0; $i<count($datos); $i++) {
        		$mes = substr($datos[$i]['fecfac'],5,2) - 1; //Obtener el Mes de la Fecha
        		
        		$tot_total_dias[0][$mes] = $tot_total_dias[0][$mes] + 1; //Total Dias Año Anterior

        		$tot_tik_mes[0][$mes] = $tot_tik_mes[0][$mes] + $datos[$i]['tot_tikets'];
        		$tot_tik_dia_avg[0][$mes] = $tot_tik_dia_avg[0][$mes] + 1; // Primero el total de dias del Mes
        		$tot_ven_mes[0][$mes] = $tot_ven_mes[0][$mes] + $datos[$i]['tot_ventas'];
        		$tot_ven_tik_avg[0][$mes] = 0; // Primero el total de dias del Mes
        	}
        	//------Calculo de los promedios---------
        	for ($mes=0; $mes<count($tot_tik_mes[0]); $mes++) {

				if ($tot_tik_mes[0][$mes] > 0) { //Promedio de Ticket por dia al Mes
					$tot_tik_dia_avg[0][$mes] = $tot_tik_mes[0][$mes] / $tot_tik_dia_avg[0][$mes];
				}else{
					$tot_tik_dia_avg[0][$mes] = 0;
				};

				if ($tot_ven_mes[0][$mes] > 0) { //Promedio de Venta por Ticker al Mes
					$tot_ven_tik_avg[0][$mes] = $tot_ven_mes[0][$mes] / $tot_tik_mes[0][$mes];
				}else{
					$tot_ven_tik_avg[0][$mes] = 0;
				};
			}
		}

        // ****  ------- EJECICIO ACTUAL   --------  **** //
		$fecha_desde=$eje_actual."/01/01";
		$fecha_hasta=$eje_actual."/12/31";

		$sql = "SELECT ejefac, fecfac, codsuc, count(*) AS tot_tikets, sum(totfac) AS tot_ventas, avg(totfac) AS tot_promeven FROM FACCAB WHERE codsuc IN $sucursal and fecfac BETWEEN '$fecha_desde' and '$fecha_hasta' GROUP BY fecfac ASC";
		$datos = $conexion->consulta($sql);
		

		$nume_regis = count($datos);
		if (count($datos) > 0){
        	//------ Calculo de Totales Generales ----
			for ($i=0; $i<count($datos); $i++) {
        		$mes = substr($datos[$i]['fecfac'],5,2) - 1; //Obtener el Mes de la Fecha

        		$tot_total_dias[1][$mes] = $tot_total_dias[1][$mes] + 1; //Total Dias Año Actual

        		$tot_tik_mes[1][$mes] = $tot_tik_mes[1][$mes] + $datos[$i]['tot_tikets'];
        		$tot_tik_dia_avg[1][$mes] = $tot_tik_dia_avg[1][$mes] + 1; // Primero el total de dias del Mes
        		$tot_ven_mes[1][$mes] = $tot_ven_mes[1][$mes] + $datos[$i]['tot_ventas'];
        		$tot_ven_tik_avg[1][$mes] = 0; // Primero el total de dias del Mes

        	}
        	
        	for ($mes=0; $mes<count($tot_tik_mes[1]); $mes++) {

        		//------Calculo de los promedios---------
				if ($tot_tik_mes[1][$mes] > 0) { //Promedio de Ticket por dia al Mes
					$tot_tik_dia_avg[1][$mes] = $tot_tik_mes[1][$mes] / $tot_tik_dia_avg[1][$mes];
				}else{
					$tot_tik_dia_avg[1][$mes] = 0;
				};

				if ($tot_ven_mes[1][$mes] > 0) { //Promedio de Venta por Ticker al Mes
					$tot_ven_tik_avg[1][$mes] = $tot_ven_mes[1][$mes] / $tot_tik_mes[1][$mes];
				}else{
					$tot_ven_tik_avg[1][$mes] = 0;
				};

				//------ Calculo de Porcentajes Mensuales ---------
				if (($tot_tik_mes[0][$mes] > 0) and ($tot_tik_mes[1][$mes] > 0)) { //Var %
					$tot_tik_mes[2][$mes] = (($tot_tik_mes[1][$mes]*100)/$tot_tik_mes[0][$mes])-100;
				}else{
					$tot_tik_mes[2][$mes] = 0;
				};

				if (($tot_tik_dia_avg[0][$mes] > 0) and ($tot_tik_dia_avg[1][$mes] > 0)) { //Var %
					$tot_tik_dia_avg[2][$mes] = (($tot_tik_dia_avg[1][$mes]*100)/$tot_tik_dia_avg[0][$mes])-100;
				}else{
					$tot_tik_dia_avg[2][$mes] = 0;
				};

				if (($tot_ven_mes[0][$mes] > 0) and ($tot_ven_mes[1][$mes] > 0)) { //Var %
					$tot_ven_mes[2][$mes] = (($tot_ven_mes[1][$mes]*100)/$tot_ven_mes[0][$mes])-100;
				}else{
					$tot_ven_mes[2][$mes] = 0;
				};

				if (($tot_ven_tik_avg[0][$mes] > 0) and ($tot_ven_tik_avg[1][$mes] > 0)) { //Var %
					$tot_ven_tik_avg[2][$mes] = (($tot_ven_tik_avg[1][$mes]*100)/$tot_ven_tik_avg[0][$mes])-100;
				}else{
					$tot_ven_tik_avg[2][$mes] = 0;
				};
				//------ Dar Formato de Numeros ---------
		        // $tot_tik_mes[1][$mes] 		= number_format($tot_tik_mes[1][$mes],2, '.', ',');
			}

			for ($mes=0; $mes<count($tot_tik_mes[1]); $mes++) {
	        	//------ Valores Trimestrales ---------
				if ($tot_tik_mes[1][$mes] > 0) {
					if ($mes == 0) {
						$tot_tik_mes[3][$mes] = ($tot_tik_mes[0][9] + $tot_tik_mes[0][10]) / 2;
						$tot_tik_dia_avg[3][$mes] = ($tot_tik_dia_avg[0][9] + $tot_tik_dia_avg[0][10]) / 2;
						$tot_ven_mes[3][$mes] = ($tot_ven_mes[0][9] + $tot_ven_mes[0][10]) / 2;
						$tot_ven_tik_avg[3][$mes] = ($tot_ven_tik_avg[0][9] + $tot_ven_tik_avg[0][10]) / 2;

					}elseif ($mes == 1) {
						$tot_tik_mes[3][$mes] = ($tot_tik_mes[0][9] + $tot_tik_mes[0][10] + $tot_tik_mes[1][0]) / 3;
						$tot_tik_dia_avg[3][$mes] = ($tot_tik_dia_avg[0][9] + $tot_tik_dia_avg[0][10] + $tot_tik_dia_avg[1][0]) / 3;
						$tot_ven_mes[3][$mes] = ($tot_ven_mes[0][9] + $tot_ven_mes[0][10] + $tot_ven_mes[1][0]) / 3;
						$tot_ven_tik_avg[3][$mes] = ($tot_ven_tik_avg[0][9] + $tot_ven_tik_avg[0][10] + $tot_ven_tik_avg[1][0]) / 3;
					}elseif ($mes == 2) {
						$tot_tik_mes[3][$mes] = ($tot_tik_mes[0][10] + $tot_tik_mes[1][0] + $tot_tik_mes[1][1]) / 3;
						$tot_tik_dia_avg[3][$mes] = ($tot_tik_dia_avg[0][10] + $tot_tik_dia_avg[1][0] + $tot_tik_dia_avg[1][1]) / 3;
						$tot_ven_mes[3][$mes] = ($tot_ven_mes[0][10] + $tot_ven_mes[1][0] + $tot_ven_mes[1][1]) / 3;
						$tot_ven_tik_avg[3][$mes] = ($tot_ven_tik_avg[0][10] + $tot_ven_tik_avg[1][0] + $tot_ven_tik_avg[1][1]) / 3;
					}else{
						$mes_1 = $mes-1;
						$mes_2 = $mes-2;
						$mes_3 = $mes-3;
						$tot_tik_mes[3][$mes] = ($tot_tik_mes[1][$mes_3] + $tot_tik_mes[1][$mes_2] + $tot_tik_mes[1][$mes_1]) / 3;
						$tot_tik_dia_avg[3][$mes] = ($tot_tik_dia_avg[1][$mes_3] + $tot_tik_dia_avg[1][$mes_2] + $tot_tik_dia_avg[1][$mes_1]) / 3;
						$tot_ven_mes[3][$mes] = ($tot_ven_mes[1][$mes_3] + $tot_ven_mes[1][$mes_2] + $tot_ven_mes[1][$mes_1]) / 3;
						$tot_ven_tik_avg[3][$mes] = ($tot_ven_tik_avg[1][$mes_3] + $tot_ven_tik_avg[1][$mes_2] + $tot_ven_tik_avg[1][$mes_1]) / 3;
					}

				}else{
					$tot_tik_mes[3][$mes] = 0;
					$tot_tik_dia_avg[3][$mes] = 0;
					$tot_ven_mes[3][$mes] = 0;
					$tot_ven_tik_avg[3][$mes] = 0;
				}
				//% Trimestrales
				//------ Calculo de Porcentajes Mensuales ---------
				if (($tot_tik_mes[3][$mes] > 0) and ($tot_tik_mes[1][$mes] > 0)) { //Var %
					$tot_tik_mes[4][$mes] = (($tot_tik_mes[1][$mes]*100)/$tot_tik_mes[3][$mes])-100;
				}else{
					$tot_tik_mes[4][$mes] = 0;
				};

				if (($tot_tik_dia_avg[3][$mes] > 0) and ($tot_tik_dia_avg[1][$mes] > 0)) { //Var %
					$tot_tik_dia_avg[4][$mes] = (($tot_tik_dia_avg[1][$mes]*100)/$tot_tik_dia_avg[3][$mes])-100;
				}else{
					$tot_tik_dia_avg[4][$mes] = 0;
				};

				if (($tot_ven_mes[3][$mes] > 0) and ($tot_ven_mes[1][$mes] > 0)) { //Var %
					$tot_ven_mes[4][$mes] = (($tot_ven_mes[1][$mes]*100)/$tot_ven_mes[3][$mes])-100;
				}else{
					$tot_ven_mes[4][$mes] = 0;
				};

				if (($tot_ven_tik_avg[3][$mes] > 0) and ($tot_ven_tik_avg[1][$mes] > 0)) { //Var %
					$tot_ven_tik_avg[4][$mes] = (($tot_ven_tik_avg[1][$mes]*100)/$tot_ven_tik_avg[3][$mes])-100;
				}else{
					$tot_ven_tik_avg[4][$mes] = 0;
				};
			}
			//Se declara que esta es una aplicacion que genera un JSON
			echo json_encode(array( 'exito' => true, 'tot_tik_mes' => $tot_tik_mes, 'tot_tik_dia_avg' => $tot_tik_dia_avg, 'tot_ven_mes' => $tot_ven_mes, 'tot_ven_tik_avg' => $tot_ven_tik_avg,
				'tot_total_dias' => $tot_total_dias));		
		}else{
			$respuesta = array('exito' => false, 'sucursal' => $sucursal);
		}
	}
}
catch (Exception $e) {//Controlar siempre el error.
	$data = $e->POSTMessage();
	echo json_encode($data, true);
}
?>