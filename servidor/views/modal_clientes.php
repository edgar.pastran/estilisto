<?php
header('Content-type: text/html');
header("Access-Control-Allow-Origin: *");

if (isset($_POST['codsuc'])) {
    $codigo_sucursal = $_POST['codsuc'];
    require_once("../php/config/Config.php");
    $conexion = new Conexion();

    $sql =
        "SELECT codcli, nomcli, ape1cli, dnicli, forpagcli, perdeuda ".
        "FROM CLIENTES ".
        "WHERE (obsoleto = 0 OR obsoleto IS NULL) ".
        "AND codsuc IN (".
            "SELECT codsuc ".
            "FROM EMPRESA ".
            "WHERE codemp IN (".
                "SELECT codemp ".
                "FROM EMPRESA ".
                "WHERE codsuc = '".$codigo_sucursal."'".
            ")".
        ")";
    $clientes = $conexion->consulta($sql);
    ?>
    <div class="table-container">
        <table class="table table-striped table-bordered table-hover table-checkable" id="tabla_clientes">
            <thead>
            <tr role="row" class="heading">
                <th>#</th>
                <th>DNI</th>
                <th>Nombres</th>
                <th>Apellidos</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i=1;
            foreach ($clientes as $cliente) {
                $cliente['forpagcli'] = isset($cliente['forpagcli'])?$cliente['forpagcli']:"";
                $cliente['perdeduda'] = (isset($cliente['perdeuda']) && $cliente['perdeuda']==1)?$cliente['perdeuda']:0;
                ?>
                <tr class="fila-cliente cursor-puntero" id="<?php echo $cliente['codcli'];?>">
                    <td>
                        <?php echo $i++;?>
                        <input type="hidden" class="forma_pago" value="<?php echo $cliente['forpagcli'];?>">
                        <input type="hidden" class="permite_deuda" value="<?php echo $cliente['perdeduda'];?>">
                    </td>
                    <td class="dni"><?php echo $cliente['dnicli'];?></td>
                    <td class="nombre"><?php echo $cliente['nomcli'];?></td>
                    <td class="apellido"><?php echo $cliente['ape1cli'];?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>

    <script type="text/javascript" src="<?php echo $URL_BASE_PATH.'servidor/js/modal_clientes.js'?>"></script>
    <?php
}
?>