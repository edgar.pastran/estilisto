<?php
header('Content-type: text/html');
header("Access-Control-Allow-Origin: *");

if (isset($_POST['codsuc'])) {
    $codigo_sucursal = $_POST['codsuc'];
    require_once("../php/config/Config.php");
    $conexion = new Conexion();

    $sql =
        "SELECT FAM.*, EMP.codemp ".
        "FROM FAMILIA1 FAM, EMPRESA EMP ".
        "WHERE FAM.codsuc = EMP.codsuc ".
        "AND FAM.vender = 1 ".
        "AND (FAM.obsoleto = 0 OR FAM.obsoleto IS NULL) ".
        "AND FAM.codsuc = '".$codigo_sucursal."' ".
        "ORDER BY FAM.orden ASC";
    $familias = $conexion->consulta($sql);
    $subdirectorio_imagenes = 'familia';
    for ($i=0; $i<count($familias); $i++) {
        $codigo_grupo_empresas = $familias[$i]['codemp'];
        $codigo_sucursal = $familias[$i]['codsuc'];
        $foto = $URL_BASE_PATH.'servidor/images/nothing.png';
        if (isset($familias[$i]['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$familias[$i]['foto'])) {
            $foto = $URL_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$familias[$i]['foto'];
        }
        $familias[$i]['foto'] = $foto;
    }

    $impuestos = $conexion->consulta("SELECT impuesto1, impuesto2, impincventas FROM EMPRESA WHERE codsuc='".$codigo_sucursal."'");
    $sql =
        "SELECT ART.*, EMP.codemp ".
        "FROM ARTICULOS ART, EMPRESA EMP ".
        "WHERE ART.codsuc = EMP.codsuc ".
        "AND ART.vender=1 ".
        "AND (ART.obsoleto = 0 OR ART.obsoleto IS NULL) ".
        "AND ART.codsuc='".$codigo_sucursal."' ".
        "ORDER BY ART.orden ASC, ART.codart ASC";
    $articulos = $conexion->consulta($sql);
    $subdirectorio_imagenes = 'articulo';
    for ($i=0; $i<count($articulos); $i++) {
        // Calculo de la URL de la foto
        $codigo_grupo_empresas = $articulos[$i]['codemp'];
        $codigo_sucursal = $articulos[$i]['codsuc'];
        $foto = $URL_BASE_PATH.'servidor/images/nothing.png';
        if (isset($articulos[$i]['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$articulos[$i]['foto'])) {
            $foto = $URL_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'.$codigo_sucursal.'/'.$articulos[$i]['foto'];
        }
        $articulos[$i]['foto'] = $foto;
        // Calculo del porcentaje de impuesto aplicado al articulo
        $impuesto_porcentaje = 0;
        if (isset($articulos[$i]['ivaart'])) {
            if ($articulos[$i]['ivaart']=='1') {
                $impuesto_porcentaje = isset($impuestos[0]['impuesto1'])?$impuestos[0]['impuesto1']:0;
            }
            elseif ($articulos[$i]['ivaart']=='2') {
                $impuesto_porcentaje = isset($impuestos[0]['impuesto2'])?$impuestos[0]['impuesto2']:0;
            }
        }
        $articulos[$i]['impuesto_porcentaje'] = $impuesto_porcentaje;
        // Calculo de la base imponible y monto del impuesto del articulo
        $precio = $articulos[$i]['pvpa'];
        $base_imponible = $precio;
        $impuesto_monto = $precio * ($impuesto_porcentaje / 100);
        if (isset($impuestos[0]['impincventas']) && $impuestos[0]['impincventas']=='X') {
            $base_imponible = $precio / (1+($impuesto_porcentaje/100));
            $impuesto_monto = $precio - $base_imponible;
        }
        $articulos[$i]['base_imponible'] = $base_imponible;
        $articulos[$i]['impuesto_monto'] = $impuesto_monto;
    }

    $filters_desktop =
    '<div class="grid button-group filters filters-desktop"><div class="row">';
    $filters_mobile =
    '<div class="grid button-group filters filters-mobile"><div class="row">';
    for ($i=0; $i<count($familias); $i++) {
        $familia = $familias[$i];
        $filters_desktop .=
        '<div class="col-md-2 col-lg-2" title="'.$familia['desfam1'].'">
            <div class="custom-button" data-filter=".' .$familia['codfam1'].'">
                <img src="'.$familia['foto'].'">
                <p>'.$familia['desfam1'].'</p>
            </div>
        </div>';
        $filters_mobile .=
        '<div class="col-xs-4" title="'.$familia['desfam1'].'" style="width: 33%;">
            <div class="custom-button" data-filter=".' .$familia['codfam1'].'">
                <img src="'.$familia['foto'].'">
                <p>'.$familia['desfam1'].'</p>
            </div>
        </div>';
    }
    $filters_desktop .= '</div></div>';
    $filters_mobile .= '</div></div>';
    echo $filters_desktop;
    echo $filters_mobile;
?>
<hr>
<?php
    $elements_desktop =
    '<div class="grid elements elements-desktop">';
    $elements_mobile =
    '<div class="grid elements elements-mobile">';
    for ($i=0; $i<count($articulos); $i++) {
        $articulo = $articulos[$i];
        $elements_desktop .=
        '<div class="element-item initially-hidden col-md-2 col-lg-2 '.$articulo['familia1'].'" data-category="'.$articulo['familia1']. '" title="'.$articulo['desart'].'">
            <div class="custom-button">
                <img src="'.$articulo['foto'].'">
                <p>' .$articulo['desart'].'</p>
                <input type="hidden" class="codart" value="'.$articulo['codart'].'">
                <input type="hidden" class="desart" value="'.$articulo['desart'].'">
                <input type="hidden" class="preart" value="'.$articulo['pvpa'].'">
                <input type="hidden" class="preart_format" value="'.number_format($articulo['pvpa'], 2, ',', '.').'">
                <input type="hidden" class="porimpart" value="'.$articulo['impuesto_porcentaje'].'">
                <input type="hidden" class="porimpart_format" value="'.number_format($articulo['impuesto_porcentaje'], 2, ',', '.').'">
                <input type="hidden" class="monimpart" value="'.$articulo['impuesto_monto'].'">
                <input type="hidden" class="basimpart" value="'.$articulo['base_imponible'].'">
            </div>
        </div>';
        $elements_mobile .=
        '<div class="element-item initially-hidden col-xs-4 '.$articulo['familia1'].'" data-category="'.$articulo['familia1']. '" title="'.$articulo['desart'].'" style="width: 33%;">
            <div class="custom-button">
                <img src="'.$articulo['foto'].'">
                <p>' .$articulo['desart'].'</p>
                <input type="hidden" class="codart" value="'.$articulo['codart'].'">
                <input type="hidden" class="desart" value="'.$articulo['desart'].'">
                <input type="hidden" class="preart" value="'.$articulo['pvpa'].'">
                <input type="hidden" class="preart_format" value="'.number_format($articulo['pvpa'], 2, ',', '.').'">
                <input type="hidden" class="porimpart" value="'.$articulo['impuesto_porcentaje'].'">
                <input type="hidden" class="porimpart_format" value="'.number_format($articulo['impuesto_porcentaje'], 2, ',', '.').'">
                <input type="hidden" class="monimpart" value="'.$articulo['impuesto_monto'].'">
                <input type="hidden" class="basimpart" value="'.$articulo['base_imponible'].'">                    
            </div>
        </div>';
    }
    $elements_desktop .= '</div>';
    $elements_mobile .= '</div>';
    echo $elements_desktop;
    echo $elements_mobile;
?>
<script type="text/javascript" src="<?php echo $URL_BASE_PATH.'servidor/js/modal_servicios.js'?>"></script>
<?php
}
?>