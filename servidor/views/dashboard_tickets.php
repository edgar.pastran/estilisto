<?php
header('Content-type: text/html');
header("Access-Control-Allow-Origin: *");

if (isset($_POST['codsuc']) && isset($_POST['fecha'])) {
    $codigo_sucursal = $_POST['codsuc'];
    $fecha = $_POST['fecha'];
    $fecha = DateTime::createFromFormat('d/m/Y', $fecha)->format('Y-m-d');

    require_once("../php/config/Config.php");
    $conexion = new Conexion();

    // Calculo de totales de sucursal en el dia
    $total_ventas = 0;
    $total_tickets = 0;
    $sql =
        "SELECT numfac, totfac ".
        "FROM FACCAB ".
        "WHERE codsuc = '".$codigo_sucursal."'".
        "AND fecfac = '".$fecha."'";
    $data = $conexion->consulta($sql);
    $total_tickets = count($data);
    for ($i=0; $i<count($data); $i++) {
        $total_ventas += $data[$i]['totfac'];
    }

    // Calculo de totales por empleado en el dia
    $sql =
        "SELECT EMPR.moneda, EMPR.impincventas, EMPL.codemp, EMPL.nomemp, EMPL.ape1emp, EMPL.foto ".
        "FROM EMPLEADOS EMPL, EMPRESA EMPR ".
        "WHERE EMPL.codsuc = EMPR.codsuc ".
        "AND (EMPL.obsoleto = 0 OR EMPL.obsoleto IS NULL) ".
        "AND EMPL.vender = 1 ".
        "AND EMPL.codsuc='".$codigo_sucursal."'";
    $empleados = $conexion->consulta($sql);
    $moneda = "";
    for ($i=0; $i<count($empleados); $i++) {
        $empleados[$i]['fullname'] = $empleados[$i]['nomemp'].(isset($empleados[$i]['ape1emp'])?" ".$empleados[$i]['ape1emp']:"");
        if (isset($empleados[$i]['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/empleado/'.strtolower($empleados[$i]['foto']))) {
            $empleados[$i]['foto'] = $URL_BASE_PATH.'servidor/images/empleado/'.strtolower($empleados[$i]['foto']);
        }
        else {
            $empleados[$i]['foto'] = $URL_BASE_PATH.'servidor/images/nothing.png';
        }
        // Calculo de Tickets y Ventas del dia
        // Si la empresa incluye el impuesto en sus montos
        // se usa la columna subtot sino se usa la columna totlin
        $columna_en_linea = "subtot";
        if (!isset($empleados[$i]['impincventas']) || strtoupper($empleados[$i]['impincventas']) != "X") {
            $columna_en_linea = "totlin";
        }
        $empleados[$i]['tickets'] = 0;
        $empleados[$i]['ventas'] = 0;
        $sql =
            "SELECT DISTINCT(FC.numfac) as tickets, SUM(FL.".$columna_en_linea.") as ventas ".
            "FROM FACLIN FL, FACCAB FC ".
            "WHERE FL.codsuc = FC.codsuc ".
            "AND FL.serfac = FC.serfac ".
            "AND FL.ejefac = FC.ejefac ".
            "AND FL.numfac = FC.numfac ".
            "AND FC.codsuc = '".$codigo_sucursal."'".
            "AND FC.fecfac = '".$fecha."' ".
            "AND FL.codemp = ".$empleados[$i]['codemp']." ".
            "GROUP BY FC.numfac";
        $data = $conexion->consulta($sql);
        $tickets = count($data);
        $empleados[$i]['tickets'] = number_format($tickets, 0, ',', '.');
        $ventas = 0;
        for ($j=0; $j<count($data); $j++) {
            $ventas += $data[$j]['ventas'];
        }
        $empleados[$i]['ventas'] = number_format($ventas, 2, ',', '.');
        $moneda = $empleados[$i]['moneda'];
    }

    $dashboard =
        '<div class="row dashbord-resumen">
            <div class="col-lg-4">
                <div class="cardbox text-white bg-gradient-secondary b0">
                  <div class="cardbox-body p-0">
                  <div class="row">
                    <div class="col-1 ml-0 dashbord-icono">
                        <i class="ion-calendar"></i>
                    </div>
                    <div class="col-10 text-right">
                        <div id="'.$codigo_sucursal.'-dashboard-fecha" class="text-bold dashbord-data"></div>
                        <p class="text-bold">Fecha</p>
                    </div>
                  </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="cardbox text-white bg-gradient-success b0">
                  <div id="'.$codigo_sucursal.'-dashboard-tarjeta-ventas" class="cardbox-body p-0">
                  <div class="row">
                    <div class="col-1 ml-0 dashbord-icono">
                        <i class="ion-cash"></i>
                    </div>
                    <div class="col-10 text-right">                    
                        <div id="'.$codigo_sucursal.'-dashboard-ventas" class="text-bold dashbord-data">'.($moneda!=""?$moneda." ":"").number_format($total_ventas, 2, ',', '.').'</div>
                        <p class="text-bold">Ventas</p>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="cardbox text-white bg-gradient-info b0">
                  <div id="'.$codigo_sucursal.'-dashboard-tarjeta-tickets"class="cardbox-body p-0">
                  <div class="row">
                    <div class="col-1 ml-0 dashbord-icono">
                        <i class="ion-pricetags"></i>
                    </div>
                    <div class="col-10 text-right">
                        <div id="'.$codigo_sucursal.'-dashboard-tickets" class="text-bold dashbord-data">'.number_format($total_tickets, 0, ',', '.').'</div>
                        <p class="text-bold">Tickets</p>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
        </div>
         
        <div class="cardbox">
            <div class="cardbox-body">
                <div style="min-height:50px;">
                    <div class="row">';
    for ($i=0; $i<count($empleados); $i++) {
        $empleado = $empleados[$i];
        $dashboard .=
            '<div class="col-lg-4" id="'.$codigo_sucursal.'-'.$empleado['codemp'].'-data"'.((intval($empleado['tickets'])<=0)?' style="display:none"':'').'>
                <div class="cardbox">
                    <div class="pb-1 bg-gradient-info top-line"></div>
                    <div class="cardbox-body">
                        <div class="d-flex flex-wrap">
                            <div class="col-2" style="padding-left:  0px;">
                                <img class="shadow-z5 thumb48 rounded" src="'.$empleado['foto'].'" alt="'.$empleado['fullname'].'">
                            </div>
                            <div class="col-4 offset-1">
                                <p class="my-1">
                                    <strong>'.$empleado['fullname'].'</strong>
                                </p>                              
                            </div>                            
                            <div class="col-5 text-right">
                                <div class="">
                                    <span id="'.$codigo_sucursal.'-'.$empleado['codemp'].'-ventas" class="btn btn-outline-success"  title="'.$empleado['moneda']." ".$empleado['ventas'].'">'.$empleado['ventas'].'</span>
                                </div>
                                <div class="">
                                    <span id="'.$codigo_sucursal.'-'.$empleado['codemp'].'-tickets" class="btn btn-info" title="'.$empleado['tickets'].' Tickets">'.$empleado['tickets'].'</span>
                                </div>
                            </div>                               
                        </div>
                    </div>
                </div>
            </div>';
    }
    $dashboard .=
        '</div>
        </div>
        </div>
    </div>';
    echo $dashboard;
}
?>