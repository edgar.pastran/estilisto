<?php
header('Content-type: text/html');
header("Access-Control-Allow-Origin: *");

if (isset($_POST['codsuc'])) {
    $subdirectorio_imagenes = 'empleado';
    $codigo_sucursal = $_POST['codsuc'];

    require_once("../php/config/Config.php");
    $conexion = new Conexion();

    $sql =
        "SELECT EMPL.codsuc, EMPL.codemp, EMPL.nomemp, EMPL.ape1emp, EMPL.foto, EMP.codemp AS codempresa  ".
        "FROM EMPLEADOS EMPL, EMPRESA EMP ".
        "WHERE EMPL.codsuc = EMP.codsuc ".
        "AND (EMPL.obsoleto = 0 OR EMPL.obsoleto IS NULL) ".
        "AND EMPL.vender = 1 ".
        "AND EMPL.codsuc='".$codigo_sucursal."' ".
        "ORDER BY EMPL.codemp";
    $empleados = $conexion->consulta($sql);
    for ($i=0; $i<count($empleados); $i++) {
        $codigo_grupo_empresas = $empleados[$i]['codempresa'];
        $codigo_sucursal = $empleados[$i]['codsuc'];
        $empleados[$i]['fullname'] = $empleados[$i]['nomemp'].(isset($empleados[$i]['ape1emp'])?" ".$empleados[$i]['ape1emp']:"");
        $foto = $URL_BASE_PATH.'servidor/images/nothing.png';
        if (isset($empleados[$i]['foto']) && file_exists($DIR_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'./*$codigo_sucursal.'/'.*/$empleados[$i]['foto'])) {
            $foto = $URL_BASE_PATH.'servidor/images/'.$codigo_grupo_empresas.'/'.$subdirectorio_imagenes.'/'./*$codigo_sucursal.'/'.*/$empleados[$i]['foto'];
        }
        $empleados[$i]['foto'] = $foto;
    }

    $elements_desktop =
    '<div id="grid_empleados" class="grid elements-desktop"><div class="row">';
    $elements_mobile =
    '<div id="grid_empleados" class="grid elements-mobile"><div class="row">';
    for ($i=0; $i<count($empleados); $i++) {
        $empleado = $empleados[$i];
        $elements_desktop .=
        '<div class="col-md-2 col-lg-2" title="'.$empleado['fullname'].'">
            <div class="custom-button">
                <img src="'.$empleado['foto'].'">
                <p>'.$empleado['fullname'].'</p>
                <input type="hidden" class="codemp" value="'.$empleado['codemp'].'">
                <input type="hidden" class="fullname" value="'.$empleado['fullname'].'">
            </div>
        </div>';
        $elements_mobile .=
        '<div class="col-xs-4" title="'.$empleado['fullname'].'" style="width: 33%;">
            <div class="custom-button">
                <img src="'.$empleado['foto'].'">
                <p>'.$empleado['fullname'].'</p>
                <input type="hidden" class="codemp" value="'.$empleado['codemp'].'">
                <input type="hidden" class="fullname" value="'.$empleado['fullname'].'">
            </div>
        </div>';
    }
    $elements_desktop .= '</div></div>';
    $elements_mobile  .= '</div></div>';
    echo $elements_desktop;
    echo $elements_mobile;
?>

<script type="text/javascript" src="<?php echo $URL_BASE_PATH.'servidor/js/modal_empleados.js'?>"></script>
<?php
}
?>